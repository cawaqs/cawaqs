/*-------------------------------------------------------------------------------
* 
* SOFTWARE NAME: cawaqs
* FILE NAME: input.y
* 
* CONTRIBUTORS: Nicolas GALLOIS, Baptiste LABARTHE, Shuaitao WANG,
*               Agnès RIVIERE, Deniz KILIC, Mathias MAILLOT,
*               Fulvia BARATELLI, Pierre GUILLOU, Emmanuel LEDOUX, Nicolas FLIPO
* 
* SOFTWARE BRIEF DESCRIPTION: Physically based coupled surface–subsurface 
* model for joint water, matter and energy flux balances and flow dynamics 
* simulation within all compartments of a hydrosystem (sub-surface, hydraulic 
* network, vadose zone, aquifer system and stream-aquifer exchanges).
*
* Software developed at the Geosciences Center, joint research center 
* of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
*
* COPYRIGHT: (c) 2022 Contributors to the cawaqs Software.
* CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
*          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
*
* All rights reserved. This Software and the accompanying materials
* are made available under the terms of the Eclipse Public License v2.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v20.html
* 
*------------------------------------------------------------------------------*/


%{
#include <stdio.h>
#include <strings.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include <stdlib.h>
#include <signal.h>
#include <time.h>
#include <sys/time.h>
#include <libprint.h>
#include <time_series.h>
#include "libpc.h"
#include <CHR.h>
#include <GC.h>
#include <IO.h>
#include <reservoir.h>
#include <spa.h>
#include <FP.h>
#include <NSAT.h>
#include <HYD.h>
#include "MSH.h"
#include "AQ.h"
#include "WET.h"
#include "TTC_coupled.h"
#include "SEB.h"
#include "CAW.h"
//#include "global_CAWAQS.h"
#include "ext_CAWAQS.h"

  /* Previous time read in a time series */
  double told = 0.;
  /* Time */
  double tnew = 0.;
  int ttstep = 0.;
  /* Unit of the time or of ft->t in a s_ft structure */
  double unit_t = 1.;
  /* Unit of ft->f in a s_ft structure */
  double unit_f = 1.;
  double unit_x,unit_y,unit_z,unit_l,unit_kappa;
  /* Pointer towards the currently read reach and the total chain of reaches */
  s_reach_hyd *preach, *preachtot;
  /* Reach in which the bathymetry is currently being defined */
  s_reach_hyd *preach_bat;
  /* Reach in which the inflow arrives */
  s_reach_hyd *preach_infl = NULL;
  /* Pointer towards a transversal face */
  s_face_hyd *pface;
  /* Pointer towards a calculation element */
  s_element_hyd *pele_hyd;
  /* Pointer towards an inflow */
  s_inflow_hyd *pinflow = NULL;
  /* Pointer towards the currently read singularity and the total chain of singularities */
  s_singularity_hyd *psing, *psingtot;
  /* Current BC characteristics */
  s_BC_char_hyd *pwork, *pworktot;
  /* Nb of hydraulic works constituting the BC */
  int nworks;
  /* Type of hydraulic work*/
  int worktype;
  /* Geometry of the face described in Lambert coordinates */
  s_pointAbscZ_hyd *pptAbscZ;
  /* Geometry of the face described in XYZ coordinates */
  s_pointXYZ_hyd *pptXYZ;
  /* Number of steps in a time series s_ft */
  int nbsteps;
  /* Loop indexes */
  int s,r,i,p,f,e;
  /* Outputs */
  s_output_hyd *pout_hyd;
  s_output_hyd *pts=NULL,*plp=NULL,*pmb=NULL;

  /* Number of catchments */
  int catchment_counter = 0;
  /* Number of chasms */
  int nb_chasm_links = 0;
  s_catchment_fp* pcatch = NULL;
  /* Number of cprod cells for the hyperdermic module */
  int nb_cprod_hderm = 1;
  /* Number of hyperdermic catchments */
  int hderm_catchment_counter = 0;
  /* Counter of hyperdermic catchments with concentration time automatically calibrated */
  int hderm_catchment_tc_auto = 0;
  /* Global counter of species in case of transport calculations */
  int nb_species=0;
  /* Counter of species unit cells declared for one given specie */
  int nb_specie_unit = 1;
  /* Multiplication factor for forcing time series */
  double multip_fact_ts = 1.;
  /* Multiplication factor for input AQ parameters */
  double mult_AQparam = 1.;
  /* Number of AQ-bypasses destinations cells for one AQ Cauchy cell */
  int nbypass = 0;
  /* Number of reaches in reach-safran corresonding file for libseb */
  int nreach_count_file = 0;
  int check = 0;

  /* GC application number for transport. Starts at TTC_GC and increases for each transport specie*/   // Warning ! Possible values are 3, 5, 6 up to IAPPLI (= 12 currently).
  int iappli_gc_ttc = TTC_GC;

  /***** NG : 20/02/2020 : "Simul->pchyd" and "Simul->plec" have been replaced by two "generic" pointers
  so all functions used in input.y dealing with the hydraulic network can be used both for the surface network
  AND the hyperdermic network without changing anyting else (useless copy/paste operations...).

  In case of simulation with hyperdermic module declared, two "riv" lex blocks are read. When the second is,
  "pgen_chyd" and "pgen_lec" pointers both switch from "Simul->pchyd" and "Simul->plec" to characteristics pointers
  for the hypderdermic module : "Simul->pchyd_hderm" and "Simul->plec_hderm"
  (see. "intro_riv" sub-block declaration of the "riv" lex block.) ****/

  /* Generic pchyd type pointer */
  s_chyd* pgen_chyd = NULL;
  /* Generic plec type pointer */
  s_lec_tmp_hyd** pgen_plec = NULL;

  /* Inflow pointer chain of all inflows (all variables combined) for the hydraulic HYD network */
  s_inflow_hyd* pchain_inflow_hyd = NULL;

  /* Inflow pointer chain of all inflows (all variables combined) for the hyperdermic HDERM network */
  s_inflow_hyd* pchain_inflow_hderm = NULL;

  s_ts_pk_hyd *pts_pk,*pts_pktot;
  s_lp_pk_hyd *plp_pk,*plp_pktot,*pmb_pk,*pmb_pktot;
  int output_type;
  char *rivername;
  char**FnodeTnode;
  int nb_outlet;
  int nb_out=0;
  int nb_reach=0;
  int dim_tmp_lec = NB_LEC_MAX;
  int nb_realloc = 1;
  int nb_mto = 0;
  int nb_param=0;
  int nb_param_nsat=0;
  int nb_bu=0;
  int nb_cprod = 1;
  int nb_zns=0;
  s_mto_fp *pmto;
  s_param_fp *param;
  s_bal_unit_fp *pbu;
  // s_cprod_fp *pcprod;

  s_nash *nash;
  s_zns *pzns;
  int *nb_lec;
  int pk_type;
  int nelebound = 0;
  int general_out = CODE;
    s_layer_msh *player;
  s_ele_msh *pele_msh;
  s_bound_aq *pbound;
  s_ft *pft;
  //s_id_io *id_list;
  int ncoord=0;
  int coordo=0;
  int nele=0;
  int nsub=0;
  int verif_layer=0;
  int verif_specie=0;
  int naquitard = 0; // Number of aquitard cells allocated in the libttc module DK AR 15 12 2021
  int nriverbed = 0; // Number of riverbed cells allocated in the libttc module DK AR 15 12 2021
  int type_input = 0; // Condition to enable riverbed or aquitard transport initialization
  int verif_ele=0;
  int nlayer=0;
  int layertot=0;
  int i=0;
  int id_new=0;
  int nm=0;
  int nb_source_cells=0;
  int intern_id = 0;
  int nele_tot=1;
  int  kindof_attribut,kindof;
  int idir=1;
  int ikdofb_t=1;
  int card_t=1;
  int kindof_transm,kindof_SOURCE;
  int kindof_forcing;
  int kindof_THRESH;
  int nwet; 
  int nit_wet;
  //int *t_min_wet, *t_max_wet, *v_wet, *rh_min_wet, *sh_max_wet, *rsw_wet, *rlw_wet, *patm_wet, *pluie_wet; //temperature vitesse vent  humidite et precipitation

  int y, nj = 0, data_nyear, data_init_year, data_fin_year;
  double theta_wet;
  double eps_wet;
  double alpha_z_wet;
  s_carac_wet *pcarac_wet;
  s_temp_wet *ptempwet, *ptempwet1, *ptempwet2;
  s_temp_wet **ptemp_wet;
  int *ncondu;//SW 17/05/2016 add pointer for conductance of lake
  double ltot;
  double stot;
  double surf_riv=CODE_TS;
  double ep_riv_bed = CODE_TS;
  double TZ_riv_bed =CODE_TS;
  int answ_out =0;
  double **val=NULL;
  int answ_dt[NSAT_IO];
  #ifdef OMP
  s_smp *psmp;
  int nthreads;
  #endif
  int answer=NO;
  int module=0;
  FILE *fr;

%}


%union {
  double real;
  int integer;
  char *string;
  s_ft *function;
  s_singularity_hyd *sing;
  s_pointXYZ_hyd *pointXYZ;
  s_pointAbscZ_hyd *pointAbscZ;
  s_face_hyd *face;
  s_inflow_hyd *inflow;
  s_BC_char_hyd *boundary;
  s_ts_pk_hyd *ts_pk;
  s_lp_pk_hyd *lp_pk;
  s_id_io *struct_id;
  s_mto_fp *struct_mto;
  s_param_fp *struct_param;
  s_bal_unit_fp *struct_bu;
  s_cprod_fp *struct_cprod;
  s_nash *struct_nash;
  s_zns *struct_zns;
  s_layer_msh *gw_layer;
  s_ele_msh *gw_ele;
  s_temp_wet *ptempwet;
  s_litho_aq *lithology;
  s_catchment_fp *struct_catch;
  s_specie_unit_fp *struct_specie_unit;

  s_rts *struct_rts;
}

%token <real> LEX_DOUBLE LEX_A_UNIT LEX_VAR_UNIT
%token <integer> LEX_INT
%token <integer> LEX_TRANSPORT
%token LEX_INIT LEX_INIT_Z LEX_INIT_Q LEX_HYDRO LEX_SPECIES LEX_SPECIES_NAME
%token LEX_OPENING_BRACE LEX_CLOSING_BRACE LEX_OPENING_BRACKET LEX_CLOSING_BRACKET LEX_REVERSE_ARROW
%token LEX_INV LEX_POW LEX_EQUAL LEX_ARROW LEX_COLON LEX_SEMI_COLON LEX_VIRGULE LEX_LOG_AQ_FLUXES LEX_UNIFORM
%token LEX_ALPHA_Z LEX_SY_FACTOR LEX_PRINT_LITHOS
%token <string> LEX_NAME
%token LEX_INPUT_FOLDER LEX_OUTPUT_FOLDER
%token LEX_SIMUL LEX_TIMESTEP LEX_DTOUT LEX_CHRONOS LEX_BEGIN LEX_SURF
%token <integer> LEX_MONTH LEX_YEAR
%token <integer> LEX_ANSWER LEX_TIME
%token <integer>  LEX_FORMAT LEX_PREF
%token LEX_SET LEX_CHECK_MESH LEX_PRINT_SURF LEX_OMP LEX_THREADS
%token LEX_DIM LEX_TYPE LEX_CALC_CURVATURE LEX_EPS_PIC LEX_NIT_STEADY_MAX LEX_NIT_PIC_MAX LEX_SIM_TYPE LEX_DEBUG
%token <integer> LEX_CALC_STATE LEX_GENERALP LEX_GENERALHYD LEX_TYPE_SIM
%token LEX_SIM_SET LEX_FORMAT_TYPE LEX_MTO LEX_MTO_PATH LEX_LAI_PATH LEX_SURFACE LEX_MTO_CELL LEX_WBU
%token <integer> LEX_LAI
%token LEX_CPROD_NOAQ LEX_RSV
%token LEX_NEWSAM  LEX_AQUIFER LEX_AVIEW LEX_LAYER LEX_SETUP_LAYER LEX_SET_UP LEX_MEAN
%token LEX_BOUND LEX_SOURCE LEX_TIME_S LEX_PARAM LEX_PARAM_RIV LEX_COND LEX_DERIVATION LEX_CASCADE LEX_BYPASS
%token <integer> LEX_MESH_ATT LEX_DIR LEX_TYPE_BC LEX_TYPE_MEAN LEX_TYPE_SOURCE LEX_SIM  LEX_TYPE_COND
%token LEX_BU LEX_CPROD LEX_FPAQ LEX_DT LEX_NONSAT LEX_NSAT LEX_NSPROD LEX_NSAQ
%token LEX_NETWORK LEX_TC  LEX_PK LEX_STRICKLER LEX_HYDWORKS LEX_POSITION
%token LEX_HCALCWAY
%token <integer> LEX_HCALCHYD
%token <integer> LEX_FION LEX_DIR_HYD
%token LEX_PROFILES LEX_INFLOWS LEX_SING LEX_REACH LEX_TRAJECTORY
%token <integer> LEX_INFLOW_TYPE LEX_INFL_VAR
%token LEX_ABSC LEX_X LEX_Y LEX_HYD_VAR LEX_SHAPE
%token <integer> LEX_BC_TYPE LEX_CROSS_SECTION_TYPE
%token <integer> LEX_WORK LEX_WORK_PARAM LEX_POINTS
%token LEX_FILE_NAME  LEX_EXTENT
%token LEX_OUTPUTS LEX_FINAL_STATE LEX_VAR LEX_TUNIT LEX_GRAPHICS  LEX_SEL LEX_KDEF LEX_INIT_FROM_FILE LEX_SPATIAL_SCALE
%token <integer> LEX_ONE_VAR LEX_OUTPUT_TYPE LEX_GRAPH_TYPE LEX_OUT_TYPE LEX_OUT_SET LEX_SCALE_TYPE
%token <integer> LEX_MUSK_PAR LEX_CALC_MUSK_PAR LEX_DEF_SCHEM_TYPE LEX_KDEF_OPT
%token LEX_AREA LEX_SLOPE LEX_SCHEM_TYPE LEX_NETWORK_MUSK LEX_REACH_MUSK LEX_ELE_MUSK LEX_REACH_CPROD
%token LEX_INTRO_CATCHMENTS LEX_INTRO_CATCHMENTS_HDERM LEX_LINK_HDERM_AQ
%token <integer> LEX_MESH_ATT_WET LEX_PARAM_ATT_WET LEX_AQ_TYPE
%token LEX_LAKE LEX_WET_COND LEX_MTO_WET LEX_DATA_INIT LEX_DATA_FIN LEX_DATA_MTO_WET
%token LEX_QLIM LEX_FILE
%token LEX_CHASM_SURF LEX_CHASM_NSAT
%token LEX_EVAP_READ
%token LEX_HYPERDERMIC LEX_LINK_NSAT_HDERM LEX_MEAN_SURF_CATCH
%token LEX_ALPHA_HDERM LEX_BETA_HDERM LEX_DYNNSAT LEX_UPDATE_DYNNSAT LEX_DYNNSAT_METHOD 
%token LEX_SURFACE_TRANSPORT LEX_NONSAT_TRANSPORT LEX_HYD_TRANSPORT LEX_HDERM_TRANSPORT LEX_AQ_TRANSPORT
%token LEX_TRANSPORT_BOUND LEX_CHEM_PROP
%token LEX_CORRESP LEX_FORCING_MTO LEX_FOLDER LEX_METEO_DATA LEX_SAFRAN_CELLS LEX_YEAR0_METEO LEX_VIEWTOSKY
%token LEX_LINK_SPECIE_GRID_ELEBU LEX_TRANSPORT_FORCING LEX_TRANSPORT_FORCING_PATH LEX_FORCING_TYPE LEX_SPECIES_CELLS
%token <integer> LEX_TRANSPORT_INPUT LEX_MET
%token LEX_TTC_SET_INTRO LEX_TTC_SPECIE_TYPE LEX_TTC_ITER_MAX LEX_TTC_EPS LEX_TTC_THETA
%token LEX_SPECIES_SETTINGS LEX_MOLAR_MASS_SPECIES LEX_MOLAR_MASS_ELEMENT LEX_DIFF_MOL
%token <integer> LEX_TTC_SPECIE LEX_TTC_PROCESS LEX_AQUITARD_SOL_TYPE LEX_AQUITARD_ACTIVE
%token <integer> LEX_TRANSPORT_ATT LEX_TRANSPORT_BASE_ATT LEX_TRANSPORT_U_ATT
%token <integer> LEX_INIT_VAR_ATT LEX_DYNSAT_TYPE
%token LEX_AQUITARD_BOTTOM LEX_AQUITARD_TOP LEX_RIVERBED LEX_THRESHOLD LEX_DYNAMIC_NSAT
%token <integer> LEX_AQUITARD_VAR_ATT LEX_TRANSPORT_SOURCE LEX_BOUND_TYPE LEX_BOUND_DIR LEX_KINDOF_BOUND_T LEX_KINDOF_BOUND_S
%token <integer> LEX_H_THRESHOLD LEX_UPTAKE_THRESHOLD

%type <ptempwet> wet_value wet_values
%type <real> flottant mesure
%type <real> a_unit units one_unit a_unit_value all_units read_units homo_value multi_coef
%type <function> f_ts f_t series serie
%type <boundary> fion hydworks hydwork hydstructures
%type <sing> singularity singularities
%type <pointAbscZ> sectionAbscZ sectionAbscZs
%type <pointXYZ> sectionXYZ sectionXYZs
%type <face> cross_sections cross_section bathymetry trajectory traj_point traj_points
%type <face> faces_musk face_musk def_elements_musk
%type <inflow> inflow inflows
%type <lp_pk> extents
%type <ts_pk> points one_pk pk_list
%type <struct_id> id_safran ids_safran
%type <struct_id> ele_ids_out ele_id_out ele_ids ele_id read_nsat_aq read_nsat_aqs simple_bound aq_ids aq_id simple_tp_bound
%type <struct_id> read_hderm_aqs read_hderm_aq id_eleaq id_eleaqs
%type <struct_mto> read_mtos read_mto
%type <struct_param> read_params read_param
%type <struct_bu> read_bus read_bu
%type <struct_cprod> read_cprods read_cprod read_cprods_hderm read_cprod_hderm
%type <struct_nash> read_params_zns read_param_zns
%type <struct_zns> nsat_units nsat_unit
%type <gw_layer> layer layers
%type <gw_ele>  lire_eles lire_ele lecture_eles
%type <lithology> one_litho  litho_series
%type <struct_id> catchment_cells catchment_cell catch_hderm_cell catch_hderm_cells elebu_id elebu_ids
%type <struct_catch> catchments_settings catchments_setting read_chasm read_chasms catch_hderm_settings catch_hderm_setting catch_hderm_tc_read catch_hderm_tc_unread
%type <struct_specie_unit> read_species_cells read_species_cell link_grid_ele_bu links_grid_ele_bu
%type <struct_rts> one_corresp ones_corresp
%start beginning
%%

// NG : 06/07/2020 : Introducing optional transport section
beginning : begin main_hydro main_transport outputs
{
//	if (Simul->pset_caw->transport_caw == YES)
//	{
		/* NG : 01/10/2020 : General function checking and analyzing inflows consistencies when declared. Declares
                        	 and attributes all inflows to the corresponding river network (river reaches, elements' faces).
                       		 Also displays (if debug is activated in the COMM file) all inflows attributes by browsing
                       	 	 an entire network. */

		if (Simul->pchyd != NULL && Simul->pchyd->counter->ninflows > 0)
		{
			HYD_inflows_settings(Simul->pchyd,Simul->pset_caw->idebug,Simul->nb_species,pchain_inflow_hyd,Simul->poutputs);
			LP_printf(Simul->poutputs,"Done ! Inflows for the surface hydraulic network all set up.\n");
		}

		if (Simul->pchyd_hderm != NULL && Simul->pchyd_hderm->counter->ninflows > 0)
		{
			HYD_inflows_settings(Simul->pchyd_hderm,Simul->pset_caw->idebug,Simul->nb_species,pchain_inflow_hderm,Simul->poutputs);
			LP_printf(Simul->poutputs,"Done ! Inflows for the hyperdermic hydraulic network all set up.\n");
		}
//	}
}
| begin main_hydro outputs
{
	if (Simul->pchyd != NULL && Simul->pchyd->counter->ninflows > 0)
	{
		HYD_inflows_settings(Simul->pchyd,Simul->pset_caw->idebug,Simul->nb_species,pchain_inflow_hyd,Simul->poutputs);
		LP_printf(Simul->poutputs,"Done ! Inflows for the surface hydraulic network all set up.\n");
	}

	if (Simul->pchyd_hderm != NULL && Simul->pchyd_hderm->counter->ninflows > 0)
	{
		HYD_inflows_settings(Simul->pchyd_hderm,Simul->pset_caw->idebug,Simul->nb_species,pchain_inflow_hderm,Simul->poutputs);
		LP_printf(Simul->poutputs,"Done ! Inflows for the hyperdermic hydraulic network all set up.\n");
	}
};


main_hydro : hydro_intro modules_hydro LEX_CLOSING_BRACE

hydro_intro : LEX_HYDRO LEX_EQUAL LEX_OPENING_BRACE

modules_hydro : module_hydro modules_hydro
| module_hydro
;

module_hydro : aquifer
{
  LP_printf(Simul->poutputs,"AQUIFER READ\n");
}
| surface
| nonsat
| lake
| hyperdermic
;

begin : paths model_settings
| model_settings
;

/* This part enables to define specific input and output folders */
paths : path paths
| path
;

path : input_folders
| output_folder
;

input_folders : LEX_INPUT_FOLDER folders
;

/* List of input folders' names */
folders : folder folders
| folder
;

folder : LEX_EQUAL LEX_NAME
{
  if (folder_nb >= NPILE) {
    fprintf(Simul->poutputs,"Only %d folder names are available as input folders\n",NPILE);
    printf("Only %d folder names are available as input folders\n",NPILE);
  }
  name_out_folder[folder_nb++] = strdup($2);
  fprintf(Simul->poutputs,"path : %s\n",name_out_folder[folder_nb-1]);
  printf("path : %s\n",name_out_folder[folder_nb-1]);
  free($2);
}
;

/* Definition of the folder where outputs are stored */
output_folder : LEX_OUTPUT_FOLDER LEX_EQUAL LEX_NAME
{
  int noname,str_len;
  char *new_name,*new_name_run;
  char cmd[MAXCHAR];
  FILE *fp;

  fp=Simul->poutputs;

  str_len=STRING_LENGTH_LP;
  new_name = (char *)calloc(str_len,sizeof(char));
  sprintf(new_name,"%s",$3);
  sprintf(cmd,"mkdir %s",new_name);
  system(cmd);
  new_name_run = (char *)calloc(str_len,sizeof(char));
  sprintf(new_name_run,"RESULT=%s/",new_name);
  noname = putenv(new_name_run);
  if (noname == -1)
    LP_error(fp,"File %s, line %d : undefined variable RESULT\n",
		current_read_files[pile].name,line_nb);
  sprintf(cmd,"mkdir -p %s",getenv("RESULT"));//NF 31/10/2020 mkdir recursive to avoid seg fault for new users
  system(cmd);
  free(new_name);
}
;

/******************************************************************************/
/*             This part defines global OMP parameters                        */
/*                     and stores them in the cawaqs settings structur        */
/******************************************************************************/
omp : intro_omp att_omps LEX_CLOSING_BRACE
intro_omp : LEX_OMP  LEX_EQUAL  LEX_OPENING_BRACE
{
  LP_printf(Simul->poutputs,"Setting computing ressources attributes for module : %s\n",CAW_name_mod(module));
#ifdef OMP

  psmp=CAW_set_OMP_param(module);
  LP_printf(Simul->poutputs,"Configuring OMP options by default\n");
  psmp->nthreads=PC_set_active_proc(Simul->poutputs,PC_NPROC_OMP);
#else
  LP_warning(Simul->poutputs,"CAWAQS%5.2f compiled in sequential mode, OMP option ignored",NVERSION_CAW);
  #endif
}
;
att_omps: att_omp att_omps
|att_omp
;

att_omp : LEX_THREADS LEX_EQUAL LEX_INT
{

#ifdef OMP
  if(answer==YES){
    LP_printf(Simul->poutputs,"Re-Configuring OMP options following user specs\n");
    psmp->nthreads=PC_set_active_proc(Simul->poutputs,$3);
  }
  #endif
}
|  LEX_ANSWER
{
#ifdef OMP
  if($1==NO){
    answer=$1;
  LP_printf(Simul->poutputs,"Re-Configuring OMP options following user specs: sequential mode\n");
  psmp->nthreads=PC_set_active_proc(Simul->poutputs,1);
  }
  else
    answer=$1;
#endif

}
;

/******************************************************************************/
/*             This part defines global parameters of the model and           */
/*                     stores them in the simulation structure                */
/******************************************************************************/

model_settings : LEX_SIMUL LEX_EQUAL LEX_OPENING_BRACE simul_name atts_simul LEX_CLOSING_BRACE
{
  LP_printf(Simul->poutputs,"Global parameters of the simulation have been defined\n\n");
  LP_printf(Simul->poutputs,"Preconfiguring SIM_PATH at . \n\n");
}
;

simul_name : LEX_NAME
{
  Simul->name = $1;
  LP_printf(Simul->poutputs,"\nSimulation name = %s\n",Simul->name);
}
;

atts_simul : att_simul atts_simul
| att_simul
;

att_simul :  def_chronos
| def_settings
{
    ttstep = Simul->chronos->t[END_CHR]/Simul->chronos->dt;
    LP_printf(Simul->poutputs,"Total number of time-step within the simulation period = %d s\n",ttstep);  // DK AR addition
}
;

def_chronos : LEX_CHRONOS LEX_EQUAL LEX_OPENING_BRACE def_times LEX_CLOSING_BRACE
{
  if (fabs(Simul->chronos->t[BEGINNING]-CODE_TS)<EPS_TS)//NF 26/11/2020 to be able to simulate whatever period of time. Values initialised by libchronos in manage_chronos.c
    {
      if(Simul->chronos->year[END]<0 || Simul->chronos->year[BEGINNING]<0)
	{
	  LP_error(Simul->poutputs," no time sttings have been defined check your command file \n");
	}

      Simul->chronos->nyear=Simul->chronos->year[END]-Simul->chronos->year[BEGINNING];
      Simul->chronos->t[BEGINNING]=0;
      Simul->chronos->t[CUR_CHR]=0;
      Simul->chronos->t[END]=CHR_convert_time((double)CHR_nj_sim(Simul->chronos),DAYS_CHR,S_CHR);
    }
  else{//NF 26/11/2020 already defined
    Simul->chronos->t[CUR_CHR]=Simul->chronos->t[BEGINNING];
  }

}

def_times : def_time def_times
| def_time
;
def_time : LEX_TIMESTEP LEX_EQUAL mesure
{
  Simul->chronos->dt = $3;
  Simul->chronos->dt_output = $3; //TV 16/12/2019
LP_printf(Simul->poutputs,"time step = %f s\n",Simul->chronos->dt);
}
| LEX_DTOUT LEX_EQUAL mesure
{
  Simul->chronos->dt_output = $3; //TV 16/12/2019
  LP_printf(Simul->poutputs,"outputs time step = %f s\n",Simul->chronos->dt_output); //TV 16/12/2019
}
| LEX_YEAR LEX_EQUAL LEX_INT
{
  Simul->chronos->year[$1]=$3;
}
| LEX_TIME  LEX_EQUAL mesure
{//NF 26/11/2020 to be able to simulate whatever period of time.
  Simul->chronos->t[$1]=$3;


}
;

def_settings : LEX_SET LEX_EQUAL LEX_OPENING_BRACE settings LEX_CLOSING_BRACE

settings : setting settings
| setting
;


setting :  LEX_TYPE LEX_EQUAL LEX_CALC_STATE
{
  Simul->pset_caw->calc_state=$3;
  LP_printf(Simul->poutputs,"CaWaQS calculation in %s state\n",HYD_name_calc_state(Simul->pset_caw->calc_state));
}
| LEX_GENERALP LEX_EQUAL mesure
{
  Simul->general_param[$1] = $3;
  LP_printf(Simul->poutputs,"CaWaQS set-up %s : %f\n",HYD_name_general_param($1),Simul->general_param[$1]);
}
//def_rac
|LEX_NIT_STEADY_MAX LEX_EQUAL LEX_INT
{
  Simul->pset_caw->steadynitmax=$3;
  LP_printf(Simul->poutputs,"Maximum number of iterations for steady state : %d\n",Simul->pset_caw->steadynitmax);
}
|LEX_NIT_PIC_MAX LEX_EQUAL LEX_INT
{
  Simul->pset_caw->picnitmax=$3;
  LP_printf(Simul->poutputs,"Maximum number of iterations for Picard loop : %d\n",Simul->pset_caw->picnitmax);
}
|LEX_EPS_PIC LEX_EQUAL mesure
{
  Simul->pset_caw->eps_pic=$3;
  LP_printf(Simul->poutputs,"Picard convergence criterion on head difference : %f\n",Simul->pset_caw->eps_pic);
}
|LEX_CHECK_MESH LEX_EQUAL LEX_ANSWER
{
  Simul->pset_caw->check_mesh=$3;
}
|LEX_PRINT_SURF LEX_EQUAL LEX_ANSWER
{
  Simul->pset_caw->print_surf=$3;
}
|LEX_DEBUG LEX_EQUAL LEX_ANSWER
{
  Simul->pset_caw->idebug=$3;
}
|LEX_TRANSPORT LEX_EQUAL LEX_ANSWER
{
  Simul->pset_caw->transport_caw=$3;
  if (Simul->pset_caw->transport_caw == YES) LP_printf(Simul->poutputs,"Transport calculations activated.\n");
};

/*def_rac : LEX_SIM_PATH LEX_EQUAL LEX_NAME
{
  char *new_name;
  int str_len,noname;
  str_len=STRING_LENGTH_LP;
  new_name = (char *)calloc(str_len,sizeof(char));
  sprintf(new_name,"SIM_PATH=%s",$3);
  noname = putenv(new_name);
  if (noname == -1)
    LP_error(Simul->poutputs,"File %s, line %d : undefined variable SIM_PATH\n",
		current_read_files[pile].name,line_nb);
};*/

/*****************************************************************************/
/*                                                                           */
/*                        H Y D R O   I N P U T S                            */
/*                                                                           */
/* --------------------------------------------------------------------------*/
/*                           Hydro-related inputs                            */
/*****************************************************************************/


/****************************************************************************/
/*                            AQUIFER INPUTS                                */
/*                                                                          */
/****************************************************************************/

aquifer : intro_aquifer create_mesh_parallel specify_simul brace ;

// NG : 11/08/2021 : Introducing optional AQ settings sub-part (all parameters have default values if left undeclared)
create_mesh_parallel : omp create_mesh
| aq_settings omp create_mesh
| create_mesh
| aq_settings create_mesh
;

intro_aquifer : LEX_AQUIFER LEX_EQUAL brace
{
  int i;
  s_carac_aq *pcarac_aq;

  pcarac_aq=AQ_create_carac();
  pcarac_aq->settings=AQ_create_param();
  AQ_init_carac(pcarac_aq);
  pcarac_aq->ppic->nitmax=Simul->pset_caw->picnitmax;
  pcarac_aq->steadynitmax=Simul->pset_caw->steadynitmax;
  if (pcarac_aq->ppic->nitmax<pcarac_aq->ppic->nitmin){
    LP_warning(Simul->poutputs,"In CaWaQS%4.2f : Picart iteration inconsistent. Maximum number of Picart iterations set to the minimum required : %d\n",NVERSION_CAW,pcarac_aq->ppic->nitmax);
    pcarac_aq->ppic->nitmax=pcarac_aq->ppic->nitmin;
  }
  pcarac_aq->ppic->eps_pic=Simul->pset_caw->eps_pic;
  if(Simul->pset_caw->calc_state==STEADY)
    {
      pcarac_aq->settings->a0=0;//NF 18/10/2017 Cette variable pondere le coefficient d'emmagasinnement pour le faire disparaitre de la matrice sans changer l'algorithme de remplissage
    }
  else
    pcarac_aq->settings->a0=1;

  for (i=0;i<NPAR_AQ;i++)
    {
	pcarac_aq->settings->general_param[i]=Simul->general_param[i];
	LP_printf(Simul->poutputs,"groundwater %s = %f\n",HYD_name_general_param(i),pcarac_aq->settings->general_param[i]);
    }
  module=AQ_CAW;

  Simul->pcarac_aq=pcarac_aq;

};

// NG : 11/08/2021 : Introducing additionnal AQ settings (optional)
aq_settings : intro_aq_settings aq_sets LEX_CLOSING_BRACE

intro_aq_settings : LEX_SET LEX_EQUAL LEX_OPENING_BRACE

aq_sets : aq_set aq_sets
| aq_set
;

aq_set : LEX_ALPHA_Z LEX_EQUAL flottant
{
    Simul->pcarac_aq->settings->anisotropy_factor=$3;
    LP_printf(Simul->poutputs,"Vertical anisotropy factor value switching from default value %10.4e to : %10.4e\n",ALPHA_Z_DEFAULT_AQ,Simul->pcarac_aq->settings->anisotropy_factor);
}
| LEX_SY_FACTOR LEX_EQUAL flottant
{
    Simul->pcarac_aq->settings->Sy_factor=$3;
    LP_printf(Simul->poutputs,"Specific yield multiplication factor value switching from default value %10.4e to : %10.4e\n",SY_DEFAULT_FACTOR_AQ,Simul->pcarac_aq->settings->Sy_factor);
}
| LEX_LOG_AQ_FLUXES LEX_EQUAL LEX_ANSWER  // NG : 29/01/2021
{
    Simul->pcarac_aq->settings->log_balance=$3;
    LP_printf(Simul->poutputs,"Mass balance log print : %s.\n",LP_answer(Simul->pcarac_aq->settings->log_balance,Simul->poutputs));
}
| LEX_PRINT_LITHOS LEX_EQUAL LEX_ANSWER  // NG : 31/08/2021
{
    Simul->pcarac_aq->settings->print_lithos=$3;
    LP_printf(Simul->poutputs,"Subsurface lithologies output file print : %s.\n",LP_answer(Simul->pcarac_aq->settings->print_lithos,Simul->poutputs));
};

/****************************************************************************/
/*                            CREATE MESH                                   */
/*                       defining mesh geometry                             */
/****************************************************************************/

create_mesh : LEX_NEWSAM LEX_EQUAL brace tab_layer brace
{

/****************************************************************************/
/*               After the mesh is created FACES are created                */
/*                     defining neighborhood settings                       */
/****************************************************************************/

  if(Simul->pset_caw->check_mesh==YES)
    { LP_printf(Simul->poutputs,"checking mesh redundancy\n");
    MSH_check_redundancy(Simul->pcarac_aq->pcmsh,Simul->poutputs); // BL pas grand interet à mon avis verifie juste si deux éléments ont le même id_gis.

    }
  MSH_referencing_mesh(Simul->pcarac_aq->pcmsh,Simul->poutputs);
  MSH_order_element_in_ref(Simul->pcarac_aq->pcmsh,Simul->poutputs);
  MSH_check_element_ordering(Simul->pcarac_aq->pcmsh,Simul->poutputs);
  MSH_create_ele_faces(Simul->pcarac_aq->pcmsh,Simul->poutputs);
  //MSH_list_all_ele_short(Simul->pcarac_aq->pcmsh,Simul->poutputs);//NF 22/5/2015 I modified libmesh in MSH_carac_ele_short in manage_element.c to have a LP_error in case the element is SUBREF. Everything works well //BL to debug
  MSH_check_boundary(Simul->pcarac_aq->pcmsh,Simul->poutputs);
  MSH_create_vertical_faces(Simul->pcarac_aq->pcmsh,Simul->poutputs);

  // MSH_check_vertical_connexion(Simul->pcarac_aq->pcmsh,Simul->poutputs); BL sert à rien cette fonction faite avec les pieds (pb pour les cellules étant à la fois TOP et BOT ie quand une cellule de la dernière couche est TOP)
  MSH_tab_all_neigh(Simul->pcarac_aq->pcmsh,Simul->poutputs);
  //MSH_list_all_ele_pneigh(Simul->pcarac_aq->pcmsh,Simul->poutputs);
  MSH_calcul_on_all_faces(Simul->pcarac_aq->pcmsh,Simul->poutputs);
  if(Simul->pset_caw->print_surf==YES)
    {
      LP_printf(Simul->poutputs,"Printing surface mesh\n");
      MSH_export_geom_surface_layer(Simul->pcarac_aq->pcmsh,Simul->poutputs);
      LP_printf(Simul->poutputs,"Done\n");
    }
}
;


tab_layer : layers
{
  int i;
  double ratio_tmp;
  double precision;
  s_cmsh *pcmsh;
  double dimension;

  pcmsh=Simul->pcarac_aq->pcmsh;

  pcmsh->pcount->nlayer=nlayer;
  pcmsh->p_layer=MSH_tab_layer(player,nlayer,Simul->poutputs);

  ratio_tmp=pcmsh->l_max/pcmsh->l_min;
  pcmsh->dimension=(int)ratio_tmp;
  precision=fabs(ratio_tmp-pcmsh->dimension);
  if (precision>EPS){
    LP_error(Simul->poutputs,"Pb de Dimension calculee %f/%f = %f estime a %d\n",pcmsh->l_max,pcmsh->l_min,ratio_tmp,pcmsh->dimension);
  }

  for (i=nlayer-1;i>=0;i--)
    {
      pele_msh=pcmsh->p_layer[i]->pele;
      MSH_calcul_sur_ele(pele_msh,Simul->pcarac_aq->pcmsh);
    }
}
;

layers : layer layers
{
$$=MSH_chain_layer($1,$2);
}
|layer {$$=$1;}
;

layer : lire_layer carac_layer  brace
{

  // player->id=++nlayer;
  $$ = player;
  printf("layer %s, id %d read\n",player->name,player->id);
}
;

lire_layer : LEX_LAYER LEX_EQUAL brace LEX_NAME
{
  int i;
  player = MSH_create_layer($4,++nlayer);
  nele=0;
  if(Simul->pcarac_aq->settings->Surf==YES && nlayer==2)
    nele_tot=1;
}
;

carac_layer : lecture_eles LEX_AVIEW
{
  int i;
  player->nele=nele;
  Simul->pcarac_aq->pcmsh->pcount->nele+=nele;
  //MSH_attribute_ele2layer(player,$1,nele);
  player->p_ele=MSH_tab_ele($1,nele,Simul->poutputs);
  nele=0;
  intern_id=Simul->pcarac_aq->pcmsh->pcount->nele;
}
;

lecture_eles : lire_eles
{
  player->pele=$1;
  $$=$1;
}
;

lire_eles : lire_ele lire_eles
{
  $$=MSH_chain_ele($1,$2);
}
| lire_ele {
  $$=$1;}
;


lire_ele : lire_id_ele lire_coordonne lire_coordonne lire_coordonne lire_coordonne lire_coordonne LEX_AVIEW
{

  MSH_ordinate_mesh(pele_msh,Simul->pcarac_aq->pcmsh);
  pele_msh->player=player;
  $$=pele_msh;
}
;

lire_id_ele : LEX_INT LEX_AVIEW
{
  pele_msh=MSH_create_element($1,ACTIVE);
  pele_msh->id[INTERN_MSH] = nele++; // pour que l'id interne corresponde à la position de l'ele dans le tableau par layer : p_layer->pele[i]
  pele_msh->id[ABS_MSH] = nele_tot++;
  pele_msh->nele=1;
  coordo=0;
}
;

lire_coordonne :  flottant LEX_VIRGULE flottant
{
  if (coordo<4)
    {
      pele_msh->pdescr->coord[coordo][0] = $1;
      pele_msh->pdescr->coord[coordo++][1] = $3;
    }
}
;

/****************************************************************************/
/*                            SPECIFY SIMUL                                 */
/*                    defining aquifer parameters                           */
/****************************************************************************/

specify_simul : intro_set setup_layers LEX_CLOSING_BRACE
{
 	/* 21/04/2021 : NG : Les liens de dérivation AQ (cascades, bypasses) sont ici traités après lecture de TOUS les setup_layer car les settings de 'cascade'/'bypass'
	de chaque couche peuvent référencer des mailles d'autres couches pour lesquelles pele->phydro peut ne pas être encore alloué
  	si ce type de fichier était traité au sein de chaque bloc setup_layer. */
	if (Simul->pcarac_aq->settings->cascade == YES)
	{
		AQ_check_derivation_setup(Simul->pcarac_aq,CASCADE_AQ,Simul->poutputs);
	//	AQ_print_all_derivation_links(Simul->pcarac_aq,CASCADE_AQ,Simul->poutputs); 	// Check NG
		AQ_define_source_derivation_all(Simul->pcarac_aq,CASCADE_AQ,CASCADE_DISCHARGE_AQ,Simul->poutputs); 	// Setting source-related pointers for the all cascade destination cells, in order to receive Cauchy discharges
		LP_printf(Simul->poutputs,"%s setup done.\n",AQ_derivation_type_name(CASCADE_AQ));
	}
	if (Simul->pcarac_aq->settings->bypass == YES)
	{
		AQ_check_derivation_setup(Simul->pcarac_aq,BYPASS_AQ,Simul->poutputs);
	//	AQ_print_all_derivation_links(Simul->pcarac_aq,BYPASS_AQ,Simul->poutputs); 		// Check NG
		AQ_define_source_derivation_all(Simul->pcarac_aq,BYPASS_AQ,BYPASS_DISCHARGE_AQ,Simul->poutputs);
		LP_printf(Simul->poutputs,"%s setup done.\n",AQ_derivation_type_name(BYPASS_AQ));
	}
	//NF 13/9/2022 Finalizing the conductance calculation
	AQ_finalize_interlayer_drainance(Simul->pcarac_aq,Simul->chronos,Simul->poutputs);
};

intro_set : LEX_SET_UP LEX_EQUAL LEX_OPENING_BRACE

setup_layers : setup_layer setup_layers
{
    if (Simul->pcarac_aq->settings->print_lithos == YES)
        AQ_print_subsurface_lithos(Simul->pcarac_aq->pcmsh,Simul->poutputs);  // NG : 30/08/2021
}
|setup_layer
{
  int ele_layer=Simul->pcarac_aq->pcmsh->pcount->nlayer;
  if(verif_layer>ele_layer ||  verif_layer<ele_layer)
    LP_error(Simul->poutputs,"problem in layer definition look at command file \n");

}
;
//BL ATTENTION les param sont toujours definis avant les BC ou les sources
setup_layer : read_setup_layer setup_aqs LEX_CLOSING_BRACE
{
	LP_printf(Simul->poutputs,"Aquifer system setup done.\n");
}
;

setup_aq : set_param
| set_bound
| set_source
| set_aq_type
| set_derivation // NG : 21/04/2021
| set_threshold  // NG : 29/02/2024
| set_aq_type LEX_OPENING_BRACE litho_series LEX_CLOSING_BRACE
{
  player->plitho=AQ_browse_litho($3,BEGINNING_TS);
  player->nlitho=intern_id;
  AQ_create_litho_for_each_ele_in_layer(player,Simul->poutputs);
};

setup_aqs : setup_aq setup_aqs
| setup_aq
{
	LP_printf(Simul->poutputs,"Aquifer setup done.\n");
}
;


read_setup_layer : LEX_SETUP_LAYER LEX_EQUAL LEX_OPENING_BRACE LEX_NAME
{
  int i;

  nlayer=MSH_get_layer_rank_by_name(Simul->pcarac_aq->pcmsh,$4,Simul->poutputs);
  verif_layer++;
  player=Simul->pcarac_aq->pcmsh->p_layer[nlayer];//NF 26/9/2017 Here I am resetting the proper layer, that will be used after set_param
  if(player!=NULL)
    {
      AQ_create_hydro_tab(player,Simul->poutputs);

    }
   else
    {
      LP_error(Simul->poutputs,"There is no layer %d in mesh",nlayer);
    }

}
;

set_aq_type : LEX_TYPE LEX_EQUAL LEX_AQ_TYPE
{
    player->type=$3;
    if(player->type==UNCONFINED_AQ)
    {
        Simul->pcarac_aq->settings->aq_type=player->type;
        intern_id=0;
	}
};


litho_series : one_litho litho_series
{
    $$=AQ_chain_fwd_litho($1,$2);
    player->plitho=$$;
}
| one_litho
{
    $$=$1;
};

one_litho : LEX_INT LEX_NAME
{
    $$=AQ_create_litho();
    player->plitho=AQ_create_litho();
    $$=AQ_fill_litho($2,$1,intern_id++);
    LP_printf(Simul->poutputs,"Lithology %s, ID %d read\n",$$->name,$$->id[GIS_MSH]);
	player->plitho=$$;
};


set_param : intro_param att_series LEX_CLOSING_BRACE
{
    int ans;
    LP_printf(Simul->poutputs," Layer %d \n",nlayer);

    if (player->type==UNCONFINED_AQ)
    {
        AQ_find_subsurface_litho(player,GIS_MSH,Simul->poutputs); // NG : 30/08/2021
        AQ_calc_param_unconfined(Simul->pcarac_aq->settings,player,Simul->poutputs);
    }

    AQ_create_face_transm(player,Simul->poutputs);//MM 24/10/2018
    AQ_create_XY_conduct_face(player,Simul->chronos,Simul->poutputs);//MM 24/10/2018

    //NF 13/9/2022 vertical conductance cannot be achieved here, because it may rely in the case of a multi-layer aquifer on the upper transmissivity. This part needs to be disentangle from the remaining. I will move it after the reading of all layers properties
    // if(player->cond_type==AUTO_COND)//NF 13/9/2022 no need for this condition, the conductance is needed in everylayer for auto calculation. If defined by a command conductance = {} then no need for creating Z conduc. I just check//NF 14/9/2022 I don't see why there is this condition. The pcond structure of the face' hydrostructure must be declared anyway ?
      //{
        AQ_create_Z_conduct_face(player,Simul->poutputs);//MM 24/10/2018 les trois fonctions create (face_transm, XY_conduct, Z_conduct) ont ete separees des fonctions calc
    //}

    AQ_calc_face_transm_layer(player,Simul->poutputs);//MM 24/10/2018 ces deux fonctions resultent du scindage de Aq_calc_face_transm_layer
    AQ_calc_face_cond_layer(Simul->pcarac_aq->settings,player,Simul->chronos,Simul->poutputs);//MM 24/10/2018//NF 6/12/2018 Transm need to be calculated befor cond

    // NG : 13/08/2021 : Calculating ztop if not declared
    ans=AQ_check_ztop_definition(player,Simul->poutputs);
    if (ans == YES)
        LP_printf(Simul->poutputs,"Top altitudes of layer %d have been automatically calculated.\n",nlayer);
    else
        LP_printf(Simul->poutputs,"Automatic calculation of top altitudes ignored for layer %d.\n",nlayer);
}
;

intro_param : LEX_PARAM LEX_EQUAL LEX_OPENING_BRACE;

att_series : att_serie att_series
| att_serie
;

att_serie : intro_descr setup_list  LEX_CLOSING_BRACE
{
  int ele_num=Simul->pcarac_aq->pcmsh->p_layer[nlayer]->nele;
  if((verif_ele>ele_num || verif_ele<ele_num) && kindof_attribut != CONDUCT)
	LP_error(Simul->poutputs,"Problem in %s parameters  check the input file\n",AQ_name_attributs(kindof_attribut));
}
| type_mean
| type_cond
| intro_descr read_file
;

intro_descr : LEX_MESH_ATT LEX_EQUAL LEX_OPENING_BRACE
{
  mult_AQparam = 1.0; // NG : 10/08/2021 Reinitializing the multiplication coeficient for AQ parameters so it doesn't mess up with the following parameters !
  kindof_attribut = $1;
  verif_ele=0;
  LP_printf(Simul->poutputs,"Attributing %s\n",AQ_name_attributs(kindof_attribut));
  if($1==CONDUCT)
    player->cond_type=COND;//NF 13/9/2022 I hope it will be sufficicent for now. My concern is that a layer that is both impluvium and CONFINED cannot be properly parameterized automatically for the conductance except if we do not allow any overflow at the surface (ie no conductance so nil) and then we should have an automatic calculation of the conductance between layers, in its confined part. On the other hand if we need to define a conductance on the impluvium on sucj=h layer, then the cond_type of the layer become COND and no automatic calibration is performed for the CONFINED AREA
}
;

read_file : LEX_FILE LEX_EQUAL LEX_NAME LEX_CLOSING_BRACE
{
  //NF 19/7/2018
  //AR MM 24/07/2018

    int nlitho,i=0,eof=0,intern_id;
    double val;
    char name[STRING_LENGTH];
    s_ele_msh *pele ;
    s_litho_aq *plitho;
    fr=NULL;

    nlitho=player->nlitho;

    for(i=0;i>nlitho;i++) player->plitho=AQ_browse_litho(player->plitho,i);    // NG : ???? i est supposé être la direction de balayage de la chaîne...


    while ((fr==NULL)&&(i<folder_nb))
    {
        sprintf(name,"%s/%s",name_out_folder[i++],$3);
        fr=LP_openr_file(name,Simul->poutputs,CODE_TS);
    }

//      pele=MSH_get_ele_from_id_gis(Simul->pcarac_aq->pcmsh,player->id,id_gis,Simul->poutputs);//NF 20/7/2018 Here there is a possibility to speed up the reading if we you intern_id and not id_gis as input data. In that case, we can use the function s_ele_msh *MSH_get_ele, which is much faster than MSH_get_ele_from_id_gis

    switch(kindof_attribut)
    {
        case TRANSM_HOMO:
        {
            eof=fscanf(fr,"%d",&intern_id);
            while(eof!=EOF)
            {
                pele=MSH_get_ele(Simul->pcarac_aq->pcmsh,player->id-1,intern_id,Simul->poutputs);
                plitho=pele->phydro->plitho;
                for (i=0;i<nlitho;i++)
                {
                    eof=fscanf(fr,"%lf",&val);
                    pele->phydro->ptransm=AQ_create_transm();
                    pele->phydro->ptransm->type=HOMO;
                    plitho->k=(double)val;
                    //	printf("ele%d litho_K = %e\n",pele->id[ABS_MSH],plitho->k);
                    if(i==nlitho-1)
                    {
                        eof=fscanf(fr,"%d",&intern_id);
                    }
                    plitho=plitho->next;
                }
            }
            break;
        }

        case THICK:
        {
            eof=fscanf(fr,"%d",&intern_id);
            while(eof!=EOF)
            {
                pele=MSH_get_ele(Simul->pcarac_aq->pcmsh,player->id-1,intern_id,Simul->poutputs);
                plitho=pele->phydro->plitho;
                for (i=0;i<nlitho;i++)
                {
                    eof=fscanf(fr,"%lf",&val);
                    plitho->thick=(double)val;
                    //	LP_printf(Simul->poutputs,"ele%d lithothick = %e\n",pele->id[ABS_MSH],plitho->thick);
                    if(i==nlitho-1)
                    {
                        eof=fscanf(fr,"%d",&intern_id);
                    }
                    plitho=plitho->next;
                }
            }
            break;
        }

        case SPECIFIC_YIELD:
        {
            eof=fscanf(fr,"%d",&intern_id);
            while(eof!=EOF)
            {
                pele=MSH_get_ele(Simul->pcarac_aq->pcmsh,player->id-1,intern_id,Simul->poutputs);
                plitho=pele->phydro->plitho;
                for (i=0;i<nlitho;i++)
                {
                    eof=fscanf(fr,"%lf",&val);
                    plitho->Sy=(double)val;
                    if(i==nlitho-1)
                    {
                        eof=fscanf(fr,"%d",&intern_id);
                    }
                    plitho=plitho->next;
                }
            }
            break;
        }
    }
    fclose(fr);
};


// NG : 10/08/2021 : New input syntaxes added for AQ parameter inputs in CONFINED mode
/************************************************************************************
 *                            AQ parameters setup (CONFINED mode)
 ************************************************************************************
 * As of CaWaQS2.98 : Several input syntaxes are available in order to define some aquifer parameters
 * in CONFINED mode :
 *
 * Available options are :
 * 1. Use a command sub-file, containing explicit declarations of the values associated
 * with each cell
 *     ==> Syntax for each cell => (Intern ID, Value) called with the usual
 *         "< param_name > = { include < sub_file_name > }" line in the COMM file.
 *
 * 2. A multiplication coefficient can also be added, so all the value of the sub-file
 * will be multiplied by it.
 *     ==> In that case, the corresponding syntax is
 *         "< param_name > = { <multiplication_factor_value> include < sub_file_name > }"
 *
 * 3. It's also possible to define a uniform field from a single value,
 * directly from the COMM file
 *     ==> In that case, possible corresponding syntaxes are :
 *         "< param_name > = { uniform [unit] < parameter_value > }"
 *     or  "< param_name > = { uniform < parameter_value > [unit] }"
 *     or  "< param_name > = { uniform < parameter_value > }"
 *
 * All three syntaxes can be mixed up from one parameter to another.
 *
 * Options 2 and 3 can be used with all types of entries for which syntax 1. is allowed.
 * *********************************************************************************/

setup_list : values
| uniform
| multi_coef values
| T_anisos //BL define_transm to define face transm as input to finish
;

values : value values
|value
;

multi_coef : flottant
{
    mult_AQparam = $1;

    if (mult_AQparam < EPS_CAW)
        LP_error(Simul->poutputs,"In CaWaQS%4.2f : Error in file %s, at line %d : Multiplication factor for AQ param cannot be null or negative. Check your command file.\n",NVERSION_CAW,__FILE__,__LINE__);

  //  LP_printf(Simul->poutputs,"Multiplication coefficient : %10.3e - Parameter : %s  - ",mult_AQparam,AQ_name_attributs(kindof_attribut)); // NG check
};

// NG 10/08/2021 : Following bits are meant to easily set uniform parameter fields from a single input value
uniform : LEX_UNIFORM homo_value
{
    AQ_set_uniform_layer_param_values(Simul->pcarac_aq,nlayer,kindof_attribut,$2,Simul->poutputs);
    verif_ele+=Simul->pcarac_aq->pcmsh->p_layer[nlayer]->nele;
    LP_printf(Simul->poutputs,"Multiplication coefficient : %10.3e - Parameter %s : Uniform value attributed for layer %d = %10.4e\n",mult_AQparam,AQ_name_attributs(kindof_attribut),nlayer,$2);
};

homo_value : flottant
| mesure // So units can be written down explicitly if needed

value : LEX_INT flottant
{
    int j,k;
    verif_ele++;

    switch(kindof_attribut)
    {
        case H_INI:
        {
            Simul->pcarac_aq->pcmsh->p_layer[nlayer]->p_ele[$1]->phydro->h[ITER] = mult_AQparam*$2;
            Simul->pcarac_aq->pcmsh->p_layer[nlayer]->p_ele[$1]->phydro->h[T] = mult_AQparam*$2;
            Simul->pcarac_aq->pcmsh->p_layer[nlayer]->p_ele[$1]->phydro->h[INTERPOL] = mult_AQparam*$2;
            Simul->pcarac_aq->pcmsh->p_layer[nlayer]->p_ele[$1]->phydro->h[ITER_INTERPOL] = mult_AQparam*$2;
            Simul->pcarac_aq->pcmsh->p_layer[nlayer]->p_ele[$1]->phydro->h[PIC] = mult_AQparam*$2;
            break;
        }

        case THICK:
        {
            Simul->pcarac_aq->pcmsh->p_layer[nlayer]->p_ele[$1]->phydro->Thick = mult_AQparam*$2;
            break;
        }

        if (player->type==0)
        {
            case TRANSM_HOMO:
            {
                Simul->pcarac_aq->pcmsh->p_layer[nlayer]->p_ele[$1]->phydro->ptransm=AQ_create_transm();
                //  LP_printf(Simul->poutputs," test layer nb %d transm %f \n",$1,mult_AQparam*$2);
                if($2 > EPS_T_AQ)
                {
                    Simul->pcarac_aq->pcmsh->p_layer[nlayer]->p_ele[$1]->phydro->ptransm->transm[T_HOMO]=mult_AQparam*$2;
                    Simul->pcarac_aq->pcmsh->p_layer[nlayer]->p_ele[$1]->phydro->ptransm->type=HOMO;
                }
                else
                    LP_error(Simul->poutputs,"In CaWaQS%4.2f : Error in file %s at line %d : Incoherent %s value for ele %d !\n",NVERSION_CAW,__FILE__,__LINE__,AQ_name_attributs(kindof_attribut),$1);
                break;
            }
        }

        case CONDUCT:
        {
    //la conductance est traitée de la manière suivante : si thick n'est pas renseignée (thick ==0)
    //  la conductance entrée est considérée comme une conductance à travers une éponte elle est entrée en s-1 (une valeur par element) à la face la conductance est recalculée pour gérée les différences de géométrie (multiplication par surface element puis division  par la surface de la face) elle est en s-1.

    //si thick est renseignée (Thick!=0) et que la conductance ne l'est pas elle sera automatiquement calcul\E9e \E0 partir de la transmissivit\E9 du voisin sup\E9rieur et de celle du voisin inf\E9rieur. Pour la surface, la conductance sera calcul\E9e \E0 partir du coef d'emmagasinement. cela impose une d\E9finition de la conductance au moins pour la rivi\E8re !!!

  //la conductance est alors définie comme une transmissivité verticale elle doit etre définie pour toutes les mailles du modèle. La conductance est alors calculée par définition de la transimissivité de passage (meme moyenne que pour la transmissivité) divisée par la longueur entre les centres puis multipliée par la surface de la face (elle sera fixée nulle aux faces de surface et de bases du modèle).

    //C=K_eponte/e_eponte Attention le Criv est une conductance les définitions précédentes s'appliquent donc elle pourra etre calculée en rentrant directement l'épaisseur de la zone hyporéhique ainsi que la transmissivité verticale!!!.
            Simul->pcarac_aq->pcmsh->p_layer[nlayer]->p_ele[$1]->phydro->pcond=AQ_create_cond(); //AR MM 18/10/2018 pb fontion n'existait pas AQ_create_Z_conduct_face(), cette fonction attribue maintenant la conductance pour les face et les subfaces.
    //Simul->pcarac_aq->pcmsh->p_layer[nlayer]->p_ele[$1]->phydro->pcond->conductance=$2*Simul->pcarac_aq->pcmsh->p_layer[nlayer]->p_ele[$1]->pdescr->surf; //BL  conductance en entr\E9e en s-1 = K_eponte/ep_eponte doit multiplier par surface pour calcul matriciel  init 1!!
            Simul->pcarac_aq->pcmsh->p_layer[nlayer]->p_ele[$1]->phydro->pcond->conductance=mult_AQparam*$2; //conductance en entr\E9e en s-1 = K_eponte/ep_eponte
//            LP_printf(Simul->poutputs," test layer nb %d ele nb %d stor %lf \n",nlayer,$1,Simul->pcarac_aq->pcmsh->p_layer[nlayer]->p_ele[$1]->phydro->pcond->conductance);

            break;
        }

        case STORAGE:
        {
            //LP_printf(Simul->poutputs," test layer nb %d ele nb %d stor %f \n",nlayer,$1,$2);
            Simul->pcarac_aq->pcmsh->p_layer[nlayer]->p_ele[$1]->phydro->S[SS]=mult_AQparam*$2;
            Simul->pcarac_aq->pcmsh->p_layer[nlayer]->p_ele[$1]->phydro->S[SCUR]=mult_AQparam*$2;//NF 15/3/2019 Needs to be allocated in case of CONFINED AQUIFER. This is SCUR that is used for calculations
            break;
        }
        
        // NG : if needed, allowing the multiplication on zbot and ztop for adjustements puroposes also, although it doesn't make much sense.
        case ZTOP:
        {
            Simul->pcarac_aq->pcmsh->p_layer[nlayer]->p_ele[$1]->phydro->ztop=mult_AQparam*$2; 
            break;
        }

        case ZBOT:
        {
            Simul->pcarac_aq->pcmsh->p_layer[nlayer]->p_ele[$1]->phydro->zbot=mult_AQparam*$2;
            break;
        }
    }
};

// BL To define face transmissivity as an input to finish
T_anisos : T_aniso T_anisos
| T_aniso
;

T_aniso : LEX_INT LEX_DOUBLE LEX_DOUBLE
{
  verif_ele++;
  Simul->pcarac_aq->pcmsh->p_layer[nlayer]->p_ele[$1]->phydro->ptransm=AQ_create_transm();
  Simul->pcarac_aq->pcmsh->p_layer[nlayer]->p_ele[$1]->phydro->ptransm->transm[T_X]=$2;
  Simul->pcarac_aq->pcmsh->p_layer[nlayer]->p_ele[$1]->phydro->ptransm->transm[T_Y]=$3;
  Simul->pcarac_aq->pcmsh->p_layer[nlayer]->p_ele[$1]->phydro->ptransm->type=ANI;

}
;

set_bound : intro_bound boundaries LEX_CLOSING_BRACE
{
  //AQ_print_elebound(Simul->pcarac_aq->pcmsh,Simul->poutputs); //NF 27/9/2017  to debug and compare with the one in lecture() to see if CAUCHY RIVER is OK
};

set_source : intro_source boundaries LEX_CLOSING_BRACE           // NG : 11/08/2021 : Introducing uniform recharge input option
| intro_source uniform_recharge LEX_CLOSING_BRACE
| intro_source uniform_recharge boundaries LEX_CLOSING_BRACE     // So we can mix up uniform recharge with explicit declarations of uptakes if needed

intro_bound : LEX_BOUND LEX_EQUAL LEX_OPENING_BRACE
{
  mult_AQparam = 1.0; // NG : 10/08/2021 To avoid any problem, reinitializing the multiplication coeficient for AQ boundaries so it doesn't mess up with the following ones
  LP_printf(Simul->poutputs,"Attibuting boundary conditions of layer : %d\n",nlayer);
};

intro_source : LEX_SOURCE LEX_EQUAL LEX_OPENING_BRACE
{
  mult_AQparam = 1.0;
  LP_printf(Simul->poutputs,"Attributing source inputs of layer %d :\n ",nlayer);
};

boundaries : a_boundary_type boundaries
| a_boundary_type
| multi_coef a_boundary_type
;

a_boundary_type : typeof_boundary boundary_series LEX_CLOSING_BRACE
{
  kindof_SOURCE=0;//NF 26/9/2017 WARNING typeof_boundary merge two different types of information. One concerns sources, and the other BOUNDARY CONDITIONS. kindof_SOURCE allows the switching between two functions, each related to SOURCE or BC in simple_bound. If kindof_SOURCE = O, we switch to the SOURCE fonction, otherwise to the BC function
};

typeof_boundary : LEX_TYPE_BC LEX_EQUAL LEX_OPENING_BRACE
{
  kindof = $1;
  LP_printf(Simul->poutputs,"Boundary type : %s\n",AQ_name_BC(kindof));
  kindof_SOURCE=CODE_AQ;//NF 26/9/2017 WARNING typeof_boundary merge two different types of information. One concerns sources, and the other BOUNDARY CONDITIONS. kindof_SOURCE allows the switching between two functions, each related to SOURCE or BC in simple_bound. If kindof_SOURCE = O, we switch to the SOURCE fonction, otherwise to the BC function
  idir =CODE_AQ;//Needs to be initialized for CAUCHY condition (will check face[Z_MSH][TWO] if idir = CODE_AQ, sinon pour neuman ou un CAUCHY FACE, a la face idir.
}
| LEX_TYPE_SOURCE LEX_EQUAL LEX_OPENING_BRACE
{
 kindof=$1;
 LP_printf(Simul->poutputs,"Source type : %s\n",AQ_name_source(kindof));
};

boundary_series : a_boundary boundary_series
| a_boundary
{
    idir = CODE_AQ;
};

a_boundary : intro_dir defs_bound
	     //| param_riv defs_bound//NF 26/9/2017 condition non utilisees par cawaqs L'initialisation des conductance riviere est lancee directement depuis la partie riviere. Il est donc possible de simplifier la definition des pbound de aq
| defs_bound
;

intro_dir : LEX_DIR LEX_EQUAL
{
  idir = $1;
};

defs_bound : simple_bound
{
IO_free_id_serie($1,Simul->poutputs);
}
| simple_bound LEX_QLIM LEX_EQUAL flottant
{
  s_id_io *id_list;
  id_list = IO_browse_id($1,BEGINNING_TS);
  AQ_set_qlim(Simul->pcarac_aq->pcmsh,id_list,-$4,CAUCHY,Simul->poutputs);//NF 30/9/2017 Si le qlim est defini de la surface vers le fond, alors il faut envoyer -$4. Je crois que c'est la convention de BL. En tout cas cela est coherent avec le calcul de AQ_check_qlim_top et AQ_calculate_cauchy_q de manage_boundary.c de libaq ou qpot est calcule par rapport a l'aquifere hnap-hsurf. A voir ensuite si cela est coherent lors de l'ecriture du RHS dans manage_mat.c de libaq
  IO_free_id_serie(id_list,Simul->poutputs);
}


simple_bound : LEX_OPENING_BRACE ele_ids LEX_CLOSING_BRACE time_serie
{
  s_id_io *id_list;

  id_list = IO_browse_id($2,BEGINNING_TS);
  //IO_print_id(id_list,Simul->poutputs);
  if(kindof_SOURCE!=CODE_AQ)//NF 26/9/2017 WARNING typeof_boundary merge two different types of information. One concerns sources, and the other BOUNDARY CONDITIONS. kindof_SOURCE allows the switching between two functions, each related to SOURCE or BC in simple_bound. If kindof_SOURCE = O, we switch to the SOURCE fonction, otherwise to the BC function
    {

      /*------------------- WARNING : --------------------*/
      /* il faudra multiplier par la surface de l'élément*/
      /*pour avoir les bon flux !!!                      */
      //LP_printf(Simul->poutputs,"\t-->stot = %f \n",stot);
      AQ_define_source(Simul->pcarac_aq->pcmsh,id_list,pft,kindof,stot,Simul->poutputs);
    }
  else
    {

      /*------------------- WARNING : --------------------*/
      /* il faudra multiplier par la longueur de l'élément*/
      /*pour avoir les bon flux !!!                       */
      AQ_define_bound(Simul->pcarac_aq->pcmsh,id_list,pft,idir,kindof,ltot,surf_riv,ep_riv_bed,TZ_riv_bed,Simul->poutputs);

    }
  nelebound=0;
  ltot=0;
  stot=0;
  $$=id_list;

}
;

/* NG : 11/08/2021 : Setting uniform constant recharge on all sub-surface cells of the current layer
 (ie. with no neighbour at the [Z_MSH][TWO] face) */
uniform_recharge : LEX_TYPE_SOURCE LEX_UNIFORM homo_value
{
    int type = $1;
    s_layer_msh *player = Simul->pcarac_aq->pcmsh->p_layer[nlayer];
    s_ele_msh* pele;
    s_id_io* pid=NULL;
    s_ft* pft=NULL;

    if (type != RECHARGE_AQ)
        LP_error(Simul->poutputs,"In CaWaQS%4.2f : Error in file %s at line %d : Uniform input option only available for RECHARGE input. Check your command file.\n",NVERSION_CAW,__FILE__,__LINE__);

    for(i = 0; i < player->nele; i++)
    {
        pele = player->p_ele[i];
        if (pele->loc == TOP_MSH)
        {
            pft = TS_create_function(0.,(double)$3); // Input value in m3/s at this stage
            pid = IO_create_id(nlayer,player->p_ele[i]->id[INTERN_MSH]);
            AQ_define_source(Simul->pcarac_aq->pcmsh,pid,pft,RECHARGE_AQ,pele->pdescr->surf,Simul->poutputs);  // Value set in m/s in this function for each cell
         //   LP_printf(Simul->poutputs,"Uniform recharge value for cell (%d,%d) = %10.4e m3/s.\n",nlayer,player->p_ele[i]->id[INTERN_MSH],$3); // NG check
        }
    }
    IO_free_id_serie(pid,Simul->poutputs);
    LP_printf(Simul->poutputs,"Uniform constant recharge value attributed for all-sub-surface cells of layer %d = %10.4e m3/s.\n",nlayer,$3);
};


ele_ids : ele_id ele_ids
{
    $$=IO_chain_bck_id($1,$2);
}
| ele_id
{
    $$=$1;
};

ele_id : LEX_INT
{

  $$=IO_create_id(nlayer,$1);
  nelebound ++;
  //LP_printf(Simul->poutputs,"id_source : GIS %d INTERN %d ABS %d\n",Simul->pcarac_aq->pcmsh->p_layer[nlayer]->p_ele[$1]->id[GIS_MSH],Simul->pcarac_aq->pcmsh->p_layer[nlayer]->p_ele[$1]->id[INTERN_MSH],Simul->pcarac_aq->pcmsh->p_layer[nlayer]->p_ele[$1]->id[ABS_MSH]);
  ltot+=Simul->pcarac_aq->pcmsh->p_layer[nlayer]->p_ele[$1]->pdescr->l_side;
  //LP_printf(Simul->poutputs," ele ABS %d l_side %f \n",Simul->pcarac_aq->pcmsh->p_layer[nlayer]->p_ele[$1]->id[ABS_MSH],Simul->pcarac_aq->pcmsh->p_layer[nlayer]->p_ele[$1]->pdescr->l_side);
  stot+=Simul->pcarac_aq->pcmsh->p_layer[nlayer]->p_ele[$1]->pdescr->surf;
}
;
time_serie : intro_time series LEX_CLOSING_BRACE
{
pft=TS_browse_ft($2,BEGINNING_TS);
}
;
intro_time : LEX_TIME_S LEX_EQUAL LEX_OPENING_BRACE;

series : serie series
{
  //init $$=TS_secured_chain_fwd_ts($2,$1);
$$=TS_chain_bck_ts($1,$2);
}
| serie
{
  $$=$1;
}
;

serie : flottant flottant
{
  //  LP_printf(Simul->poutputs,"Value of multiplication factor : %f\n",mult_AQparam); // NG check
   //   LP_printf(Simul->poutputs,"Value of multiplication factor : %f\n",$2); // DK check

    $$=TS_create_function((double)$1,mult_AQparam*(double)$2);
};

type_mean : LEX_MEAN LEX_EQUAL LEX_TYPE_MEAN
{
  int type_mean;
  s_layer_msh *playtmp;

  playtmp=Simul->pcarac_aq->pcmsh->p_layer[nlayer];
  playtmp->mean_type = $3;

};

type_cond : LEX_COND LEX_EQUAL LEX_TYPE_COND
{
  s_layer_msh *playtmp;

  playtmp=Simul->pcarac_aq->pcmsh->p_layer[nlayer];
  playtmp->cond_type = $3;
};

// NG : 21/04/2021
/************************************************************************************
 *                  AQUIFER DERIVATIONS SETTINGS AND DEFINITION FILE
 ************************************************************************************
 * The libaq object 'derivation' encapsulate all types of derivation
 * links : CASCADE and/or BYPASS so far (see 2.94's (and onward) user and technical guides).
 * ********************************************************************************** */

set_derivation : intro_derivation set_cascade LEX_CLOSING_BRACE
| intro_derivation set_bypass LEX_CLOSING_BRACE
| intro_derivation set_cascade set_bypass LEX_CLOSING_BRACE

intro_derivation : LEX_DERIVATION LEX_EQUAL LEX_OPENING_BRACE

/************************************************************************************
 *                                   CASCADE settings
 ************************************************************************************
 *
 * A cascade aquifer cell sends its Cauchy discharge to another AQ cell.
 * Cascade setup is part of the DERIVATION definition file. It's structured as follows :
 *
 * CASCADE = {
 * { X1 } { Y1 Z1 ; }
 * ...
 * { Xn } { Yn Zn ; }
 * }
 *
 * where
 * X : Intern ID of the emitter cell (therefore in the layer which is currently being read.)
 * Y : Intern ID of the receiving cell
 * Z : Layer ID of the receiver cell
 *
 * Receiver cell can be any AQ cell but the emitter itself ( checked by the code).
 *
 * Each emitter cell has to be previously set as a CAUCHY-type cell ( checked
 * by the code). If not, an error will be displayed.
 *
 * Reminder : all IDs start at zero !
 *
 * *********************************************************************************/

set_cascade : cascade_intro cascades LEX_CLOSING_BRACE

cascade_intro : LEX_CASCADE LEX_EQUAL LEX_OPENING_BRACE
{
	Simul->pcarac_aq->settings->cascade = YES;
};

cascades : cascade cascades
| cascade

cascade : LEX_OPENING_BRACE LEX_INT LEX_CLOSING_BRACE LEX_OPENING_BRACE LEX_INT LEX_INT LEX_SEMI_COLON LEX_CLOSING_BRACE
{
	s_ele_msh *pelefrom, *peleto;
	s_cmsh *pcmsh = Simul->pcarac_aq->pcmsh;

	pelefrom = MSH_get_ele(Simul->pcarac_aq->pcmsh,nlayer,$2,Simul->poutputs);

   if (pelefrom->phydro->emitting_status == YES)
        LP_error(Simul->poutputs,"CaWaQS%4.2f : Error in file %s at line %d : AQ Cell INT ids (%d,%d) is already set as a %s derivation emitting cell. Check your input data !\n",NVERSION_CAW,__FILE__,__LINE__,pelefrom->id[INTERN_MSH],pelefrom->player->id,AQ_derivation_type_name(CASCADE_AQ));

	peleto = MSH_get_ele(Simul->pcarac_aq->pcmsh,$6,$5,Simul->poutputs);
	AQ_init_derivation_links(pelefrom,peleto,1.,CASCADE_AQ,NULL,Simul->poutputs); // *pft remains unused in case of cascade-type links.
};

/******************************************************************************
 *                               BYPASS settings
 ******************************************************************************
 *
 * A bypass derivation link is a bit more complex to comprehend than a regular
 * Cascade-type link.
 *
 * In the case where a bypass-link is defined between two AQ cells, a given
 * fraction of the Cauchy discharge of the emitting cell can be automatically
 * reinjected in one (or more) receiving AQ cells.
 *
 * At each calculation time step, the reinjected discharge Qinj is calculated in order
 * to take into account a time serie (user defined) describing the evolution of the drainage
 * discharge which is exported (Qexport) from the hydrosystem.
 *
 * Therefore, the reinjected discharge Qinj is calculated as follows :
 *
 *              Qinj = max(Qcauchy-Qexport,0).
 *
 * The Qinj discharge can be automatically splited between several receiving AQ cell. In this case,
 * Qinj is split up equally at each time step between all the receiving cells.
 *
 * Each line of the bypass setup part (still included if the DERIVATION definition file),
 * can be defined in addition (or not) of Cascade-links, as follows :
 *
 * BYPASS = {
 * { X } { I1 L1 ; I2 L2 ; ... ; In Ln ; } time_serie = { t1 tf1 ... tn ftn }
 * ...
 * }
 *
 * where :
 * - X is the Int ID of the emitting cell from which the Cauchy discharge will be taken out.
 * - (I,L) is the list of receiving cells, identified by their respectives (INT ID,Layer ID),
 * (Reminder : ALL IDS START AT ZERO !)
 * - The TS descrbing the exported fraction of the Qcauchy discharge.
 *
 * If multiple emitting cells are directed to the same receiving cell, the total Cacuchy
 * discharge at each step is taken into account.
 *
 * *********************************************************************************/

set_bypass : bypass_intro bypasses LEX_CLOSING_BRACE

bypass_intro : LEX_BYPASS LEX_EQUAL LEX_OPENING_BRACE
{
	Simul->pcarac_aq->settings->bypass = YES;
};

bypasses : bypass bypasses
| bypass

bypass : LEX_OPENING_BRACE LEX_INT LEX_CLOSING_BRACE LEX_OPENING_BRACE aq_ids LEX_CLOSING_BRACE intro_time series LEX_CLOSING_BRACE
{
	s_id_io *ptmp;
	s_ele_msh *pelefrom, *peleto;

	pelefrom = MSH_get_ele(Simul->pcarac_aq->pcmsh,nlayer,$2,Simul->poutputs);

    if (pelefrom->phydro->emitting_status == YES)
        LP_error(Simul->poutputs,"CaWaQS%4.2f : Error in file %s at line %d : AQ Cell INT ids (%d,%d) is already set as a %s derivation emitting cell. Check your input data !\n",NVERSION_CAW,__FILE__,__LINE__,pelefrom->id[INTERN_MSH],pelefrom->player->id,AQ_derivation_type_name(BYPASS_AQ));

	ptmp = IO_browse_id($5,BEGINNING_IO);  // List of receiving cells IDs

	while (ptmp != NULL)
	{
		peleto = MSH_get_ele(Simul->pcarac_aq->pcmsh,ptmp->id_lay,ptmp->id,Simul->poutputs);
		AQ_init_derivation_links(pelefrom,peleto,(1./(float)nbypass),BYPASS_AQ,$8,Simul->poutputs);
		ptmp = ptmp->next;
	}
	nbypass=0;
};

aq_ids : aq_id aq_ids
{
$$=IO_chain_bck_id($1,$2);
}
| aq_id
{
  $$=$1;
};

aq_id : LEX_INT LEX_INT LEX_SEMI_COLON
{
    $$=IO_create_id($2,$1);
	nbypass++;
};


// NG : 29/02/2024
/****************************************************************************/
/*                       AQUIFER THRESHOLDS SETUP                           */
/*            defining thresholds on water table and/or uptakes             */
/* ------------------------------------------------------------------------ */
/*              see CaWaQS user guide (version 3.44 and after)              */
/****************************************************************************/

set_threshold : intro_threshold set_threshs LEX_CLOSING_BRACE

intro_threshold : LEX_THRESHOLD LEX_EQUAL LEX_OPENING_BRACE

set_threshs : set_thresh set_threshs
| set_thresh

set_thresh : thresh_type thresh_series LEX_CLOSING_BRACE

thresh_type : LEX_H_THRESHOLD LEX_EQUAL LEX_OPENING_BRACE
{
    mult_AQparam = 1.0; // Security resets !
    stot = 0.; 
    kindof_THRESH = $1;
    LP_printf(Simul->poutputs,"Threshold type : %s\n",AQ_threshold_type(kindof_THRESH));    
}
| LEX_UPTAKE_THRESHOLD LEX_EQUAL LEX_OPENING_BRACE
{
    mult_AQparam = 1.0; 
    stot = 0.;
    kindof_THRESH = $1;
    LP_printf(Simul->poutputs,"Threshold type : %s\n",AQ_threshold_type(kindof_THRESH)); 
};

thresh_series : thresh_serie thresh_series
| thresh_serie

thresh_serie : LEX_OPENING_BRACE id_eleaqs LEX_CLOSING_BRACE intro_time series LEX_CLOSING_BRACE 
{
    AQ_define_threshold(Simul->pcarac_aq->pcmsh,$2,$5,stot,kindof_THRESH,Simul->poutputs);
    stot = 0.; // NG 04/03/2024 : bug fix ! otherwise, cell areas keep adding up.
};

id_eleaqs : id_eleaq id_eleaqs
{
    $$ = IO_chain_bck_id($1,$2);
}
| id_eleaq
{
    $$ = $1;
};

id_eleaq : LEX_INT
{
    $$ = IO_create_id(nlayer,$1); 
    stot+=Simul->pcarac_aq->pcmsh->p_layer[nlayer]->p_ele[$1]->pdescr->surf;
};

/****************************************************************************/
/*                                  LAKE                                    */
/*            defining lake parameters nwet, id_wet data                    */
/****************************************************************************/

lake : intro_wet set_param_wet att_series_wet intro_condu_wet att_condu_wet att_read_evap intro_mto_wet att_mto_wet brace
| intro_wet set_param_wet att_series_wet intro_condu_wet att_condu_wet brace
;
intro_wet : LEX_LAKE LEX_EQUAL brace
{
 Simul->pcarac_wet=WET_create_carac_lake();
 pcarac_wet=Simul->pcarac_wet;
}
;
set_param_wet : intro_param_wet att_param_series LEX_CLOSING_BRACE
;
intro_param_wet : LEX_PARAM LEX_EQUAL LEX_OPENING_BRACE
;
att_param_series : att_param_serie att_param_series
| att_param_serie
;

att_param_serie : LEX_PARAM_ATT_WET LEX_EQUAL flottant
{
  kindof_attribut = $1;
  LP_printf(Simul->poutputs,"Attributing : %s\n",WET_name_param_attributs(kindof_attribut));

  switch(kindof_attribut){
  case WET_NUM_ATT : {
                  nwet = (int)$3;

                  pcarac_wet->p_wet = WET_ini_tab_lake(nwet,Simul->poutputs);
                  pcarac_wet->nwet = nwet;
                  ptemp_wet = WET_ini_tab_temp_lake(nwet,Simul->poutputs);
                  ptempwet = WET_ini_temp_lake();
                  ptempwet1 = NULL;
                  LP_printf(Simul->poutputs,"The number of lak is %d\n",pcarac_wet->nwet);
                  break;
  }
  case WET_EPS_ATT : {
                  eps_wet = $3;
                  pcarac_wet->eps_wet = eps_wet;
                  LP_printf(Simul->poutputs,"The eps for picard loop in lake mode is %.10f\n",pcarac_wet->eps_wet);
                  break;
 }
  case WET_NIT_ATT : {
                  nit_wet = (int)$3;
                  pcarac_wet->nit_wet = nit_wet;
                  LP_printf(Simul->poutputs,"The number of iteration for lake is %d\n",pcarac_wet->nit_wet);
                  break;
 }
  case WET_THETA_ATT : {
                  theta_wet = $3;
                  pcarac_wet->theta = theta_wet;
                  LP_printf(Simul->poutputs,"The theta of lak is %f\n",pcarac_wet->theta);
                  break;
 }
 case WET_ALPHA_ATT : {
                   alpha_z_wet = $3;
		   pcarac_wet->alpha_z_wet = alpha_z_wet;
		   LP_printf(Simul->poutputs,"The vertical anisotropy of lak is %f\n",pcarac_wet->alpha_z_wet);
	          break;
 }
}
}
;

att_series_wet : att_serie_wet att_series_wet
|att_serie_wet
;
att_serie_wet : intro_descr_wet wet_values LEX_CLOSING_BRACE
{
     if(kindof_attribut == WET_ID_ATT)
     {
        int nw , ng, e, id;
        s_condu_wet **pcondu_wet;
        s_lake_wet *pwet;
        s_geom_wet *pgeom;
        //ncondu = (int *)malloc(nwet*sizeof(int));
        //bzero((char*)ncondu,nwet*sizeof(int))
        WET_browse_temp(ptempwet,BEGINNING_WET);

        for(nw = 0; nw <nwet; nw++)
        {
           ptempwet1 = ptempwet;
           ptemp_wet[nw] = NULL;
           while(ptempwet1 != NULL)
            {
               id = ptempwet1->id_wet - 1;
               if(id == nw)
               {
                  ptempwet2 = WET_create_temp_lake(ptempwet1->id_wet,ptempwet1->name_aq,ptempwet1->id_ele);
                  ptemp_wet[nw] = WET_secured_chain_temp_fwd(ptemp_wet[nw],ptempwet2);
                }
               ptempwet1 = ptempwet1->next;
            }
           ptemp_wet[nw] = WET_browse_temp(ptemp_wet[nw],BEGINNING_WET);

         }
        WET_free_temp_serie(ptempwet,Simul->poutputs);
        WET_define_geom(pcarac_wet,Simul->pcarac_aq,ptemp_wet,Simul->poutputs);

  }

}
;

intro_descr_wet : LEX_MESH_ATT_WET LEX_EQUAL LEX_OPENING_BRACE
{
  kindof_attribut = $1;
  LP_printf(Simul->poutputs,"Attributing : %s\n",WET_name_attributs(kindof_attribut));
}
;

wet_values : wet_value wet_values
{

    if(kindof_attribut == WET_ID_ATT)
        ptempwet = WET_secured_chain_temp_bck($1,$2);

}
|wet_value
{
   if(kindof_attribut == WET_ID_ATT)
       ptempwet = $1;
}
;

wet_value : LEX_INT LEX_NAME flottant
{

  switch(kindof_attribut){
  case WET_ID_ATT : {
                   $$ = WET_create_temp_lake($1,$2,(int)$3);
                   free($2);
                   break;
  }
}
}
| LEX_INT flottant
{


  switch(kindof_attribut){
  case WET_EVAP_ATT : {
                   //double surf = 0.0;
                   //surf = Simul->pcarac_wet->p_wet[$1-1]->surf;
                   Simul->pcarac_wet->p_wet[$1-1]->phydro->q[WET_EVAP] = $2; // en m/s
                   break;
  }
  case WET_PREP_ATT : {
                   //double surf = 0.0;
                   //surf = Simul->pcarac_wet->p_wet[$1-1]->surf;
                   Simul->pcarac_wet->p_wet[$1-1]->phydro->q[WET_PRE] = $2;
                   break;
  }
  case WET_H_INI_ATT : {

                   Simul->pcarac_wet->p_wet[$1-1]->phydro->h[INI_WET] = $2;// time index??

                   Simul->pcarac_wet->p_wet[$1-1]->phydro->h[T_WET] = $2;

                   Simul->pcarac_wet->p_wet[$1-1]->phydro->h[ITER_WET] = $2;
                   break;
  }
  case WET_Z_MAX_ATT : {
                   Simul->pcarac_wet->p_wet[$1-1]->phydro->Z_max = $2;
                   break;
  }
  case WET_Z_MIN_ATT : {
                   Simul->pcarac_wet->p_wet[$1-1]->phydro->Z_min = $2;
                   break;
  }
}
}
;
intro_condu_wet : LEX_WET_COND LEX_EQUAL LEX_OPENING_BRACE
{
   int nw, ng,nele_wet; //initialize phydro->pcondu_wet
   s_condu_wet **pcondu_wet;
   s_lake_wet *pwet;
   s_geom_wet *pgeom;
   ncondu = (int *)malloc(nwet*sizeof(int));//for conductances
   bzero((char*)ncondu,nwet*sizeof(int));
   for(nw = 0; nw < nwet; nw++)
   {
      nele_wet = 0;
      pwet = Simul->pcarac_wet->p_wet[nw];
      ncondu[nw] = 0; //for conductances
      for(ng = 0; ng < pwet->ngeom; ng++)
      {
         pgeom = Simul->pcarac_wet->p_wet[nw]->p_geom[ng];
         nele_wet += pgeom->nele;
      }
      pwet->phydro->pcondu_wet = WET_ini_tab_condu(nele_wet,Simul->poutputs);

    }
  LP_printf(Simul->poutputs,"Attributing : Condutance for lake elements\n");
}
;
att_condu_wet : values_condu_wet LEX_CLOSING_BRACE
{
  free(ncondu);
  ncondu=NULL;
}
;

values_condu_wet : value_condu_wet values_condu_wet
|value_condu_wet
;
value_condu_wet : LEX_INT LEX_NAME LEX_INT flottant flottant flottant flottant flottant
{

   Simul->pcarac_wet->p_wet[$1-1]->phydro->pcondu_wet[ncondu[$1-1]]->id_ele = $3;
   strcpy(Simul->pcarac_wet->p_wet[$1-1]->phydro->pcondu_wet[ncondu[$1-1]]->name_aq,$2);
   //Simul->pcarac_wet->p_wet[$1-1]->phydro->pcondu_wet[ncondu[$1-1]]->condu[V_WET] = $4; //TV 25/09/2019
   Simul->pcarac_wet->p_wet[$1-1]->phydro->pcondu_wet[ncondu[$1-1]]->condu[V_WET_N] = $4; //TV 25/09/2019
   Simul->pcarac_wet->p_wet[$1-1]->phydro->pcondu_wet[ncondu[$1-1]]->condu[V_WET_E] = $5; //TV 25/09/2019
   Simul->pcarac_wet->p_wet[$1-1]->phydro->pcondu_wet[ncondu[$1-1]]->condu[V_WET_S] = $6; //TV 25/09/2019
   Simul->pcarac_wet->p_wet[$1-1]->phydro->pcondu_wet[ncondu[$1-1]]->condu[V_WET_W] = $7; //TV 25/09/2019
   Simul->pcarac_wet->p_wet[$1-1]->phydro->pcondu_wet[ncondu[$1-1]++]->condu[H_WET_B] = $8; //TV 25/09/2019
   LP_printf(Simul->poutputs,"Lateral lake North cond = %.10f \n",Simul->pcarac_wet->p_wet[$1-1]->phydro->pcondu_wet[ncondu[$1-1]-1]->condu[V_WET_N]);
   LP_printf(Simul->poutputs,"Lateral lake East cond = %.10f \n",Simul->pcarac_wet->p_wet[$1-1]->phydro->pcondu_wet[ncondu[$1-1]-1]->condu[V_WET_E]);
   LP_printf(Simul->poutputs,"Lateral lake South cond = %.10f \n",Simul->pcarac_wet->p_wet[$1-1]->phydro->pcondu_wet[ncondu[$1-1]-1]->condu[V_WET_S]);
   LP_printf(Simul->poutputs,"Lateral lake West cond = %.10f \n",Simul->pcarac_wet->p_wet[$1-1]->phydro->pcondu_wet[ncondu[$1-1]-1]->condu[V_WET_W]);
   LP_printf(Simul->poutputs,"Bottom lake cond = %.10f \n",Simul->pcarac_wet->p_wet[$1-1]->phydro->pcondu_wet[ncondu[$1-1]-1]->condu[H_WET_B]);
   free($2);
}
;

att_read_evap : LEX_EVAP_READ LEX_EQUAL LEX_ANSWER
{
  Simul->pcarac_wet->read_evap=$3;
}
;

intro_mto_wet : LEX_MTO_WET LEX_EQUAL LEX_OPENING_BRACE att_year_init att_year_fin
{
    data_nyear = data_fin_year - data_init_year;
    for(y=0;y<data_nyear;y++)
    {
      nj += TS_nb_jour_an(data_init_year + y +1);
    }
    Simul->pcarac_wet->length_mto = nj;
    if(Simul->pset_caw->calc_state == STEADY)
        WET_ini_mto_wet(Simul->pcarac_wet, nj, Simul->poutputs);
}
;

att_year_init : LEX_DATA_INIT LEX_EQUAL LEX_INT
{
    data_init_year = $3;
    LP_printf(Simul->poutputs,"data_init_year = %d\n",data_init_year);
}
;
att_year_fin : LEX_DATA_FIN LEX_EQUAL LEX_INT
{
   data_fin_year = $3;
   LP_printf(Simul->poutputs,"data_fin_year = %d\n",data_fin_year);
}
;

att_mto_wet : LEX_DATA_MTO_WET LEX_EQUAL LEX_NAME LEX_CLOSING_BRACE
{

   WET_open_mto_file(pcarac_wet, $3,name_out_folder,folder_nb,Simul->poutputs);
}
;

/****************************************************************************/
/*                                SURFACE                                   */
/*         defining surface parameters FP,NSAT and MTO data                 */
/****************************************************************************/

surface : intro_surface mto water_balance_unit riv brace
{
  LP_printf(Simul->poutputs,"All surface inputs have been read\n");
};

intro_surface : LEX_SURFACE LEX_EQUAL brace
{Simul->pcarac_fp=FP_create_carac_fp();}
;
/******************************************************************************
 *                               MTO PART                                     *
 *****************************************************************************/

mto :  intro_mto setup_mtos read_mto_cells brace
{
    FP_check_input_setup(Simul->pcarac_fp->pinputs,FP_MTO,Simul->poutputs);    // NG : 11/06/2021 : Checking MTO input setup
    LP_printf(Simul->poutputs,"METEO data reading done.\n");
}
|intro_mto read_mto_cells brace
;

intro_mto : LEX_MTO LEX_EQUAL brace
{
  Simul->pcarac_fp->pinputs=FP_create_input();
};

setup_mtos : setup_mto setup_mtos
|setup_mto
{
   LP_printf(Simul->poutputs,"Reading mto cell\n");
}
;

setup_mto : LEX_PREF LEX_EQUAL LEX_NAME
{
  char *name_mto=FP_name_mto($1);
  sprintf(Simul->pcarac_fp->pinputs->prefix[$1],"%s",$3);
  free(name_mto);
}
|LEX_FORMAT_TYPE LEX_EQUAL LEX_FORMAT
{
  Simul->pcarac_fp->pinputs->format=$3;
}
| LEX_MTO_PATH LEX_EQUAL LEX_NAME     // NG : 11/06/2021
{
  Simul->pcarac_fp->pinputs->input_paths[FP_MTO]=$3;
};
/******************************************************************************
 *                               MTO_CELL file
 *****************************************************************************
 *contain the gis id of meteo cell
 *id_mto
 *
 */
read_mto_cells : LEX_MTO_CELL LEX_EQUAL brace read_mtos brace
{
  s_input_fp *pinputs ;
  Simul->clock->time_spent[LEC_CHR] += CHR_end_timer();//LV 3/09/2012
  CHR_begin_timer();//LV 3/09/2012
  pinputs=Simul->pcarac_fp->pinputs;
  LP_printf(Simul->poutputs,"%d mto cells have been read\n",nb_mto);
  pinputs->nb_mto=nb_mto;
  pinputs->mto=FP_tab_mto($4,nb_mto,Simul->poutputs);
  //FP_read_all_MTO(Simul->pcarac_fp,Simul->param->year[FP_INIT],Simul->poutputs); // BL to debug
  //FP_print_MTO(Simul->pcarac_fp,Simul->poutputs); //BL to debug
  Simul->clock->time_spent[TAB_CHR] += CHR_end_timer();//LV 3/09/2012
  CHR_begin_timer();//LV 3/09/2012
};

read_mtos : read_mto read_mtos
{
  $$=chain_mto($1,$2);
}
| read_mto
{ $$ =$1;}
;

read_mto : LEX_INT
{
  int i;
  s_input_fp *pinput;
  pinput=Simul->pcarac_fp->pinputs;
  pmto=FP_create_mto($1,FP_GIS);
  nb_mto++;
  $$=pmto;
}
;

/*****************************************************************************
 *                                FP PART                                    *
 *                                                                           *
 ****************************************************************************/
// NG 26/11/2019 : Introducing optional LAI sub-part
// NG 19/01/2020 : Introducing MANDATORY catchment sub-part
// NG 23/01/2020 : Introducing optional chasm sub-part
water_balance_unit :
| intro_wbu omp params bu cprod catchments cprod_noaq surf_aq brace;
| intro_wbu omp params bu cprod catchments chasm cprod_noaq surf_aq brace;
| intro_wbu params bu cprod catchments cprod_noaq surf_aq brace;
| intro_wbu params bu cprod catchments chasm cprod_noaq surf_aq brace;
| intro_wbu omp params bu cprod catchments surf_aq brace;
| intro_wbu omp params bu cprod catchments chasm surf_aq brace;
| intro_wbu params bu cprod catchments surf_aq brace;
| intro_wbu params bu cprod catchments chasm surf_aq brace;
| intro_wbu omp params bu cprod catchments surf_aq cprod_noaq rsv brace;
| intro_wbu omp params bu cprod catchments chasm surf_aq cprod_noaq rsv brace;
| intro_wbu params bu cprod catchments surf_aq cprod_noaq rsv brace;
| intro_wbu params bu cprod catchments chasm surf_aq cprod_noaq rsv brace;
| intro_wbu omp params bu cprod catchments cprod_noaq  brace;
| intro_wbu omp params bu cprod catchments chasm cprod_noaq  brace;
| intro_wbu params bu cprod catchments cprod_noaq brace;
| intro_wbu params bu cprod catchments chasm cprod_noaq brace;
| intro_wbu omp params bu cprod catchments brace;
| intro_wbu omp params bu cprod catchments chasm brace;
| intro_wbu params bu cprod catchments brace;
| intro_wbu params bu cprod catchments chasm brace;
| intro_wbu omp params lai bu cprod catchments cprod_noaq surf_aq brace;
| intro_wbu omp params lai bu cprod catchments chasm cprod_noaq surf_aq brace;
| intro_wbu omp params lai bu cprod catchments surf_aq brace;
| intro_wbu omp params lai bu cprod catchments chasm surf_aq brace;
| intro_wbu omp params lai bu cprod catchments surf_aq cprod_noaq rsv brace;
| intro_wbu omp params lai bu cprod catchments chasm surf_aq cprod_noaq rsv brace;
| intro_wbu omp params lai bu cprod catchments cprod_noaq  brace;
| intro_wbu omp params lai bu cprod catchments chasm cprod_noaq  brace;
| intro_wbu omp params lai bu cprod catchments brace;
| intro_wbu omp params lai bu cprod catchments chasm brace;
| intro_wbu params lai bu cprod catchments cprod_noaq surf_aq brace;
| intro_wbu params lai bu cprod catchments chasm cprod_noaq surf_aq brace;
| intro_wbu params lai bu cprod catchments surf_aq brace;
| intro_wbu params lai bu cprod catchments chasm surf_aq brace;
| intro_wbu params lai bu cprod catchments surf_aq cprod_noaq rsv brace;
| intro_wbu params lai bu cprod catchments chasm surf_aq cprod_noaq rsv brace;
| intro_wbu params lai bu cprod catchments cprod_noaq brace;
| intro_wbu params lai bu cprod catchments chasm cprod_noaq brace;
| intro_wbu params lai bu cprod catchments brace;
| intro_wbu params lai bu cprod catchments chasm brace;

intro_wbu : LEX_WBU LEX_EQUAL brace
{
  module=FP_CAW;
};

/******************************************************************************
 *                               FP param FILE
 *****************************************************************************
 *contains :
 * - name of the FP
 * - CRT parameter
 * - DCRT parameter
 * - FN parameter
 * - CQRMAX parmater
 * - CQR parameter
 * - CQIMAX parameter
 * - CQI parameter
 * Optional parameters :
 * - LCRT parameter (in order to calculate : CRT(t) = CRT + LCRT*LAI(t))
 * - LDCR parameter (in order to calculate = DCRT(t) = DCRT + LDCR*LAI(t))
 *
 */
params : LEX_PARAM LEX_EQUAL brace read_params brace
{
  s_carac_fp *pcarac_fp;
  Simul->clock->time_spent[LEC_CHR] += CHR_end_timer();//LV 3/09/2012
  CHR_begin_timer();//LV 3/09/2012
  pcarac_fp=Simul->pcarac_fp;
  pcarac_fp->nb_FP=nb_param;
  pcarac_fp->p_param_fp=FP_tab_param($4,nb_param);
  LP_printf(Simul->poutputs,"%d surface parameter have been read\n",nb_param);
  Simul->clock->time_spent[TAB_CHR] += CHR_end_timer();//LV 3/09/2012
  CHR_begin_timer();//LV 3/09/2012
}
;

read_params : read_param read_params
{
  $$=FP_secured_chain_param_fwd($1,$2);
}
|read_param
{
  $$=$1;
}
;


read_param : LEX_NAME flottant flottant flottant flottant flottant flottant flottant flottant flottant // Most common syntax case
{
  param=FP_create_param(nb_param,$1);
  //LP_printf(Simul->poutputs," Reading %s ,parameter nb %d\n",param->name,param->id); // BL to debug
  FP_fill_param(param,$2,$3,$5,$6,$7,$8,$9,LCRT_DEF_FP,LDCR_DEF_FP,NO);
  nb_param ++;
  $$=param;
}
// NG : 26/11/2019 : New input format including optional LCRT and LDCR parameters
| LEX_NAME flottant flottant flottant flottant flottant flottant flottant flottant flottant flottant flottant
{
  param=FP_create_param(nb_param,$1);
  FP_fill_param(param,$2,$3,$5,$6,$7,$8,$9,$11,$12,NO);
  nb_param ++;
  $$=param;
}
// NG : 05/12/2023 : Common syntax including optional YES/NO parameter to reroute FP bucket overflow to another one. 
| LEX_NAME flottant flottant flottant flottant flottant flottant flottant flottant flottant LEX_ANSWER
{
  param=FP_create_param(nb_param,$1);
  FP_fill_param(param,$2,$3,$5,$6,$7,$8,$9,LCRT_DEF_FP,LDCR_DEF_FP,$11);
  nb_param ++;
  $$=param;
}
;

// NG : 26/11/2019 :: Optional LAI part
/******************************************************************************
 *                               LAI set-up
 *****************************************************************************/


lai : intro_lai setup_lais brace
{
    FP_check_input_setup(Simul->pcarac_fp->pinputs,FP_LAI,Simul->poutputs); // NG : 11/06/2021
    LP_printf(Simul->poutputs,"LAI data reading done.\n");
};

intro_lai : LEX_LAI LEX_EQUAL brace
{
  Simul->pcarac_fp->input_lai = $1;
};

setup_lais : setup_lai setup_lais
| setup_lai;

setup_lai : LEX_FORMAT_TYPE LEX_EQUAL LEX_FORMAT
{
  Simul->pcarac_fp->pinputs->format_lai=$3;
}
| LEX_PREF LEX_EQUAL LEX_NAME
{
  Simul->pcarac_fp->pinputs->prefix_lai = $3;
}
| LEX_LAI_PATH LEX_EQUAL LEX_NAME
{
  Simul->pcarac_fp->pinputs->input_paths[FP_LAI] = $3;
}
;

/******************************************************************************
 *                    Water Balance calculation unit FILE
 *****************************************************************************
 *
 * This file is constructed on GIS program by intersecting FP spatial
 * distribution file and MTO cell spatial distribution file.
 * The intersection of the two geometries define the water Balance calculation
 * Unit (BU)
 *
 * it contains :
 * - id FP (FP row number in param FP)
 * - id mto
 * - intersection Area between the two objects (m2)
 *
 * *****************************************************************************/

bu : LEX_BU LEX_EQUAL brace read_bus brace
{
  s_carac_fp *pcarac_fp;
  Simul->clock->time_spent[LEC_CHR] += CHR_end_timer();//LV 3/09/2012
  CHR_begin_timer();//LV 3/09/2012
  pcarac_fp=Simul->pcarac_fp;
  pcarac_fp->nb_BU=nb_bu;
  LP_printf(Simul->poutputs,"%d water balance calculation units have been read\n",nb_bu);
  pcarac_fp->p_bu=FP_tab_BU($4,nb_bu,Simul->poutputs);
  FP_finalize_link_mto(pcarac_fp,Simul->poutputs);

  // FP_print_BU( pcarac_fp,Simul->poutputs);//BL to debug
  //SPA_print_ids(pcarac_fp->p_bu[0]->pmto->link,Simul->poutputs); //BL to debug
  Simul->clock->time_spent[TAB_CHR] += CHR_end_timer();//LV 3/09/2012
  CHR_begin_timer();//LV 3/09/2012
};

read_bus : read_bu read_bus
{
  $$=FP_secured_chain_bu_fwd($1,$2);
}
|read_bu
{$$=$1;};

read_bu : LEX_INT LEX_INT flottant
{
  s_carac_fp *pcarac_fp;
  pcarac_fp=Simul->pcarac_fp;
  pbu=FP_define_bal_unit(pcarac_fp,nb_bu,$1,$2,$3,Simul->poutputs);
  //LP_printf(Simul->poutputs,"Reading water balance calculation unit : %d linked with mto GIS %d INTERN %d \n surface of bu %f \n",pbu->id,pbu->pmto->id[FP_GIS],pbu->pmto->id[FP_INTERN],pbu->area); //BL to debug
  nb_bu ++;
  $$=pbu;
}
;
/******************************************************************************
 *                      Production cell FILE
 *****************************************************************************
 *
 *  This file is constructed on GIS programm by intersecting the BU geometry
 * file with the surface cell geometry. Usually the watershed on which the
 * hydraulic network is defined WARNING : the production cells are created
 * in a much faster way if the file is ordered by the cprod id !!
 *
 *
 * contains :
 * - id_gis of cprod
 * - intern id of bu (water balance calculation units) ie : row number in BU file
 * - intersection Area between the two objects (m2)
 * WARNING the prodution cell file also HAVE TO reference the data over non aquifer area !!
 *
 ******************************************************************************/

cprod : LEX_CPROD LEX_EQUAL brace read_cprods brace
{
  s_carac_fp *pcarac_fp;
  Simul->clock->time_spent[LEC_CHR] += CHR_end_timer();//LV 3/09/2012
  CHR_begin_timer();//LV 3/09/2012
  pcarac_fp=Simul->pcarac_fp;
  pcarac_fp->nb_cprod=nb_cprod;
  LP_printf(Simul->poutputs,"%d surface cells have been read \n",pcarac_fp->nb_cprod);

  pcarac_fp->p_cprod=FP_tab_cprod($4,pcarac_fp->nb_cprod,Simul->poutputs);
  FP_finalize_link_cp(pcarac_fp,BU_FP,Simul->poutputs);
 // FP_print_cprod($4,BU_FP,Simul->poutputs); // BL to debug
   Simul->clock->time_spent[TAB_CHR] += CHR_end_timer();//LV 3/09/2012
  CHR_begin_timer();//LV 3/09/2012
};

read_cprods : read_cprod read_cprods
{
  s_cprod_fp *cprod_tmp;

  cprod_tmp=FP_check_cprod($1,$2,BU_FP,Simul->poutputs);
  if(cprod_tmp!=NULL)
    {
           ++nb_cprod;

    }
  $$=FP_secured_chain_cprod_fp(cprod_tmp,$2);
}
|read_cprod
{
  $$=$1;
}
;

read_cprod : LEX_INT LEX_INT flottant
{
  s_cprod_fp *pcprod;
  pcprod=FP_create_cprod($1,FP_GIS);
  pcprod->link[BU_FP]=SPA_create_ids($2-1,$3);
  pcprod->area=$3;

  $$=pcprod;

};


// NG : 19/01/2020
/******************************************************************************
 *                            CATCHMENTS SECTION
 *****************************************************************************
 *
 * This section characterizes catchments (i.e. sub-basins) properties within the
 * surface model area (concentration times, names, etc.). A catchment is defined
 * as a predetermined set of cprod cells.
 *
 * For each catchment, 4 éléments need to be defined : name, GIS_ID, concentration time
 * and the cprod list within the catchment.
 *
 ******************************************************************************/

catchments : catchments_intro catchments_settings LEX_CLOSING_BRACE
{
  Simul->clock->time_spent[LEC_CHR] += CHR_end_timer();
  Simul->pcarac_fp->nb_catchment = catchment_counter;
  Simul->pcarac_fp->p_catchment=$2;
//  FP_print_catchments_properties_from_chain(Simul->pcarac_fp->p_catchment, Simul->poutputs, BEGINNING_IO); // NG check
//  FP_check_cprod_catchment_connexion(Simul->pcarac_fp,Simul->poutputs); // NG check
  if(Simul->pcarac_fp->nb_catchment > 1) FP_check_catchment_duplicate_ids($2,Simul->poutputs);
  CHR_begin_timer();
};


catchments_intro : LEX_INTRO_CATCHMENTS LEX_EQUAL LEX_OPENING_BRACE

catchments_settings : catchments_setting catchments_settings
{
  $$=FP_secured_chain_catchment_fwd($1,$2);
};
| catchments_setting
{
  $$=$1;
};


catchments_setting : LEX_OPENING_BRACE LEX_INT LEX_NAME LEX_TC LEX_EQUAL mesure catchment_cells LEX_CLOSING_BRACE
{
  s_catchment_fp *pcatch = NULL;
  s_carac_fp* p_carac_fp;
  s_cprod_fp* p_cprod_st = NULL;
  s_id_io* list_cells = NULL;
  p_carac_fp=Simul->pcarac_fp;

  int nb_cprod_catchment = 0, nb_cprod_tot = 0;
  int i, count_pos = 0;
  double surface = 0.;

  nb_cprod_catchment = IO_length_id($7);

  // FP_create_catchment alloue le tableau pour stocker les pointeurs vers les cprod du catchment
  pcatch = FP_create_catchment($2,nb_cprod_catchment,p_carac_fp);
  pcatch->catchment_name = $3;
  pcatch->concentration_time = $6;

  // Stockage des pointeurs vers cprod du bassin
  nb_cprod_tot = p_carac_fp->nb_cprod;

  if ($7 == NULL) LP_printf(Simul->poutputs,"Cprod IDs list is null. Check your catchment cell file for catchment named %s.\n",pcatch->catchment_name);
  list_cells = IO_browse_id($7,BEGINNING_IO);

  while (list_cells != NULL){
      for (i = 0; i < nb_cprod_tot; i++){
        if (p_carac_fp->p_cprod[i]->id[FP_GIS] == list_cells->id) {
            p_cprod_st = p_carac_fp->p_cprod[i];
            surface += p_cprod_st->area;
            break;
        }
     }
     if (p_cprod_st == NULL) LP_error(Simul->poutputs,"Surface cell with GIS_ID %d in catchment GIS ID %d doesn't exist in Cprod input file.\n",list_cells->id,pcatch->id_gis);

	//On affecte la cprod a son bassin en vérifiant qu'elle n'appartient pas déja (par erreur de l'user) à un autre bassin
	if (p_cprod_st->p_catch == NULL)
		p_cprod_st->p_catch = pcatch;
	else
		LP_error(Simul->poutputs,"Surface Cprod cell GIS ID %d can't be linked with catchment GIS ID %d as it's already linked to the catchment GIS ID %d.\n",p_cprod_st->id[FP_GIS],pcatch->id_gis,p_cprod_st->p_catch->id_gis);

	pcatch->cprod_catchment[count_pos] = p_cprod_st;
	list_cells = list_cells->next;
	count_pos++;
	}

  pcatch->area = surface;
  catchment_counter++;

  $$ = pcatch;

};

/* *******************************************
 *
 * The LIST_CPROD FILE only list, in one unique column
 * the set of Cprod (using their GIS ID)
 * which are part of the catchment.
 *
**************************************************** */

catchment_cells : catchment_cell catchment_cells
{
   $$ = IO_secured_chain_fwd_id($2,$1);
};
| catchment_cell
{ $$ = $1;};

catchment_cell : LEX_INT
{
   s_id_io* pid = NULL;
   pid = IO_create_id(0,$1);
   $$ = pid;
};


// NG : 23/01/2020
/******************************************************************************
 *                            CHASM SECTION
 *****************************************************************************
 *
 * In this optional section, the user defines a set of chasms as a list of two IDs :
 *      - the GIS id of the chasm
 *      - the GIS id of the Cprod cell
 * A chasm is defined as one unique cprod cell with no river reach connexion.
 * For each chasm listed, a catchment is created (catchment type = FP_CATCH_CHASM)
 *
 ******************************************************************************/

chasm : chasm_intro read_chasms LEX_CLOSING_BRACE
{
  s_carac_fp* pcarac = Simul->pcarac_fp;
  s_catchment_fp* pcatch = Simul->pcarac_fp->p_catchment;
  Simul->pcarac_fp->nb_catchment = catchment_counter;                                          // On réactualise le nombre total de catchment !
  pcatch = FP_browse_catchment_chain(Simul->pcarac_fp->p_catchment, END_IO, Simul->poutputs);  // On chaine les catchment CHASMS avec les catchments pré-éxistants
  pcatch = FP_chain_catchment(pcatch, $2);
// FP_print_catchments_properties_from_chain(Simul->pcarac_fp->p_catchment, Simul->poutputs, BEGINNING_IO); // NG check
// FP_check_cprod_catchment_connexion(Simul->pcarac_fp,Simul->poutputs);    // NG check
  if(Simul->pcarac_fp->nb_catchment > 1) FP_check_catchment_duplicate_ids(pcatch,Simul->poutputs);
};


chasm_intro : LEX_CHASM_SURF LEX_EQUAL LEX_OPENING_BRACE

read_chasms : read_chasm read_chasms
{
  $$=FP_secured_chain_catchment_fwd($1,$2);
}
| read_chasm
{ $$=$1; }
;

read_chasm : LEX_INT LEX_INT
{
  s_cprod_fp* pcprod = NULL;
  int nb_cprod_tot = Simul->pcarac_fp->nb_cprod;
  int i;
  char *catch_name = NULL;
  s_catchment_fp* pcatch = NULL;
  int nb_cprod_catchment = 1;

  // On check que la cprod ciblée existe bien et on récupère son pointeur
  for (i = 0; i < nb_cprod_tot; i++){
     if (Simul->pcarac_fp->p_cprod[i]->id[FP_GIS] == $2){
           pcprod = Simul->pcarac_fp->p_cprod[i];
           break;
     }
  }
  if (pcprod == NULL) LP_error(Simul->poutputs,"Cprod GIS ID %d doesn't exist. Therefore, cannot be defined as a chasm-type catchment.\n",$1);

   // NG : 24/03/2020 : On vérifie que la cprod a déclarer comme sous-bv chasm n'appartient pas déja (par erreur de l'user) à un autre bassin (soit un chasm déja déclaré, soit comme partie intégrante d'un sous-bassin rivière déja déclaré)
   if (pcprod->p_catch != NULL)
          LP_error(Simul->poutputs,"Cprod cell GIS ID %d can't be declared as a chasm-type catchment as it's already part of the catchment GIS ID %d.\n",pcprod->id[FP_GIS],pcprod->p_catch->id_gis);

  pcatch = FP_create_catchment($1,nb_cprod_catchment,Simul->pcarac_fp);

  catch_name = (char*)malloc(ALLOCSCD_LP*sizeof(char));
  bzero(catch_name,ALLOCSCD_LP*sizeof(char));

  pcprod->cell_prod_type = FP_CATCH_CHASM;

  sprintf(catch_name,"Chasm_%d",$1);
  pcatch->catchment_name = catch_name;
  pcatch->area=pcprod->area;
  pcatch->catchment_type = FP_CATCH_CHASM;
  pcatch->pcprod_outlet = pcprod;
  pcatch->pcprod_outlet->cell_prod_type = FP_CATCH_CHASM;
  pcatch->cprod_catchment[0] = pcprod;
  pcprod->p_catch=pcatch;  // On lie le cprod chasm a son catchment

  catchment_counter++;
  $$ = pcatch;
};

/******************************************************************************
 *                      NO_AQ_SURF FILE
 *****************************************************************************
 *
 * This file is constructed on GIS by intersecting the BU geometry
 * file with the surface cell geometry which is not connected with aquifer.
 * Usually the watershed on which the hydraulic network is defined.
 *
 * If this input file is not defined, the infiltrated water where no aquifer
 * system is defined won't be routed to the river system.
 *
 *
 * contains :
 * - id_gis of cprod
 * - intern id of bu (water balance calculation units) ie : row number in BU file
 * - intersection Area between the two objects (m2)
 *
 ******************************************************************************/

cprod_noaq : LEX_CPROD_NOAQ LEX_EQUAL brace noaqs brace
{

};

noaqs : noaq noaqs
|noaq
;

noaq : LEX_INT LEX_INT flottant
{
  s_cprod_fp *pcprod;
  FP_create_noaq_cprod(Simul->pcarac_fp,$1,$2-1,$3,Simul->poutputs);

};

/******************************************************************************
 *                      RSV FILE
 *****************************************************************************
 *
 * This file contains the id_gis of the cell prod and the reservoir parameters
 *
 *
 * contains :
 * - id_gis of cprod
 * - double heigth of reservoir
 * - double Q_out
 * - double bond water
 *
 ******************************************************************************/
rsv : LEX_RSV LEX_EQUAL brace def_rsvs brace
{};

def_rsvs : def_rsv def_rsvs
|def_rsv
;

def_rsv : LEX_INT flottant flottant flottant
{
  s_cprod_fp *pcprod;
  pcprod=FP_get_cprod(Simul->pcarac_fp,$1,FP_GIS,Simul->poutputs);
  if(pcprod->prsv != NULL)
    {
      pcprod->prsv=RSV_free_rsv(pcprod->prsv);
      pcprod->prsv=RSV_create_reservoir($2,$3,$4);

    }
  else
    pcprod->prsv=RSV_create_reservoir($2,$3,$4);

};

/*****************************************************************************
 *                             CPROD_AQ File
 *****************************************************************************
 *
 * This file is construted by intersecting the spatial distribution of the cprod file (Surfaces cells) and the TOP_layers cells of the aquifer model
 *
 * contains :
 * - integer : GIS id of surface cells (here BV_id)
 * - integer : gis id of the aquifer element (WARNING the GIS id has to be
 * created by "$id +1" on QGIS)
 * - integer : layer number (from 0 to nlayer-1)
 * - double : intersection Area between the two objects (m2)
 * - double : the  surface elevation (m)
 ******************************************************************************/
surf_aq : LEX_FPAQ LEX_EQUAL brace read_fp_aqs brace
{
  LP_printf(Simul->poutputs," The link between surface and Aquifer defined\n");
};

read_fp_aqs : read_fp_aq read_fp_aqs
|read_fp_aq
;

read_fp_aq : LEX_INT LEX_INT LEX_INT flottant flottant
{
  s_carac_fp *pcarac_fp;
  s_cprod_fp *pcprod;
  s_carac_aq *pcarac_aq;
  s_ele_msh *pele;
  int id_ele;
  int id_cprod;
  pcarac_fp=Simul->pcarac_fp;
  pcprod=FP_get_cprod(pcarac_fp,$1,FP_GIS,Simul->poutputs);
  id_cprod=pcprod->id[FP_INTERN];
  pcarac_aq=Simul->pcarac_aq;
  pele=pcarac_aq->pcmsh->p_layer[$3]->p_ele[$2-1];
  id_ele=pele->id[ABS_MSH];
  FP_init_cprod(pcprod,id_ele,$4/pele->pdescr->surf,AQ_FP,Simul->poutputs);
  AQ_init_link_ele(pele,id_cprod,$4/pele->pdescr->surf,SURF_AQ,Simul->poutputs);
  // AQ_define_cauchy_coupled(Simul->pcarac_aq,pele,$5,0.,0.,(double)CODE_TS,Simul->poutputs);//NF 27/9/2017 Je pense que cela met par defaut toute la surface en cauchy alors que ce n'est pas forcement judicieux. Je prefere mettre en cauchy toutes les mailles de surface non CL, une fois CAUCHY lu. JE supprime cette ligne!
};

/*****************************************************************************
 *                           NONSAT PART                                     *
 ****************************************************************************/

nonsat : intro_nonsat nsat_sets param_nonsat nsat nsat_prod nsat_aq brace;
|intro_nonsat nsat_sets param_nonsat nsat nsat_prod nsat_chasm nsat_aq brace;
|intro_nonsat param_nonsat nsat nsat_prod nsat_aq brace;
|intro_nonsat param_nonsat nsat nsat_prod nsat_chasm nsat_aq brace;
|intro_nonsat param_nonsat nsat nsat_prod brace;
|intro_nonsat param_nonsat nsat nsat_prod nsat_chasm brace;
|intro_nonsat param_nonsat nsat nsat_prod init_from_file brace;
|intro_nonsat param_nonsat nsat nsat_prod nsat_chasm init_from_file brace;
|intro_nonsat param_nonsat nsat nsat_prod init_from_file nsat_aq brace;
|intro_nonsat param_nonsat nsat nsat_prod nsat_chasm init_from_file nsat_aq brace;
|intro_nonsat nsat_sets param_nonsat nsat nsat_prod brace;
|intro_nonsat nsat_sets param_nonsat nsat nsat_prod nsat_chasm brace;
|intro_nonsat nsat_sets param_nonsat nsat nsat_prod init_from_file brace;
|intro_nonsat nsat_sets param_nonsat nsat nsat_prod nsat_chasm init_from_file brace;
|intro_nonsat nsat_sets param_nonsat nsat nsat_prod init_from_file nsat_aq brace;
|intro_nonsat nsat_sets param_nonsat nsat nsat_prod nsat_chasm init_from_file nsat_aq brace;

intro_nonsat : LEX_NONSAT LEX_EQUAL brace
{
  Simul->pcarac_zns=NSAT_create_carac_zns();
  module=NSAT_CAW;
};

/******************************************************************************
 *                      NONSAT settings (OMP, dynamic behavior)
 ***************************************************************************** */

nsat_sets : nsat_set nsat_sets
| nsat_set

nsat_set : omp
| dynnsat_sets

dynnsat_sets : dynnsat_intro dyn_sets LEX_CLOSING_BRACE
{
    LP_printf(Simul->poutputs,"Dynamic unsaturated zone activation status : %s\n", LP_answer(Simul->pcarac_zns->dynzns_io,Simul->poutputs));
    if (Simul->pcarac_zns->dynzns_io == YES)
        LP_printf(Simul->poutputs,"Update time step : %f sec. Reservoir update method : %s\n", Simul->pcarac_zns->dyn_refresh_dt,NSAT_nres_update_method (Simul->pcarac_zns->nres_method));       
}

dynnsat_intro : LEX_DYNAMIC_NSAT LEX_EQUAL LEX_OPENING_BRACE
{
    Simul->pcarac_zns->dynzns_io = YES;
};

dyn_sets: dyn_set dyn_sets
| dyn_set

dyn_set : LEX_UPDATE_DYNNSAT LEX_EQUAL mesure
{
    Simul->pcarac_zns->dyn_refresh_dt = $3;
}
| LEX_DYNNSAT_METHOD LEX_EQUAL LEX_DYNSAT_TYPE
{
    Simul->pcarac_zns->nres_method = $3;
}
| LEX_ANSWER 
{
    Simul->pcarac_zns->dynzns_io = $1;  // Optional anwser : if not declared, it will be ON (cf. dynnsat_intro rule) => The option can be turned off by just saying NO, while keeping the subblock in the COMM.
};
  
  
/******************************************************************************
 *                      Param Nonsat File
 *****************************************************************************
 *contains :
 * - integer : the buckets number in the zns
 * - double : the high of the buckets
 * - double : the discharge ratio at the output of the bucket
 * - double : (optional) the bound water volume of the reservoir
 * - double : (optional) the initial water volume in the buckets
 * - double : (optional) the initial transport variable (concentration) to 
 * associate with the initial water volume.
 */
param_nonsat : LEX_PARAM LEX_EQUAL brace read_params_zns brace
{
  s_carac_zns *pcarac_zns;
  Simul->clock->time_spent[LEC_CHR] += CHR_end_timer();//LV 3/09/2012
  CHR_begin_timer();//LV 3/09/2012
  pcarac_zns=Simul->pcarac_zns;
  pcarac_zns->nb_nash=nb_param_nsat;
  pcarac_zns->p_nash=NSAT_tab_nash($4,nb_param_nsat);
  LP_printf(Simul->poutputs,"%d ZNS parameter have been read\n",nb_param_nsat);
  //NSAT_print_all_nash(pcarac_zns,Simul->poutputs); // BL to debug
  Simul->clock->time_spent[TAB_CHR] += CHR_end_timer();//LV 3/09/2012
  CHR_begin_timer();//LV 3/09/2012
};
read_params_zns : read_param_zns read_params_zns
{
  $$=NSAT_secured_chain_nash_fwd($1,$2);
}
|read_param_zns
{
  $$=$1;
}
;

read_param_zns : LEX_INT LEX_DOUBLE LEX_DOUBLE LEX_DOUBLE LEX_DOUBLE LEX_DOUBLE
{
	nash=NSAT_create_nash(nb_param_nsat);
	//LP_printf(Simul->poutputs," Reading %s ,parameter nb %d\n",param->name,param->id); // BL to debug
	NSAT_fill_nash(nash,$1,$2,$3,$4,$5,$6); // NG : 24/11/2024 : Now including initial transport variable directly from the input file
	nb_param_nsat ++;
	$$=nash;
}
|LEX_INT LEX_DOUBLE LEX_DOUBLE 
{

	nash=NSAT_create_nash(nb_param_nsat);
	//LP_printf(Simul->poutputs," Reading %s ,parameter nb %d\n",param->name,param->id); // BL to debug
	NSAT_fill_nash(nash,$1,$2,$3,0.0,0.0,0.0);
 	nb_param_nsat ++;
	$$=nash;
}
;
/******************************************************************************
 *                      Nsat unit File
 *****************************************************************************
 *
 * This file is the spatial repartition of the Nsat parameters
 *
 * contains :
 * - integer : intern id of the param nsat (row num of the file param nsat)
 *
 *
 ******************************************************************************/
nsat : LEX_NSAT LEX_EQUAL brace nsat_units brace
{
  s_carac_zns *pcarac_zns;
  pcarac_zns=Simul->pcarac_zns;
  pcarac_zns->nb_zns=nb_zns;
  pcarac_zns->p_zns=NSAT_tab_zns($4,pcarac_zns->nb_zns,Simul->poutputs);

  LP_printf(Simul->poutputs,"%d zns cells have been read \n",pcarac_zns->nb_zns);
};

nsat_units : nsat_unit nsat_units
{
  $$=NSAT_secured_chain_zns_fwd($1,$2);
}
|nsat_unit
{$$=$1;};

nsat_unit : LEX_INT
{
  s_carac_zns *pcarac_zns;
  pcarac_zns=Simul->pcarac_zns;
  pzns=NSAT_create_zns(nb_zns,pcarac_zns->p_nash[$1-1]);
  nb_zns++;
  
  /* NG : 23/06/2023 : If dynamic behavior is activated in COMM, propagated by default to all ZNS cells.
  Later on, it will eventually be turned off if some necessary data is missing (ex : no link between NSAT and AQ) */
  if (Simul->pcarac_zns->dynzns_io == YES) pzns->dyn_io = YES;
    
  $$=pzns;
};
/*****************************************************************************
 *                      Nsat_BU File
 *****************************************************************************
 *
 * This file is construted by intersecting the spatial distribution of the Nsat
 * parameters and the spatial distribution of the BU
 *
 * contains :
 * - integer : intern id of nsat units (row num of the file nsat unit)
 * - integer : intern id of the water balance unit (row num of BU file)
 * - double : intersection Area between the two objects (m2)
 ******************************************************************************/
nsat_prod : LEX_NSPROD LEX_EQUAL brace read_nsat_prods brace
{
  s_carac_zns *pcarac_zns;
  pcarac_zns=Simul->pcarac_zns;
  NSAT_finalize_link_zns(pcarac_zns,SURF_NS,Simul->poutputs);
  NSAT_create_all_cascade(Simul->pcarac_zns,Simul->poutputs);

  //NSAT_print_zns(pcarac_zns,Simul->poutputs); // BL to debug
};

read_nsat_prods : read_nsat_prod read_nsat_prods
|read_nsat_prod
;

read_nsat_prod : LEX_INT LEX_INT flottant
{
  s_carac_zns *pcarac_zns;
  s_zns *pzns;
  pcarac_zns=Simul->pcarac_zns;
  pzns=pcarac_zns->p_zns[$1-1];
  NSAT_init_zns(pzns,$2-1,$3,SURF_NS,Simul->poutputs);
  pzns->area+=$3;

};


/*****************************************************************************
 *                             Nsat_AQ File
 *****************************************************************************
 *
 * This file is construted by intersecting the spatial distribution of the Nsat
 * parameters and the TOP_layers cells of the aquifer model
 * ATTENTION : POUR ACCELERER LA PROCEDURE MIEUX VAUT CLASSER LE FICHIER PAR ORDRE CROISSANT DE ID_LAYER PUIS GIS ID AQUIFER
 * contains :
 * - integer : intern id of nsat units (row num of the file nsat unit)
 * - integer : gis id of the aquifer element (WARNING the GIS id has to be
 * created by "$id+1" on QGIS)
 * - integer : layer number (from 0 to nlayer-1)
 * - double : intersection Area between the two objects (m2)
 ******************************************************************************/
nsat_aq : intro_nsat_aq brace read_nsat_aqs brace
{
  s_ft *pft;
  s_id_io *id_list;

  id_list=$3;
  //IO_print_id(id_list,Simul->poutputs); // BL to debug
  AQ_define_recharge_coupled(Simul->pcarac_aq->pcmsh,id_list,ZNS_SOURCE_AQ,Simul->poutputs); 
  IO_free_id_serie(id_list,Simul->poutputs);
};

// NG : 03/03/2020
intro_nsat_aq : LEX_NSAQ LEX_EQUAL
{
  if (Simul->pcarac_aq == NULL) LP_error(Simul->poutputs,"Cannot link NSAT module to AQ module. The latter has not been declared. Check your command file.\n");
}
;

read_nsat_aqs : read_nsat_aq read_nsat_aqs
{
  s_id_io *id_list_tmp;
  id_list_tmp=IO_check_id_list($1,$2,Simul->poutputs);
  $$=IO_secured_chain_bck_id(id_list_tmp,$2);
}
|read_nsat_aq
{
  $$=$1;
}
;



read_nsat_aq : LEX_INT LEX_INT LEX_INT flottant
{
  s_carac_zns *pcarac_zns;
  s_zns *pzns;
  s_carac_aq *pcarac_aq;
  s_ele_msh *pele;
  int id_ele;
  int id_zns;
  s_id_io *id_list;

  pcarac_zns=Simul->pcarac_zns;
  pzns=pcarac_zns->p_zns[$1-1];
  id_zns=pzns->id[NS_INTERN];
  pcarac_aq=Simul->pcarac_aq;
  pele=pcarac_aq->pcmsh->p_layer[$3]->p_ele[$2-1];
  id_ele=pele->id[ABS_MSH];
  NSAT_init_zns(pzns,id_ele,$4/pele->pdescr->surf,AQ_NS,Simul->poutputs);
  AQ_init_link_ele(pele,($1-1),$4/pele->pdescr->surf,NSAT_AQ,Simul->poutputs);
  id_list=IO_create_id($3,($2-1));
  $$=id_list;
};


// NG : 23/01/2020
/******************************************************************************
 *                             NSAT CHASM FILE
 *****************************************************************************
 *
 * This file lists relationships between chasm-type catchments and nsat units.
 * It contains three columns :
 *      - the first one is the GIS ID of the catchment
 *      - the second one is the GIS ID of the nsat unit
 *      - the third one is the infiltration coefficient (coef € [0;1]) which allows to only
 *        infiltrate a given fraction of the chasm discharge (whether it is river
 *        outlet discharge or subsurface chasm run-off).
 *
 ******************************************************************************/

nsat_chasm : LEX_CHASM_NSAT LEX_EQUAL brace read_link_chasms brace
{
  // NG : 27/03/2020 : Checks if chasm-type catchments / nsat units links are correct in order to avoid flux losses
  NSAT_check_chasm_connexion(Simul->pcarac_zns,Simul->poutputs);
  LP_printf(Simul->poutputs,"%d catchment-nonsat links have been read.\n",nb_chasm_links);
};

read_link_chasms : read_link_chasm read_link_chasms
|read_link_chasm

read_link_chasm : LEX_INT LEX_INT flottant           // NG : 10/03/2020 : Infiltration coefficient added
{
  s_carac_zns *pcarac_zns = Simul->pcarac_zns;
  s_zns *pzns_stock = NULL;
  s_catchment_fp *pcatch, *pcatch_stock = NULL;
  int nb_zns = pcarac_zns->nb_zns;
  int i;

  // On récupère les deux pointeurs vers la nsat unit d'une part...
  for (i = 0; i < nb_zns; i++){
     if (pcarac_zns->p_zns[i]->id[NS_GIS] == $2-1){  // Les ID GIS de NSAT units doivent bien commencer à 1 dans les input files.
         pzns_stock = pcarac_zns->p_zns[i];
         break;
     }
  }

  // ...et le catchment d'autre part
  pcatch = FP_browse_catchment_chain(Simul->pcarac_fp->p_catchment, BEGINNING_IO, Simul->poutputs);

  while (pcatch != NULL){
     if (pcatch->id_gis == $1) {
          pcatch_stock = pcatch;
          break;
     }
     pcatch = pcatch->next;
  }

  if (pzns_stock == NULL || pcatch_stock == NULL)
        LP_error(Simul->poutputs,"Either catchment GIS ID or nsat_unit GIS ID not found in nsat chasm file. Check input file at line %d.\n",nb_chasm_links+1);

  // on s'assure que le bassin est chasm
  pcatch_stock->catchment_type = FP_CATCH_CHASM;
  pcatch_stock->infil_coef = $3; // Assignation au catchment chasm du coefficient d'infiltration

  // On assigne le pointeur de la nsat unit vers le catchment pour en récupérer le ruissellement
  if (pzns_stock->pcatchment_chasm != NULL)
       LP_error(Simul->poutputs,"Nsat unit GIS ID %d is already linked to the chasm-type catchment GIS ID %d!\n",pzns_stock->id[NS_GIS],pzns_stock->pcatchment_chasm->id_gis);
  else{
      pzns_stock->pcatchment_chasm = pcatch_stock;
  }

  ++nb_chasm_links;

};



/****************************************************************************/
/*                             HYDRAULICS                                   */
/*         Data giving information on the river network :                   */
/*          singularities, reaches, inflows, cross_sections...              */
/****************************************************************************/


riv :  intro_riv network_attributes brace
{

    HYD_BC_faces(pgen_chyd,Simul->poutputs);
    // HYD_print_sing(pgen_chyd,Simul->poutputs);
    HYD_verif_singularities(pgen_chyd,Simul->poutputs);
    HYD_calculate_pk_sing(pgen_chyd,Simul->poutputs);
    HYD_calculate_pk_faces_centers(pgen_chyd,Simul->poutputs);

    if (pgen_chyd->settings->calc_curvature == YES)
        HYD_calculate_curvature_radius(pgen_chyd);
    else if (pgen_chyd->settings->calc_curvature == NO)
        HYD_zero_curvature(pgen_chyd,Simul->poutputs);

	/* NG : 04/08/2022 : Now the hydraulic network has been read (being either HYD or HDERM) :
                         we determine catchments' cprod_outlet cells. */

    LP_printf(Simul->poutputs,"Current module reading in CaWaQS%4.2f input: %s.\n",NVERSION_CAW,CAW_name_mod(module));

    switch(module){
        case HYD_CAW : 
        {
	        CAW_cFP_assign_catchment_cprod_outlet(pgen_chyd,Simul->pcarac_fp,Simul->poutputs);  
            /* Making sure that declared chasm-type catchments (i.e. pure runoff) are 
                effectively not linked with a river reach. If so, they're ignored and a WARNING is displayed. */ 
	        CAW_cFP_check_catchment_chasm_river(Simul->pcarac_fp,pgen_chyd,Simul->poutputs);
	        FP_print_outlet_cprod_type(Simul->pcarac_fp,Simul->poutputs);
            break;
        }
        case HDERM_CAW : 
        {
	        CAW_cFP_assign_catchment_cprod_outlet(pgen_chyd,Simul->pcarac_fp_hderm,Simul->poutputs);  
            CAW_cFP_check_catchment_chasm_river(Simul->pcarac_fp_hderm,pgen_chyd,Simul->poutputs);
	        FP_print_outlet_cprod_type(Simul->pcarac_fp_hderm,Simul->poutputs);
            break;
        }
        default : { 
            LP_error(Simul->poutputs,"In CaWaQS%4.2f in file %s : Either inappropriate or unknown module.\n",NVERSION_CAW,__FILE__);
            break;
        }
    }
}
;
/*************************************************************************************************/
/*************************************************************************************************/
/*  NG : 20/02/2020 : Switch between adequate pchyd and plec pointers if                         */
/*  the first or the second (i.e. hyperdermic) hydraulic network is currently being read.        */
/*************************************************************************************************/
/*************************************************************************************************/

intro_riv : LEX_NETWORK LEX_EQUAL brace
{
  int i;
  if (Simul->pchyd != NULL){
     Simul->pchyd_hderm = HYD_create_chyd();

	 pgen_chyd = Simul->pchyd_hderm;
     HYD_init_chyd(pgen_chyd);
	 pgen_chyd->calcul_hydro->sw_int[APPL_NB] = HDERM_GC;    // NG : 21/02/2020 : On change le numéro d'application défini par défaut à HYD_GC=2 dans HYD_init_chyd()
     pgen_plec = Simul->plec_hderm;
  }
  else
  {
     Simul->pchyd = HYD_create_chyd();
     pgen_chyd = Simul->pchyd;
     HYD_init_chyd(pgen_chyd);
     pgen_plec = Simul->plec;
  }

  //  HYD_init_chyd(pgen_chyd);
  for (i=0;i<NPAR_AQ;i++)
    {
      pgen_chyd->settings->general_param[i]=Simul->general_param[i];
      LP_printf(Simul->poutputs,"hydraulics %s = %f\n",HYD_name_general_param(i),pgen_chyd->settings->general_param[i]);
    }

  module=HYD_CAW;
  if (Simul->pchyd_hderm != NULL) module=HDERM_CAW;
};

def_hyd_settings : LEX_SET LEX_EQUAL LEX_OPENING_BRACE hyd_settings LEX_CLOSING_BRACE
;

hyd_settings : hyd_setting hyd_settings
| hyd_setting
;

hyd_setting : LEX_DIM LEX_EQUAL LEX_INT
{
  if(pgen_chyd->settings != NULL)
    {
      pgen_chyd->settings->ndim = $3;
      LP_printf(Simul->poutputs,"ndim = %d\n",pgen_chyd->settings->ndim);
    }
}
| LEX_CALC_CURVATURE LEX_EQUAL LEX_ANSWER
{
  if(pgen_chyd->settings !=NULL)
    {
  pgen_chyd->settings->calc_curvature = $3;
  LP_printf(Simul->poutputs,"calculation of the curvature radius = %s\n",
	  HYD_name_answer(pgen_chyd->settings->calc_curvature));
    }
}
| LEX_SCHEM_TYPE LEX_EQUAL LEX_DEF_SCHEM_TYPE
{
  if(pgen_chyd->settings !=NULL)
    {
      pgen_chyd->settings->schem_type=$3;
      LP_printf(Simul->poutputs,"hydraulics calculation scheme %s\n",HYD_name_scheme(pgen_chyd->settings->schem_type));
    }
}
| LEX_GENERALHYD LEX_EQUAL mesure
{
  pgen_chyd->settings->general_param[$1] = $3;
  LP_printf(Simul->poutputs,"hydraulics %s = %f\n",HYD_name_general_param($1),pgen_chyd->settings->general_param[$1]);
}
| def_param_musk
| LEX_HCALCWAY LEX_EQUAL LEX_HCALCHYD
{
  if(pgen_chyd->settings != NULL)
    {
      pgen_chyd->settings->hCalcWay = $3;
      LP_printf(Simul->poutputs,"River height calculation method = %s\n", HYD_river_height_method_name(pgen_chyd->settings->hCalcWay,Simul->poutputs));
    }
}
;

def_param_musk : TC KDEF
| KDEF
;

TC : LEX_TC LEX_EQUAL mesure
{
  //pgen_chyd->settings->Tc[TC_OBS]=$3;  // NG : 29/02/2020 : libhyd Tc doesn't exist anymore. Everything Tc-related in the command file is now set up at the catchment scale.
}
;

KDEF : LEX_KDEF LEX_EQUAL LEX_KDEF_OPT
{
  pgen_chyd->settings->Kdef=$3;
  LP_printf(Simul->poutputs,"Setting user-defined K_DEF method : %s\n",HYD_K_method_name(pgen_chyd->settings->Kdef,Simul->poutputs));
}
;

network_attributes : network_att network_attributes
| network_att
;

network_att : omp
| def_hyd_settings
| network_StVenant def_inflows
| network_StVenant
| network_Muskingum
| network_Muskingum init_from_file def_inflows
| network_Muskingum def_inflows
| network_Muskingum init_from_file
;



network_StVenant : def_singularities
| bathymetry
{
  Simul->clock->time_spent[LEC_CHR] += CHR_end_timer();
  CHR_begin_timer();
  HYD_table_hydro_faces($1,pgen_chyd,Simul->chronos,Simul->poutputs);
  HYD_create_faces($1,pgen_chyd,Simul->chronos,Simul->poutputs);
  HYD_create_elements(pgen_chyd,Simul->poutputs);
  HYD_table_hydro_elements(pgen_chyd,Simul->chronos,Simul->poutputs);
  Simul->clock->time_spent[TAB_CHR] += CHR_end_timer();
  CHR_begin_timer();
}
;

/******************************************************************************
 *
 *                             NETWORK MUSKINGUM FILE
 *
 ******************************************************************************
 *
 * network muskingum attributes
 * - char name
 * - int id
 * - int FromNode
 * - int ToNode
 * - double Altitude of Fnode
 * - double Altitude of Tnode
 * - double Pente
 * - int strahler
 * - double reach length
 * - double watershed Area
 * - double K parameter of muskingum (-9999 if calculated)
 * - double alpha parameter of musikingum (-9999 if constant value 0.3)
 * - double reach width
 * - double reach strickler
 * - double Qlim
 * Syntax :  tab of :
 *           name id FromNode ToNode AltFnode AltTnode pente strahler length BV_Area  K alpha width strickler Qlim
 *
 *
 *
 *
 *
 */

network_Muskingum : def_network_musk
| def_reaches_cprod_link
| def_elements_musk
{

  Simul->clock->time_spent[LEC_CHR] += CHR_end_timer();//LV 3/09/2012
  CHR_begin_timer();//LV 3/09/2012
  LP_printf(Simul->poutputs,"Getting hydraulic parameters \n");
  HYD_table_hydro_faces($1,pgen_chyd,Simul->chronos,Simul->poutputs);
  HYD_create_faces($1,pgen_chyd,Simul->chronos,Simul->poutputs);
  LP_printf(Simul->poutputs,"Done \n");
  LP_printf(Simul->poutputs,"Creating hydraulic elements\n");
  HYD_create_elements(pgen_chyd,Simul->poutputs);
  HYD_table_hydro_elements(pgen_chyd,Simul->chronos,Simul->poutputs);
  LP_printf(Simul->poutputs,"Done \n");
  if(Simul->pcarac_aq != NULL)
    {
      CAW_cAQ_create_riv_aq(pgen_chyd,Simul->pcarac_aq,Simul->chronos,Simul->poutputs);
    }

  Simul->clock->time_spent[TAB_CHR] += CHR_end_timer();//LV 3/09/2012
  CHR_begin_timer();//LV 3/09/2012
}
;

def_network_musk : intro_network_musk  network_musks brace
{
  int i;
  int max_FnodeTnode;
  s_singularity_hyd *psingtot;
  s_reach_hyd *preach;
  double tmp;
  char **FnodeTnode;

  nb_outlet=0;
  LP_printf(Simul->poutputs,"Done\n");
   LP_printf(Simul->poutputs,"Creating singularities \n");
  psingtot=HYD_create_musk_sing(pgen_plec,pgen_chyd,dim_tmp_lec);
  max_FnodeTnode=HYD_get_max_node(pgen_plec,dim_tmp_lec,Simul->poutputs);
  FnodeTnode=HYD_finalyze_sing(psingtot,pgen_chyd,Simul->chronos,max_FnodeTnode,Simul->poutputs);
  //LP_printf(Simul->poutputs,"max_Fnode_Tnode %d\n",max_FnodeTnode); // BL to debug
  HYD_create_singularities(psingtot,pgen_chyd,Simul->poutputs);
  LP_printf(Simul->poutputs,"Done\n");
  LP_printf(Simul->poutputs,"Creating reaches \n");

  // NG : 20/02/2020
  if (Simul->pchyd_hderm != NULL)
     preach=HYD_create_reaches_musk_coupled(pgen_plec,pgen_chyd,dim_tmp_lec,FnodeTnode,Simul->pcarac_fp_hderm,Simul->poutputs);
  else
     preach=HYD_create_reaches_musk_coupled(pgen_plec,pgen_chyd,dim_tmp_lec,FnodeTnode,Simul->pcarac_fp,Simul->poutputs);

  HYD_create_reaches(preach,pgen_chyd);
  LP_printf(Simul->poutputs,"Done\n");
  LP_printf(Simul->poutputs,"Freeing temporary memory allocated \n");
  for(i=0;i<2*max_FnodeTnode;i++)
    {
      if(FnodeTnode[i]!=NULL)
	{
	  free(FnodeTnode[i]);
	  FnodeTnode[i]=NULL;
	}
    }
  free(FnodeTnode);
  FnodeTnode=NULL;

  pgen_plec=HYD_free_plec(pgen_plec,dim_tmp_lec,Simul->poutputs);
 LP_printf(Simul->poutputs,"Done\n");

 LP_printf(Simul->poutputs,"Calculating reaches concentration time using catchments properties\n");

 /* NG : 25/02/2020 : Fonction obsolète remplacée par HYD_set_K_basin_by_Tc_catchments() dans HYD_hydraulics_coupled.c
   Désormais, le calcul des K musk par reaches est effectué en utilisant les valeurs de TC par sous-bassins (catchments)  */
 // HYD_set_K_basin(pgen_chyd,Simul->poutputs);

 if (Simul->pchyd_hderm == NULL)
    CAW_cHYD_set_K_basin_by_Tc_catchments(Simul->pchyd,Simul->pcarac_fp,Simul->poutputs);
 else
    CAW_cHYD_set_K_basin_by_Tc_catchments(Simul->pchyd_hderm,Simul->pcarac_fp_hderm,Simul->poutputs);


 //tmp=HYD_get_TC_Bassin(pgen_chyd,Simul->poutputs); //BL to debug
 LP_printf(Simul->poutputs,"Done\n");
 LP_printf(Simul->poutputs,"Network organized and concentration time defined\n");
};

intro_network_musk : LEX_NETWORK_MUSK LEX_EQUAL brace
{
  pgen_chyd->settings->general_param[DX]=CODE; //BL si muskingum pas de discretisation longitudinale !!
  LP_printf(Simul->poutputs,"Reading reaches input file \n");
}
;

network_musks : network_musk network_musks
| network_musk
;


network_musk : LEX_NAME LEX_INT LEX_INT LEX_INT flottant flottant flottant LEX_INT flottant flottant flottant flottant flottant flottant flottant  //BL avec Qlim et pente
	       //network_musk : LEX_NAME LEX_INT LEX_INT LEX_INT flottant flottant flottant flottant flottant flottant flottant flottant flottant
{
  int size_old;
  // LP_printf(Simul->poutputs," try to reach row nb %d \n",$3); //BL to debug
  if($3 >= dim_tmp_lec)
    {

      size_old=dim_tmp_lec;
      //LP_printf(Simul->poutputs,"resizing : size init %d",size_old); //BL to debug
      while(dim_tmp_lec<=$3)
	{
	  dim_tmp_lec+=NB_LEC_MAX;
	}

      pgen_plec=HYD_resize_tmp_lec(pgen_plec,dim_tmp_lec,size_old,Simul->poutputs);
      //LP_printf(Simul->poutputs," size end %d \n",dim_tmp_lec); // BL to debug
    }
  if(pgen_plec[$3]==NULL)
    {
      if($14<0)
	pgen_plec[$3]=HYD_create_tmp_lec($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,Simul->poutputs); // BL avec Qlim et pente
      else
	pgen_plec[$3]=HYD_create_tmp_lec($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,-$14,$15,Simul->poutputs); // BL avec Qlim et pente
    }
  else
    {

      LP_error(Simul->poutputs," the plec[%d] is already fill up ! \n",$3);
    }
  //pgen_plec[$3]=HYD_create_tmp_lec($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13);

  nb_reach++;
  // LP_printf(Simul->poutputs," nb_reach %d \n",nb_reach);

}
;

/*****************************************************************************
 *
 *                            CPROD REACHES LINK FILE
 *
 ***************************************************************************
 *
*******************************************************************************
it's optional to define the link between cprod and reaches. by default a reach
 is linked with the cprod whom GIS id is identical WARNING for adding new link
the reach's intern id have to be known (colomn num of the "def_network_musk"
file)
*******************************************************************************
*
*
* def_reaches_cprod_link attribute :
* - reach intern id (column number of the "def_network_musk" file)
* - cprod inter id (column number of the "cprod" file)
* - prct of the cprod feeding the reach
*
*
*/

def_reaches_cprod_link : intro_reaches_cprod reaches_link brace
{
  if(nb_lec!=NULL)
  free(nb_lec);

}
;



intro_reaches_cprod : LEX_REACH_CPROD LEX_EQUAL brace
{

  nb_lec=(int *)malloc(pgen_chyd->counter->nreaches*sizeof(int));
  bzero((char*)nb_lec,pgen_chyd->counter->nreaches*sizeof(int));

}
;

reaches_link : reach_link reaches_link
|reach_link
;

reach_link : LEX_INT LEX_INT LEX_DOUBLE
{
  nb_lec[$1-1]++;
  HYD_finalyse_link_reach_coupled($1-1,$2,$3,nb_lec[$1-1],pgen_chyd,Simul->poutputs);

};

/* ****************************************************************************
 *
 *                             MUSKINGUM ELEMENT FILE
 *
 * *****************************************************************************
 * NF 3/10/2017 This comment is misleading. In this part the element and the faces are created. The qlim is defined at the reach scale, so we have to define the boundaries of the element with it
 * Muskingum element
 * A Muskingum element is constructed by intersecting reach geometry
 *  and groundwater mesh
 *
 * A Muskingum element is defined by tabular :
 *  river_name id_river fnode tnode id_gw ele_length Z_bottom h_ini
 *
 * Where :
 * - river_name is the name of the river in wich the element is defined
 * - id_river is the river id
 * - id_gis is the gis id of ele_musk
 * - fnode is the From node of the element in the river permiting to locate the element in the river reach
 * - tnode is the To node of the element in the river permiting to locate the element in the river reach
 * - id_gw is the identifient of the groundwater mesh  !!
 * - id_layer the layer identifiant of the groundwater mesh
 * - ele_length is the length of the element
 * - Z_bottom is the elevation of the river bottom in element (optional)
 * - h_ini is the river deepness in element (optional)
 * - !!!!! Z_bottom OR h_ini HAVE TO BE DEFINED !!!!!
 * - stream bed thickness
 * - hyporehic zone transmissivity
 */

def_elements_musk: intro_ele_musk faces_musk  brace
{
  s_face_hyd *pface_tot,*pface_cpy;
  pface_tot = $2;
  LP_printf(Simul->poutputs," Done \n");

  pface_cpy=HYD_reorder_musk_faces(pface_tot,pgen_chyd,Simul->poutputs);
  LP_printf(Simul->poutputs,"Freeing chained faces \n");
  pface_tot=HYD_free_chained_faces(pface_tot,Simul->poutputs);
  LP_printf(Simul->poutputs,"Done \n");
  LP_printf(Simul->poutputs,"Creating hydraulic geometry \n");
  HYD_create_all_musk_geometry(pface_cpy,Simul->poutputs);
  LP_printf(Simul->poutputs,"Done \n");
  $$=pface_cpy;
}
;

intro_ele_musk : LEX_ELE_MUSK LEX_EQUAL brace
{
LP_printf(Simul->poutputs," Creating faces in reaches \n");

}

faces_musk : face_musk faces_musk
{
  $$ = HYD_chain_faces($1,$2);
}
| face_musk
{
  $$ = $1;
}
;

face_musk : LEX_NAME LEX_INT LEX_INT LEX_INT LEX_INT LEX_INT LEX_INT flottant flottant flottant flottant flottant
{
  char *new_name;
  s_face_hyd *pftmp;
  int str_len,order1,order2;

  //LP_printf(Simul->poutputs,"looking for reach %d \n",$2);
  pftmp=NULL;
  preach_bat = HYD_find_reach_by_id($2,1,pgen_chyd,Simul->poutputs);
  if(preach_bat!=NULL)
    preach_bat->nfaces++;
  else
    LP_error(Simul->poutputs,"CaWaQS%4.2f %s in %s l%d: the reach return by find_id_reach_by_id is null, input.y\n",NVERSION_CAW,__func__,__FILE__,__LINE__);

  order1=HYD_get_order((double)$2);
  order2=HYD_get_order((double)$3);
  str_len=STRING_LENGTH_LP;
  new_name = (char *)calloc(str_len,sizeof(char));
  sprintf(new_name,"%s_%d0%d",$1,$2,$3);

  pftmp=HYD_create_musk_face(new_name,$3,$4,$5,$6,$7,$8,$9,$10,preach_bat,NULL,pgen_chyd,Simul->poutputs);
  if(fabs($11-CODE_TS)>EPS)
    {
      pftmp->hyporeic_sets[EP_HYD]=$11;
    }
  else
      pftmp->hyporeic_sets[EP_HYD]=HZ_THICK_CAW;
  pftmp->hyporeic_sets[TZ_HYD]=$12;
   if(pftmp!=NULL)
    $$=pftmp;
  else
    LP_error(Simul->poutputs,"CaWaQS%4.2f %s in %s l%d: the face is null in face_musk, input.y\n",NVERSION_CAW,__func__,__FILE__,__LINE__);

  free($1);
}
;


/* Singularities attributes
 * - name
 * - pk
 * - weir characteristics for dams
 *
 * Syntax : upstream_point;
 *          downstream_dam {
 * 	                     pk = [km] 100.
 *	                     z(t) = { [d]  [m]
 *		                      0.0 17.40
 *	                            }
 *                         };
 */
def_singularities : LEX_SING LEX_EQUAL brace singularities brace
{
  psingtot = $4;
  HYD_create_singularities(psingtot,pgen_chyd,Simul->poutputs);
}
;

singularities : singularity singularities
{
  $$ = HYD_chain_singularities($1,$2);
}
| singularity
{
  $$ = $1;
}
;

singularity : read_sing_name sing_attributes
{
  $$ = new_singularity(); //curieux non ?
  $$ = psing;
  LP_printf(Simul->poutputs,"The singularity %s has been read\n",psing->name);

}
;

read_sing_name : LEX_NAME
{
  psing = HYD_create_singularity($1,pgen_chyd);
  nworks = 0;
  free($1);
}
;

sing_attributes : brace sequence_sing_attributes brace LEX_SEMI_COLON
| LEX_SEMI_COLON
;

sequence_sing_attributes : sing_attribute sequence_sing_attributes
| sing_attribute
;

sing_attribute : LEX_PK LEX_EQUAL mesure
{
  psing->pk = $3;
  psing->passage = YES;
}
| LEX_TYPE LEX_EQUAL LEX_BC_TYPE
{
  psing->type = $3;
}
| LEX_POSITION LEX_EQUAL LEX_DIR_HYD LEX_INT
{
  if (psing->BC_char == NULL)
    psing->BC_char = ((s_BC_char_hyd **) calloc(1,sizeof(s_BC_char_hyd *)));
  psing->BC_char[0]->position[0] = $3;
  psing->BC_char[0]->position[1] = $4 - 1;
}
| fion
{
  if (psing->BC_char == NULL)
    psing->BC_char = ((s_BC_char_hyd **) calloc(1,sizeof(s_BC_char_hyd *)));
  psing->BC_char[0] = $1;
  psing->nworks = 1;
}
| hydstructures
{
  pworktot = $1;
  psing->nworks = nworks;
  psing->BC_char = ((s_BC_char_hyd **) calloc(nworks,sizeof(s_BC_char_hyd *)));
  psing->BC_char[0] = pworktot;

  for (i = 1; i < nworks; i++) {
    pworktot = pworktot->next;
    psing->BC_char[i] = pworktot;
  }
}
;

hydstructures : LEX_HYDWORKS LEX_EQUAL brace hydworks brace
{
  $$ = $4;
}
;

hydworks : hydwork hydworks
{
  $$ = HYD_chain_hydworks($1,$2);
}
| hydwork
{
  $$ = $1;
}
;

hydwork : intro_work work_atts brace
{
  $$ = pwork;
}
;


intro_work : LEX_WORK LEX_EQUAL brace
{
  worktype = $1;
  pwork = HYD_create_hydwork(worktype);
  nworks++;
}
;

work_atts : work_att work_atts
| work_att
;

work_att : LEX_WORK_PARAM LEX_EQUAL mesure
{
  pwork->hydwork_param[$1] = $3;
}
| LEX_POSITION LEX_EQUAL LEX_DIR_HYD LEX_INT
{
  pwork->position[0] = $3;
  pwork->position[1] = $4 - 1;
}
| fion
{
  //LP_printf(Simul->poutputs,"Dans le fion !! \n"); // to n'importe quoi
  pwork->fion_type = $1->fion_type;
  pwork->fion = $1->fion;
  pwork->fion_t = TS_function_value_t(Simul->chronos->t[INI],pwork->fion,Simul->poutputs);//LV nov2014
}
;

fion : LEX_FION LEX_EQUAL brace read_units f_ts brace
{
  $$ = HYD_create_hydwork(NONE);
  $$->fion_type = $1;
  $$->fion = $5;
}
;



/* Bathymetry
 * An element is created for each defined cross_section.
 * Cross_Sections' characteristics :
 * - name
 * - reach (upstream limit and branch nb)
 * - type of description : Absc Z or X Y Z
 *   - X Y Z description (X Y in Lambert coordinates)
 *     calculation of the transversal cross_section with the values of X and Y
 *     the first point is taken as the referance
 *     (function transversal_abscisse()).
 *   - ABSC Z description
 *     ABSC = curvilinear distance from a reference point
 * - Length of the cross_section. A length of zero is only used for the
 *   interpolation of the sections that are not used as calculation
 *   elements (usually at the end of a reach)
 *
 * Synthaxe : cross_section_name <- limit_name i {
 *                         dx = 700 [m]
 *                         type  = ABSC	 Z
 *  		                   0.0	 20.97
 *  		                   10.7	 15.64
 *  		                   60.8	 15.64
 *  		                   71.5	 20.97
 *                         } ;
 */
bathymetry : LEX_PROFILES LEX_EQUAL brace cross_sections brace
{
  $$ = $4;
}
| LEX_PROFILES LEX_EQUAL brace cross_sections shape brace
{
  $$ = $4;
}
| LEX_TRAJECTORY LEX_EQUAL brace trajectory brace
{
  FILE *fp;
  fp=Simul->poutputs;
  $$ = $4;
  if (pgen_chyd->counter->nreaches > 1) {
    LP_error(fp,"There cannot be more than one reach if a trajectory shape is given\n");
  }
  if (pgen_chyd->p_reach[0]->nfaces > 1) {
    HYD_calculate_faces_traj($4,pgen_chyd);
  }
  else {
    LP_error(fp,"The trajectory shape must be defined by more than one point\n");
  }
}
;

cross_sections : cross_section cross_sections
{
  $$ = HYD_chain_faces($1,$2);
}
| cross_section
{
  $$ = $1;
}
;

cross_section : read_geometry_name brace cross_section_options brace  LEX_SEMI_COLON
{
  $$ = new_face();
  $$ = pface;
  pface = NULL;
}
;

read_geometry_name : LEX_NAME LEX_REVERSE_ARROW LEX_NAME LEX_INT
{
  preach_bat = HYD_find_reach($3,$4,pgen_chyd);
  preach_bat->nfaces++;
  free($3);
  pface = HYD_create_face(X_HYD,pgen_chyd);
  pface->name = $1;
  pface->def = RAW_SECTION;
  pface->reach = preach_bat;
  pface->geometry = HYD_create_geometry();
  LP_printf(Simul->poutputs,"The cross-section %s in reach %s %s %d has been read\n",
	  $1,preach_bat->limits[ONE]->name,preach_bat->limits[TWO]->name,preach_bat->branch_nb);

}
;

cross_section_options : cross_section_option cross_section_options
| cross_section_option
;

cross_section_option : LEX_GENERALP LEX_EQUAL mesure
{
  if ($1 == DX) {
    pface->geometry->dx = $3;
    preach_bat->length += $3;
  }

  else if ($1 == CURVATURE) {
    pface->description->curvature = $3;
  }
}
| intro_sectionAbscZ sectionAbscZs
{
  pptAbscZ = $2;
  HYD_create_ptsAbscZ(pface,pptAbscZ);
  // !!!!! il faut libérer pptAbscZ car recrée tableau dans HYD_create_ptsAbscZ(pface,pptAbscZ); --> fuite memoire !!!
}
| intro_sectionXYZ sectionXYZs
{
  pptXYZ = $2;
  HYD_create_ptsXYZ(pface,pptXYZ);
  HYD_calculate_AbscZ_pts(pface->geometry);
// !!!!! J'imagine idem plus haut il faut libérer pptAbscZ car recrée tableau dans HYD_create_ptsAbscZ(pface,pptAbscZ); --> fuite memoire !!!
}
;

intro_sectionAbscZ : LEX_TYPE LEX_EQUAL LEX_ABSC LEX_HYD_VAR
{
  pface->geometry->type = ABSC_Z;
}
;

intro_sectionXYZ :LEX_TYPE LEX_EQUAL LEX_X LEX_Y LEX_HYD_VAR
{
  pface->geometry->type = X_Y_Z;
}
;

sectionAbscZs : sectionAbscZ sectionAbscZs
{
  $$ = HYD_chain_ptsAbscZ($1,$2);
}
| sectionAbscZ
{
  $$ = $1;
}
;


sectionXYZs : sectionXYZ sectionXYZs
{
  $$ = HYD_chain_ptsXYZ($1,$2);
}
| sectionXYZ
{
  $$ = $1;
}
;

sectionAbscZ : flottant flottant
{
   $$ = HYD_create_ptAbscZ($1,$2);
   pface->geometry->npts++;
}
;

sectionXYZ : flottant flottant flottant
{
  $$ = HYD_create_ptXYZ($1,$2,$3);
  pface->geometry->npts++;
}
;

shape : LEX_SHAPE LEX_EQUAL LEX_CROSS_SECTION_TYPE
{
  pgen_chyd->settings->cs_shape = $3;
}
;

trajectory : traj_units traj_points
{
  $$ = $2;
}
;

traj_units : a_unit a_unit a_unit a_unit a_unit
{
  unit_x = $1;
  unit_y = $2;
  unit_z = $3;
  unit_l = $4;
  unit_kappa = $5;
}
;

traj_points : traj_point traj_points
{
  $$ = HYD_chain_faces($1,$2);
}
| traj_point
{
  $$ = $1;
}
;

traj_point : flottant flottant flottant flottant flottant
{
  preach_bat = pgen_chyd->p_reach[0];
  preach_bat->nfaces++;

  pface = HYD_create_face(X_HYD,pgen_chyd);
  pface->def = RAW_SECTION;
  pface->reach = preach_bat;
  pface->geometry = HYD_create_geometry();
  pface->description->xc = $1 * unit_x;
  pface->description->yc = $2 * unit_y;
  pface->description->Zbottom = $3 * unit_z;
  pface->description->l = $4 * unit_l;
  pface->description->curvature = $5 * unit_kappa;
  $$ = pface;
}
;

// NG : 01/10/2020
/****************************************************************************
 *                  EXOGENOUS INFLOWS TO HYDRAULIC NETWORKS                 *
 ****************************************************************************
 *
 * NB : The 'def_inflows' sub-part can be called several times throughout the entire
 * command file (ie. for the HYD network and/or the HDERM network and for each species !).
 * Meant to deal with both hydro (ie. water inputs only) and transport
 * (ie. solute/temperature inflows) inputs.
 *
 * -------------------------------------------------------------------------- *
 *
 * This input block deals with the definition of exogenous inflows inputs to an
 * hydraulic network.
 *
 * Syntax to be defined in the optional 'inflows' input file is as follows :
 *
 *  name_inflow_1 : [River_reach GIS ID]
 *                {  TYPE = { UPSTREAM_INFLOW, INFLUENT, EFFLUENT, DIFFUSE_INFLOW }
 *                   X = [length unit] 1.2
 *                   DX = [length_unit] 0.4 (for diffuse inflow only)
 *                   Y = 0.2
 *                   Q_inflow = { [t_unit] [ft_unit] 0. 60.0 1. 45.5 }
 *          } ;
 *
 *  name_inflow_2 : [River_reach GIS ID]
 *                {  TYPE = {UPSTREAM_INFLOW,INFLUENT,EFFLUENT,DIFFUSE_INFLOW}
 *                   X = [length unit] 1.2
 *                   DX = [length_unit] 0.4
 *                   Y = 0.4
 *                   Q_inflow = { [t_unit] [ft_unit] 0. 60.0 }
 *                   C_inflow = { [t_unit] [ft_unit] 0. 2.5 }
 *                   T_inflow = { [t_unit] [ft_unit] 0. 273.15 }
 *          } ;
 *
 *  etc.
 *
 * ----------------------------------------------------------
 * Meanings of option attributes :
 *
 * TYPE : point or diffuse inflow
 * X : distance from upstream limit of the river reach
 * DX : length on which the inflow occurs (in case of diffuse inflow)
 * Y : transversal localization of inflow (0<Y<1) :
 *       if Y <= 0.5 : Inflows will be attributed to the left bank (ie. [Y_HYD][ONE] type face), to the
 *       right bank (ie. [Y_HYD][TWO] type face) otherwise.
 * '[q,t,c]_inflow' sub-parts define time and value series associated with the inflow.
 *
 */

/* Note NG : Etant donné que les défintions des inflows relatifs à un même réseau hydraulique (HYD, HDERM)
   peuvent être disséminées à plusieurs endroits du fichier de commande, à la fin de chaque appel au bloc 'def_inflows',
   les inflows nouvellement définis sont collectés dans une unique chaine (1 pour chque type de réseau).
   On constitue alors deux chaines globales de pointers s_inflow_hyd 'pchain_inflow_hyd' et 'pchain_inflow_hderm' qui
   sont analysées en toute fin de la lecture du fichier de commande avant d'être associés aux faces des éléments dans
   la sous-fonction HYD_create_inflows() de HYD_inflows_settings(). */

def_inflows : LEX_INFLOWS LEX_EQUAL brace inflows brace
{
	 /* NG : Building unique s_inflow_hyd pointer chain for HYD and/or HDERM hydraulic networks gathering all inflows.
    We're refering to the 'module' current value to link inflows to the proper hydraulic network. */

	s_inflow_hyd* ptmp;

	switch (module)
	{
		case HYD_CAW :
		{
			if (pchain_inflow_hyd != NULL)
			{
				pchain_inflow_hyd = HYD_browse_inflows(pchain_inflow_hyd,END_HYD,Simul->poutputs);
				ptmp = HYD_browse_inflows($4,BEGINNING_HYD,Simul->poutputs);
				pchain_inflow_hyd = HYD_chain_inflows(pchain_inflow_hyd,ptmp);
			}
			else
				pchain_inflow_hyd = $4;
			break;
		}
		case HDERM_CAW :
		{
			if (pchain_inflow_hderm != NULL)
			{
				pchain_inflow_hderm = HYD_browse_inflows(pchain_inflow_hderm,END_HYD,Simul->poutputs);
				ptmp = HYD_browse_inflows($4,BEGINNING_HYD,Simul->poutputs);
				pchain_inflow_hderm = HYD_chain_inflows(pchain_inflow_hderm,ptmp);
			}
			else
				pchain_inflow_hderm = $4;
			break;
		}
    	default: LP_error(Simul->poutputs,"CaWaQS%4.2f : Error in file %s, at line %d : Incoherent current module value : %s.\n",NVERSION_CAW,__FILE__,__LINE__,CAW_name_mod(module));
      		break;
	}
};

inflows : inflow inflows
{
  $$ = HYD_chain_inflows($1,$2);
}
| inflow
{ $$ = $1; };

inflow : read_name_inflow brace options_inflow brace LEX_SEMI_COLON
{
  $$ = new_inflow();
  $$ = pinflow;
  pinflow = NULL;
};

read_name_inflow : LEX_NAME LEX_COLON LEX_INT
{
	s_chyd* pgen;

	//LP_printf(Simul->poutputs,"Reading inflows : Current module value : %s\n",CAW_name_mod(module));  // NG check

	if (module == HYD_CAW)
		pgen = Simul->pchyd;
	else
		pgen = Simul->pchyd_hderm;

	preach_infl = HYD_find_reach_by_id($3,1,pgen,Simul->poutputs);
	preach_infl->ninflows++;
	pinflow = HYD_create_inflow($1,pgen);
	pinflow->reach = preach_infl;
    LP_printf(Simul->poutputs,"Inflow %s associated with river reach GIS ID %d has been detected.\n",pinflow->name,preach_infl->id[GIS_HYD]);
	free($1);
};

options_inflow : option_inflow options_inflow
| option_inflow
;

option_inflow : LEX_TYPE LEX_EQUAL LEX_INFLOW_TYPE
{
	pinflow->type = $3;

	if (pinflow->type == DIFFUSE_INFLOW)
		pinflow->diff_inflow = HYD_create_new_diffuse_inflow();
	else
		pinflow->pt_inflow = HYD_create_point_inflow(pinflow->name);
}
| LEX_X LEX_EQUAL mesure
{
    if (pinflow->type == UPSTREAM_INFLOW)
	{
		LP_warning(Simul->poutputs,"Useless X position value ignored for inflow type %s (name = %s). Value reset to zero.\n",HYD_name_inflow(pinflow->type),pinflow->name);
        pinflow->x = 0.;
	}
	else
		pinflow->x = $3;    // Set to 0 by default (ie. reach upstream limit)
}
| LEX_GENERALHYD LEX_EQUAL mesure
{
	if (pinflow->type != DIFFUSE_INFLOW)
		LP_error(Simul->poutputs,"CaWaQS%4.2f : Error in file %s at line %d : Incompatible option for inflow named %s in river reach GIS id %d : dx attribute (length of inflow) is diffuse-inflow specific !\n",NVERSION_CAW,__FILE__,__LINE__,pinflow->name,pinflow->reach->id[GIS_HYD]);
  	else
    	pinflow->diff_inflow->dx = $3;
}
| LEX_Y LEX_EQUAL flottant
{
	if (TS_is_value_closed_interval($3,0,1) == NO)
		LP_error(Simul->poutputs,"CaWaQS%4.2f : Error in file %s at line %d : y parameter value out of range. Check your inputs for inflow named %s.\n",NVERSION_CAW,__FILE__,__LINE__,pinflow->name);
	else
		pinflow->transversal_position = $3;
}
| LEX_INFL_VAR LEX_EQUAL brace read_units f_ts brace
{
	switch ($1)
	{
		case DISCHARGE_INFLOW_HYD:
		{
			pinflow->discharge = $5;
			if (pinflow->type != DIFFUSE_INFLOW) pinflow->pt_inflow->discharge = $5;
			break;
		}
		case CONCENTRATION_INFLOW_HYD:
		{
			if (nb_species == 0)
				LP_error(Simul->poutputs,"CaWaQS%4.2f : Error in file %s, line %d : Impossible to assign concentration to inflow to reach %d in HYDRO part of the COMM file !\n",NVERSION_CAW,__FILE__,__LINE__,pinflow->reach->id[GIS_HYD]);
			else
			{
				pinflow->concentration[nb_species-1] = $5;
 				if (pinflow->type != DIFFUSE_INFLOW) pinflow->pt_inflow->concentration[nb_species-1] = $5;
			}
			break;
		}
		case TEMPERATURE_INFLOW_HYD:
		{
			pinflow->temperature = $5;
			if (pinflow->type != DIFFUSE_INFLOW) pinflow->pt_inflow->temperature = $5;
			break;
		}
	}
};

/****************************************************************************
 *                           Init_From_File                                 *
 ****************************************************************************/
init_from_file : LEX_INIT_FROM_FILE LEX_EQUAL LEX_NAME
{
  CAW_init_from_file(name_out_folder,$3, NPILE,module,Simul->pcarac_aq,Simul->pcarac_fp,Simul->pcarac_zns,pgen_chyd,Simul->pout,Simul->chronos,Simul->poutputs);
}
;

// NG : 18/02/2020 : Introducing new optional hyperdermic module
// NG : 05/03/2020 : HDERM-AQ link file added
/**************************************************************************/
/*                                                                        */
/*                      HYPERDERMIC MODULE                                */
/*                                                                        */
/**************************************************************************/

// NG : 27/03/2020 : optional initialization from file added
hyperdermic : hyperdermic_intro gen_settings omp link_nsat_hderm hderm_catchments riv LEX_CLOSING_BRACE
| hyperdermic_intro gen_settings omp link_nsat_hderm hderm_catchments riv init_from_file LEX_CLOSING_BRACE
| hyperdermic_intro gen_settings omp link_nsat_hderm hderm_catchments riv link_hderm_aq LEX_CLOSING_BRACE
| hyperdermic_intro gen_settings omp link_nsat_hderm hderm_catchments riv init_from_file link_hderm_aq LEX_CLOSING_BRACE
| hyperdermic_intro gen_settings link_nsat_hderm hderm_catchments riv LEX_CLOSING_BRACE
| hyperdermic_intro gen_settings link_nsat_hderm hderm_catchments riv init_from_file LEX_CLOSING_BRACE
| hyperdermic_intro gen_settings link_nsat_hderm hderm_catchments riv link_hderm_aq LEX_CLOSING_BRACE
| hyperdermic_intro gen_settings link_nsat_hderm hderm_catchments riv init_from_file link_hderm_aq LEX_CLOSING_BRACE

hyperdermic_intro : LEX_HYPERDERMIC LEX_EQUAL LEX_OPENING_BRACE
{
	Simul->pcarac_fp_hderm = FP_create_carac_fp();
	module=HDERM_CAW;

	LP_printf(Simul->poutputs,"\n ****************************************************\n");
	LP_printf(Simul->poutputs," ************* HYPERDERMIC MODULE SETTINGS **********\n");
	LP_printf(Simul->poutputs," ****************************************************\n\n");

}
;

// NG : 19/02/2020
/******************************************************************************
 *                    HYPERDERMIC GENERAL SETTINGS
 *****************************************************************************
 *
 * Four values need to be set here :
 *   -> Tc : the concentration time for the hyperdermic catchments
 *   -> Smoy : Threshold catchment surface value. If a catchment surface value is greater
 *             than Smoy, catchment concentration time will be recalculated according to
 *             Tc_new = [alpha*(S/Smoy)^beta]*Tc_old
 *   -> alpha, beta parameters

 ******************************************************************************/

gen_settings : gen_set_intro tc_set smoy_set values_tc_set LEX_CLOSING_BRACE

gen_set_intro : LEX_SET LEX_EQUAL LEX_OPENING_BRACE

tc_set : LEX_TC LEX_EQUAL mesure
{
   Simul->pcarac_fp_hderm->default_concentration_time=$3;
   LP_printf(Simul->poutputs,"Default concentration time for hyperdermic catchments set to : %.1f s (%.1f days).\n",Simul->pcarac_fp_hderm->default_concentration_time,Simul->pcarac_fp_hderm->default_concentration_time/TS_unit2sec(DAY_TS,Simul->poutputs));
};

smoy_set : LEX_MEAN_SURF_CATCH LEX_EQUAL mesure
{
   Simul->pcarac_fp_hderm->thresh_mean_surf=$3;
   LP_printf(Simul->poutputs,"If left undeclared, concentration times for hyperdermic catchments bigger than %.1f m2 will be automatically calibrated.\n",Simul->pcarac_fp_hderm->thresh_mean_surf);

};

values_tc_set : LEX_ALPHA_HDERM LEX_EQUAL flottant LEX_BETA_HDERM LEX_EQUAL flottant
{
	Simul->pcarac_fp_hderm->param_tc[FP_ALPHA]=$3;
	Simul->pcarac_fp_hderm->param_tc[FP_BETA]=$6;
};


// NG : 18/02/2020
/******************************************************************************
 *                    HYPERDERMIC NSAT-CPROD LINK FILE
 *****************************************************************************
 *
 *  This file is constructed via GIS by intersecting the NSAT geometry mesh
 * file with the hyperdermic cprod geometry.
 *
 * contains by columns :
 * - GIS_ID of the hyperdermic Cprod cell
 * - The GIS ID of NSAT unit (i.e. row number in the "Nsat unit File")
 * - intersection Area between the two objects (in m2)
 *
 ******************************************************************************/

link_nsat_hderm : link_nsat_hderm_intro read_cprods_hderm brace
{
  s_carac_fp *pcarac_fp_hderm;
  Simul->clock->time_spent[LEC_CHR] += CHR_end_timer();
  CHR_begin_timer();
  pcarac_fp_hderm = Simul->pcarac_fp_hderm;
  pcarac_fp_hderm->nb_cprod = nb_cprod_hderm;
  LP_printf(Simul->poutputs,"%d hyperdermic Cprod cells have been read.\n",pcarac_fp_hderm->nb_cprod);
  pcarac_fp_hderm->p_cprod = FP_tab_cprod($2,pcarac_fp_hderm->nb_cprod,Simul->poutputs);
  FP_finalize_link_cp(pcarac_fp_hderm,NSAT_HDERM_FP,Simul->poutputs);
//  FP_print_cprod($4,NSAT_HDERM_FP,Simul->poutputs); // check NG
  Simul->clock->time_spent[TAB_CHR] += CHR_end_timer();
  CHR_begin_timer();
};

link_nsat_hderm_intro : LEX_LINK_NSAT_HDERM LEX_EQUAL brace
{
  if (Simul->pcarac_zns == NULL) LP_error(Simul->poutputs,"Cannot link HDERM module to ZNS module. The latter has not been declared. Check your command file.\n");
};

read_cprods_hderm : read_cprod_hderm read_cprods_hderm
{
  s_cprod_fp *cprod_tmp;

  cprod_tmp=FP_check_cprod($1,$2,NSAT_HDERM_FP,Simul->poutputs);
  if(cprod_tmp != NULL)
  {
    ++nb_cprod_hderm;
  }
  $$ = FP_secured_chain_cprod_fp(cprod_tmp,$2);
}
|read_cprod_hderm
{
  $$=$1;
}
;

read_cprod_hderm : LEX_INT LEX_INT flottant
{
  s_cprod_fp *pcprod;
  pcprod = FP_create_cprod($1,FP_GIS);
  pcprod->link[NSAT_HDERM_FP] = SPA_create_ids(($2-1),$3);  // L'identifiant du fichier pour la maille NS doit commencer à 1 !
  pcprod->area = $3;
  $$ = pcprod;
};

// NG : 19/02/2020
/******************************************************************************
 *                    HYPERDERMIC CATCHMENTS SETTINGS
 *****************************************************************************
 *
 *  This section defines the relationship between hyperdermic cprod cells and
 *  the catchment they belong to. In a analog way with the surface domain,
 *  an hyperdermic catchment is defined by a set of cprod cells (as defined
 *  after reading the HYPERDERMIC NSAT-CPROD LINK file) (see above).
 *
 * For each hyperdermic catchment to be defined, 2 elements are needed :
 * - The GIS_ID of the catchment
 * - The list of Cprod cells (liste of GIS ID) which belong to a given catchment
 *
 ******************************************************************************/

hderm_catchments : hderm_catch_intro catch_hderm_settings LEX_CLOSING_BRACE
{
	Simul->clock->time_spent[LEC_CHR] += CHR_end_timer();
	Simul->pcarac_fp_hderm->nb_catchment = hderm_catchment_counter;
	Simul->pcarac_fp_hderm->p_catchment = $2;
	// FP_print_catchments_properties_from_chain(Simul->pcarac_fp_hderm->p_catchment, Simul->poutputs, BEGINNING_IO); // check NG
	// FP_check_cprod_catchment_connexion(Simul->pcarac_fp_hderm,Simul->poutputs); // check NG
	if(Simul->pcarac_fp_hderm->nb_catchment > 1) FP_check_catchment_duplicate_ids(Simul->pcarac_fp_hderm->p_catchment,Simul->poutputs);
	CHR_begin_timer();
	if (hderm_catchment_tc_auto != 0) LP_printf(Simul->poutputs,"Concentration times for %d hyperdermic catchments have been automatically determinated.\n",hderm_catchment_tc_auto);
};


hderm_catch_intro : LEX_INTRO_CATCHMENTS_HDERM LEX_EQUAL LEX_OPENING_BRACE

catch_hderm_settings : catch_hderm_setting catch_hderm_settings
{
	$$=FP_secured_chain_catchment_fwd($1,$2);
};
| catch_hderm_setting
{
  $$=$1;
};

catch_hderm_setting : catch_hderm_tc_read
| catch_hderm_tc_unread


/* NG : 27/02/2020 : Je suis amené à copier/coller le bloc d'instruction selon si on impose ou pas le Tc du catchment.
Ca me paraît bien crado... */
catch_hderm_tc_read : LEX_OPENING_BRACE LEX_INT LEX_TC LEX_EQUAL mesure catch_hderm_cells LEX_CLOSING_BRACE
{
	s_carac_fp* p_carac_fp_hderm;
	int nb_cprod_catch_derm = 0, nb_cprod_tot_hderm = 0;
	char* catch_name;
	int i, count_pos = 0;
	s_cprod_fp* p_cprod_st = NULL;
	s_id_io* list_cells = NULL;
	s_catchment_fp *pcatch_derm = NULL;
	double surface = 0.;

	p_carac_fp_hderm = Simul->pcarac_fp_hderm;
	nb_cprod_catch_derm = IO_length_id($6);
	pcatch_derm = FP_create_catchment($2,nb_cprod_catch_derm,p_carac_fp_hderm);

	catch_name = (char*)malloc(STRING_LENGTH_LP*sizeof(char));
	sprintf(catch_name,"Catchderm_%d",$2);

	pcatch_derm->catchment_name = catch_name;

  	// Stockage des pointeurs vers cprod du bassin
	nb_cprod_tot_hderm = p_carac_fp_hderm->nb_cprod;

	if ($6 == NULL) LP_printf(Simul->poutputs,"Hyperdermic Cprod IDs list is null. Check your catchment cell file for catchment named %s.\n",pcatch_derm->catchment_name);
	list_cells = IO_browse_id($6,BEGINNING_IO);

	while (list_cells != NULL){
		for (i = 0; i < nb_cprod_tot_hderm; i++){
			if (p_carac_fp_hderm->p_cprod[i]->id[FP_GIS] == list_cells->id) {
				p_cprod_st = p_carac_fp_hderm->p_cprod[i];
				surface += p_cprod_st->area;
				break;
			}
		}
		if (p_cprod_st == NULL) LP_error(Simul->poutputs,"Hyperdermic cprod cell with GIS_ID %d in catchment %d doesn't exist. Check your Link Cprod-Nsat file.\n",list_cells->id, pcatch_derm->id_gis);

        // On affecte la cprod a son bassin hyperdermique en vérifiant qu'elle n'appartient pas déja (par erreur de l'user) à un autre bassin
		if (p_cprod_st->p_catch == NULL)
               p_cprod_st->p_catch = pcatch_derm;
        else
              LP_error(Simul->poutputs,"Hyperdermic Cprod cell GIS ID %d can't be linked with catchment GIS ID %d as it's already linked to the catchment GIS ID %d.\n",p_cprod_st->id[FP_GIS],pcatch_derm->id_gis,p_cprod_st->p_catch->id_gis);

		pcatch_derm->cprod_catchment[count_pos] = p_cprod_st;
		list_cells = list_cells->next;
		count_pos++;
	}

	pcatch_derm->area = surface;
	pcatch_derm->concentration_time=$5; // On impose directement le TC lu pour le catchment
	hderm_catchment_counter++;
 	$$ = pcatch_derm;
};


catch_hderm_tc_unread : LEX_OPENING_BRACE LEX_INT catch_hderm_cells LEX_CLOSING_BRACE
{
	s_carac_fp* p_carac_fp_hderm;
	int nb_cprod_catch_derm = 0, nb_cprod_tot_hderm = 0;
	char* catch_name;
	int i, count_pos = 0;
	s_cprod_fp* p_cprod_st = NULL;
	s_id_io* list_cells = NULL;
	s_catchment_fp *pcatch_derm = NULL;
	double surface = 0.;

	p_carac_fp_hderm = Simul->pcarac_fp_hderm;
	nb_cprod_catch_derm = IO_length_id($3);
	pcatch_derm = FP_create_catchment($2,nb_cprod_catch_derm,p_carac_fp_hderm);

	catch_name = (char*)malloc(STRING_LENGTH_LP*sizeof(char));
	sprintf(catch_name,"Catchderm_%d",$2);

	pcatch_derm->catchment_name = catch_name;

  	// Stockage des pointeurs vers cprod du bassin
	nb_cprod_tot_hderm = p_carac_fp_hderm->nb_cprod;

	if ($3 == NULL) LP_printf(Simul->poutputs,"Hyperdermic Cprod IDs list is null. Check your catchment cell file for catchment named %s.\n",pcatch_derm->catchment_name);
	list_cells = IO_browse_id($3,BEGINNING_IO);

	while (list_cells != NULL){
		for (i = 0; i < nb_cprod_tot_hderm; i++){
			if (p_carac_fp_hderm->p_cprod[i]->id[FP_GIS] == list_cells->id) {
				p_cprod_st = p_carac_fp_hderm->p_cprod[i];
				surface += p_cprod_st->area;
				break;
			}
		}
		if (p_cprod_st == NULL) LP_error(Simul->poutputs,"Hyperdermic cprod cell with GIS_ID %d in catchment %d doesn't exist. Check your Link Cprod-Nsat file.\n",list_cells->id, pcatch_derm->id_gis);

        // On affecte la cprod a son bassin hyperdermique en vérifiant qu'elle n'appartient pas déja (par erreur de l'user) à un autre bassin
		if (p_cprod_st->p_catch == NULL)
               p_cprod_st->p_catch = pcatch_derm;
        else
              LP_error(Simul->poutputs,"Hyperdermic Cprod cell GIS ID %d can't be linked with catchment GIS ID %d as it's already linked to the catchment GIS ID %d.\n",p_cprod_st->id[FP_GIS],pcatch_derm->id_gis,p_cprod_st->p_catch->id_gis);

		pcatch_derm->cprod_catchment[count_pos] = p_cprod_st;
		list_cells = list_cells->next;
		count_pos++;
	}

	pcatch_derm->area = surface;
	hderm_catchment_counter++;

	// Calibration auto du TC du catchment en cours de déclaration
  	FP_calibrate_hderm_catchment_tc(pcatch_derm,Simul->pcarac_fp_hderm,Simul->poutputs);
    hderm_catchment_tc_auto++;

 	$$ = pcatch_derm;
};


/* *******************************************
 *
 * The Cprod list file (one by catchement) only lists, in one unique column
 * the set of Cprod (using their GIS ID) which are part of the hyperdermic catchment.
 *
**************************************************** */

catch_hderm_cells : catch_hderm_cell catch_hderm_cells
{
   $$ = IO_secured_chain_fwd_id($2,$1);
};
| catch_hderm_cell
{$$ = $1;};


catch_hderm_cell : LEX_INT
{
   s_id_io* pid = NULL;
   pid = IO_create_id(0,$1);
   $$ = pid;
};


// NG : 05/03/2020
/******************************************************************************
 *                    HYPERDERMIC-AQUIFER LINK FILE
 *****************************************************************************
 *
 * This section defines relationships between hyperdermic river network
 * outlets and aquifer grid cells.
 * Each line of the file defines a link between a hyperdermic river element,
 * for which the river discharge will be redirected as a local additional recharge
 * of the targeted aquifer cell.
 *
 * The river element which is linked to an aquifer cell is supposed to be an outlet of the
 * hyperdermic river network. If it isn't, it will display an error !
 * Multiple river outlets can be redirected towards the same aquifer cell.
 *
 * The structur of this link file is as follows :
 *    --> First column  : GIS ID of the river element to be redirected to the aquifer
 *    --> Second column : INTERN ID+1 of the aquifer cell
 *    --> Third column  : Layer ID.
 *
 ******************************************************************************/

link_hderm_aq : link_hderm_aq_intro read_hderm_aqs LEX_CLOSING_BRACE
{
	s_id_io* id_list;
	id_list=$2;
	//AQ_print_links_ele(Simul->pcarac_aq,HDERM_AQ,Simul->poutputs); // Check NG
  	AQ_define_recharge_coupled(Simul->pcarac_aq->pcmsh,id_list,HDERM_SOURCE_AQ,Simul->poutputs);
	LP_printf(Simul->poutputs,"%d aquifer cells have been linked to at least one hyperdermic river outlet.\n",IO_length_id($2));
	IO_free_id_serie($2,Simul->poutputs);
};

link_hderm_aq_intro : LEX_LINK_HDERM_AQ LEX_EQUAL LEX_OPENING_BRACE
{
	if (Simul->pcarac_aq == NULL) LP_error(Simul->poutputs,"Link HDERM-AQ impossible. AQ module has not been declared. Check your command file.\n");
}
;

read_hderm_aqs : read_hderm_aq read_hderm_aqs
{
	s_id_io *id_list_tmp;
	id_list_tmp=IO_check_id_list($1,$2,Simul->poutputs);
	$$=IO_secured_chain_bck_id(id_list_tmp,$2);
};
| read_hderm_aq
{
	$$=$1;
};

read_hderm_aq : LEX_INT LEX_INT LEX_INT
{
	int is_outlet = YES;
	s_carac_aq* pcarac_aq = Simul->pcarac_aq;
	s_ele_msh *pele;
	s_id_io *id_list;

	pele = pcarac_aq->pcmsh->p_layer[$3]->p_ele[$2-1];

 	is_outlet = HYD_check_is_element_outlet(Simul->pchyd_hderm,$1,Simul->poutputs); // Check si l'élément rivière est bien exutoire

	if (is_outlet == NO)
		LP_error(Simul->poutputs,"River element GIS ID %d is not an hyperdermic river outlet. It can't be linked with an aquifer cell. Check your HDERM-AQ link file.\n",$1);
    else{
		AQ_init_link_ele(pele,$1,1.,HDERM_AQ,Simul->poutputs);  // Link de l'élément rivière à la maille souterraine
		id_list = IO_create_id($3,($2-1));
		$$ = id_list;
	}

};


/**************************************************************************/
/**************************************************************************/
/*                                                                        */
/*                    T R A N S P O R T   I N P U T S                     */
/*                                                                        */
/* -----------------------------------------------------------------------*/
/*                                                                        */
/*                        Transport-related inputs                        */
/*                                                                        */
/**************************************************************************/
/**************************************************************************/

main_transport : transport_intro species LEX_CLOSING_BRACE
{
	int i, j, nb_BU, nb_cprod, nb_zns, nb_eleaq;
	s_layer_msh* player;
	s_cmsh* pcmsh;
	s_ele_msh* pele;

   	Simul->nb_species = nb_species;
    CAW_print_transport_activation_status(Simul->pset_caw,Simul->poutputs);

	// Display of all attributes of all species read, for all CaWaQS modules.
	if (Simul->pset_caw->transport_caw == YES)
	{
		CAW_display_species_properties(Simul,Simul->poutputs);
		LP_printf(Simul->poutputs,"Properties for %d CaWaQS species have been read and stored.\n",Simul->nb_species);
	}

    // Memory allocations for transport attributes for all species of WATBAL module 
	if (Simul->pset_caw->transport_module[FP_CAW] == YES)
	{  
		nb_BU = Simul->pcarac_fp->nb_BU;
		nb_cprod = Simul->pcarac_fp->nb_cprod;

		for (i=0;i<nb_BU;i++) FP_transport_manage_memory_watbal_ele_bu(Simul->pcarac_fp->p_bu[i],Simul->nb_species,Simul->poutputs);
    	for (i=0;i<nb_cprod;i++) FP_transport_manage_memory_watbal_cprod(Simul->pcarac_fp->p_cprod[i],Simul->nb_species,Simul->poutputs);
	}

    // Memory allocations for transport attributes for all species of NSAT module 
	if (Simul->pset_caw->transport_module[NSAT_CAW] == YES)
	{
		nb_zns = Simul->pcarac_zns->nb_zns;

		for (i=0;i<nb_zns;i++)
			RSV_manage_memory_transport(Simul->pcarac_zns->p_zns[i]->wat_bal,Simul->nb_species,Simul->poutputs);

    // Sets initial layers piles for all reservoirs chains of all ZNS cells
		NSAT_initialize_layer_all_reservoirs(Simul->pcarac_zns,Simul->nb_species,Simul->poutputs);    
		LP_printf(Simul->poutputs,"All layers for NSAT transport calculations have been initialized.\n");
	}

    // Memory allocations for transport attributes for all species of AQ module (concentrations and temperature arrays for each species)
	if (Simul->pset_caw->transport_module[AQ_CAW] == YES)
	{  
		pcmsh = Simul->pcarac_aq->pcmsh;
		nlayer = pcmsh->pcount->nlayer;

		for(i=0;i<nlayer;i++)
        {
            player=pcmsh->p_layer[i];
            nele=player->nele;
            for(j=0;j<nele;j++) {
                pele=player->p_ele[j];
                AQ_transport_attributes_allocation(pele->phydro,Simul->nb_species,Simul->poutputs);
            }
        }
    }
};

transport_intro : LEX_TRANSPORT LEX_EQUAL LEX_OPENING_BRACE
{
    // Initializing general chemical properties matrix
	Simul->chem_param = CAW_init_chem_param(Simul->poutputs);   
}

species : specie species
| specie


specie : specie_intro specie_gen_settings specie_surface specie_nsat specie_hderm specie_aq LEX_CLOSING_BRACE
| specie_intro specie_gen_settings specie_surface LEX_CLOSING_BRACE
| specie_intro specie_gen_settings specie_surface specie_nsat LEX_CLOSING_BRACE
| specie_intro specie_gen_settings specie_surface specie_nsat specie_hderm LEX_CLOSING_BRACE
| specie_intro specie_gen_settings specie_surface specie_nsat specie_aq LEX_CLOSING_BRACE
| specie_intro specie_gen_settings specie_aq LEX_CLOSING_BRACE


specie_intro : LEX_SPECIES LEX_EQUAL LEX_OPENING_BRACE
{
	nb_species++;
    // (Re)-Allocation of memory when each new specie is read.
	Simul->pspecies = CAW_manage_memory_species(Simul->pspecies,nb_species,Simul->poutputs); 
	Simul->chem_param = CAW_manage_memory_pchem_param(Simul->chem_param,nb_species,Simul->poutputs);
};

specie_gen_settings : specie_gen_set_intro specie_gen_sets LEX_CLOSING_BRACE

specie_gen_set_intro : LEX_SPECIES_SETTINGS LEX_EQUAL LEX_OPENING_BRACE

specie_gen_sets : specie_gen_set specie_gen_sets
| specie_gen_set

specie_gen_set : specie_name
| specie_type
| specie_mmass
| element_mmass
| specie_diffmol

specie_name : LEX_SPECIES_NAME LEX_EQUAL LEX_NAME
{
    sprintf(Simul->pspecies[nb_species-1]->name,"%s",$3);
    verif_specie++; 
};

specie_type : LEX_TTC_SPECIE_TYPE LEX_EQUAL LEX_TTC_SPECIE
{
	Simul->pspecies[nb_species-1]->type=$3;
};

specie_mmass : LEX_MOLAR_MASS_SPECIES LEX_EQUAL mesure
{
	int i;
    // General parameters are assigned by default to all modules. Can be specified for each module afterwards.
	for (i=0;i<NMOD_CAW;i++) Simul->chem_param[i][MMASS_SPECIES_CAW][nb_species-1] = $3;
};

element_mmass : LEX_MOLAR_MASS_ELEMENT LEX_EQUAL mesure
{
	int i;
	for (i=0;i<NMOD_CAW;i++) Simul->chem_param[i][MMASS_ELEMENT_CAW][nb_species-1] = $3;
};

specie_diffmol : LEX_DIFF_MOL LEX_EQUAL mesure
{
	int i;
	for (i=0;i<NMOD_CAW;i++) Simul->chem_param[i][DIFF_MOL_CAW][nb_species-1] = $3;
};

/*****************************************************************************
/*****************************************************************************
 *                      S U R F A C E   T R A N S P O R T
 ****************************************************************************
 **************************************************************************** */

specie_surface : specie_surface_intro specie_watbal_settings specie_hyd_settings LEX_CLOSING_BRACE;

specie_surface_intro : LEX_SURFACE_TRANSPORT LEX_EQUAL LEX_OPENING_BRACE
{
	int i;
	module = FP_CAW;
	if (Simul->pcarac_fp == NULL)
		LP_error(Simul->poutputs,"In CaWaQS%4.2f : In file %s at line %d : Can't proceed with %s transport inputs. Module has not been declared for hydro calculations.\n",NVERSION_CAW,__FILE__,__LINE__,CAW_name_mod(module));

	nb_specie_unit = 1; // Reset for each specie

	if (Simul->pset_caw->transport_caw == YES)
		Simul->pset_caw->transport_module[module]=YES;
	
    LP_printf(Simul->poutputs,"Entering surface transport..\n"); 

    // Allocating memory for s_carac_fp transport attributes
    FP_manage_memory_carac_species(Simul->pcarac_fp,nb_species,Simul->poutputs);   	 
};

specie_watbal_settings : omp specie_cells link_specie_grid_ele_bu forcing_conditions transport_boundary
| omp specie_cells link_specie_grid_ele_bu forcing_conditions
| specie_cells link_specie_grid_ele_bu forcing_conditions transport_boundary
| specie_cells link_specie_grid_ele_bu forcing_conditions
;

/******************************************************************************
 *                               SPECIE_CELL file
 *****************************************************************************
 *
 * File containing the GIS IDs of the species cells mesh.
 *
 * WARNING : The order, from first to last needs to be coherent with the way
 * each record in the binary files are organized.
 *
 *****************************************************************************/

specie_cells : specie_cells_intro read_species_cells LEX_CLOSING_BRACE
{
	int i, id_species = nb_species-1;
	s_carac_fp* pcarac_fp = Simul->pcarac_fp;

	// NG : Before storage in pointer table, making sure there are no duplicates in the chain. Error returned if so.
	FP_check_duplicates_specie_unit(id_species,$2,Simul->poutputs);

	pcarac_fp->nb_specie_unit[id_species] = FP_count_specie_units_from_chain($2,Simul->poutputs);
	Simul->clock->time_spent[LEC_CHR] += CHR_end_timer();
	CHR_begin_timer();

	pcarac_fp->p_species_unit[id_species] = FP_tab_specie_unit($2,pcarac_fp->nb_specie_unit[id_species],Simul->poutputs);
 	LP_printf(Simul->poutputs,"Species INTERN ID %d : %d differents specie units avec been defined in the specie_cell file.\n",id_species,pcarac_fp->nb_specie_unit[id_species]); 
	Simul->clock->time_spent[TAB_CHR] += CHR_end_timer();
	CHR_begin_timer();
};

specie_cells_intro : LEX_SPECIES_CELLS LEX_EQUAL LEX_OPENING_BRACE;

read_species_cells : read_species_cell read_species_cells
{
  $$ = FP_secured_chain_specie_unit_fwd($1, $2);

};
| read_species_cell
{
	$$ = $1;
};

read_species_cell : LEX_INT
{
	s_specie_unit_fp* p_speunit = NULL;
	p_speunit = FP_create_specie_unit($1,FP_GIS);
	nb_specie_unit++;
	$$=p_speunit;
}
;

/*****************************************************************************
 *                        SPECIES GRID-ELE_BU LINK FILE
 *****************************************************************************
 *
 * This file is built via GIS by intersecting the geometry mesh related
 * to one specific species with the surface water balance calculation elements
 * (previously determined for the hydro calculations).
 *
 * libspa-type file containing by columns (from first to last) :
 * - ID of the water balance calculation element (ele_bu)
 * - the GIS ID of species grid cell
 * - the intersection area between the two objects (in m2).
 *
 ******************************************************************************/

link_specie_grid_ele_bu : link_specie_grid_ele_bu_intro links_grid_ele_bu LEX_CLOSING_BRACE
{
	int i, id_species = nb_species-1;
	s_carac_fp* pcarac_fp = Simul->pcarac_fp;
	s_specie_unit_fp *pchain = NULL, *ptemp = NULL;

	// NG check : making sure everything is well stored and defined once the tabular of specie unit is filled up
   // FP_print_species_unit_tab(pcarac_fp->p_species_unit[id_species],pcarac_fp->nb_specie_unit[id_species],id_species,Simul->poutputs);

	Simul->clock->time_spent[TAB_CHR] += CHR_end_timer();
	CHR_begin_timer();

	// NG check : Verifying links between ele_bu and specie grid for the specie being read. Displays warnings if bu cells are not covered entirely.
	//FP_check_BU_species_links(Simul->pcarac_fp,nb_species,Simul->poutputs);
};


link_specie_grid_ele_bu_intro : LEX_LINK_SPECIE_GRID_ELEBU LEX_EQUAL LEX_OPENING_BRACE
{
  int i, nb_BU=Simul->pcarac_fp->nb_BU;

  for (i=0;i<nb_BU;i++) 
    FP_manage_memory_links_ele_bu_species(Simul->pcarac_fp->p_bu[i],nb_species,Simul->poutputs);
};


links_grid_ele_bu : link_grid_ele_bu links_grid_ele_bu
| link_grid_ele_bu

link_grid_ele_bu : LEX_INT LEX_INT flottant
{
	int id_species = nb_species-1;
	int i;
	int id_bu = $1-1; // An 'ele_bu' does not have a GIS ID, only a INTERN ID, which starts at 0.
	int nb_spe_units = Simul->pcarac_fp->nb_specie_unit[id_species];
	int nb_BU = Simul->pcarac_fp->nb_BU;
	s_id_spa* pid, *pid_temp;
	s_specie_unit_fp *p_speunit=NULL, *punit=NULL;
	s_bal_unit_fp* p_balunit=NULL;
	s_bal_unit_fp** tab_watbal = Simul->pcarac_fp->p_bu;
	s_specie_unit_fp** tab_speunit = Simul->pcarac_fp->p_species_unit[id_species];

	// Checking, based on tabular of pointers that the two elements to be linked do exist.
	p_balunit = FP_check_is_ele_bu_defined(tab_watbal,nb_BU,$1,Simul->poutputs);
	punit = FP_check_is_ele_specie_unit_defined(tab_speunit,nb_spe_units,$2,Simul->poutputs);

	// NG : Filling **link for each ele_bu
	pid = SPA_create_ids($2,$3/p_balunit->area);

	if (p_balunit->link[id_species] != NULL){
		p_balunit->link[id_species] = SPA_browse_id(p_balunit->link[id_species],END_SPA,Simul->poutputs);
		p_balunit->link[id_species] = SPA_secured_chain_id_fwd(p_balunit->link[id_species],pid);
	}
	else
		p_balunit->link[id_species] = pid;

	punit->area+=$3;
};


/******************************************************************************
 *                            FORCING CONDITIONS
 *****************************************************************************
 *
 * Sub-block defining all settings, parameters and inputs conditions (forcings)
 * of the surface compartment
 *
 ******************************************************************************/

forcing_conditions : forcing_intro forcing_settings LEX_CLOSING_BRACE
{
	int id_species = nb_species-1;
    FP_check_input_transport_setup(Simul->pcarac_fp->pinputs,id_species,Simul->poutputs);  
};

forcing_intro : LEX_TRANSPORT_FORCING LEX_EQUAL LEX_OPENING_BRACE
{
	FP_manage_memory_input_species(Simul->pcarac_fp,nb_species,Simul->poutputs);     // Allocation de mémoire pour les attributs transport de s_input_fp
};

forcing_settings : forcing_setting forcing_settings
| forcing_setting

forcing_setting : LEX_FORCING_TYPE LEX_EQUAL LEX_TRANSPORT_INPUT
{
	Simul->pcarac_fp->pinputs->transport_forcing_conditions[nb_species-1]=$3;
}
| LEX_FORMAT_TYPE LEX_EQUAL LEX_FORMAT
{
    Simul->pcarac_fp->pinputs->transport_input_format[nb_species-1]=$3;
}
| forcing_prefixes
| LEX_TRANSPORT_FORCING_PATH LEX_EQUAL LEX_NAME
{
   Simul->pcarac_fp->pinputs->transport_paths[nb_species-1]=$3;
}

forcing_prefixes : forcing_prefixe forcing_prefixes
| forcing_prefixe

forcing_prefixe : LEX_PREF LEX_EQUAL mesure LEX_NAME   // 'mesure' corresponds to the unit
{
	Simul->pcarac_fp->pinputs->transport_input_prefixes[nb_species-1][$1] = $4;
    // Storing the multiplication factor so it can be directly be applied when forcing files are read (to ensure unit consistency with boundaries units)
	Simul->pcarac_fp->pinputs->forcing_unit[nb_species-1][$1] = $3;  
};



/*****************************************************************************
 *                           FORCING BOUNDARY
 *****************************************************************************
 *
 * Here, are defined the forcing time series which will be used in case part of the
 * surface domain (i.e. ele_bu's) is not covered by the specie forcing grid.
 * This file is structured as a source-term like file (multiple time series defined
 * for one or multiple water balance cells.
 *
 ******************************************************************************/

transport_boundary : intro_trans_bound trans_bounds LEX_CLOSING_BRACE
{
	int id_species=nb_species-1;

    // NG check : printing out all bounds attributes to make sure it's been stored properly
    //	FP_print_forcing_boundary_one_specie(Simul->pcarac_fp, id_species,Simul->poutputs);  
	LP_printf(Simul->poutputs,"Transport forcing boundaries all set for specie named %s\n",Simul->pspecies[id_species]->name);
};

intro_trans_bound : LEX_TRANSPORT_BOUND LEX_EQUAL LEX_OPENING_BRACE
{
	int i;
	int nb_bu=Simul->pcarac_fp->nb_BU;
	for (i=0;i<nb_bu;i++) FP_manage_memory_boundary_ele_bu(Simul->pcarac_fp->p_bu[i],nb_species,Simul->poutputs);
};

trans_bounds : trans_bound trans_bounds
| trans_bound;

trans_bound : kindof_trans_bound trans_bound_series LEX_CLOSING_BRACE

kindof_trans_bound : LEX_PREF mesure LEX_EQUAL LEX_OPENING_BRACE  
{
	kindof_forcing = $1;
    multip_fact_ts = $2;
	LP_printf(Simul->poutputs,"Setting boundaries for forcing variable : %s...\n",FP_transport_input_variable_name(kindof_forcing));
};


trans_bound_series : trans_bound_serie trans_bound_series
| trans_bound_serie

trans_bound_serie : LEX_OPENING_BRACE elebu_ids LEX_CLOSING_BRACE time_serie 
{
	int id_species=nb_species-1;
	s_id_io* id_list=$2;

    // Converting the bound time serie from its input unit to the SI/cawaqs unit
	pft = TS_multiply_ft_double(pft,multip_fact_ts); 	

    // Assigning the bound to the wat_bal unit
	FP_define_forcing_boundary(Simul->pcarac_fp,id_species,kindof_forcing,$2,pft,Simul->poutputs); 
	IO_free_id_serie(id_list,Simul->poutputs);
};

elebu_ids : elebu_id elebu_ids
{
	$$=IO_chain_bck_id($1,$2);
}
| elebu_id
{
	$$=$1;
};

elebu_id : LEX_INT
{
	$$ = IO_create_id(0,$1);
};


/*****************************************************************************
/*****************************************************************************
 *   H Y D R A U L I C   T R A N S P O R T  (both for HYD and HDERM modules)
 *****************************************************************************
 ***************************************************************************** */

/* Reminder : The 'specie_hyd_settings' block is common to HYD and
              HDERM modules --> Beware of the current value of 'module' */

specie_hyd_settings : specie_hyd_intro specie_hyd_sets LEX_CLOSING_BRACE

specie_hyd_intro : LEX_HYD_TRANSPORT LEX_EQUAL LEX_OPENING_BRACE
{
	int r, ne, id_species=nb_species-1;
    s_reach_hyd *preach;
    s_element_hyd *pele;
    
    // So we can distinguish calls of 'specie_hyd_intro' for HYD or HDERM
    if (module == FP_CAW) module = HYD_CAW;   

	if ((module == HYD_CAW && Simul->pchyd == NULL) || (module == HDERM_CAW && Simul->pchyd_hderm == NULL))
		LP_error(Simul->poutputs,"In CaWaQS%4.2f : In file %s at line %d : Cannot proceed with %s transport inputs. Module has not been declared for hydro calculations.\n",NVERSION_CAW,__FILE__,__LINE__,CAW_name_mod(module));

	if (Simul->pset_caw->transport_caw == YES){
	
	    Simul->pset_caw->transport_module[module]=YES;
		LP_printf(Simul->poutputs,"Entering %s transport...\n",CAW_name_mod(module));
	
        switch (module)
        {
            case HYD_CAW: // NG : 07/06/2023 : Revising allocation of transp_var array so it accounts for multispecie, HYD transp_var array is allocated here, otherwise there is nowhere to fill transp_var at initialization
                for (r = 0; r < Simul->pchyd->counter->nreaches; r++){
                    preach = Simul->pchyd->p_reach[r];
                    for (ne = 0; ne < preach->nele; ne++){
                        pele=preach->p_ele[ne];
                        HYD_manage_memory_transport(pele,nb_species,Simul->poutputs);
                    }
                }
                break;
            case HDERM_CAW: // Allocating memory for transport attributes for watbal_rsv for all HDERM cprod cells and all river elements, for all species
	        
                nb_cprod = Simul->pcarac_fp_hderm->nb_cprod;
                for (i=0;i<nb_cprod;i++) FP_transport_manage_memory_watbal_cprod(Simul->pcarac_fp_hderm->p_cprod[i],Simul->nb_species,Simul->poutputs);
    	
                for (r = 0; r < Simul->pchyd_hderm->counter->nreaches; r++){
                    preach = Simul->pchyd_hderm->p_reach[r];
                    for (ne = 0; ne < preach->nele; ne++){
                        pele=preach->p_ele[ne];
                        HYD_manage_memory_transport(pele,nb_species,Simul->poutputs);
                    }
                }
                break;
            default:
                LP_error(Simul->poutputs,"In CaWaQS%4.2f : in %s, line %d : Input inconsistency when searching for module %s. Check your .COMM file.\n",NVERSION_CAW,__FILE__,__LINE__,CAW_name_mod(module));
        }
        
      
	    // Initializes *carac_ttc parameters and **species_ttc, each time a new species is being read, later completed with user ttc-specific parametrization
        if (Simul->pset_caw->transport_module[module] == YES) 
            CAW_initialize_TTC_carac_species(Simul,module,nb_species,iappli_gc_ttc,ttstep,Simul->poutputs); // ttstep is a AR DK addition.

        LP_printf(Simul->poutputs,"In CaWaQS%4.2f : Specie ID : %d - Module : %s = Current Iappli GC number : %d.\n",NVERSION_CAW,nb_species-1,CAW_name_mod(module),iappli_gc_ttc);

        iappli_gc_ttc++;

        if (iappli_gc_ttc == HDERM_GC) iappli_gc_ttc++;  // HDERM_GC is dedicated to rier system calculations in HDERM network  
        	
    }
};

specie_hyd_sets : omp ttc_settings
| omp ttc_settings def_inflows
| ttc_settings
| ttc_settings def_inflows
| omp ttc_settings forcing_meteo
| omp ttc_settings set_init_riv
| omp ttc_settings set_init_riv forcing_meteo

ttc_settings : ttc_species_settings_intro ttc_attributes LEX_CLOSING_BRACE

ttc_species_settings_intro : LEX_TTC_SET_INTRO LEX_EQUAL LEX_OPENING_BRACE

ttc_attributes : ttc_attribute ttc_attributes
| ttc_attribute

ttc_attribute : ttc_specie_iter_max
| ttc_specie_eps
| ttc_specie_calc_process
| ttc_diffmol			// si on veut préciser la valeur par spécie et par compartiment (réécrase la valeur par défaut)
| ttc_aquitard
| ttc_aquitard_sol    		// The aquitard solution type (Implicit: within coeff matrix; Explicit: outside coeff matrix)

ttc_specie_iter_max : LEX_TTC_ITER_MAX LEX_EQUAL LEX_INT
{
	if (Simul->pset_caw->transport_module[module] == YES)
		Simul->pcarac_ttc[module]->p_species[nb_species-1]->pset->iteration=$3;
};

ttc_specie_eps : LEX_TTC_EPS LEX_EQUAL mesure
{
	if (Simul->pset_caw->transport_module[module] == YES)
		Simul->pcarac_ttc[module]->p_species[nb_species-1]->pset->crconv=$3;
};

ttc_specie_calc_process : LEX_TTC_PROCESS LEX_EQUAL LEX_ANSWER
{
   if (Simul->pset_caw->transport_module[module] == YES)
	Simul->pcarac_ttc[module]->p_species[nb_species-1]->pset->calc_process[$1] = $3;
 	
    // NG : 04/04/2023 : Quick display upgrade
	LP_printf(Simul->poutputs,"Calculation in module %s for process %s : %s\n",CAW_name_mod(module), TTC_name_process($1), LP_answer($3,Simul->poutputs)); 
};

ttc_diffmol : LEX_DIFF_MOL LEX_EQUAL mesure
{
	if (Simul->pset_caw->transport_module[module] == YES)
		Simul->chem_param[module][DIFF_MOL_CAW][nb_species-1] = $3;
};


ttc_aquitard : LEX_AQUITARD_ACTIVE LEX_EQUAL LEX_ANSWER
{
	if (Simul->pset_caw->transport_module[module] == YES)
		if ( $3 == YES)
 			Simul->pcarac_ttc[module]->p_species[nb_species-1]->pset->aquitard = YES_TTC;
 			LP_printf( Simul->poutputs,"DKDK Aquitard solution Open Or Not %d YES: %d , Answer: %d On_Off: %d  \n",module, YES,   $3 , YES_TTC);
};


ttc_aquitard_sol : LEX_AQUITARD_SOL_TYPE LEX_EQUAL LEX_ANSWER
{
	if (Simul->pset_caw->transport_module[module] == YES)
 		LP_printf( Simul->poutputs,"DKDK Aquitard solution explicit %d for %d  \n",module,   $3);
};


/****************************************************************************/
/*          DK 10/11/2021 Transport river network initialization            

NG : 04/04/2023
NB : As hydro->transp_var is directly filled here, init file should contain, for each 
     river calculation element, an init, either being temperature (in K)
     or a concentration (in g/m3).

/****************************************************************************/

// NG : 04/04/2023 : Some rubbish cleaned. Partly rewritten.
/* NG : 06/11/2024 : Simplifying. Moving from INIT = { INIT_C = {} } to INIT_VAR = {} */

set_init_riv : intro_descr_init_riv_t init_riv_values LEX_CLOSING_BRACE
{
    LP_printf(Simul->poutputs,"Transport initialization for %s module done.\n",CAW_name_mod(module));
};

intro_descr_init_riv_t : LEX_INIT_VAR_ATT LEX_EQUAL LEX_OPENING_BRACE   

init_riv_values : init_riv_value init_riv_values
|init_riv_value
;

init_riv_value : LEX_INT flottant
{
  s_element_hyd *pele;
  //int id_species = 0; // NG fix : 04/04/2023 : Not suitable for multispecie ! Use the current specie ID
  int id_species = nb_species-1;

  pele = HYD_check_is_ele_defined(Simul->pchyd, $1, GIS_HYD, Simul->poutputs); 

  if (Simul->pset_caw->transport_caw == YES)
    pele->center->hydro->transp_var[id_species] = $2;
};


/****************************************************************************/
/***                 SW 13/07/2021 coupling libseb in cawaqs              ***/
/*** Safran accessing: (i) list of safran cells, (ii) Directory that contains
/*** all the Safran's files, (iii) the file that makes reaches and safran cells
/*** correspond to each other, (iv) the optional file containing for each reach
/*** the vegetal cover or some other zone parameters.                     ***/
/****************************************************************************/
forcing_meteo : intro_meteo_safran args_forcing_mto LEX_CLOSING_BRACE
{
 // Initialization of meteo cells should be handled here DK 09 05 2022
}
;

arg_forcing_mto : list_safran_cells
| saccess
| view2sky
;

args_forcing_mto : arg_forcing_mto args_forcing_mto
| arg_forcing_mto
;


intro_meteo_safran : LEX_FORCING_MTO LEX_EQUAL LEX_OPENING_BRACE
{
    Simul->mto_seb = SEB_init_meteodata();
    Simul->pset_caw->transport_module[SEB_CAW] = YES;
    LP_printf(Simul->poutputs,"Module %s activated.\n",CAW_name_mod(SEB_CAW));
};


list_safran_cells : intro_list_safran ids_safran
{
    int ne;
    s_id_io* id_list=$2;
    // SW 13/07/2021 if we want to store this list, add here
    Simul->mto_seb->id_meteo = (int **)malloc(Simul->mto_seb->n_meteocell*sizeof(int *));

    for(ne = 0; ne < Simul->mto_seb->n_meteocell; ne++)
    {
        Simul->mto_seb->id_meteo[ne] = (int *)malloc(NID_SEB*sizeof(int ));
        Simul->mto_seb->id_meteo[ne][INTERN_SEB] = Simul->mto_seb->id_meteo[ne][ABS_SEB] = ne;
        Simul->mto_seb->id_meteo[ne][GIS_SEB] = id_list->id;
        id_list = id_list->next;
    }

    //id_list = IO_browse_id(id_list, BEGINNING_IO);
    //IO_free_id_serie(id_list,Simul->poutputs);
    LP_printf(Simul->poutputs, "Safran cells list is read\n");
}
;
intro_list_safran : LEX_SAFRAN_CELLS LEX_EQUAL
;
ids_safran : id_safran ids_safran
{
    $$=IO_chain_bck_id($1,$2);
}
| id_safran
{
    $$ = $1;
}
;
id_safran : LEX_INT
{
    Simul->mto_seb->n_meteocell++;
    $$ = IO_create_id(0,$1);
}
;

view2sky : LEX_VIEWTOSKY LEX_EQUAL flottant
{
 	Simul->mto_seb->viewToSky = $3;
 	LP_printf(Simul->poutputs,"LIBSEB INIT: viewToSky factor: %lf" , Simul->mto_seb->viewToSky);

}
;

saccess : corresp meteo_accesses // "riverside_param" to possibly add
{
}
;

corresp : intro_corresp ones_corresp
{
    int nr, nreach;
    int id_abs = CODE_TS;
    s_rts *prts;

    nreach = Simul->pchyd->counter->nreaches;
    if(nreach_count_file != nreach)
        LP_error(Simul->poutputs,"The number of extracted safran's correspondencies does not match the number of reaches...\n");

    Simul->mto_seb->p_rts = SEB_alloc_rts(nreach);
    prts = $2;
//    for(nr = 0; nr < nreach; nr++)
//    {
//        /** SW 13/07/2021 voie is the absolute reach id. **/
//        Simul->mto_seb->p_rts[prts->pid_reach->voie -1] = prts;
//        prts = prts->next;
//    }


//SW voie is the reach GIS_HYD id
	while ( prts != NULL )
    	{
    		for ( nr = 0; nr < Simul->pchyd->counter->nreaches; nr++ )
    		 {
    		 	if ( Simul->pchyd->p_reach[nr]->id[GIS_HYD] == prts->pid_reach->voie )
    		 	{
    		 		id_abs = Simul->pchyd->p_reach[nr]->id[ABS_HYD];
				Simul->mto_seb->p_rts[id_abs] = prts;
//				LP_printf(Simul->poutputs, "id_abs = %d voie = %d\n", id_abs, prts->pid_reach->voie);
				break;
    		 	}
    		 }
    	    	prts = prts->next;
            	id_abs = CODE_TS;
    	}


}
;
intro_corresp : LEX_CORRESP LEX_EQUAL
;

ones_corresp : one_corresp ones_corresp
{

    $$ = SEB_chain_bck_rts($1, $2);
}
| one_corresp
{
    $$ = $1;
}
;

/*** SW 13/07/2021 read reach safran corresponding file ***/
/***       from node; to node; id_reach; id_safran      ***/

one_corresp : LEX_NAME LEX_NAME LEX_INT LEX_INT
{
    $$ = SEB_create_rts($1, $2, $3, $4);
    nreach_count_file++;
}
| LEX_INT LEX_INT LEX_INT LEX_INT
{
    char from_node[STRING_LENGTH_LP], to_node[STRING_LENGTH_LP];
    sprintf(from_node, "%d", $1);
    sprintf(to_node, "%d", $2);
    $$ = SEB_create_rts(from_node, to_node, $3, $4);
    nreach_count_file++;
}
| LEX_NAME LEX_INT LEX_INT LEX_INT
{
    char to_node[STRING_LENGTH_LP];
    sprintf(to_node, "%d", $2);

    $$ = SEB_create_rts($1, to_node, $3, $4);
    nreach_count_file++;
}
| LEX_INT LEX_NAME LEX_INT LEX_INT
{
    char from_node[STRING_LENGTH_LP];
    sprintf(from_node, "%d", $1);

    $$ = SEB_create_rts(from_node, $2, $3, $4);
    nreach_count_file++;
}
;
meteo_accesses : meteo_access meteo_accesses
| meteo_access
;
meteo_access : meteo_path
| year_init_meteo
| meteo_data
;

meteo_path : LEX_FOLDER LEX_EQUAL LEX_NAME
{
  int noname;
  char new_name[STRING_LENGTH_LP];

  sprintf(new_name,"FOLDER_SAFRAN=%s",$3);
  sprintf(Simul->mto_seb->folder,"%s",$3);
  printf("%s\n",new_name);
  noname = putenv(new_name);
  if (noname == -1)
    LP_error(Simul->poutputs,"File %s, line %d : undefined variable FOLDER_SAFRAN\n",current_read_files[pile].name,line_nb);
 LP_printf(Simul->poutputs, "meteo path folder read : %s\n",getenv("FOLDER_SAFRAN"));

}
;
year_init_meteo : LEX_YEAR0_METEO LEX_EQUAL LEX_INT
{
  Simul->mto_seb->year0_meteo = $3; // default 1850

  /* SW 13/07/2021 add here the calculation of julian day of begin and end dates, with origin = LEX_YEAR0_METEO.
  These julian day are used to extract the meteo data. it is nice to save this year0_meteo.*/

  Simul->mto_seb->t_extrema = (double*)malloc(NEXTREMA_TS*sizeof(double));

  if(Simul->chronos->pd[BEGINNING_TS]->yyyy == 0) // pd (s_date_ts) is not defined
  {
     Simul->chronos->pd[BEGINNING_TS]->yyyy = Simul->chronos->year[BEGINNING_TS];
     Simul->chronos->pd[END_TS]->yyyy = Simul->chronos->year[END_TS];

     Simul->chronos->pd[BEGINNING_TS]->mm = 8; // 08-01
     Simul->chronos->pd[BEGINNING_TS]->dd = 1;

     Simul->chronos->pd[END_TS]->mm = 7; //07-31
     Simul->chronos->pd[END_TS]->dd = 31;

  }

  Simul->mto_seb->t_extrema[BEGINNING_TS] = TS_date2julian_dd_hm(Simul->chronos->pd[BEGINNING_TS],Simul->mto_seb->year0_meteo,Simul->poutputs);
  Simul->mto_seb->t_extrema[END] = TS_date2julian_dd_hm(Simul->chronos->pd[END_TS],Simul->mto_seb->year0_meteo,Simul->poutputs);

}
;

meteo_data : LEX_METEO_DATA LEX_EQUAL brace sfiles brace
;

sfiles : sfile sfiles
| sfile
;

sfile : LEX_MET LEX_EQUAL LEX_NAME
{
  char name[10*STRING_LENGTH_LP], name2[STRING_LENGTH_LP];
  FILE *fp;
  long n_reach, n_safran;
  double t_beg, t_end;

  n_reach = Simul->pchyd->counter->nreaches; //n_reach = N_REACH;
  n_safran = Simul->mto_seb->n_meteocell;
  t_beg = Simul->mto_seb->t_extrema[BEGINNING_TS];
  t_end = Simul->mto_seb->t_extrema[END_TS];
  //sprintf(name,"%s",$3);
  //LP_printf(Simul->poutputs, "folder = %s\n",getenv("FOLDER_SAFRAN"));
  //sprintf(name,"%s/%s",getenv("FOLDER_SAFRAN"),$3);
  sprintf(name,"%s/%s",Simul->mto_seb->folder,$3);
  fp=LP_openr_file(name,Simul->poutputs,ERR_LP);

  printf("Reading meteo variable: %s\n",SEB_name_meteo($1));

  if (check == 0)
	Simul->mto_seb->p_met = SEB_alloc_met(n_safran);
  check = 0;
  if ($1 == P_ATM)
	CAW_Patm_reading_in_inputy(check, fp, n_reach, n_safran, $1, Simul->mto_seb->p_rts, Simul->mto_seb->p_met, Simul->poutputs);
  else
	CAW_safransreading_in_inputy(check, fp, n_reach, n_safran, $1, t_beg, t_end, Simul->mto_seb->p_rts, Simul->mto_seb->p_met, Simul->poutputs);
  fclose(fp);
  printf("%s closed\n\n",$3);
  check++;
}
;

/*****************************************************************************
/*****************************************************************************
 *                      N O N S A T   T R A N S P O R T
 ****************************************************************************
 **************************************************************************** */

specie_nsat : specie_nsat_intro specie_nsat_settings LEX_CLOSING_BRACE

specie_nsat_intro : LEX_NONSAT_TRANSPORT LEX_EQUAL LEX_OPENING_BRACE
{
	module=NSAT_CAW;
	if (Simul->pcarac_zns == NULL)
		LP_error(Simul->poutputs,"CaWaQS%4.2f : In file %s at line %d : Cannot proceed with %s transport inputs. Module has not been declared for hydro calculations.\n",NVERSION_CAW,__FILE__,__LINE__,CAW_name_mod(module));

	if (Simul->pset_caw->transport_caw == YES)
		Simul->pset_caw->transport_module[module]=YES;
};

specie_nsat_settings : omp


/*****************************************************************************
/*****************************************************************************
 *                      H Y P E R D E R M I C  T R A N S P O R T
 ****************************************************************************
 **************************************************************************** */

specie_hderm : specie_hderm_intro specie_hderm_settings LEX_CLOSING_BRACE

specie_hderm_intro : LEX_HDERM_TRANSPORT LEX_EQUAL LEX_OPENING_BRACE
{
	module=HDERM_CAW;
	if (Simul->pset_caw->transport_caw == YES)
		Simul->pset_caw->transport_module[module]=YES;

	if (Simul->pcarac_fp_hderm == NULL)
		LP_error(Simul->poutputs,"CaWaQS%4.2f : In file %s at line %d : Cannot proceed with %s transport inputs. Module has not been declared for hydro calculations.\n",NVERSION_CAW,__FILE__,__LINE__,CAW_name_mod(module));
};

specie_hderm_settings : omp specie_hyd_settings;
| specie_hyd_settings;

/*****************************************************************************
/*****************************************************************************
 *                        A Q U I F E R  T R A N S P O R T
 ****************************************************************************
 **************************************************************************** */

/* NG : 05/04/2023 : Rewritten part.
  Misconception from DK/Bug fix. SPECIES_AQUIFER is meant to be embedded in the SPECIES block for each specie. 
  Therefore they cannot be "setup_species : setup_specie setup_species" type rules. */

specie_aq : specie_aq_intro setup_species_aq LEX_CLOSING_BRACE

specie_aq_intro : LEX_AQ_TRANSPORT LEX_EQUAL LEX_OPENING_BRACE
{
	module = AQ_CAW;
    int id_specie = nb_species-1;

	if (Simul->pcarac_aq == NULL)
		LP_error(Simul->poutputs,"In CaWaQS%4.2f : In file %s at line %d : Cannot proceed with %s transport inputs. Module has not been declared for hydro calculations.\n",NVERSION_CAW,__FILE__,__LINE__,CAW_name_mod(module));

	if (Simul->pset_caw->transport_caw == YES){
		Simul->pset_caw->transport_module[module] = YES;
        LP_printf(Simul->poutputs,"Entering %s transport for specie : %d (name : %s, current Iappli GC number : %d)\n",CAW_name_mod(module),id_specie,Simul->pspecies[id_specie]->name,iappli_gc_ttc); 
        CAW_initialize_TTC_carac_species(Simul,module,nb_species,iappli_gc_ttc,ttstep,Simul->poutputs);  
	}

    iappli_gc_ttc++;
    if (iappli_gc_ttc == HDERM_GC) iappli_gc_ttc++;  // HDERM_GC is dedicated to hydro calculations in HDERM network
};

setup_species_aq : omp ttc_settings setup_layers_transp
| ttc_settings setup_layers_transp

setup_layers_transp :  intro_set setup_layers_transp_aqs LEX_CLOSING_BRACE

setup_layers_transp_aqs : setup_layers_transp_aq setup_layers_transp_aqs
| setup_layers_transp_aq
;

setup_layers_transp_aq : set_layer_transp_intro sets_lay_aq_transp LEX_CLOSING_BRACE

set_layer_transp_intro : LEX_SETUP_LAYER LEX_EQUAL LEX_OPENING_BRACE LEX_NAME
{
	int total_layer = Simul->pcarac_aq->pcmsh->pcount->nlayer; 
    nlayer = MSH_get_layer_rank_by_name(Simul->pcarac_aq->pcmsh,$4,Simul->poutputs);
};


sets_lay_aq_transp : set_lay_aq_transp sets_lay_aq_transp
| set_lay_aq_transp
;

set_lay_aq_transp : set_param_aq_transp 
| set_aquitard_aq_transp
| set_init_aq_transp
| set_source_aq_transp
| set_bound_aq_transp 
;

set_param_aq_transp : intro_param_t att_t_series LEX_CLOSING_BRACE

intro_param_t : LEX_PARAM LEX_EQUAL LEX_OPENING_BRACE
{
	int i, nelelay;

	//LP_printf(Simul->poutputs,"Current layer is %d \n", nlayer);
	nelelay=Simul->pcarac_aq->pcmsh->p_layer[nlayer]->nele;
	Simul->pcarac_aq->pcmsh->pcount->pcount_ttc = TTC_create_counter(); // Opens the counter to add the boundary and source cell id's

	for (i=0;i<nelelay;i++){
		Simul->pcarac_aq->pcmsh->p_layer[nlayer]->p_ele[i]->pparam_ttc = new_param_ttc(); // DK AR 25 08 2021 Modification of param strcutre, allocate for each cell
	//	LP_printf(Simul->poutputs,"param initalized for layer: %d, ele: %d  \n",nlayer, i);
    }
};

att_t_series : att_t_serie att_t_series
| att_t_serie
;

// NG 05/04/2023 : Adding checks to make sure amount of parameter values is consistent with nb of elements in current layer
att_t_serie : intro_descr_t_base t_trans_base_values LEX_CLOSING_BRACE  // Parameters useful for T and C transport
{
    int nl = Simul->pcarac_aq->pcmsh->p_layer[nlayer]->nele;
    if (verif_ele != nl)
        LP_error(Simul->poutputs,"Number of %s values for layer %d non consistent with number of aquifer cells : %d values detected (%d cells).\n",TTC_param_syst(kindof_attribut),nlayer,verif_ele,nl);
}
| intro_descr_t t_trans_values LEX_CLOSING_BRACE // Parameters useful for T transport only
{
    int nl = Simul->pcarac_aq->pcmsh->p_layer[nlayer]->nele;
    if (verif_ele != nl)
        LP_error(Simul->poutputs,"Number of %s values for layer %d non consistent with number of aquifer cells : %d values detected (%d cells).\n",TTC_thermic_param(kindof_attribut),nlayer,verif_ele,nl);
};

intro_descr_t_base : LEX_TRANSPORT_BASE_ATT LEX_EQUAL LEX_OPENING_BRACE
{   
    verif_ele = 0; // Number of parameter values check reset
    kindof_attribut = $1;
	LP_printf(Simul->poutputs,"Attributing %s base parameter for layer %d\n",TTC_param_syst(kindof_attribut), nlayer);
};

intro_descr_t : LEX_TRANSPORT_ATT LEX_EQUAL LEX_OPENING_BRACE
{
    verif_ele = 0; 
    kindof_attribut = $1;
    LP_printf(Simul->poutputs,"Attributing %s thermic parameter for layer %d\n",TTC_thermic_param(kindof_attribut), nlayer);
};

t_trans_base_values : t_trans_base_value t_trans_base_values
| t_trans_base_value
;

t_trans_base_value : LEX_INT flottant
{
	CAW_init_TTC_ele_NPARAM_SYST(Simul->pcarac_aq->pcmsh->p_layer[nlayer]->p_ele[$1],kindof_attribut,$2,Simul->poutputs);
	verif_ele++;
};

t_trans_values : t_trans_value t_trans_values
|t_trans_value
;

t_trans_value : LEX_INT flottant
{
    int id;
    s_ele_msh *pele;

    pele = Simul->pcarac_aq->pcmsh->p_layer[nlayer]->p_ele[$1];
    id = MSH_get_ele_id(pele,ABS_MSH);

    CAW_init_TTC_ele_NPARAM_THERM(Simul->pcarac_aq->pcmsh->p_layer[nlayer]->p_ele[$1],SOLID_TTC,kindof_attribut,id,$2,Simul->poutputs);
    verif_ele++;
};

set_init_aq_transp : intro_descr_init_t init_values LEX_CLOSING_BRACE
{
    int nl = Simul->pcarac_aq->pcmsh->p_layer[nlayer]->nele;
    if (verif_ele != nl)
        LP_error(Simul->poutputs,"Number of initialization values for layer %d non consistent with number of aquifer cells : %d values detected (%d cells).\n",nlayer,verif_ele,nl);
};

intro_descr_init_t : LEX_INIT_VAR_ATT LEX_EQUAL LEX_OPENING_BRACE  
{
    int i, nelelay;
    nelelay = Simul->pcarac_aq->pcmsh->p_layer[nlayer]->nele;

	verif_ele = 0; // Reset from previous calls
    for (i = 0; i < nelelay; i++){
	    Simul->pcarac_aq->pcmsh->p_layer[nlayer]->p_ele[i]->pval_ttc=TTC_init_val(1);  
    }
};

init_values : init_value init_values
|init_value
;

init_value : LEX_INT flottant
{
    verif_ele++;
    CAW_init_TTC_ele_INIT(Simul->pcarac_aq->pcmsh->p_layer[nlayer],$1,$2,Simul->poutputs);
};


/*
Tokens for aquitard at the bottom of the aquifer layer
Input reading aquitard:
Variables read: Thickness, Heat conductivity, Volumetric Heat capacity of the aquitard layer

DK 25 11 2021
// */
set_aquitard_aq_transp : intro_aquitard_t_bot att_aquitard_series LEX_CLOSING_BRACE
| intro_aquitard_t_top att_aquitard_series LEX_CLOSING_BRACE
| intro_riverbed_t att_aquitard_series LEX_CLOSING_BRACE
;

intro_aquitard_t_bot : LEX_AQUITARD_BOTTOM LEX_EQUAL LEX_OPENING_BRACE
{
 int i,nelelay,idir,itype;
 type_input = 0;  // input type 0 enables aquitard input & input type 2 enables riverbed input


  nelelay=Simul->pcarac_aq->pcmsh->p_layer[nlayer]->nele;
  for (i=0;i<nelelay;i++) {

	Simul->pcarac_aq->pcmsh->p_layer[nlayer]->p_ele[i]->paquitard_ttc = new_aquitard_ttc();  //	NG : 24/11/2020 DK AR 25 08 2021 changes in the pvar structure
	int id = MSH_get_ele_id(Simul->pcarac_aq->pcmsh->p_layer[nlayer]->p_ele[i], ABS_MSH);
	Simul->pcarac_aq->pcmsh->p_layer[nlayer]->p_ele[i]->paquitard_ttc->active = 0; // Initialize with no aquitard

//	LP_printf(Simul->poutputs," Inside AQUITARD, naquitard is %d \n", naquitard);
  }


	LP_printf(Simul->poutputs," Final number of AQUITARD cells is %d \n", Simul->pcarac_ttc[module]->count[NAQUITARD_TTC]); // DK Check
};

intro_aquitard_t_top : LEX_AQUITARD_TOP LEX_EQUAL LEX_OPENING_BRACE
{
 int i,nelelay,idir,itype;
 type_input = 0;

  nelelay=Simul->pcarac_aq->pcmsh->p_layer[nlayer]->nele;
  for (i=0;i<nelelay;i++) {

	Simul->pcarac_aq->pcmsh->p_layer[nlayer]->p_ele[i]->paquitard_ttc = new_aquitard_ttc();  //	NG : 24/11/2020 DK AR 25 08 2021 changes in the pvar structure

	int id = MSH_get_ele_id(Simul->pcarac_aq->pcmsh->p_layer[nlayer]->p_ele[i], ABS_MSH);
//	LP_printf(Simul->poutputs," Inside AQUITARD TOP  \n");
  }

//  	LP_printf(Simul->poutputs," Final number of AQUITARD cells is %d \n", naquitard); // DK Check

};


intro_riverbed_t : LEX_RIVERBED LEX_EQUAL LEX_OPENING_BRACE
{
 int i,nelelay,idir,itype;
 type_input = 1;

 preach_bat = pgen_chyd->p_reach[0];
 pface = pgen_chyd->p_reach[0]->p_faces[0];
// HYD_print_faces(pface, Simul->poutputs);

 while ( pface->next != NULL ) {
	pface->hydro->param_riverbed=(double*)calloc(2,sizeof(double)); // Allocate for the parameters of the riverbed

	pface = pface->next;
//	  LP_printf(Simul->poutputs,"Going to next face \n");

	}

pface = HYD_rewind_faces(pface);

//LP_printf(Simul->poutputs,"Going to first face, next face is NULL\n");

  Simul->pcarac_ttc[module]->p_species[nb_species-1]->pset->riverbed = AQUITARD_ON_TTC;
  LP_printf(Simul->poutputs,"The riverbed calculation is ON: %d \n", Simul->pcarac_ttc[module]->p_species[nb_species-1]->pset->riverbed); // DK Check


};


att_aquitard_series : att_aquitard_serie att_aquitard_series
| att_aquitard_serie
{

	Simul->pcarac_ttc[module]->count[NAQUITARD_TTC] = naquitard;
}
;
att_aquitard_serie : intro_descr_aquitard_t aquitard_values LEX_CLOSING_BRACE
;

intro_descr_aquitard_t : LEX_AQUITARD_VAR_ATT LEX_EQUAL LEX_OPENING_BRACE 
{
 kindof_attribut = $1;
};

aquitard_values : aquitard_value aquitard_values
| aquitard_value
;

aquitard_value : LEX_INT flottant
{
	int id;
	int id_layer;
	s_ele_msh *pele;
	char* param_aq = NULL;


if ( type_input == 0 ) { // if this token is enabled by aquitard or riverbed, 0 is aquitard 1 is riverbed
  	pele=Simul->pcarac_aq->pcmsh->p_layer[nlayer]->p_ele[$1];
  	id=MSH_get_ele_id(pele,ABS_MSH);

	if ( kindof_attribut == 0 ) {
		naquitard++;
	}

	CAW_init_TTC_aquitard(Simul->pcarac_aq->pcmsh->p_layer[nlayer]->p_ele[$1],id,nlayer,kindof_attribut,$2,Simul->poutputs);

	verif_ele++;
} else if ( type_input == 1 ) {

 	if ( pface->id_gw != NULL ) {
// 		LP_printf(Simul->poutputs,"pface is not null\n");
		id = pface->id_gw->id;
		id_layer = pface->id_gw->id_lay;
//		LP_printf(Simul->poutputs,"Current layer is %d, id_abs %d, nriverbed = %d from pface, type of face %d \n", id_layer, id, $1, pface->def);

		CAW_init_TTC_riverbed(pface,id,id_layer,kindof_attribut,$2,Simul->poutputs);
		nriverbed++;
	}
// 	} else {
//		LP_printf(Simul->poutputs,"pface is null\n");
//
// 	}


	if ( pface->next != NULL ) {

	pface = pface->next;
	//	  LP_printf(Simul->poutputs,"Going to next face \n");

    	} else {
    		pface = HYD_rewind_faces(pface);

    		Simul->pcarac_ttc[module]->count[NRIVERBED_TTC] = nriverbed;
  //		LP_printf(Simul->poutputs," Final number of RIVERBED cells is %d \n", Simul->pcarac_ttc[module]->count[NRIVERBED_TTC]); // DK Check
  //  		LP_printf(Simul->poutputs,"Going to first face, next face is NULL\n");
    		nriverbed = 0;
    	} // End of pface->next check

    	if (pface->fnode == -9999) {
  //  		LP_printf(Simul->poutputs,"Fnode is -9999, skipping to next face \n");
    		if (pface->next != NULL) {
    			pface = pface->next;
		} else {
//  			LP_printf(Simul->poutputs,"Next case is null, rewinding\n");
			Simul->pcarac_ttc[module]->count[NRIVERBED_TTC] = nriverbed;
//			LP_printf(Simul->poutputs," Final number of RIVERBED cells is %d \n", Simul->pcarac_ttc[module]->count[NRIVERBED_TTC]); // DK Check
			pface = HYD_rewind_faces(pface);
//  			LP_printf(Simul->poutputs,"Going to first face, next face is NULL\n");

			nriverbed = 0;
    		} // End of pface->next check
    	} // End of fnode check


}
free(param_aq);


};

// DK AR 10 12 2021  Setup of source terms for the geothermal source and heat injection
set_bound_aq_transp : intro_bound_t boundaries_tp LEX_CLOSING_BRACE


/* NG fix : 19/06/2023 : If BOUNDARIES not declared in the COMM file --> nothing gets initialized properly !! 
--> making the rule declaration optional and moving init stuff below in CAW_initialize_TTC_all(). 
Instead, just turning a TTC setting boundaries flag to acknowledge COMM bounds input. */
intro_bound_t : LEX_BOUND LEX_EQUAL LEX_OPENING_BRACE                    
{
  Simul->pcarac_ttc[module]->p_species[nb_species-1]->pset->boundaries = YES_TTC;
/*  int i,nelelay;
  s_layer_msh *player;
  nelelay = Simul->pcarac_aq->pcmsh->p_layer[nlayer]->nele;
  player = Simul->pcarac_aq->pcmsh->p_layer[nlayer];
  for (i=0; i < nelelay; i++)
  {
     Simul->pcarac_aq->pcmsh->p_layer[nlayer]->p_ele[i]->nbound_ttc=0; 
     Simul->pcarac_aq->pcmsh->p_layer[nlayer]->p_ele[i]->nvois_ttc=0;
     CAW_init_TTC_face_bound(Simul->pcarac_aq->pcmsh->p_layer[nlayer],i,Simul->poutputs);
  }
  CAW_init_TTC_ele_nbound(player,Simul->poutputs); 
  Simul->pcarac_ttc[module]->count[NBC_TTC] = CAW_init_TTC_calc_nbc(Simul->pcarac_aq->pcmsh,Simul->poutputs);  */
}; 

set_source_aq_transp : intro_source_transport boundaries_tp LEX_CLOSING_BRACE 
| intro_source_transport uniform_input_tp LEX_CLOSING_BRACE
| intro_source_transport uniform_input_tp boundaries_tp LEX_CLOSING_BRACE     // DK : So we can mix up uniform recharge with explicit declarations of uptakes if needed

intro_source_transport : LEX_SOURCE LEX_EQUAL LEX_OPENING_BRACE 
{
  mult_AQparam = 1.0;
  Simul->pcarac_ttc[module]->p_species[nb_species-1]->pset->source = YES_TTC; // Activate the source transport

//  LP_printf(Simul->poutputs,"Attributing source inputs of layer %d :\n ",nlayer);
};

boundaries_tp : a_tp_boundary_type boundaries_tp
| a_tp_boundary_type
| multi_coef a_tp_boundary_type
;

a_tp_boundary_type : typeof_boundary_tp boundary_tp_series  LEX_CLOSING_BRACE 
{
  kindof_SOURCE=0;
};

typeof_boundary_tp : LEX_BOUND_TYPE LEX_EQUAL LEX_OPENING_BRACE
{
  kindof = $1;
//  LP_printf(Simul->poutputs,"Boundary type : %s\n",TTC_name_bound(kindof));
  kindof_SOURCE=CODE_AQ;
  idir =CODE_AQ;//Needs to face[be initialized for CAUCHY condition (will check Z_MSH][TWO] if idir = CODE_AQ, sinon pour neuman ou un CAUCHY FACE, a la face idir.
}
| LEX_TRANSPORT_SOURCE LEX_EQUAL LEX_OPENING_BRACE
{
 kindof=$1;
// LP_printf(Simul->poutputs,"Source type : %s\n",TTC_name_source(kindof));
};

boundary_tp_series : a_transp_boundary boundary_tp_series
| a_transp_boundary
{
    idir = CODE_AQ;
//    LP_printf(Simul->poutputs,"test in boundary_tp_series\n");

};

a_transp_boundary : intro_tp_dir defs_tp_bound
| defs_tp_bound
;

intro_tp_dir : LEX_BOUND_DIR LEX_EQUAL
{
  idir = $1;
};

defs_tp_bound : simple_tp_bound
{
IO_free_id_serie($1,Simul->poutputs);
//    LP_printf(Simul->poutputs,"test in defs_tp_bound\n");

}

// Here we get in each cell we browse the id of the cell and we add the time series structure inside AQ
simple_tp_bound : LEX_OPENING_BRACE ele_ids LEX_CLOSING_BRACE time_serie
{
  s_id_io *id_list;
//    LP_printf(Simul->poutputs,"test in simple_tp_bound\n");

  id_list = IO_browse_id($2,BEGINNING_TS);
//  IO_print_id(id_list,Simul->poutputs);
  if(kindof_SOURCE!=CODE_AQ)
    {

      /*------------------- WARNING : --------------------*/
      /* il faudra multiplier par la surface de l'élément*/
      /*pour avoir les bon flux !!!                      */
      //LP_printf(Simul->poutputs,"\t-->stot = %f \n",stot);
      CAW_init_TTC_source_cells(Simul->pcarac_aq->pcmsh,id_list,pft,kindof,stot,Simul->poutputs);
    }
  else
    {

      /*------------------- WARNING : --------------------*/
      /* il faudra multiplier par la longueur de l'élément*/
      /*pour avoir les bon flux !!!                       */
      CAW_init_TTC_define_bound(Simul->pcarac_aq->pcmsh,id_list,pft,idir,kindof,ltot,surf_riv,ep_riv_bed,TZ_riv_bed,Simul->poutputs);
//      LP_printf(Simul->poutputs,"\t--> Inside boundaries \n");

    }
  nelebound=0;
  ltot=0;
  stot=0;
  $$=id_list;

}
;

uniform_input_tp : LEX_TRANSPORT_SOURCE LEX_UNIFORM homo_value
{
    int type = $1;
    s_layer_msh *player = Simul->pcarac_aq->pcmsh->p_layer[nlayer];
    s_ele_msh* pele;
    s_id_io* pid=NULL;
    s_ft* pft=NULL;
    LP_printf(Simul->poutputs,"test in uniform_input_tp\n");

    for(i = 0; i < player->nele; i++)
    {
        pele = player->p_ele[i];
        if (pele->loc == BOT_MSH)
        {
            pft = TS_create_function(0.,(double)$3); // Input value in m3/s at this stage
            pid = IO_create_id(nlayer,player->p_ele[i]->id[INTERN_MSH]);


            CAW_init_TTC_source_cells(Simul->pcarac_aq->pcmsh,pid,pft,SOURCE_GEOTHERMAL_TTC,pele->pdescr->surf,Simul->poutputs);  // Value set in m/s in this function for each cell
//            LP_printf(Simul->poutputs,"Uniform recharge value for cell (%d,%d) = %10.4e m3/s.\n",nlayer,player->p_ele[i]->id[INTERN_MSH],$3);
        }
    }
    IO_free_id_serie(pid,Simul->poutputs);
    LP_printf(Simul->poutputs,"Uniform constant recharge value attributed for all-sub-surface cells of layer %d = %10.4e m3/s.\n",nlayer,$3);
};



/**************************************************************************/
/*                                                                        */
/*                             O U T P U T S                              */
/*                                                                        */
/* -----------------------------------------------------------------------*/
/*                            Outputs settings                            */
/**************************************************************************/

outputs : intro_outputs out_types LEX_CLOSING_BRACE
{
  char cmd[MAXCHAR];

  if (pts != NULL) {
    HYD_create_time_series(pts,Simul->outputs,pgen_chyd->counter->nts);
    sprintf(cmd,"mkdir %s/time_series",getenv("RESULT"));
    system(cmd);
  }
  if (plp != NULL) {
    HYD_create_long_profiles(plp,Simul->outputs,pgen_chyd->counter->nlp);
    sprintf(cmd,"mkdir %s/longitudinal_profiles",getenv("RESULT"));
    system(cmd);
  }
  if (pmb != NULL) {
    HYD_create_mass_balances(pmb,Simul->outputs,pgen_chyd->counter->nmb);
    sprintf(cmd,"mkdir %s/mass_balances",getenv("RESULT"));
    system(cmd);
  }
  LP_printf(Simul->poutputs,"End of outputs formats reading\n");
}
;

intro_outputs : LEX_OUTPUTS LEX_EQUAL LEX_OPENING_BRACE
{
  int i;
  LP_printf(Simul->poutputs,"Begin outputs formats reading\n");
  for(i=0;i<NSAT_IO;i++)
    {
      answ_dt[i]=0;

    }
 }
;

out_types : out_type out_types
|out_type
;

out_type : intro_out_type  def_outputs LEX_CLOSING_BRACE
;

intro_out_type : LEX_OUT_TYPE LEX_EQUAL LEX_OPENING_BRACE
{

 general_out = $1;
 if(general_out == CODE) // NG : Useless error print. If general_out is undefined, will be detected as a unknown syntax.
   LP_error(Simul->poutputs,"In CaWaQS%4.2f : Error in file %s : Unknown output file type. Check your command file !\n",NVERSION_CAW,__FILE__);

  if(general_out<NMOD_IO)
    {
      if(Simul->pout[general_out]==NULL)
	Simul->pout[general_out]=IO_init_out_struct(Simul->chronos->t[BEGINNING],Simul->chronos->t[END],Simul->chronos->dt);
      if(general_out<NSAT_IO)
	answ_out++;
    }
  else
    {
      if(nb_out==0)
	{
	  Simul->pinout=HYD_init_inout_set();
	  Simul->pinout->name_outputs = getenv("RESULT");
	  Simul->outputs=HYD_init_output_hyd();
	  nb_out++;
	}
    }

};
def_outputs : def_output def_outputs
| def_output
;

def_output : intro_output output_options brace
{
  if(general_out > NMOD_IO)
    {
      if (output_type == TRANSV_PROFILE) {
	//Simul->outputs[TRANSV_PROFILE] = pout;
	if (pts == NULL)
	  pts = pout_hyd;
	else
	  pts = HYD_chain_outputs(pout_hyd,pts);
	HYD_create_output_points(pts,pts_pktot);
	pgen_chyd->counter->nts++;
      }

      else if (output_type == LONG_PROFILE) {
	//Simul->outputs[LONG_PROFILE] = pout;
	if (plp == NULL)
	  plp = pout_hyd;
	else
	  plp = HYD_chain_outputs(pout_hyd,plp);
	HYD_create_output_extents(plp,plp_pktot);
	pgen_chyd->counter->nlp++;
      }

      else if (output_type == MASS_BALANCE) {
	if (pmb == NULL)
	  pmb = pout_hyd;
	else
	  pmb = HYD_chain_outputs(pout_hyd,pmb);
	HYD_create_output_extents(pmb,pmb_pktot);
	pgen_chyd->counter->nmb++;
      }

      else Simul->outputs[output_type][0] = pout_hyd;

      plp_pktot = NULL;
      pmb_pktot = NULL;
      pts_pktot = NULL;
    }
}
| intro_settings output_options brace
;

intro_settings : LEX_OUT_SET LEX_EQUAL brace;


intro_output : LEX_OUTPUT_TYPE LEX_EQUAL brace
{
  Simul->pinout->calc[$1] = YES;
  pout_hyd = HYD_initialize_output(Simul->chronos);
  output_type = $1;
}
;

output_options : output_option output_options
| output_option
;

output_option : LEX_ANSWER
{
  if(general_out > NMOD_IO)
    Simul->pinout->calc[output_type] = $1;

  // NG : 31/07/2020 : so the writing of the outputs can be switched off although the sub-block is still written in the command file
  if (general_out < NMOD_IO) Simul->pout[general_out]->write_out=$1;

}
| time_unit
| out_chronos
| variables
| points
{
  if (pts_pktot != NULL)
    pts_pktot = HYD_chain_ts_pk(HYD_browse_ts_pk(pts_pktot,END),$1);
  else
    pts_pktot = $1;
}

| extents
{
  if (output_type == LONG_PROFILE) {
    if (plp_pktot != NULL)
      plp_pktot = HYD_chain_lp_pk(HYD_browse_lp_pk(plp_pktot,END),$1);
    else
      plp_pktot = $1;
  }

  else if (output_type == MASS_BALANCE) {
    if (pmb_pktot != NULL)
      pmb_pktot = HYD_chain_lp_pk(HYD_browse_lp_pk(pmb_pktot,END),$1);
    else
      pmb_pktot = $1;
  }
}
| graphics
| selection
| format
| final_state
| scale  // NG : 20/07/2020 : New option so the user chan choose the spatial scale the output will be printed out (ex : ele bu or cprod for watbal MB)
;

final_state : LEX_FINAL_STATE LEX_EQUAL LEX_ANSWER
{
if(general_out<NMOD_IO)
    Simul->pout[general_out]->final_state = $3;
}

// NG : 20/07/2020
scale : LEX_SPATIAL_SCALE LEX_EQUAL LEX_SCALE_TYPE
{
	Simul->pout[general_out]->spatial_scale = $3;
	LP_printf(Simul->poutputs,"Spatial scale the output file %s will be printed at : %s\n",IO_name_out(general_out),IO_geometry_unit_type(Simul->pout[general_out]->spatial_scale));
};

format : LEX_FORMAT_TYPE LEX_EQUAL LEX_FORMAT
{
  if(general_out<NMOD_IO)
    Simul->pout[general_out]->format = $3;
  else
    pout_hyd->pout->format=$3;

};

selection : sel_intro ele_ids_out brace
{
  if(general_out<NMOD_IO)
    {
      Simul->pout[general_out]->type_out=WIND_IO;
      Simul->pout[general_out]->id_output=$2;
      Simul->pout[general_out]->nb_output=nelebound;
    }
};

sel_intro : LEX_SEL LEX_EQUAL brace
{
  nlayer=0;
  nelebound=0;
}
;

time_unit : LEX_TUNIT LEX_EQUAL a_unit
{
  if(general_out > NMOD_IO)
    pout_hyd->pout->time_unit = 1 / $3;
  else
    Simul->pout[general_out]->time_unit = 1 / $3;
}
;

out_chronos : LEX_CHRONOS LEX_EQUAL brace out_times brace
{
  if(general_out > NMOD_IO)
    pout_hyd->pout->t_out[INI_IO] = Simul->chronos->t[BEGINNING];
  else
    Simul->pout[general_out]->t_out[INI_IO] = Simul->chronos->t[BEGINNING];
}
;

out_times : out_time out_times
| out_time
;

out_time : LEX_TIME LEX_EQUAL mesure
{
  if(general_out > NMOD_IO)
    pout_hyd->pout->t_out[$1] = $3;
  else
    Simul->pout[general_out]->t_out[$1] = $3;

}
| LEX_TIMESTEP LEX_EQUAL mesure
{
  if(general_out > NMOD_IO)
    pout_hyd->pout->deltat = $3 < Simul->chronos->dt ? Simul->chronos->dt : $3;
  else
    {
      Simul->pout[general_out]->deltat = $3 < Simul->chronos->dt ? Simul->chronos->dt : $3;
      if(general_out<NSAT_IO)
	answ_dt[general_out]=Simul->pout[general_out]->deltat;
    }
  Simul->pout[general_out]->deltat = Simul->chronos->dt; //FB 28/02/18 The case deltat_out different from deltat_calc has not been treated yet, so I set deltat_out = deltat_calc.
}
;

variables : LEX_VAR LEX_EQUAL brace var_list brace
;

var_list : one_var var_list
| one_var
;

one_var : LEX_ONE_VAR a_unit
{
  pout_hyd->pout->hydvar[$1] = YES;
  pout_hyd->pout->hydvar_unit[$1] = $2;
}
;

points : point_intro river pk_list brace
{
  $$ = $3;
}
;

river : river_pk
| river_len
;

point_intro : LEX_POINTS LEX_EQUAL brace
{
  pk_type = $1;
  if(pk_type == LENGTH_HYD && pgen_chyd->settings->schem_type != MUSKINGUM)
    LP_error(Simul->poutputs,"Cannot set pk_type to LENGTH for ST-VENANT hydraulic scheme\n"); //BL car il faut avoir pour l'instant un nom de river different par reach seulement vrai pour muskingum pour modifier cela il faut changer dans la fonction HYD_calculate_ts_domain de manage_output.c
  // LP_printf(Simul->poutputs,"pk_type in point_intro %d\n",pk_type);
}
;
river_pk : LEX_NAME LEX_SEMI_COLON
{//NF 25/4/2017 Je ne comprends pas a quoi sert rivername. Non utilise ou recupere par la suite
  rivername=(char*)malloc(STRING_LENGTH_LP*sizeof(char));//NF 25/4/2017 l'espace memoire doit etre alloue!
  sprintf(rivername,"%s",$1);//NF 25/4/2017 l'espace memoire doit etre alloue!
  //river = $1;
  LP_lowercase(rivername);
}
;

river_len : LEX_NAME LEX_INT LEX_SEMI_COLON
{
  //char *name;//NF 25/4/2017 ne sert a rien!
  rivername=(char*)malloc(STRING_LENGTH_LP*sizeof(char));
  sprintf(rivername,"%s_%d",$1,$2);
  LP_lowercase(rivername);

}
;

pk_list : one_pk pk_list
{
  $$ = HYD_chain_ts_pk($1,$2);
}
| one_pk
{
  $$ = $1;
}
;

one_pk : mesure LEX_INT
{
  //LP_printf(Simul->poutputs,"pk_type in one_pk %d\n",pk_type);
  pts_pk = new_ts_pk();
  pts_pk->river = rivername;
  pts_pk->pk = $1;
  pts_pk->branch_nb = $2;
  pts_pk->pk_type=pk_type;
  //find_element_reach(ts_pk);
  $$ = pts_pk;
  pout_hyd->npk++;
}
;

extents : LEX_EXTENT LEX_EQUAL brace river two_pk brace
{
  if (output_type == LONG_PROFILE) {
    $$ = plp_pk;
  }

  else if (output_type == MASS_BALANCE) {
    $$ = pmb_pk;
  }
}
;

two_pk : mesure LEX_INT mesure LEX_INT
{
  if (output_type == LONG_PROFILE) {
    plp_pk = HYD_init_lp_pk(rivername,$1,$2,$3,$4,pgen_chyd);
  }

  else if (output_type == MASS_BALANCE) {
    pmb_pk = HYD_init_lp_pk(rivername,$1,$2,$3,$4,pgen_chyd);
  }

  pout_hyd->npk++;
}
;

graphics : LEX_GRAPHICS LEX_EQUAL LEX_ANSWER
{
  pout_hyd->graphics = NO;
}
| LEX_GRAPHICS LEX_EQUAL LEX_GRAPH_TYPE
{
  pout_hyd->graphics = $3;

  if ($3 == GNUPLOT) {
    char cmd[MAXCHAR];
    sprintf(cmd,"mkdir %s/gnuplot",getenv("RESULT"));
    system(cmd);
  }
}
;

/* This part describes how to deal with numbers and units*/
read_units : a_unit a_unit
{
  unit_t = $1;
  unit_f = $2;
  nbsteps = 0;
}
;

a_unit : LEX_OPENING_BRACKET units LEX_CLOSING_BRACKET {$$ = $2;}
;


/* This part allows a_unit conversion in the programm units (SI), understanding power, division... */
units : one_unit units
{
  $$ = $1 * $2;
}
| one_unit
{
  $$ = $1;
}
;

one_unit : a_unit_value
{
  $$ = $1;
}
| LEX_INV a_unit_value
{
  $$ = 1.0/$2;
}
; /* inversion */


a_unit_value : all_units
{
  $$ = $1;
} /* simple a_unit */
| all_units LEX_POW flottant
{
  $$ = pow($1,$3);
}
; /* power a_unit */

all_units : LEX_A_UNIT
{
  $$ = $1;
}
| LEX_A_UNIT LEX_VAR_UNIT
{
  $$ = $1*$2;
}
;

mesure : a_unit flottant
{
  $$ = $1 * $2;
}
| flottant a_unit//LV 06/06/2012 pour ne pas a avoir à modifier tous les fichiers de profils ProSe !
{
  $$ = $1 * $2;
}
|flottant
{
  $$ = $1;
}   // NG : 21/07/2020 : so we can directly assign a unit to a forcing boundary time serie
| a_unit
{
  $$ = $1*1.;
}
;

flottant : LEX_DOUBLE
{
  $$ = $1;
}
| LEX_INT
{
  $$ = (double)$1;
}
;


/* Examples of chaining function of t s_ft */
f_ts : f_t f_ts
{
  $$ = TS_chain_bck_ts($1,$2);
}
|  f_t
{
  $$ = $1;
};

f_t : flottant flottant
{
  nbsteps++;
  told = tnew;
  tnew = $1 * unit_t;
  if ((nbsteps >= 2) && (tnew < told))
  if (tnew < told)
    LP_warning(Simul->poutputs,"Cawaqs %4.2f -> Inversion of the time steps, line %d\n",NVERSION_CAW,nbsteps);
  $$ = TS_create_function($1*unit_t,$2*unit_f);
}
;

ele_ids_out : ele_id_out ele_ids_out
{
  //init $$=TS_secured_chain_fwd_ts($2,$1);
$$=IO_chain_bck_id($1,$2);
}
| ele_id_out
{
  $$=$1;
}
;

ele_id_out : LEX_INT
{
  s_id_io *id_out;
  int id,struct_type;

      if(general_out==FP_IO)
	{
	  struct_type=CPROD_SPA;
	  id=SPA_search_id(CPROD_SPA,Simul->pcarac_fp,$1,FP_GIS,FP_INTERN,Simul->poutputs);
	  nlayer=0;
	}
      else if(general_out!=CODE)
	{
	  nlayer=0;
	  id=$1;
	}
      else
	id=$1;
  $$=IO_create_id(nlayer,id);
  nelebound ++;
  //ltot+=Simul->pcmsh->p_layer[nlayer]->p_ele[$1]->pdescr->l_side;
  //stot+=Simul->pcmsh->p_layer[nlayer]->p_ele[$1]->pdescr->surf;
}
;

brace : LEX_OPENING_BRACE
| LEX_CLOSING_BRACE
;
%%


/* Procedure used to display errors
 * automatically called in the case of synthax errors
 * Specifies the file name and the line where the error was found
 */

#if defined GCC540 ||  defined GCC482 || defined GCC472 ||  defined GCC471
void yyerror(char const *s)
#else
void yyerror(char *s)
#endif
{
  if (pile >= 0) {
    //LP_printf(Simul->poutputs,"File %s, line %d : %s\n",current_read_files[pile].name,line_nb+1,s);
    //exit -1;
    LP_error(Simul->poutputs,"File %s, line %d : %s\n",current_read_files[pile].name,line_nb+1,s);
  }
}


void lecture(char *name,FILE *fp)
{
  pile = 0;
  current_read_files[pile].name = strdup(name);
  if ((yyin = fopen(name,"r")) == NULL)
    LP_error(fp,"File %s doesn't exist\n",name);
  current_read_files[pile].address = yyin;
  current_read_files[pile].line_nb = line_nb;

   LP_printf(fp,"\n****Reading input data****\n");
  yyparse();
//  if (Simul->pcarac_aq!=NULL) {//NF 27/9/2017 To debug and check CAUCHY conditions.
//    AQ_print_elebound(Simul->pcarac_aq->pcmsh,Simul->poutputs); //NF 27/9/2017  to debug and compare with the one in lecture() to see if CAUCHY RIVER is OK
//  }
  LP_printf(fp,"\n****Input data read****\n");
}
