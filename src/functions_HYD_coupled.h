/*-------------------------------------------------------------------------------
 *
 * SOFTWARE NAME: cawaqs
 * FILE NAME: functions_hydro_coupled.h
 *
 * CONTRIBUTORS: Nicolas GALLOIS, Baptiste LABARTHE, Shuaitao WANG,
 *               Agnès RIVIERE, Deniz KILIC, Mathias MAILLOT,
 *               Fulvia BARATELLI, Pierre GUILLOU, Emmanuel LEDOUX, Nicolas FLIPO
 *
 * SOFTWARE BRIEF DESCRIPTION: Physically based coupled surface–subsurface
 * model for joint water, matter and energy flux balances and flow dynamics
 * simulation within all compartments of a hydrosystem (sub-surface, hydraulic
 * network, vadose zone, aquifer system and stream-aquifer exchanges).
 *
 * Software developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the cawaqs Software.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Software and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

void HYD_calc_Qapp_coupled_reach(s_reach_hyd *, double, s_carac_fp *, FILE *);
void HYD_calc_Qapp_coupled(double, s_chyd *, s_carac_fp *, FILE *);
void HYD_calc_Qapp_GW(s_chyd *, s_carac_aq *, double, double, double, FILE *);
void HYD_steady_state_coupled(double, s_mb_hyd *, s_mb_hyd *, FILE *, s_inout_set_io *, s_output_hyd ***, s_chyd *, s_clock_CHR *, s_chronos_CHR *, s_carac_fp *, s_carac_aq *, int, FILE *);
void HYD_transient_hydraulics_coupled(double, double, s_mb_hyd *, s_mb_hyd *, FILE *, s_chyd *, s_clock_CHR *, s_chronos_CHR *, s_output_hyd ***, s_inout_set_io *, s_carac_fp *, s_carac_aq *, int, FILE *);
s_mb_hyd *HYD_solve_hydro_coupled_old(double, FILE *, s_mb_hyd *, s_chyd *, s_clock_CHR *, s_chronos_CHR *, s_output_hyd ***, s_inout_set_io *, s_carac_fp *, s_carac_aq *, int, FILE *);
void HYD_solve_hydro_coupled(double, s_chyd *, s_clock_CHR *, s_chronos_CHR *, s_carac_fp *, s_carac_aq *, int, int, int *, FILE *); // NG : 11/06/2021 : iteration/ts counter added
void HYD_refresh_qapp_coupled(s_chyd *, FILE *);
int HYD_check_reach(s_id_io *, s_chyd *, FILE *);
s_id_io *HYD_get_id_riv_down(s_chyd *, s_id_io *, int, FILE *);

// NG : 24/01/2020 : Fonctions de redirection de débits aux outlets vers le non-saturé en cas de rivière à exutoire infiltrant
void CAW_cHYD_calc_runoff_catchment_chasm_all(s_carac_zns *, s_chyd *, FILE *);
void CAW_cHYD_calc_runoff_catchment_chasm(s_zns *, s_chyd *, FILE *);

// NG : 29/02/2020 : Fonctions de détermination des Ki muskingum par les méthodes TTRA et FORMULA avec prise en compte des catchments
void CAW_cHYD_set_K_basin_by_Tc_catchments(s_chyd *, s_carac_fp *, FILE *);
void CAW_cHYD_set_K_TTRA_Bassin_by_Tc_catchments(s_chyd *, s_carac_fp *, FILE *);
void CAW_cHYD_calc_Itr_i_outlets(s_chyd *, s_carac_fp *, double **, FILE *);
void CAW_cHYD_get_TC_Catchments(s_chyd *, s_carac_fp *, FILE *);
void CAW_cHYD_set_TC_reach_Catchment(s_chyd *, s_catchment_fp *, FILE *);
