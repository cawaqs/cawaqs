/*-------------------------------------------------------------------------------
 *
 * SOFTWARE NAME: cawaqs
 * FILE NAME: ext_CAWAQS.h
 *
 * CONTRIBUTORS: Nicolas GALLOIS, Baptiste LABARTHE, Shuaitao WANG,
 *               Agnès RIVIERE, Deniz KILIC, Mathias MAILLOT,
 *               Fulvia BARATELLI, Pierre GUILLOU, Emmanuel LEDOUX, Nicolas FLIPO
 *
 * SOFTWARE BRIEF DESCRIPTION: Physically based coupled surface–subsurface
 * model for joint water, matter and energy flux balances and flow dynamics
 * simulation within all compartments of a hydrosystem (sub-surface, hydraulic
 * network, vadose zone, aquifer system and stream-aquifer exchanges).
 *
 * Software developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the cawaqs Software.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Software and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

/* Variables used when reading input data */
extern FILE *yyin;
extern int yydebug;
/* Structure containing the simulation caracteristics */
extern s_simul_cawaqs *Simul;
/* Table of the addresess of the read files */
extern s_file_io current_read_files[NPILE];
/* Number of folders defined by the ugloser */
extern int folder_nb;
/* File number */
extern int pile;
/* Position within the file being processed */
extern int line_nb;
/* Name of file folders */
extern char *name_out_folder[NPILE];

/* Functions of input.y */
void lecture(char *, FILE *);
int yylex();

#if defined GCC540 || defined GCC481 || defined GCC473 || defined GCC472 || defined GCC471
void yyerror(char const *);
#else
void yyerror(char *);
#endif /*test on GCC*/
