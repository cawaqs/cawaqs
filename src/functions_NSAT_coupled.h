/*-------------------------------------------------------------------------------
 *
 * SOFTWARE NAME: cawaqs
 * FILE NAME: functions_NSAT_coupled.h
 *
 * CONTRIBUTORS: Nicolas GALLOIS, Baptiste LABARTHE, Shuaitao WANG,
 *               Agnès RIVIERE, Deniz KILIC, Mathias MAILLOT,
 *               Fulvia BARATELLI, Pierre GUILLOU, Emmanuel LEDOUX, Nicolas FLIPO
 *
 * SOFTWARE BRIEF DESCRIPTION: Physically based coupled surface–subsurface
 * model for joint water, matter and energy flux balances and flow dynamics
 * simulation within all compartments of a hydrosystem (sub-surface, hydraulic
 * network, vadose zone, aquifer system and stream-aquifer exchanges).
 *
 * Software developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the cawaqs Software.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Software and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

// in NSAT_coupled.c
void CAW_cNSAT_dynNsat_init(s_carac_zns *, s_carac_aq *, double, FILE *);
void CAW_cNSAT_update_dynamic_nsat(s_carac_zns *, s_carac_aq *, s_chyd *, double, int, int, FILE *);
void CAW_cNSAT_define_soil_elevation(s_zns *, s_carac_aq *, s_chyd *, double, FILE *);
void CAW_cNSAT_update_hydraulic_head(s_zns *, s_carac_aq *, double, FILE *);
void CAW_cNSAT_update_nres(s_zns *, int, double, FILE *);
void CAW_cNSAT_update_aquifer_tr_variable(s_zns *, s_carac_aq *, double, int, FILE *);