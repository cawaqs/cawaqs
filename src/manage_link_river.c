/*-------------------------------------------------------------------------------
 *
 * SOFTWARE NAME: cawaqs
 * FILE NAME: manage_link_river.c
 *
 * CONTRIBUTORS: Nicolas GALLOIS, Baptiste LABARTHE, Shuaitao WANG,
 *               Agnès RIVIERE, Deniz KILIC, Mathias MAILLOT,
 *               Fulvia BARATELLI, Pierre GUILLOU, Emmanuel LEDOUX, Nicolas FLIPO
 *
 * SOFTWARE BRIEF DESCRIPTION: Physically based coupled surface–subsurface
 * model for joint water, matter and energy flux balances and flow dynamics
 * simulation within all compartments of a hydrosystem (sub-surface, hydraulic
 * network, vadose zone, aquifer system and stream-aquifer exchanges).
 *
 * Software developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the cawaqs Software.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Software and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <time.h>
#include <stdarg.h>
#include <sys/time.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <malloc.h>
#include <math.h>
#include <libprint.h>
#ifdef OMP
#include <omp.h>
#endif
#include <time_series.h>
#include "libpc.h"
#include "IO.h"
#include "GC.h"
#include "reservoir.h"
#include "spa.h"
#include "FP.h"
#include "NSAT.h"
#include "CHR.h"
#include "HYD.h"
#include "MSH.h"
#include "AQ.h"
#include "WET.h"
#include "TTC_coupled.h"
#include "SEB.h"
#include "CAW.h"
#include "global_CAWAQS.h"

/**
 * \fn CAW_alloc_TTC_u_dist_one_species_riv(s_species_ttc*, s_chyd*, int, int, FILE*)
 * \brief Allocates memory for sub-cards, passes HYD ele length onto TTC and initializes uface
 * \return -
 */
void CAW_alloc_TTC_u_dist_one_species_riv(s_carac_ttc *pcarac, s_chyd *pchyd, int nele, int id_species, FILE *flog) {

  int r, ne, icard, sub_icard;
  s_reach_hyd *preach;
  s_element_hyd *pele;

  // Initializing uface : TTC function allocates and initialize to 0 the entire ***array.
  pcarac->uface = TTC_allocate_uface(nele);

  // Filling river elements' length
  for (r = 0; r < pchyd->counter->nreaches; r++) {
    preach = pchyd->p_reach[r];

    for (ne = 0; ne < preach->nele; ne++) {
      pele = preach->p_ele[ne];
      pcarac->p_link[id_species]->pspecies->p_param_ttc[pele->id[ABS_HYD]]->param_syst[SIZE_TTC] = pele->length;
    }
  }
}

/**
 * \fn CAW_init_TTC_val_one_species_riv(s_species_ttc*, s_chyd*, int, int, FILE*)
 * \brief Passes value stored in the pchyd hydro structure of the river cell to pcarac val_ttc
 * \return -
 *
 * DK AR 18 11 2021 + NG 04/04/2023 : Function likely to be added by SW. Its current call from main_cawaqs.c
 * is inappropriate.
 *
 * NG 04/04/2023: I don't get any of this. nele is overwritten by its definition...which is itself
 * accessible through pcarac !
 * Isn't the val[] supposed to be filled using the pele->id[ABS_HYD] to be consistent with
 * the way all ttc arrays are filled elsewhere ? Why is there a pre-increment ?
 *
 */
void CAW_init_TTC_val_one_species_riv(s_carac_ttc *pcarac, s_chyd *pchyd, int nele, int id_species, FILE *flog) {

  int nttc, r, ne, nr, icard, sub_icard;
  s_reach_hyd *preach;
  s_element_hyd *pele;
  nele = pcarac->count[NELE_TTC];

  for (nttc = 0; nttc < nele; ++nttc) {
    for (r = 0; r < pchyd->counter->nreaches; r++) {
      preach = pchyd->p_reach[r];

      for (ne = 0; ne < preach->nele; ne++) {
        pele = preach->p_ele[ne];
        pcarac->p_species[id_species]->pval_ttc->val[nttc] = pele->center->hydro->transp_var[id_species];
      }
    }
  }
}

/**
 * \fn void CAW_fill_neighbor_one_species_riv(s_link_ttc*, s_chyd*, FILE*)
 * \brief Fills neighbour river elements table, initializes boundaries of all faces, computes deltaX for all elements
 * \return -
 */
void CAW_fill_neighbor_one_species_riv(s_link_ttc *plink, s_chyd *pchyd, FILE *flog) {

  int r, n, id_abs_ele, ne = 0;
  s_reach_hyd *preach;
  s_element_hyd *pele;
  s_face_hyd *pface, *pfacep;

  // Setting NEUMANN boundary conditions on lateral, bottom and top faces of river calculations elements
  for (r = 0; r < pchyd->counter->nreaches; r++) {

    preach = pchyd->p_reach[r];
    for (n = 0; n < preach->nele; n++) {
      pele = preach->p_ele[n];
      id_abs_ele = pele->id[ABS_HYD];

      plink->pspecies->p_boundary_ttc[id_abs_ele]->icl[BANKS_TTC][0] = NEU_FACE_TTC;
      plink->pspecies->p_boundary_ttc[id_abs_ele]->valcl[BANKS_TTC][0] = 0.0;
      plink->pspecies->p_boundary_ttc[id_abs_ele]->nbound++;

      plink->pspecies->p_boundary_ttc[id_abs_ele]->icl[ATMOS_TTC][0] = NEU_FACE_TTC;
      plink->pspecies->p_boundary_ttc[id_abs_ele]->valcl[ATMOS_TTC][0] = 0.0;
      plink->pspecies->p_boundary_ttc[id_abs_ele]->nbound++;

      plink->pspecies->p_boundary_ttc[id_abs_ele]->icl[RIVAQ_TTC][0] = NEU_FACE_TTC;
      plink->pspecies->p_boundary_ttc[id_abs_ele]->valcl[RIVAQ_TTC][0] = 0.0;
      plink->pspecies->p_boundary_ttc[id_abs_ele]->nbound++;
      // LP_printf(flog,"Setting NEUMANN condition on river element ABS ID %d\n", id_abs_ele);
    }
  }

  /* Step 1 : Setting boundaries, calculating deltaX (center-to-center distance) and counting
   * neighbors for the FIRST element of each river reach */

  for (r = 0; r < pchyd->counter->nreaches; r++) {
    preach = pchyd->p_reach[r];
    pele = preach->p_ele[0];
    pface = pele->face[X_HYD][ONE];
    id_abs_ele = pele->id[ABS_HYD];

    switch (HYD_test_sing_face(pface, UPSTREAM)) {
    case DISCHARGE: // River network upstream
    {
      // pspecies->plink->pboundary_ttc->ptype_bound_ttc->picl[id_abs_ele].icl[WEST_TTC] = DIRI_FACE_TTC;
      // pspecies->plink->pboundary_ttc->nbound[id_abs_ele]++;
      // LP_printf(flog,"ABS ele in the discharge= %d\n", id_abs_ele);
      // LP_printf(flog,"ABS ele in test, id ele is %d: nvois is = %d\n",id_abs_ele, plink->p_neigh_ttc[id_abs_ele]->nvois);
      break;
    }

    case CONFLUENCE: {
      plink->p_neigh_ttc[id_abs_ele]->ivois[UPSTREAM_TTC][0] = pele->face[X_HYD][ONE]->limits[UPSTREAM]->faces[ONE][0]->element[ONE]->id[ABS_HYD];
      plink->p_neigh_ttc[id_abs_ele]->nvois++;
      // LP_printf(flog,"ABS ele in test, id ele is %d: nvois is = %d\n",id_abs_ele, plink->p_neigh_ttc[id_abs_ele]->nvois);
      plink->p_neigh_ttc[id_abs_ele]->ivois[UPSTREAM_TTC][1] = pele->face[X_HYD][ONE]->limits[UPSTREAM]->faces[ONE][1]->element[ONE]->id[ABS_HYD];
      plink->p_neigh_ttc[id_abs_ele]->nvois++;

      plink->p_neigh_ttc[id_abs_ele]->delta_L[UPSTREAM_TTC][0] = (pele->face[X_HYD][ONE]->limits[UPSTREAM]->faces[ONE][0]->element[ONE]->length + pele->length) / 2;
      plink->p_neigh_ttc[id_abs_ele]->delta_L[UPSTREAM_TTC][1] = (pele->face[X_HYD][ONE]->limits[UPSTREAM]->faces[ONE][1]->element[ONE]->length + pele->length) / 2;
      // LP_printf(flog,"ABS ele in the cnfluence= %d\n", id_abs_ele);
      // LP_printf(flog,"ABS ele in test, id ele is %d: nvois is = %d\n",id_abs_ele, plink->p_neigh_ttc[id_abs_ele]->nvois);
      break;
    }

    case TRICONFLUENCE: {

      plink->p_neigh_ttc[id_abs_ele]->ivois[UPSTREAM_TTC][0] = pele->face[X_HYD][ONE]->limits[UPSTREAM]->faces[ONE][0]->element[ONE]->id[ABS_HYD];
      plink->p_neigh_ttc[id_abs_ele]->nvois++;
      // LP_printf(flog,"ABS ele in test, id ele is %d: nvois is = %d\n",id_abs_ele, plink->p_neigh_ttc[id_abs_ele]->nvois);
      plink->p_neigh_ttc[id_abs_ele]->ivois[UPSTREAM_TTC][1] = pele->face[X_HYD][ONE]->limits[UPSTREAM]->faces[ONE][1]->element[ONE]->id[ABS_HYD];
      plink->p_neigh_ttc[id_abs_ele]->nvois++;
      // LP_printf(flog,"ABS ele in test, id ele is %d: nvois is = %d\n",id_abs_ele, plink->p_neigh_ttc[id_abs_ele]->nvois);
      plink->p_neigh_ttc[id_abs_ele]->ivois[UPSTREAM_TTC][2] = pele->face[X_HYD][ONE]->limits[UPSTREAM]->faces[ONE][2]->element[ONE]->id[ABS_HYD];
      plink->p_neigh_ttc[id_abs_ele]->nvois++;
      // LP_printf(flog,"ABS ele in test, id ele is %d: nvois is = %d\n",id_abs_ele, plink->p_neigh_ttc[id_abs_ele]->nvois);

      plink->p_neigh_ttc[id_abs_ele]->delta_L[UPSTREAM_TTC][0] = (pele->face[X_HYD][ONE]->limits[UPSTREAM]->faces[ONE][0]->element[ONE]->length + pele->length) / 2;
      plink->p_neigh_ttc[id_abs_ele]->delta_L[UPSTREAM_TTC][1] = (pele->face[X_HYD][ONE]->limits[UPSTREAM]->faces[ONE][1]->element[ONE]->length + pele->length) / 2;
      plink->p_neigh_ttc[id_abs_ele]->delta_L[UPSTREAM_TTC][2] = (pele->face[X_HYD][ONE]->limits[UPSTREAM]->faces[ONE][2]->element[ONE]->length + pele->length) / 2;
      // LP_printf(flog,"ABS ele in the TRICONF = %d\n", id_abs_ele);
      break;
    }

    case CONF_DIFF: {
      plink->pspecies->p_boundary_ttc[id_abs_ele]->icl[UPSTREAM_TTC][0] = CONF_DIFF_TTC;
      plink->pspecies->p_boundary_ttc[id_abs_ele]->nbound++;

      plink->p_neigh_ttc[id_abs_ele]->ivois[UPSTREAM_TTC][0] = pele->face[X_HYD][ONE]->limits[UPSTREAM]->faces[ONE][0]->element[ONE]->id[ABS_HYD];
      plink->p_neigh_ttc[id_abs_ele]->nvois++;
      // LP_printf(flog,"ABS ele in test, id ele is %d: nvois is = %d\n",id_abs_ele, plink->p_neigh_ttc[id_abs_ele]->nvois);
      plink->p_neigh_ttc[id_abs_ele]->ivois[UPSTREAM_TTC][1] = pele->face[X_HYD][ONE]->limits[UPSTREAM]->faces[ONE][1]->element[ONE]->id[ABS_HYD];
      plink->p_neigh_ttc[id_abs_ele]->nvois++;
      // LP_printf(flog,"ABS ele in test, id ele is %d: nvois is = %d\n",id_abs_ele, plink->p_neigh_ttc[id_abs_ele]->nvois);

      plink->p_neigh_ttc[id_abs_ele]->delta_L[UPSTREAM_TTC][0] = (pele->face[X_HYD][ONE]->limits[UPSTREAM]->faces[ONE][0]->element[ONE]->length + pele->length) / 2;
      plink->p_neigh_ttc[id_abs_ele]->delta_L[UPSTREAM_TTC][1] = (pele->face[X_HYD][ONE]->limits[UPSTREAM]->faces[ONE][1]->element[ONE]->length + pele->length) / 2;
      // LP_printf(flog,"ABS ele in the CONF DIFF = %d\n", id_abs_ele);
      break;
    }

    case DIFFLUENCE: {
      plink->p_neigh_ttc[id_abs_ele]->ivois[UPSTREAM_TTC][0] = pele->face[X_HYD][ONE]->limits[UPSTREAM]->faces[ONE][0]->element[ONE]->id[ABS_HYD];
      plink->p_neigh_ttc[id_abs_ele]->nvois++;
      plink->p_neigh_ttc[id_abs_ele]->delta_L[UPSTREAM_TTC][0] = (pele->face[X_HYD][ONE]->limits[UPSTREAM]->faces[ONE][0]->element[ONE]->length + pele->length) / 2;
      // LP_printf(flog,"ABS ele in the DIFFLUENCE = %d\n", id_abs_ele);
      // LP_printf(flog,"ABS ele in test, id ele is %d: nvois is = %d\n",id_abs_ele, plink->p_neigh_ttc[id_abs_ele]->nvois);
      break;
    }

    case TRIDIFFLUENCE: {
      plink->p_neigh_ttc[id_abs_ele]->ivois[UPSTREAM_TTC][0] = pele->face[X_HYD][ONE]->limits[UPSTREAM]->faces[ONE][0]->element[ONE]->id[ABS_HYD];
      plink->p_neigh_ttc[id_abs_ele]->nvois++;
      plink->p_neigh_ttc[id_abs_ele]->delta_L[UPSTREAM_TTC][0] = (pele->face[X_HYD][ONE]->limits[UPSTREAM]->faces[ONE][0]->element[ONE]->length + pele->length) / 2;
      // LP_printf(flog,"ABS ele in the TRI DIFF = %d\n", id_abs_ele);
      // LP_printf(flog,"ABS ele in test, id ele is %d: nvois is = %d\n",id_abs_ele, plink->p_neigh_ttc[id_abs_ele]->nvois);
      break;
    }

    default: {
      plink->p_neigh_ttc[id_abs_ele]->ivois[UPSTREAM_TTC][0] = pele->face[X_HYD][ONE]->limits[UPSTREAM]->faces[ONE][0]->element[ONE]->id[ABS_HYD];
      plink->p_neigh_ttc[id_abs_ele]->nvois++;
      plink->p_neigh_ttc[id_abs_ele]->delta_L[UPSTREAM_TTC][0] = (pele->face[X_HYD][ONE]->limits[UPSTREAM]->faces[ONE][0]->element[ONE]->length + pele->length) / 2;
      // LP_printf(flog,"ABS ele in the default upstream= %d\n", id_abs_ele);
      // LP_printf(flog,"ABS ele in test, id ele is %d: nvois is = %d\n",id_abs_ele, plink->p_neigh_ttc[id_abs_ele]->nvois);
      break;
    }
    }

    if (preach->nele > 1) // Dealing directly with the DOWNSTREAM face
    {
      plink->p_neigh_ttc[id_abs_ele]->ivois[DOWNSTREAM_TTC][0] = pele->face[X_HYD][TWO]->element[TWO]->id[ABS_HYD];
      plink->p_neigh_ttc[id_abs_ele]->nvois++;
      plink->p_neigh_ttc[id_abs_ele]->delta_L[DOWNSTREAM_TTC][0] = (pele->face[X_HYD][TWO]->element[TWO]->length + pele->length) / 2;
      // LP_printf(flog,"ABS ele in the downstream= %d\n", id_abs_ele);
      // LP_printf(flog,"ABS ele in test, id ele is %d: nvois is = %d\n",id_abs_ele, plink->p_neigh_ttc[id_abs_ele]->nvois);
    }

    /* Step 2 : Setting boundaries, calculating deltaX (center-to-center distance) and counting
     * neighbors for the river elements INSIDE a reach */

    for (ne = 1; ne < preach->nele - 1; ne++) {
      pele = preach->p_ele[ne];
      id_abs_ele = pele->id[ABS_HYD];

      // UPSTREAM face
      plink->p_neigh_ttc[id_abs_ele]->ivois[UPSTREAM_TTC][0] = pele->face[X_HYD][ONE]->element[ONE]->id[ABS_HYD]; // No singularity
      plink->p_neigh_ttc[id_abs_ele]->nvois++;
      plink->p_neigh_ttc[id_abs_ele]->delta_L[UPSTREAM_TTC][0] = (pele->face[X_HYD][ONE]->element[ONE]->length + pele->length) / 2;
      // LP_printf(flog,"ABS ele in test, id ele is %d: nvois is = %d\n",id_abs_ele, plink->p_neigh_ttc[id_abs_ele]->nvois);

      // DOWNSTREAM face
      plink->p_neigh_ttc[id_abs_ele]->ivois[DOWNSTREAM_TTC][0] = pele->face[X_HYD][TWO]->element[TWO]->id[ABS_HYD];
      plink->p_neigh_ttc[id_abs_ele]->nvois++;
      plink->p_neigh_ttc[id_abs_ele]->delta_L[DOWNSTREAM_TTC][0] = (pele->face[X_HYD][TWO]->element[TWO]->length + pele->length) / 2;
      // LP_printf(flog,"ABS ele in test, id ele is %d: nvois is = %d\n",id_abs_ele, plink->p_neigh_ttc[id_abs_ele]->nvois);
    }

    // Now dealing with the LAST element of the reach
    pele = preach->p_ele[preach->nele - 1];
    pfacep = pele->face[X_HYD][TWO];
    id_abs_ele = pele->id[ABS_HYD];

    if (preach->nele > 1) {
      plink->p_neigh_ttc[id_abs_ele]->ivois[UPSTREAM_TTC][0] = pele->face[X_HYD][ONE]->element[ONE]->id[ABS_HYD];
      plink->p_neigh_ttc[id_abs_ele]->nvois++;
      plink->p_neigh_ttc[id_abs_ele]->delta_L[UPSTREAM_TTC][0] = (pele->face[X_HYD][ONE]->element[ONE]->length + pele->length) / 2;
      // LP_printf(flog,"ABS ele in test, id ele is %d: nvois is = %d\n",id_abs_ele, plink->p_neigh_ttc[id_abs_ele]->nvois);
    }

    // DOWNSTREAM faces
    switch (HYD_test_sing_face(pfacep, DOWNSTREAM)) {
    case DIFFLUENCE: {

      plink->p_neigh_ttc[id_abs_ele]->ivois[DOWNSTREAM_TTC][0] = pele->face[X_HYD][TWO]->limits[DOWNSTREAM]->faces[TWO][0]->element[TWO]->id[ABS_HYD];
      plink->p_neigh_ttc[id_abs_ele]->nvois++;
      // LP_printf(flog,"ABS ele in test, id ele is %d: nvois is = %d\n",id_abs_ele, plink->p_neigh_ttc[id_abs_ele]->nvois);
      plink->p_neigh_ttc[id_abs_ele]->ivois[DOWNSTREAM_TTC][1] = pele->face[X_HYD][TWO]->limits[DOWNSTREAM]->faces[TWO][1]->element[TWO]->id[ABS_HYD];
      plink->p_neigh_ttc[id_abs_ele]->nvois++;

      plink->p_neigh_ttc[id_abs_ele]->delta_L[DOWNSTREAM_TTC][0] = (pele->face[X_HYD][TWO]->limits[DOWNSTREAM]->faces[TWO][0]->element[TWO]->length + pele->length) / 2;
      plink->p_neigh_ttc[id_abs_ele]->delta_L[DOWNSTREAM_TTC][1] = (pele->face[X_HYD][TWO]->limits[DOWNSTREAM]->faces[TWO][1]->element[TWO]->length + pele->length) / 2;
      // LP_printf(flog,"ABS ele in the downstream DIFF= %d\n", id_abs_ele);
      // LP_printf(flog,"ABS ele in test, id ele is %d: nvois is = %d\n",id_abs_ele, plink->p_neigh_ttc[id_abs_ele]->nvois);
      break;
    }

    case CONFLUENCE: {
      plink->p_neigh_ttc[id_abs_ele]->ivois[DOWNSTREAM_TTC][0] = pele->face[X_HYD][TWO]->limits[DOWNSTREAM]->faces[TWO][0]->element[TWO]->id[ABS_HYD];
      plink->p_neigh_ttc[id_abs_ele]->nvois++;

      plink->p_neigh_ttc[id_abs_ele]->delta_L[DOWNSTREAM_TTC][0] = (pele->face[X_HYD][TWO]->limits[DOWNSTREAM]->faces[TWO][0]->element[TWO]->length + pele->length) / 2;
      // LP_printf(flog,"ABS ele in the downstream CONF= %d\n", id_abs_ele);
      // LP_printf(flog,"ABS ele in test, id ele is %d: nvois is = %d\n",id_abs_ele, plink->p_neigh_ttc[id_abs_ele]->nvois);
      break;
    }

    case TRICONFLUENCE: {
      plink->p_neigh_ttc[id_abs_ele]->ivois[DOWNSTREAM_TTC][0] = pele->face[X_HYD][TWO]->limits[DOWNSTREAM]->faces[TWO][0]->element[TWO]->id[ABS_HYD];
      plink->p_neigh_ttc[id_abs_ele]->nvois++;
      plink->p_neigh_ttc[id_abs_ele]->delta_L[DOWNSTREAM_TTC][0] = (pele->face[X_HYD][TWO]->limits[DOWNSTREAM]->faces[TWO][0]->element[TWO]->length + pele->length) / 2;
      // LP_printf(flog,"ABS ele in the downstream TRIOCONF= %d\n", id_abs_ele);
      // LP_printf(flog,"ABS ele in test, id ele is %d: nvois is = %d\n",id_abs_ele, plink->p_neigh_ttc[id_abs_ele]->nvois);
      break;
    }

    case TRIDIFFLUENCE: {

      plink->p_neigh_ttc[id_abs_ele]->ivois[DOWNSTREAM_TTC][0] = pele->face[X_HYD][TWO]->limits[DOWNSTREAM]->faces[TWO][0]->element[TWO]->id[ABS_HYD];
      plink->p_neigh_ttc[id_abs_ele]->nvois++;
      // LP_printf(flog,"ABS ele in test, id ele is %d: nvois is = %d\n",id_abs_ele, plink->p_neigh_ttc[id_abs_ele]->nvois);
      plink->p_neigh_ttc[id_abs_ele]->ivois[DOWNSTREAM_TTC][1] = pele->face[X_HYD][TWO]->limits[DOWNSTREAM]->faces[TWO][1]->element[TWO]->id[ABS_HYD];
      plink->p_neigh_ttc[id_abs_ele]->nvois++;
      // LP_printf(flog,"ABS ele in test, id ele is %d: nvois is = %d\n",id_abs_ele, plink->p_neigh_ttc[id_abs_ele]->nvois);
      plink->p_neigh_ttc[id_abs_ele]->ivois[DOWNSTREAM_TTC][2] = pele->face[X_HYD][TWO]->limits[DOWNSTREAM]->faces[TWO][2]->element[TWO]->id[ABS_HYD];
      plink->p_neigh_ttc[id_abs_ele]->nvois++;
      // LP_printf(flog,"ABS ele in test, id ele is %d: nvois is = %d\n",id_abs_ele, plink->p_neigh_ttc[id_abs_ele]->nvois);

      plink->p_neigh_ttc[id_abs_ele]->delta_L[DOWNSTREAM_TTC][0] = (pele->face[X_HYD][TWO]->limits[DOWNSTREAM]->faces[TWO][0]->element[TWO]->length + pele->length) / 2;
      plink->p_neigh_ttc[id_abs_ele]->delta_L[DOWNSTREAM_TTC][1] = (pele->face[X_HYD][TWO]->limits[DOWNSTREAM]->faces[TWO][1]->element[TWO]->length + pele->length) / 2;
      plink->p_neigh_ttc[id_abs_ele]->delta_L[DOWNSTREAM_TTC][2] = (pele->face[X_HYD][TWO]->limits[DOWNSTREAM]->faces[TWO][2]->element[TWO]->length + pele->length) / 2;
      // LP_printf(flog,"ABS ele in the downstream TRIDIFF= %d\n", id_abs_ele);
      break;
    }

    case CONF_DIFF: {
      plink->p_neigh_ttc[id_abs_ele]->ivois[DOWNSTREAM_TTC][0] = pele->face[X_HYD][TWO]->limits[DOWNSTREAM]->faces[TWO][0]->element[TWO]->id[ABS_HYD];
      plink->p_neigh_ttc[id_abs_ele]->nvois++;
      // LP_printf(flog,"ABS ele in test, id ele is %d: nvois is = %d\n",id_abs_ele, plink->p_neigh_ttc[id_abs_ele]->nvois);
      plink->p_neigh_ttc[id_abs_ele]->ivois[DOWNSTREAM_TTC][1] = pele->face[X_HYD][TWO]->limits[DOWNSTREAM]->faces[TWO][1]->element[TWO]->id[ABS_HYD];
      plink->p_neigh_ttc[id_abs_ele]->nvois++;

      plink->p_neigh_ttc[id_abs_ele]->delta_L[DOWNSTREAM_TTC][0] = (pele->face[X_HYD][TWO]->limits[DOWNSTREAM]->faces[TWO][0]->element[TWO]->length + pele->length) / 2;
      plink->p_neigh_ttc[id_abs_ele]->delta_L[DOWNSTREAM_TTC][1] = (pele->face[X_HYD][TWO]->limits[DOWNSTREAM]->faces[TWO][1]->element[TWO]->length + pele->length) / 2;
      // LP_printf(flog,"ABS ele in the downstream CONF_DIFF= %d\n", id_abs_ele);
      // LP_printf(flog,"ABS ele in test, id ele is %d: nvois is = %d\n",id_abs_ele, plink->p_neigh_ttc[id_abs_ele]->nvois);
      break;
    }

    case WATER_LEVEL: // River network outlet
    {
      if (pele->face[X_HYD][TWO]->limits[DOWNSTREAM]->ndownstr_reaches == 0) {
      }
    } break;
    default: {
      plink->p_neigh_ttc[id_abs_ele]->ivois[DOWNSTREAM_TTC][0] = pele->face[X_HYD][TWO]->limits[DOWNSTREAM]->faces[TWO][0]->element[TWO]->id[ABS_HYD];
      plink->p_neigh_ttc[id_abs_ele]->nvois++;
      plink->p_neigh_ttc[id_abs_ele]->delta_L[DOWNSTREAM_TTC][0] = (pele->face[X_HYD][TWO]->limits[DOWNSTREAM]->faces[TWO][0]->element[TWO]->length + pele->length) / 2;
      // LP_printf(flog,"ABS ele in the downstream Default= %d\n", id_abs_ele);
      // LP_printf(flog,"ABS ele in test, id ele is %d: nvois is = %d\n",id_abs_ele, plink->p_neigh_ttc[id_abs_ele]->nvois);
      break;
    }
    }
  }
}

/**
 * \fn void CAW_fill_nsub_one_species_riv(s_link_ttc*, int, s_chyd*, FILE*)
 * \brief Fills number of subfaces in each direction (face) for an hydraulic network calculation element
 * \return -
 */
void CAW_fill_nsub_one_species_riv(s_link_ttc *plink, int nele, s_chyd *pchyd, FILE *flog) {
  int n, icar, isub, nneigh;

  for (n = 0; n < nele; n++) {
    for (icar = 0; icar < NB_CARD_TTC; icar++) {
      for (isub = 0; isub < SUB_CARD_TTC; isub++) {
        nneigh = plink->p_neigh_ttc[n]->ivois[icar][isub];
        if (nneigh != NONE_TTC)
          plink->nsub[n][icar]++;
      }
    }
  }
}

/**
 * \fn void CAW_fill_var_one_species_riv(s_species_ttc*, s_chyd*, FILE*)
 * \brief Fetching previous time step TTC variable in libhyd for all elements, for one specie
 * \return -
 *
 * NG : 04/04/2023 : ??? Why is there a duplicate function from CAW_build_var_one_species_riv() ?!
 *
 */
void CAW_fill_var_one_species_riv(s_species_ttc *pspecies, s_chyd *pchyd, int id_species, FILE *flog) {
  int r, n;
  s_reach_hyd *preach;
  s_element_hyd *pele;
  s_hydro_hyd *phydro;

  for (r = 0; r < pchyd->counter->nreaches; r++) {
    preach = pchyd->p_reach[r];

    for (n = 0; n < preach->nele; n++) {
      pele = preach->p_ele[n];
      phydro = pele->center->hydro;
      pspecies->pval_ttc->val[pele->id[ABS_HYD]] = phydro->transp_var[id_species];
      // LP_printf(flog,"Variable de ele %d = %f\n",pele->id[ABS_HYD],phydro->transp_var[id_species]); // NG check
    }
  }
}

/**
 * \fn void CAW_build_var_one_species_riv(s_species_ttc*, s_chyd*, FILE*)
 * \brief Fetching previous time step TTC variable in libhyd for all elements, for one specie
 * \return -
 */
void CAW_build_var_one_species_riv(s_species_ttc *pspecies, s_chyd *pchyd, int id_species, FILE *flog) {
  int r, n;
  s_reach_hyd *preach;
  s_element_hyd *pele;
  s_hydro_hyd *phydro;

  for (r = 0; r < pchyd->counter->nreaches; r++) {
    preach = pchyd->p_reach[r];

    for (n = 0; n < preach->nele; n++) {
      pele = preach->p_ele[n];
      phydro = pele->center->hydro;
      pspecies->pval_ttc->val[pele->id[ABS_HYD]] = phydro->transp_var[id_species];
      // LP_printf(flog,"Variable de ele %d = %f\n",pele->id[ABS_HYD],phydro->transp_var[id_species]); // NG check
    }
  }
}

/**
 * \fn void CAW_fill_Q_one_species_riv(s_carac_ttc*, s_chyd*, int, int, FILE*)
 * \brief Fills river discharges at element faces into carac TTC
 * \return -
 */
void CAW_fill_Q_one_species_riv(s_carac_ttc *pcarac, s_chyd *pchyd, int modname, int id_species, FILE *flog) {

  int id_abs_ele, r, ne, icard, sub_icard;
  s_reach_hyd *preach;
  s_element_hyd *pele;
  s_face_hyd *pface;

  for (r = 0; r < pchyd->counter->nreaches; r++) {
    preach = pchyd->p_reach[r];
    for (ne = 0; ne < preach->nele; ne++) {
      pele = preach->p_ele[ne];
      id_abs_ele = pele->id[ABS_HYD];

      // Update stock change in the flow (in m3/day)
      pcarac->dVdt[id_abs_ele] = pele->mb->stock[MUSK_HYD] / pele->length;

      pface = pele->face[X_HYD][ONE];

      for (icard = 0; icard < BANKS_TTC; icard++) // WEST(=ONE, upstream) upstream et EAST (=TW0, downstream)
      {
        for (sub_icard = 0; sub_icard < SUB_CARD_TTC; sub_icard++) {
          if (icard == DOWNSTREAM_TTC) {
            pcarac->uface[id_abs_ele][icard][sub_icard] = 0.;
            if (sub_icard == 0)
              pcarac->uface[id_abs_ele][icard][sub_icard] = pele->face[X_HYD][TWO]->hydro->Q[T_HYD];

            if (isnan(pele->face[X_HYD][TWO]->hydro->Q[T_HYD]) || isinf(pele->face[X_HYD][TWO]->hydro->Q[T_HYD])) {
              LP_warning(flog, "In Q river update %s, line %d nan or inf produced: %lf  \n", __func__, __LINE__, pele->face[X_HYD][TWO]->hydro->Q[T_HYD]);
              if (sub_icard == 0)
                pcarac->uface[id_abs_ele][icard][sub_icard] = 0;
            }

            /*if (  id_abs_ele == 7144 || id_abs_ele == 7143 || id_abs_ele == 7142  || id_abs_ele == 26715 || id_abs_ele == 26754 || id_abs_ele == 24284 || id_abs_ele == 26161 || id_abs_ele == 13971 )  {
                LP_printf(flog,"%s idabs %d gisid %d Qdownstream %e specdVdt %e  S[MUSK_HYD]: %e S[mb_hyd] %e Err musk hyd %e \n",__func__,id_abs_ele, pele->id[GIS_HYD],
                            pele->face[X_HYD][TWO]->hydro->Q[T_HYD], pcarac->dVdt[id_abs_ele],
                            pele->mb->stock[MUSK_HYD]/86400,   pele->mb->stock[MB_HYD]/86400, pele->mb->error[MUSK_HYD] );
            }*/

          } else // ie. WEST/upstream
          {
            //	if (ne==0) LP_printf(flog,"Type de singularite en amont de lele %d: %s\n",id_abs_ele,HYD_name_sing_type(HYD_test_sing_face(pface,UPSTREAM)));   // NG check
            // Why do we have ne == 0 check ? because we go from upstream to downstream hence at ne == 0 we have the upstream limit.
            if ((ne == 0 && HYD_test_sing_face(pface, UPSTREAM) == CONFLUENCE)) {
              if (sub_icard < 2) { // Here it appears to fill only one face in the upstream? Where does the second Q from upstream come?
                pcarac->uface[id_abs_ele][icard][sub_icard] = pface->limits[UPSTREAM]->faces[ONE][sub_icard]->hydro->Q[T_HYD];

                //                            if (  id_abs_ele == 7144 || id_abs_ele == 7143 || id_abs_ele == 7142  || id_abs_ele == 24284 || id_abs_ele == 26161 || id_abs_ele == 13971)  {
                //                                LP_printf(flog,"%s idabs %d gisid %d subicard %d Qupstream %e  CONFLUENCe \n ",__func__,id_abs_ele, pele->id[GIS_HYD],
                //                                        sub_icard, pface->limits[UPSTREAM]->faces[ONE][sub_icard]->hydro->Q[T_HYD]);
                //                            }
                if (isnan(pface->limits[UPSTREAM]->faces[ONE][sub_icard]->hydro->Q[T_HYD]) == 1 || isinf(pface->limits[UPSTREAM]->faces[ONE][sub_icard]->hydro->Q[T_HYD]) == 1) {
                  LP_warning(flog, "In Q river update %s,at line %d nan or inf produced: %e \n", __func__, __LINE__, pface->limits[UPSTREAM]->faces[ONE][sub_icard]->hydro->Q[T_HYD]);
                  pcarac->uface[id_abs_ele][icard][sub_icard] = 0;
                }
              }

              else {
                pcarac->uface[id_abs_ele][icard][sub_icard] = 0.;
              }
            } else if ((ne == 0 && HYD_test_sing_face(pface, UPSTREAM) == TRICONFLUENCE)) {
              if (sub_icard < 3) {
                pcarac->uface[id_abs_ele][icard][sub_icard] = pface->limits[UPSTREAM]->faces[ONE][sub_icard]->hydro->Q[T_HYD];

                if (isnan(pface->limits[UPSTREAM]->faces[ONE][sub_icard]->hydro->Q[T_HYD]) == 1 || isinf(pface->limits[UPSTREAM]->faces[ONE][sub_icard]->hydro->Q[T_HYD]) == 1) {
                  LP_warning(flog, "In Q river update %s, at line %d nan or inf produced: %lf\n ", __func__, __LINE__, pface->limits[UPSTREAM]->faces[ONE][sub_icard]->hydro->Q[T_HYD]);
                  pcarac->uface[id_abs_ele][icard][sub_icard] = 0.;
                }
              }
            } else if ((ne == 0 && HYD_test_sing_face(pface, UPSTREAM) == DISCHARGE)) // Qsource amont
            {
              //	  *pspecies->plink->pu_ttc->puface[id_abs_ele].uface[icard][sub_icard] = preach->q_upstr;
              //	LP_printf(flog,"qupt = %f\n", preach->q_upstr);
              pcarac->uface[id_abs_ele][icard][sub_icard] = 0.;
              if (sub_icard == 0)
                pcarac->uface[id_abs_ele][icard][sub_icard] = preach->q_upstr;
              /*if (  id_abs_ele == 7144 || id_abs_ele == 7143 || id_abs_ele == 7142 || id_abs_ele == 24284 || id_abs_ele == 26161 || id_abs_ele == 13971)  {
                         LP_printf(flog,"%s idabs %d gisid %d subicard %d Qupstream %e  DISCHARGE \n",__func__,id_abs_ele, pele->id[GIS_HYD],
                             sub_icard, preach->q_upstr);
                        }*/

              if (isnan(preach->q_upstr) == 1 || isinf(preach->q_upstr) == 1) {
                LP_warning(flog, "In Q river update %s, at line %d nan or inf produced: %lf \n", __func__, __LINE__, preach->q_upstr);
                if (sub_icard == 0)
                  pcarac->uface[id_abs_ele][icard][sub_icard] = 0;
              }
            } else {
              pcarac->uface[id_abs_ele][icard][sub_icard] = 0.;
              if (sub_icard == 0)
                pcarac->uface[id_abs_ele][icard][0] = pele->face[X_HYD][ONE]->hydro->Q[T_HYD];

              /*if (  id_abs_ele == 7144 || id_abs_ele == 7143 || id_abs_ele == 7142 || id_abs_ele == 24284 || id_abs_ele == 26161 || id_abs_ele == 13971)  {
                LP_printf(flog,"%s idabs %d gisid %d subicard %d Qupstream %e  other \n",__func__,id_abs_ele, pele->id[GIS_HYD],
                  sub_icard, pele->face[X_HYD][ONE]->hydro->Q[T_HYD]);
                        }*/

              if (isnan(pele->face[X_HYD][ONE]->hydro->Q[T_HYD]) == 1 || isinf(pele->face[X_HYD][ONE]->hydro->Q[T_HYD]) == 1) {
                LP_warning(flog, "In Q river update %s, at line %d nan or inf produced: %lf \n", __func__, __LINE__, pele->face[X_HYD][ONE]->hydro->Q[T_HYD]);
                if (sub_icard == 0)
                  pcarac->uface[id_abs_ele][icard][0] = 0;
              }
            }
          }
        }
      }
      // if (id_abs_ele == 30) LP_printf(flog, "debit (face aval) : %f\n",pcarac->uface[id_abs_ele][EAST_TTC][0]);
    }
  }
}

/**
 * \fn void CAW_fill_inflows_one_species_riv(s_species_ttc*, double, s_chyd*, s_carac_fp*, int , double, FILE*)
 * \brief Computes lateral inflows (ie. from coupling with libfp + external inflows) for all river calculation elements. For non-coupled (repsur) simulation mode.
 * \return -
 *
 * NG 18/04/2023 : Initial function was getting crazy. Has been sliced using elementary bits based on module flow origin (FP, SEB, AQ).
 * in order to directly reuse these in CAW_fill_inflows_one_species_riv_aq() for SW_GW simulation mode.
 *
 */
void CAW_fill_inflows_one_species_riv(s_species_ttc *pspecies, double t, s_chyd *pchyd, s_carac_fp *pcarac_fp, int id_species, double dt, FILE *flog) {

  CAW_cFP_get_fp_fluxes_to_riv(pspecies, pchyd, pcarac_fp, id_species, t, dt, flog);

  if (Simul->pset_caw->transport_module[SEB_CAW] == YES) {
    CAW_cSEB_get_seb_fluxes_to_riv(pspecies, pchyd, flog);
  }
}

/**
 * \fn void CAW_fill_inflows_one_species_riv_aq (s_species_ttc*, double, s_chyd*, s_carac_fp*, s_carac_aq*, int, double, FILE*)
 * \brief Computes lateral inflows (ie. from run-off (coupling with libfp) + external inflows + river-aquifer exchanges)
 * Function called in coupled (SW_GW) mode.
 * \return -
 *
 * NG 18/04/2023 : Initial function was getting crazy. Has been sliced using elementary bits based on module flow origin (FP, SEB, AQ).
 * NG 12/06/2023 : Adding integration of transport overflows from AQ is case of coupled SOLUTE simulation.
 */
void CAW_fill_inflows_one_species_riv_aq(s_species_ttc *pspecies, double t, s_chyd *pchyd, s_carac_fp *pcarac_fp, s_carac_aq *pcarac_aq, int id_species, double dt, FILE *flog) {

  // Calculation of SOLUTE aquifer overflows to libfp (= filling of WATBAL RSV_SOUT reservoirs).
  if (pspecies->pset->type == SOLUTE_TTC) {
    CAW_cFP_calc_overflows_cprod_sout_all(pcarac_fp, pcarac_aq, dt, id_species, flog);
  }

  // Calculation of total lateral transport inputs (associated with run-off, overflow and rsv_direct and external point inflows) to river elements.
  CAW_cFP_get_fp_fluxes_to_riv(pspecies, pchyd, pcarac_fp, id_species, t, dt, flog);

  // Calculation of aquifer riv-aq transport fluxes.
  CAW_cAQ_get_rivaq_fluxes_to_riv(pspecies, pchyd, pcarac_aq->pcmsh, flog);

  // If libseb activated, calculation of fluxes from the atmosphere
  if (Simul->pset_caw->transport_module[SEB_CAW] == YES) {
    CAW_cSEB_get_seb_fluxes_to_riv(pspecies, pchyd, flog);
  }
}

/**
 * \fn void CAW_cFP_calc_overflows_cprod_sout_all (s_carac_fp*, s_carac_aq*, double, int, FILE*)
 * \brief
 * \return -
 */
void CAW_cFP_calc_overflows_cprod_sout_all(s_carac_fp *pcarac_fp, s_carac_aq *pcarac_aq, double dt, int id_species, FILE *fpout) {

  int l, nlayer;
  double theta = pcarac_aq->settings->general_param[THETA];
  s_layer_msh **p_layer = pcarac_aq->pcmsh->p_layer;

  FP_reinit_transp_rsv_cprod(pcarac_fp, RSV_SOUT, id_species, fpout); // Full reset of all RSV_SOUT CPROD fields.
  nlayer = pcarac_aq->pcmsh->pcount->nlayer;

  for (l = 0; l < nlayer; l++) {
    CAW_cFP_calc_overflows_cprod_layer(p_layer[l], pcarac_fp, dt, theta, id_species, fpout);
  }
}

/**
 * \fn void CAW_cFP_calc_overflows_cprod_layer (s_layer_msh*, s_carac_fp*, double, double, int, FILE*)
 * \brief
 * \return -
 */

void CAW_cFP_calc_overflows_cprod_layer(s_layer_msh *player, s_carac_fp *pcarac_fp, double dt, double theta, int id_species, FILE *fpout) {

  int e, nele;
  s_ele_msh **p_ele;
  s_id_spa *link;
  s_cprod_fp *pcprod;
  double vol, flux;
  double *q_ovf;
  s_ele_msh *pele;

#ifdef OMP
  s_smp *psmp;
  psmp = pcarac_fp->psmp;
#endif
  p_ele = player->p_ele;
  // private(e,pele) shared(pcarac_fp,theta,fpout)
  nele = player->nele;

  /* #ifdef OMP
    int taille;
    int nthreads;
    nthreads=psmp->nthreads;
    omp_set_num_threads(psmp->nthreads);
    psmp->chunk=PC_set_chunk_size_silent(fpout,player->nele,nthreads);
    taille=psmp->chunk;
  // #pragma omp parallel shared(p_ele,nthreads,taille) private(e,link,pele,output_SURF,prct)
    {
  // #pragma omp for schedule(dynamic,taille)
  #endif  */

  for (e = 0; e < nele; e++) {
    pele = p_ele[e];
    link = pele->link[SURF_AQ];

    if (link != NULL) {
      link = (s_id_spa *)SPA_browse_all_struct(IDS_SPA, link, BEGINNING_FP);
    } else {
      if (pele->loc == TOP_MSH && pele->phydro->pbound == NULL) {
        LP_error(fpout, "In CaWaQS%4.2f : In file %s, function %s at line %d : TOP aquifer cell GIS_ID %d ABS_ID %d is not linked with surface. CAUCHY condition missing.\n", NVERSION_CAW, __FILE__, __func__, __LINE__, pele->id[GIS_MSH], pele->id[ABS_MSH]);
      }
    }

    while (link != NULL) {
      pcprod = pcarac_fp->p_cprod[link->id];
      vol = 0.;
      flux = 0.;

      if (pele->loc == TOP_MSH && (pele->phydro->pbound != NULL && pele->phydro->pbound->type == CAUCHY)) {

        if (pele->modif[MODIF_CALC_AQ] == NO_LIM_AQ) {
          q_ovf = SPA_calcul_values(CPROD_GW_SPA, pele, link->prct, theta, 0., fpout); // Fetching water overflow
          // if (q_ovf[0] < 0.) LP_error(fpout,"ovf negatif\n");
          vol = RSV_q_to_volume(q_ovf[0], dt, SEC_TS, fpout);
          flux = link->prct * vol * pele->phydro->transp_var[id_species]; // g

          if (q_ovf[0] > 0) {
            pcprod->pwat_bal->trflux[id_species][RSV_SOUT] += (flux / pcprod->area); // Overflow matter flux in g.m-2
            // LP_printf(fpout, " Specie %d - AQ_cell %d Q_from_sout %e [m3/s] Flux_from_sout %e [g/m2] Covf_from_sout %e [g/m3]\n", id_species, pele->id[ABS_MSH], q_ovf[0],
            //          pcprod->pwat_bal->trflux[id_species][RSV_SOUT], pele->phydro->transp_var[id_species]);  // NG check
          }
          free(q_ovf);
        } else {
          pcprod->pwat_bal->trflux[id_species][RSV_SOUT] += 0.;
        }
      }
      link = link->next;
    }
  }
  /*
#ifdef OMP
} // end of parallel section
#endif
*/
}

/**
 * \fn void CAW_cFP_get_fp_fluxes_to_riv (s_species_ttc*, s_chyd*, s_carac_fp*, int, double, double, FILE*) {
 * \brief Computes lateral inputs, associated with run-off (from coupling with libfp) and external point inflows
 * Affects pspecies->p_boundary_ttc[id_abs]->valcl[BANKS_TTC] with input value for all river elements.
 * \return
 */
void CAW_cFP_get_fp_fluxes_to_riv(s_species_ttc *pspecies, s_chyd *pchyd, s_carac_fp *pcarac_fp, int id_species, double t, double dt, FILE *flog) {

  int i, r, ne, iface, id_abs;
  int flag_amont = 0;
  double *fluxlat;
  double q, trvar, qele, rho, cp, mapp, trmix;
  s_reach_hyd *preach;
  s_id_spa *link_fp;
  s_cprod_fp *pcprod;
  s_element_hyd *pele;

  for (r = 0; r < pchyd->counter->nreaches; r++) {
    preach = pchyd->p_reach[r];

    if (preach->limits[ONE]->nupstr_reaches == 0) {
      flag_amont = 1;
    } else {
      flag_amont = 0;
    }

    link_fp = preach->link_fp;

    if (link_fp != NULL) {
      link_fp = (s_id_spa *)SPA_browse_all_struct(IDS_SPA, link_fp, BEGINNING_HYD);
    } else {
      LP_error(flog, "CaWaQS%4.2f : File %s, function %s at line %d : Link pointer towards CPROD unit is NULL for reach GIS id %d.\n", NVERSION_CAW, __FILE__, __func__, __LINE__, preach->id[GIS_HYD]);
    }

    // Fetching linked CPROD pointer. As river reach/CPROD link is 1-on-1, no need to browse *link
    pcprod = pcarac_fp->p_cprod[link_fp->id];

    // Lateral transport fluxes at the reach scale (due to libfp coupling = flux from rsv_direct + runoff + overflow (if coupled) (= either matter in g/m2 or energy en W/m2)
    fluxlat = SPA_calcul_transport_fluxes(RIV_SPA, pcprod, id_species, link_fp->prct, 0., 0., flog);

    // Distributing at the river element scale
    for (ne = 0; ne < preach->nele; ne++) {
      trmix = 0.; // Global mixing transport variable going to the element. Unused for calculations. For printf purposes only.
      mapp = 0.;  // Global transport flux to the element.
      qele = 0.;  // Water discharge due to TOTAL lateral inflows (i.e. runoff + pt_inflows)) m3/s

      pele = preach->p_ele[ne];
      id_abs = pele->id[ABS_HYD];

      // SW : Saving parameters to calculate the fluxes at the NEUMANN face
      pspecies->p_param_ttc[id_abs]->surf = pele->center->hydro->Surf;
      pspecies->p_param_ttc[id_abs]->width = pele->reach->width;

      // Step 1 = if declared, fetching exogenous flows
      if (pspecies->pset->type == SOLUTE_TTC) {

        for (iface = 0; iface < NELEMENT_HYD; iface++) {
          for (i = 0; i < pele->face[Y][iface]->ninflows; i++) {
            q = 0.;
            trvar = 0.;

            if (pele->face[Y][iface]->pt_inflows[i]->concentration[id_species] != NULL) {
              q = TS_function_value_t(t, pele->face[Y][iface]->pt_inflows[i]->discharge, flog) * dt;                // Water volumes on lateral faces ONE, TWO at time t (m3)
              trvar = TS_function_value_t(t, pele->face[Y][iface]->pt_inflows[i]->concentration[id_species], flog); // Inflow concentration (g/m3)
              mapp += (q * trvar);                                                                                  // g
              // LP_printf(flog,"nele %d face %s q = %f mapp = %f c = %f \n",pele->id[GIS_HYD], HYD_riverbank_inflow(iface), q, mapp, trvar); // NG check
            }
          }
        }
      } else {
        // Not developed yet for HEAT transport.
      }

      qele = pele->center->hydro->qapp[RUNOFF_HYD][T_HYD] * pele->length; // m3/s (Reminder : RUNOFF_HYD field is actually runoff + overflows)

      // Step 2 : Adding fluxes from libfp forcing
      if (pspecies->pset->type == SOLUTE_TTC) {
        if (flag_amont) {
          if (ne == 0) {
            mapp += ((fluxlat[0] * FRAC_TR_UPSTREAM_CAW * pcprod->area) + (fluxlat[0] * FRAC_TR_UPSTREAM_CAW * pcprod->area * (pele->length / preach->length))); // NG : checked
          } else {
            mapp += (fluxlat[0] * FRAC_TR_UPSTREAM_CAW * pcprod->area * (pele->length / preach->length));
          }
        } else {
          mapp += (fluxlat[0] * pcprod->area * (pele->length / preach->length));
        }

        // Affecting to TTC

        /*pspecies->p_boundary_ttc[id_abs]->valcl[BANKS_TTC][0] = 0.;
        pspecies->p_boundary_ttc[id_abs]->valcl[BANKS_TTC][0] = mapp / dt / pele->length;  // Setting flux to face with NEU_FACE_TTC condition. Unit is g.s-1.m-1

        if (qele > EPS_RSV) {
          trmix = mapp / (qele * dt);
          LP_printf(flog," id_abs = %d - Flux [g/m2] = %f Concentration [g/m3] = %f Qele [m3/s] = %f Apport bank %f \n",id_abs, fluxlat[0], trmix, qele,mapp / dt / pele->length);
        } */

        pspecies->p_boundary_ttc[id_abs]->valcl[BANKS_TTC][0] = 0.;

        trmix = mapp / (qele * dt);
        // if (trmix < CAW_SOLUTE_CRIV_THRESH) {                                                   // WARNING : Creating a matter flux MB error between subsurface inputs and hydraulic network !!
        if (qele > 1e-6) {
          pspecies->p_boundary_ttc[id_abs]->icl[BANKS_TTC][0] = NEU_FACE_TTC;
          pspecies->p_boundary_ttc[id_abs]->valcl[BANKS_TTC][0] = mapp / dt / pele->length; // Setting flux to face with NEU_FACE_TTC condition. Unit is g.s-1.m-1
          // LP_printf(flog," id_abs = %d - Flux [g/m2] = %f Concentration [g/m3] = %f Qele [m3/s] = %f Apport bank %f \n",id_abs, fluxlat[0], trmix, qele,mapp / dt / pele->length);    // NG check
        }

      } else if (pspecies->pset->type == HEAT_TTC) {
        /* SW 19/07/2021 The unit of the fluxes calculated by libfp (fluxlat[0]) is W/m2 */

        rho = pspecies->p_param_ttc[id_abs]->param_thermic[WATER_TTC][RHO_TTC];
        cp = pspecies->p_param_ttc[id_abs]->param_thermic[WATER_TTC][HEAT_CAP_TTC];

        if (flag_amont) {
          if (ne == 0) {
            // K m3 s-1 = energy from libfp : divise par rho*cp pour avoir la meme unite que q*c
            mapp += (fluxlat[0] * FRAC_TR_UPSTREAM_CAW * pcprod->area + fluxlat[0] * FRAC_TR_UPSTREAM_CAW * pcprod->area * (pele->length / preach->length)) / (rho * cp);
          } else {
            mapp += fluxlat[0] * FRAC_TR_UPSTREAM_CAW * pcprod->area * (pele->length / preach->length) / (rho * cp);
          }
        } else {
          // K m3 s-1 = energy from libfp : divise par rho*cp pour avoir la meme unite que q*c
          mapp += fluxlat[0] * pcprod->area * (pele->length / preach->length) / (rho * cp);
        }

        pspecies->p_boundary_ttc[id_abs]->valcl[BANKS_TTC][0] = 0.;                                                         // NORTH is for lateral input. SOUTH is for atmosphere
        pspecies->p_boundary_ttc[id_abs]->valcl[BANKS_TTC][0] = mapp / pspecies->p_param_ttc[id_abs]->param_syst[SIZE_TTC]; // Setting flux to face with NEU_FACE_TTC condition. Unit is K m2 s-1 Q*T/dx
      }
    }
    free(fluxlat);
  }
}

/**
 * \fn void CAW_cSEB_get_seb_fluxes_to_riv (s_species_ttc*, s_chyd*, FILE*)
 * \brief Passes the libseb flux to libttc for all river elements
 * \return -
 */
void CAW_cSEB_get_seb_fluxes_to_riv(s_species_ttc *pspecies, s_chyd *pchyd, FILE *flog) {

  int ne, r, id_abs;
  s_reach_hyd *preach;
  s_element_hyd *pele;

  for (r = 0; r < pchyd->counter->nreaches; r++) {
    preach = pchyd->p_reach[r];
    for (ne = 0; ne < preach->nele; ne++) {

      pele = preach->p_ele[ne];
      id_abs = pele->id[ABS_HYD];

      pspecies->p_boundary_ttc[id_abs]->valcl[ATMOS_TTC][0] = 0.;
      // The sign here might be negative, it has to be verified
      pspecies->p_boundary_ttc[id_abs]->valcl[ATMOS_TTC][0] = Simul->mto_seb->H_flux_ttc[id_abs]; // Unit in K m2 s-1
    }
  }
}

/**
 * \fn void CAW_cAQ_get_rivaq_fluxes_to_riv (s_species_ttc*, s_chyd*, s_cmsh*, FILE*)
 * \brief
 * \return -
 *
 * NG : 07/06/2023 : WARNING. Has to be properly updated in case of HEAT transport
 * with a proper storage of flux_conduction, flux_advection and T_rivbed !!!
 * Commenting for now.
 *
 */
void CAW_cAQ_get_rivaq_fluxes_to_riv(s_species_ttc *pspecies, s_chyd *pchyd, s_cmsh *pcmsh, FILE *flog) {

  int r, ne, id_abs, id_aqlayer, id_aqele;
  double rho, cp;
  double flux_conduction = 0;
  double flux_advection = 0; // Inflows from the aquifer
  double T_rivbed;
  double surf, watlevel;
  double rivtemp;
  double flux_rivaq, q_rivaq;
  double T_aquifer;
  s_layer_msh *player;
  s_ele_msh *pele_aq;
  s_reach_hyd *preach;
  s_element_hyd *pele;
  s_face_hyd *pface;

  for (r = 0; r < pchyd->counter->nreaches; r++) {
    preach = pchyd->p_reach[r];

    for (ne = 0; ne < preach->nele; ne++) {

      pele = preach->p_ele[ne];
      pface = preach->p_faces[ne];
      id_abs = pele->id[ABS_HYD];

      if (pface->id_gw != NULL) {

        id_aqlayer = pface->id_gw->id_lay;
        id_aqele = pface->id_gw->id;
        player = pcmsh->p_layer[id_aqlayer];
        pele_aq = player->p_ele[id_aqele];

        q_rivaq = pele->center->hydro->qapp[INR_HYD][T_HYD] / preach->width;

        if (pele_aq->priverbed_ttc != NULL) {
          flux_advection = pele_aq->priverbed_ttc->flux_adv[VAR_ABOVE_TTC];
          flux_conduction = pele_aq->priverbed_ttc->flux_cond[VAR_ABOVE_TTC];
          T_rivbed = pele_aq->priverbed_ttc->temperature[VAR_AQUITARD_TTC][0];

          /* NG : 07/06/2023 : ***WARNING*** *transp_var is meant to store the transport variable of the cell for all species !!!!
              Size management of *transp_var messy and non-generic. This part has to be revised. Commenting for now here and in the writing of
              the mass balance as well (manage_link_output.c > CAW_river_budget_transport()). */

          // pele->center->hydro->transp_var[3] = flux_conduction;
          // pele->center->hydro->transp_var[4] = flux_advection;
          // pele->center->hydro->transp_var[5] = T_rivbed;
        }
      }

      watlevel = pele->center->hydro->Width;
      surf = pele->center->hydro->H[T_HYD] * preach->width;

      // Save parameters to calculate the fluxes at the NEUMANN Face
      pspecies->p_param_ttc[id_abs]->surf = surf;
      pspecies->p_param_ttc[id_abs]->width = pele->reach->width;
      rivtemp = pele->center->hydro->transp_var[0];

      if (pspecies->pset->type == SOLUTE_TTC) {

        if (pface->id_gw != NULL) {

          flux_rivaq = 0.;

          if (pele->center->hydro->qapp[INR_HYD][T_HYD] > EPS_CAW) {
            flux_rivaq = pele->center->hydro->qapp[INR_HYD][T_HYD] * pele_aq->phydro->transp_var[0]; // flux from AQ to RIV in g.s-1.m-1 (NEUMANN condition)
          } else {
            flux_rivaq = pele->center->hydro->qapp[INR_HYD][T_HYD] * pele->center->hydro->transp_var[0]; // flux from RIV to AQ in g.s-1.m-1
          }

          pspecies->p_boundary_ttc[id_abs]->valcl[RIVAQ_TTC][0] = flux_rivaq;
        } else {
          pspecies->p_boundary_ttc[id_abs]->valcl[RIVAQ_TTC][0] = 0.;
        }
      } else if (pspecies->pset->type == HEAT_TTC) {

        rho = pspecies->p_param_ttc[id_abs]->param_thermic[WATER_TTC][RHO_TTC];
        cp = pspecies->p_param_ttc[id_abs]->param_thermic[WATER_TTC][HEAT_CAP_TTC];

        /* NG : 07/06/2023 : ***WARNING*** *transp_var is meant to store the transport variable of the cell for all species !!!!
                              Size management of *transp_var messy and non-generic. This part has to be revised. Commenting if test below for now */

        /*if (watlevel != 0) {
          pele->center->hydro->transp_var[2] = (pspecies->p_boundary_ttc[id_abs]->valcl[BANKS_TTC][0] * pspecies->p_param_ttc[id_abs]->param_syst[SIZE_TTC]) * rho * cp;  // Calculate the heat stored in the cell
        }
        else {
          pele->center->hydro->transp_var[2] = 0;
        }*/

        if (pface->id_gw != NULL) {
          // Set the temperatures to put boundary conditions on riverbed temperatures
          if (pele_aq->phydro->transp_var != NULL) {
            T_aquifer = pele_aq->phydro->transp_var[0];
          } else {
            T_aquifer = 278.15;
            // LP_warning(flog, "Transp_var[%d] is NULL T_aquifer is %lf \n ", id_abs, T_aquifer);
          }

          //  LP_printf(flog,"BANKS id_abs= %d, flux_conduction %lf flux_advection %lf\n",id_abs,flux_conduction, flux_advection);
          // flux_rivaq = q_rivaq*(pele_aq->phydro->transp_var[0] + 273.15) /pspecies->p_param_ttc[id_abs]->param_syst[SIZE_TTC];

          // flux_rivaq=pele->reach->width*(flux_conduction+flux_advection)/rho/cp;
          // flux_rivaq = 0.;

          if (pele->center->hydro->qapp[INR_HYD][T_HYD] >= 0) { // NG : 12/06/2023 : Weird line as pele->length and pspecies->p_param_ttc[id_abs]->param_syst[SIZE_TTC] refer to as the same thing.
            flux_rivaq = pele->center->hydro->qapp[INR_HYD][T_HYD] * pele->length * T_rivbed / pspecies->p_param_ttc[id_abs]->param_syst[SIZE_TTC];
          } else {
            flux_rivaq = pele->center->hydro->qapp[INR_HYD][T_HYD] * pele->length / pspecies->p_param_ttc[id_abs]->param_syst[SIZE_TTC];
            // LP_warning(flog, "Qele[%d] is negative outflow to aquifer: cmel %lf, qele %e \n ", id_abs, rivtemp, pele->center->hydro->qapp[INR_HYD][T_HYD] * pele->length);
          }

          pspecies->p_boundary_ttc[id_abs]->valcl[RIVAQ_TTC][0] = flux_rivaq; // K m2 s-1

        } else {
          pspecies->p_boundary_ttc[id_abs]->valcl[RIVAQ_TTC][0] = 0.;

          // Set BC DIRICHLET Center to the cells that are not explicitly simulating river-aquifer exchanges
          pspecies->p_boundary_ttc[id_abs]->icl[0][0] = DIRI_CENTER_TTC;
          pspecies->p_boundary_ttc[id_abs]->icl[0][1] = DIRI_CENTER_TTC;
          pspecies->p_boundary_ttc[id_abs]->icl[1][0] = DIRI_CENTER_TTC;
          pspecies->p_boundary_ttc[id_abs]->icl[1][1] = DIRI_CENTER_TTC;
          pspecies->p_boundary_ttc[id_abs]->icl[1][2] = DIRI_CENTER_TTC;
          pspecies->p_boundary_ttc[id_abs]->icl[2][0] = DIRI_CENTER_TTC;
          pspecies->p_boundary_ttc[id_abs]->icl[3][0] = DIRI_CENTER_TTC;
          pspecies->p_boundary_ttc[id_abs]->icl[4][0] = DIRI_CENTER_TTC;
          pspecies->p_boundary_ttc[id_abs]->icl[5][0] = DIRI_CENTER_TTC;
        }
        // Set BC DIRICHLET Center to the cells that have water level lower than 1e-10 m2 wet surface area
        if (surf < 1e-10) {
          pspecies->p_boundary_ttc[id_abs]->icl[0][0] = DIRI_CENTER_TTC;
          pspecies->p_boundary_ttc[id_abs]->icl[0][1] = DIRI_CENTER_TTC;
          pspecies->p_boundary_ttc[id_abs]->icl[1][0] = DIRI_CENTER_TTC;
          pspecies->p_boundary_ttc[id_abs]->icl[1][1] = DIRI_CENTER_TTC;
          pspecies->p_boundary_ttc[id_abs]->icl[1][2] = DIRI_CENTER_TTC;
          pspecies->p_boundary_ttc[id_abs]->icl[2][0] = DIRI_CENTER_TTC;
          pspecies->p_boundary_ttc[id_abs]->icl[3][0] = DIRI_CENTER_TTC;
          pspecies->p_boundary_ttc[id_abs]->icl[4][0] = DIRI_CENTER_TTC;
          pspecies->p_boundary_ttc[id_abs]->icl[5][0] = DIRI_CENTER_TTC;
        }

        if (rivtemp < T_0 || rivtemp > T_LIMIT_UP) {
          // Set BC DIRICHLET Center to the cells that have temperatures below 0C and above 35C to by-pass initialisation of hydraulics
          pspecies->p_boundary_ttc[id_abs]->icl[0][0] = DIRI_CENTER_TTC;
          pspecies->p_boundary_ttc[id_abs]->icl[0][1] = DIRI_CENTER_TTC;
          pspecies->p_boundary_ttc[id_abs]->icl[1][0] = DIRI_CENTER_TTC;
          pspecies->p_boundary_ttc[id_abs]->icl[1][1] = DIRI_CENTER_TTC;
          pspecies->p_boundary_ttc[id_abs]->icl[1][2] = DIRI_CENTER_TTC;
          pspecies->p_boundary_ttc[id_abs]->icl[2][0] = DIRI_CENTER_TTC;
          pspecies->p_boundary_ttc[id_abs]->icl[3][0] = DIRI_CENTER_TTC;
          pspecies->p_boundary_ttc[id_abs]->icl[4][0] = DIRI_CENTER_TTC;
          pspecies->p_boundary_ttc[id_abs]->icl[5][0] = DIRI_CENTER_TTC;
        }

        // pele_aq->phydro->transp_var[0] < 273.15 || pele_aq->phydro->transp_var[0] > 320.15
        /*	//if ( id_abs == 270 || id_abs == 7144 || id_abs == 7143 || id_abs == 7142 || id_abs == 24284 || id_abs == 26161   || id_abs == 26222 || id_abs == 26223 || id_abs == 13971 || id_abs == 7527 || id_abs ==  8127) { //id_abs == 14055 || id_abs == 13971 || id_abs == 8179 rivtemp < 273.15 || rivtemp > 310.15

                  LP_printf(flog," RIVAQ id_abs= %d,  cmel = %lf qele = %e size: %lf Fx = %e pele_length: %lf, v_rivaq %e flux adv %lf cond %lf \n",
                        id_abs, pele_aq->phydro->transp_var[0],
                        pele->center->hydro->qapp[INR_HYD][T_HYD]*pele->length,
                        pspecies->p_param_ttc[id_abs]->param_syst[SIZE_TTC],
                        pspecies->p_boundary_ttc[id_abs]->valcl[RIVAQ_TTC][0],
                        pele->length, q_rivaq,  flux_advection, flux_conduction);
                  //}*/
        // add here libseb fluxes aux SOUTH faces
      }
    }
  }
}

/**
 * \fn void CAW_fill_wetsurf_one_specie_riv(s_link_ttc*, s_chyd*, int, FILE*)
 * \brief Computes wet surfaces at the center of each river element
 * \return -
 */
void CAW_fill_wetsurf_one_specie_riv(s_link_ttc *plink, s_chyd *pchyd, int id_species, FILE *flog) {

  int r, ne, id_abs_ele;
  double dVdt;
  double dV_wetsurf;
  double coeff_correction = 1.;
  s_reach_hyd *preach;
  s_element_hyd *pele;

  for (r = 0; r < pchyd->counter->nreaches; r++) {
    preach = pchyd->p_reach[r];

    for (ne = 0; ne < preach->nele; ne++) {
      pele = preach->p_ele[ne];
      id_abs_ele = pele->id[ABS_HYD];

      // Calcul de surface mouillée sur section rectangulaire à largeur constante
      if (pele->center->hydro->H[T_HYD] < HWET_RES_CAW) {
        plink->p_param_calc_ttc[id_abs_ele]->coeff[SURF_T_TTC] = HWET_RES_CAW * preach->width; // J'impose une hauteur minimale, sinon crash en cas d'assecs.
      } else {
        plink->p_param_calc_ttc[id_abs_ele]->coeff[SURF_T_TTC] = pele->center->hydro->H[T_HYD] * preach->width;
      }

      if (Simul->chronos->t[BEGINNING] <= 0) {
        plink->p_param_calc_ttc[id_abs_ele]->coeff[SURF_TITER_TTC] = HWET_RES_CAW * preach->width; // H[ITER_HYD] n'est pas défini au premier pas de temps de calcul.
      } else {
        if (pele->center->hydro->H[ITER_HYD] < HWET_RES_CAW) {
          plink->p_param_calc_ttc[id_abs_ele]->coeff[SURF_TITER_TTC] = HWET_RES_CAW * preach->width;
        } else {
          plink->p_param_calc_ttc[id_abs_ele]->coeff[SURF_TITER_TTC] = pele->center->hydro->H[ITER_HYD] * preach->width;
        }
      }
    }
  }

  // DK VERSION
  /*  for (r = 0; r < pchyd->counter->nreaches; r++) {
     preach = pchyd->p_reach[r];

     for (ne = 0; ne < preach->nele; ne++) {
       pele = preach->p_ele[ne];
       id_abs_ele = pele->id[ABS_HYD];

       dVdt = pele->mb->stock[MUSK_HYD];
       dV_wetsurf = (pele->center->hydro->H[ITER_HYD] * preach->width - pele->center->hydro->H[ITER_HYD] * preach->width) * pele->length;
       if (dV_wetsurf != 0) {
         coeff_correction = fabs(dVdt) / fabs(dV_wetsurf);

       }

       plink->p_param_calc_ttc[id_abs_ele]->coeff[SURF_T_TTC] = pele->center->hydro->Q[T_HYD] * 86400 / pele->length;
       plink->p_param_calc_ttc[id_abs_ele]->coeff[SURF_TITER_TTC] = (pele->center->hydro->Q[T_HYD] * 86400 - dVdt) / pele->length;

       //LP_printf(flog, "%s abs %d gis %d  wetsurft %e  wetsurftit %e preach_width %lf coeff_correct %d \n ", __func__, id_abs_ele, pele->id[GIS_HYD],
        //         plink->p_param_calc_ttc[id_abs_ele]->coeff[SURF_T_TTC], plink->p_param_calc_ttc[id_abs_ele]->coeff[SURF_TITER_TTC], preach->width, coeff_correction);

     //  LP_printf(flog, "%s abs %d gis %d  H[t] %e H[tit] %e \n ", __func__, id_abs_ele, pele->id[GIS_HYD],
      //           pele->center->hydro->H[T_HYD], pele->center->hydro->H[ITER_HYD]);

     //  LP_printf(flog, "%s abs %d gis %d  Q[t] %e  Q[tit] %e \n ", __func__, id_abs_ele, pele->id[GIS_HYD],
     //            pele->center->hydro->Q[T_HYD], pele->center->hydro->Q[ITER_HYD]);

     //  LP_printf(flog, "%s abs %d gis %d  VEL %e  SURF %e \n ", __func__, id_abs_ele, pele->id[GIS_HYD],
      //           pele->center->hydro->Vel, pele->center->hydro->Surf);


       if (isnan(pele->center->hydro->H[T_HYD]) == 1 || isinf(pele->center->hydro->H[T_HYD]) == 1) {
         LP_warning(flog, "In Q river update %s, line %d nan or inf produced: %lf at ele %d ", __func__, __LINE__, pele->center->hydro->H[T_HYD], pele->id[GIS_HYD]);
         plink->p_param_calc_ttc[id_abs_ele]->coeff[SURF_T_TTC] = 0;
       }
       if (isnan(pele->center->hydro->H[ITER_HYD]) == 1 || isinf(pele->center->hydro->H[ITER_HYD]) == 1) {
         LP_warning(flog, "In Q river update %s, line %d nan or inf produced: %lf at ele %d ", __func__, __LINE__, pele->center->hydro->H[T_HYD], pele->id[GIS_HYD]);
         plink->p_param_calc_ttc[id_abs_ele]->coeff[SURF_TITER_TTC] = 0;
       }
     }
   }*/
}

/**
 * \fn void CAW_update_variable_one_specie_riv(s_species_ttc*, s_chyd*, int, FILE*)
 * \brief Refreshes libhyd elements transport variable for one specie, after TTC calculations
 * \return -
 */
void CAW_update_variable_one_specie_riv(s_species_ttc *pspecies, s_chyd *pchyd, int id_species, FILE *flog) {

  int r, ne;
  s_reach_hyd *preach;
  s_element_hyd *pele;
  s_hydro_hyd *phydro;

  for (r = 0; r < pchyd->counter->nreaches; r++) {
    preach = pchyd->p_reach[r];
    for (ne = 0; ne < preach->nele; ne++) {
      pele = preach->p_ele[ne];
      phydro = pele->center->hydro;
      phydro->transp_var[id_species] = pspecies->pgc->x[pele->id[ABS_HYD]];
    }
  }
}