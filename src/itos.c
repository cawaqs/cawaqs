/*-------------------------------------------------------------------------------
 *
 * SOFTWARE NAME: cawaqs
 * FILE NAME: itos.c
 *
 * CONTRIBUTORS: Nicolas GALLOIS, Baptiste LABARTHE, Shuaitao WANG,
 *               Agnès RIVIERE, Deniz KILIC, Mathias MAILLOT,
 *               Fulvia BARATELLI, Pierre GUILLOU, Emmanuel LEDOUX, Nicolas FLIPO
 *
 * SOFTWARE BRIEF DESCRIPTION: Physically based coupled surface–subsurface
 * model for joint water, matter and energy flux balances and flow dynamics
 * simulation within all compartments of a hydrosystem (sub-surface, hydraulic
 * network, vadose zone, aquifer system and stream-aquifer exchanges).
 *
 * Software developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the cawaqs Software.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Software and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <time.h>
#include <stdarg.h>
#include <sys/time.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <malloc.h>
#include <math.h>
#include <libprint.h>
#include <time_series.h>
#include "libpc.h"
#include "IO.h"
#include "GC.h"
#include "reservoir.h"
#include "spa.h"
#include "FP.h"
#include "NSAT.h"
#include "CHR.h"
#include "HYD.h"
#include "MSH.h"
#include "AQ.h"
#include "WET.h"
#include "TTC_coupled.h"
#include "SEB.h"
#include "CAW.h"

char *CAW_sim_name(int type, FILE *fp) {

  char *name;

  switch (type) {
  case SW_GW: {
    name = strdup("HYDROSYSTEM");
    break;
  }
  case SURF_WAT: {
    name = strdup("SURFACE WATER");
    break;
  }
  case GROUND_WAT: {
    name = strdup("GROUNDWATER");
    break;
  }
  case GW_LAKE: { // SW 28/04/2016 add simulation type GW_LAKE
    name = strdup("GROUNDWATER AND LAKE");
    break;
  }
  case SURF_WAT_NSAT: {
    name = strdup("SURFACE WATER AND ZNS");
    break;
  }
  case SURF_WAT_NSAT_HDERM: {
    name = strdup("SURFACE ZNS AND HYPERDERMIC FLOW");
    break;
  }
  case SW_HDERM_GW: {
    name = strdup("VOLCANIC HYDROSYSTEM");
    break;
  }
  default: {
    LP_error(fp, "No sim_type num %d \n", type);
    break;
  }
  }
  return name;
}

char *CAW_sim_outputs(int type) {

  char *name;

  switch (type) {
  case MASS_IO: {
    name = strdup("GROUNDWATER MASS BALANCE");
    break;
  }
  case NSAT_IO: {
    name = strdup("UNSATURATED ZONE MASS BALANCE");
    break;
  }
  case FP_IO: {
    name = strdup("SURFACE MASS BALANCE");
    break;
  }
  case HYD_Q_IO: {
    name = strdup("HYDRAULIC DISCHARGE");
    break;
  }
  case HYD_H_IO: {
    name = strdup("HYDRAULIC LEVEL");
    break;
  }
  case MBHYD_IO: {
    name = strdup("HYDRAULIC MASS BALANCE");
    break;
  }
  case HDERM_Q_IO: {
    name = strdup("HYPERDERMIC DISCHARGE");
    break;
  }
  case HDERM_MB_IO: {
    name = strdup("HYPERDERMIC MASS BALANCE");
    break;
  }
  }
  return name;
}

char *CAW_name_mod(int itype) {

  char *name;

  switch (itype) {
  case AQ_CAW: {
    name = strdup("AQUIFER");
    break;
  }
  case HYD_CAW: {
    name = strdup("HYDRAULIC");
    break;
  }
  case FP_CAW: {
    name = strdup("SURFACE WATER BALANCE");
    break;
  }
  case NSAT_CAW: {
    name = strdup("UNSATURED ZONE");
    break;
  }
  case HDERM_CAW: {
    name = strdup("HYPERDERMIC ZONE");
    break;
  }
  case WET_CAW: {
    name = strdup("AQUIFER-LAKE");
    break;
  }
  case SEB_CAW: {
    name = strdup("RIVER-ATMOSPHERE");
    break;
  }
  }
  return name;
}

char *CAW_species_type(int itype) {

  char *name;

  switch (itype) {
  case SOLUTE_CAW: {
    name = strdup("SOLUTE");
    break;
  }
  case HEAT_CAW: {
    name = strdup("HEAT");
    break;
  }
  }
  return name;
}

char *CAW_species_variable(int itype) {

  char *name;

  switch (itype) {
  case SOLUTE_CAW: {
    name = strdup("CONCENTRATION");
    break;
  }
  case HEAT_CAW: {
    name = strdup("TEMPERATURE");
    break;
  }
  }
  return name;
}

char *CAW_specie_chem_param(int itype) {

  char *name;

  switch (itype) {
  case MMASS_SPECIES_CAW: {
    name = strdup("Species molar mass");
    break;
  }
  case MMASS_ELEMENT_CAW: {
    name = strdup("Chemical element molar mass");
    break;
  }
  case DIFF_MOL_CAW: {
    name = strdup("Molecular diffusivity");
    break;
  }
  }
  return name;
}