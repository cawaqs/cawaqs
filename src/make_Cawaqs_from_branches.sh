#-------------------------------------------------------------------------------
# 
# SOFTWARE NAME: cawaqs
# FILE NAME: make_Cawaqs_from_branches.sh
# 
# CONTRIBUTORS: Nicolas GALLOIS, Baptiste LABARTHE, Shuaitao WANG,
#               Agnès RIVIERE, Deniz KILIC, Mathias MAILLOT,
#               Fulvia BARATELLI, Pierre GUILLOU, Emmanuel LEDOUX, Nicolas FLIPO
# 
# SOFTWARE BRIEF DESCRIPTION: Physically based coupled surface–subsurface model 
# for joint water, matter and energy flux balances and flow dynamics simulation
# within all compartments of a hydrosystem (sub-surface, hydraulic network, 
# vadose zone, aquifer system and stream-aquifer exchanges).
#
# Software developed at the Geosciences Center, joint research center 
# of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
#
# COPYRIGHT: (c) 2022 Contributors to the cawaqs Software.
# CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
#          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
#
# All rights reserved. This Software and the accompanying materials
# are made available under the terms of the Eclipse Public License v2.0
# which accompanies this distribution, and is available at
# http://www.eclipse.org/legal/epl-v20.html
# 
#------------------------------------------------------------------------------*/



#NF 31/7/2021 updated for branching compilation
chmod +x *.sh
DIR_cawaqs=`pwd`
CODE="all"
BNAME="main"

if [ -z "$LIB_HYDROSYSTEM_PATH" ]
then
    LIB_HYDROSYSTEM_PATH=$HOME/Programmes/LIBS/
fi

PATH_INST="$LIB_HYDROSYSTEM_PATH"



if [ $# -eq 0 ] ; then
    ./make_Cawaqs.sh
    exit
elif [ $# -eq 1 ] ; then
    ./make_Cawaqs.sh $1
    exit
else
    if [ $1 != "-b" ] ; then

	echo "unknown option" $1
	exit
    elif [ $# -eq 3 ] ; then
	PATH_INST="$3"
    fi
    BNAME="$2"
fi

git checkout $BNAME

TMP=$PATH_INST
echo "PATH_INST before check" $TMP
if [ \( $TMP == "./" \) -o \( $TMP == "." \) ] ; then
    PATH_INST=`pwd`
fi
echo "PATH_INST after check" $PATH_INST


if [ ! -d "$PATH_INST" ] ; then 
    mkdir -p $PATH_INST
fi

if [ ! -f "$PATH_INST/delete_links.sh" ] ; then
    echo "No links yet.... Creating them...."
else
    echo "First deleting old links"
    $PATH_INST/delete_links.sh ./
fi

echo "installing scripts in" $PATH_INST
cd $PATH_INST
if [ -d "$PATH_INST/scripts" ] ; then
    rm -rf scripts
fi
cd $PATH_INST
git clone https://gitlab.com/gutil/scripts.git

DIR_SHELLS="$PATH_INST/scripts/"
cd $DIR_SHELLS/
git checkout $BNAME
chmod 755 *
echo "./create_links.sh" $PATH_INST
./create_links.sh $PATH_INST

cd $DIR_cawaqs
cp Makefile Makefile_tmp
LIST_DEP=`awk -F= -f $PATH_INST/print_dependencies.awk Makefile`
echo "Dependencies of cawaqs :" $LIST_DEP

cd $PATH_INST
echo "./clean_install_2.sh gcc"  $PATH_INST -b $BNAME
./clean_install_2.sh gcc $PATH_INST -b $BNAME


for i in $LIST_DEP
do
ACR=`$PATH_INST/acronyme.sh $i`
INCL=`$PATH_INST/acronyme.sh $i INCL_`
echo "ACR" $ACR
echo "INCL" $INCL

cd $DIR_cawaqs
VERSION=`$PATH_INST/get_version.sh $i $ACR $PATH_INST $BNAME`
echo "Formatting Makefile_tmp ACR=" $ACR "VERSION=" $VERSION 
awk -F= -f $PATH_INST/write_dep_version.awk -v ACR=$ACR -v VERSION=$VERSION Makefile_tmp
mv awk.out Makefile_tmp

INCL_PATH=`$PATH_INST/get_incl.sh $i $PATH_INST $BNAME`
awk -F= -f $PATH_INST/write_incl.awk -v ACR=$INCL PATH=$INCL_PATH Makefile_tmp
mv awk.out Makefile_tmp
done #for i in $LIST_DEP

awk -F= -f $PATH_INST/modify_lib_path.awk -v path=$PATH_INST Makefile_tmp
mv awk.out Makefile_tmp

awk -F= -f ./bname.awk -v bname=$BNAME Makefile_tmp
mv awk.out Makefile_tmp

cd $DIR_cawaqs
make -f Makefile_tmp clean
make -f Makefile_tmp all

cd $PATH_INST
./delete_links.sh $PATH_INST
