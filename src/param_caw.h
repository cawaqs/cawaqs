/*-------------------------------------------------------------------------------
 *
 * SOFTWARE NAME: cawaqs
 * FILE NAME: param_caw.h
 *
 * CONTRIBUTORS: Nicolas GALLOIS, Baptiste LABARTHE, Shuaitao WANG,
 *               Agnès RIVIERE, Deniz KILIC, Mathias MAILLOT,
 *               Fulvia BARATELLI, Pierre GUILLOU, Emmanuel LEDOUX, Nicolas FLIPO
 *
 * SOFTWARE BRIEF DESCRIPTION: Physically based coupled surface–subsurface
 * model for joint water, matter and energy flux balances and flow dynamics
 * simulation within all compartments of a hydrosystem (sub-surface, hydraulic
 * network, vadose zone, aquifer system and stream-aquifer exchanges).
 *
 * Software developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the cawaqs Software.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Software and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#define CODE_CAW CODE_FP
#define CONV_FAC_CAW 0.01
#define ERROR_MAX_ON_SURF 0.6
#define NVERSION_CAW 3.50
#define NVERSION NVERSION_CAW // NF 25/5/2012
#define RECH_MAX_CAW 100000   // NF 19/9/2017
#define EPS_CAW EPS_TS
#define CAW_SOLUTE_CRIV_THRESH 200.

// Default riverbed thickness (m) FB 09/04/2018
#define HZ_THICK_CAW 1

// Minimum threshold (m) river water height to compute wet surface in river for transport
#define HWET_RES_CAW 0.50

/* Fraction indicator used to redirect to given proportion of transport input flux
to a river reach in case of network upstream (0.5 means 50%) */
#define FRAC_TR_UPSTREAM_CAW 0.5

// Number of MB values printed out in output files in case of transport in hydraulic networks (NG 14/09/2020)
#define NB_MB_VAL_HYDNET_TRANSPORT 25

// Number of transport variable fields for each specie to write out in transport AQ files
#define NB_MB_VAL_AQ_TRANSPORT 1

// CaWaQS simulation-types
enum type_sim { SW_GW, SURF_WAT, GROUND_WAT, GW_LAKE, SURF_WAT_NSAT, SURF_WAT_NSAT_HDERM, SW_HDERM_GW, NTYPE_SIM };

// CaWaQS modules list (NG 24/03/2023 : SEB_CAW added)
enum module_caw { AQ_CAW, HYD_CAW, FP_CAW, NSAT_CAW, WET_CAW, HDERM_CAW, SEB_CAW, NMOD_CAW };

// Transport specie types
enum species_type { HEAT_CAW, SOLUTE_CAW, SPETYPE_CAW };

/**
 * Chemical properties associated with transport specie
 *
 * MMASS_SPECIES_CAW : Molar mass of the specie (e.g. NO3, PO4, etc.)
 * MMASS_ELEMENT_CAW : Molar mass of the element (e.g. N, P, etc.)
 * DIFF_MOL_CAW : Molecular diffusion
 *
 * **/
enum species_pchem { MMASS_SPECIES_CAW, MMASS_ELEMENT_CAW, DIFF_MOL_CAW, NSPEPAR_CAW };

// a voir SW 13/07/2021
#define N_SEINE_SAFRAN 1097
// DK This variable has to be dealt with properly as it will change from application to application, doesn't make sense to have hard value
// DK Note: This variable was intended to set the number of columns (SAFRAN cells or SEB METEO cells) in the input file to force meteo variables on libseb by SW
// Note cont. It should be changed to read input forcing dynamically
