/*-------------------------------------------------------------------------------
 *
 * SOFTWARE NAME: cawaqs
 * FILE NAME: manage_pc.c
 *
 * CONTRIBUTORS: Nicolas GALLOIS, Baptiste LABARTHE, Shuaitao WANG,
 *               Agnès RIVIERE, Deniz KILIC, Mathias MAILLOT,
 *               Fulvia BARATELLI, Pierre GUILLOU, Emmanuel LEDOUX, Nicolas FLIPO
 *
 * SOFTWARE BRIEF DESCRIPTION: Physically based coupled surface–subsurface
 * model for joint water, matter and energy flux balances and flow dynamics
 * simulation within all compartments of a hydrosystem (sub-surface, hydraulic
 * network, vadose zone, aquifer system and stream-aquifer exchanges).
 *
 * Software developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the cawaqs Software.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Software and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <time.h>
#include <stdarg.h>
#include <sys/time.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <malloc.h>
#include <math.h>
#include <libprint.h>
#include <time_series.h>
#include "IO.h"
#include "GC.h"
#include "libpc.h"
#include "reservoir.h"
#include "spa.h"
#include "FP.h"
#include "NSAT.h"
#include "CHR.h"
#include "HYD.h"
#include "MSH.h"
#include "AQ.h"
#include "WET.h"
#include "TTC_coupled.h"
#include "SEB.h"
#include "CAW.h"
#ifdef OMP
extern s_simul_cawaqs *Simul;

s_smp *CAW_set_OMP_param(int module) {

  switch (module) {
  case AQ_CAW: {
    s_carac_aq *pcarac_aq;
    pcarac_aq = Simul->pcarac_aq;
    if (pcarac_aq->settings->psmp == NULL)
      pcarac_aq->settings->psmp = new_smp();
    return pcarac_aq->settings->psmp;
    break;
  }
  case HYD_CAW: {
    s_chyd *pchyd;
    pchyd = Simul->pchyd;
    if (pchyd->settings->psmp == NULL)
      pchyd->settings->psmp = new_smp();
    return pchyd->settings->psmp;
    break;
  }
  case FP_CAW: {
    s_carac_fp *pcarac_fp;
    pcarac_fp = Simul->pcarac_fp;
    if (pcarac_fp->psmp == NULL)
      pcarac_fp->psmp = new_smp();
    return pcarac_fp->psmp;
    break;
  }
  case NSAT_CAW: {
    s_carac_zns *pcarac_zns;
    pcarac_zns = Simul->pcarac_zns;
    if (pcarac_zns->psmp == NULL)
      pcarac_zns->psmp = new_smp();
    return pcarac_zns->psmp;
    break;
  }
  case HDERM_CAW: // NG : 20/02/2020 : New case added for parallel computing in hyperdermic module
  {
    s_carac_fp *pcarac_fp;
    pcarac_fp = Simul->pcarac_fp_hderm;
    if (pcarac_fp->psmp == NULL)
      pcarac_fp->psmp = new_smp();
    return pcarac_fp->psmp;
    break;
  }
  }
}
#endif
