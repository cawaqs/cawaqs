/*-------------------------------------------------------------------------------
 *
 * SOFTWARE NAME: cawaqs
 * FILE NAME: struct_caw.h
 *
 * CONTRIBUTORS: Nicolas GALLOIS, Baptiste LABARTHE, Shuaitao WANG,
 *               Agnès RIVIERE, Deniz KILIC, Mathias MAILLOT,
 *               Fulvia BARATELLI, Pierre GUILLOU, Emmanuel LEDOUX, Nicolas FLIPO
 *
 * SOFTWARE BRIEF DESCRIPTION: Physically based coupled surface–subsurface
 * model for joint water, matter and energy flux balances and flow dynamics
 * simulation within all compartments of a hydrosystem (sub-surface, hydraulic
 * network, vadose zone, aquifer system and stream-aquifer exchanges).
 *
 * Software developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the cawaqs Software.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Software and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

typedef struct simulation s_simul_cawaqs;
typedef struct settings_cawaqs s_settings_cawaqs;
typedef struct species_cawaqs s_species_cawaqs;

struct simulation {

  /*!< Simulation name */
  char *name;
  /*!< Log file */
  FILE *poutputs;
  /*!<  Settings pointer */
  s_settings_cawaqs *pset_caw;
  /*!< Temporal characteristics of the simulation (tini, tend, dt) */
  s_chronos_CHR *chronos;
  /*!< Timer */
  s_clock_CHR *clock;

  /* **************************************
   *          Library structures          *
   * **************************************/

  /*!< libhyd : structure of hydraulic characteristics*/
  s_chyd *pchyd;
  /*!< libhyd : hyperdermic characteristics structure for HYD objects (network, river elements, etc.) */
  s_chyd *pchyd_hderm;
  /*!< libhyd : hydraulic temporary structure for input settings */
  s_lec_tmp_hyd **plec;
  /*!< libhyd : hyperdermic hydraulic temporary structure for input settings */
  s_lec_tmp_hyd **plec_hderm;
  /*!< libfp : characteristics structure */
  s_carac_fp *pcarac_fp;
  /*!< libfp : hyperdermic characteristics structure for FP objects (cprod, catchments, etc.) */
  s_carac_fp *pcarac_fp_hderm;
  /*!< libnsat : unsaturated zone characteristics structure */
  s_carac_zns *pcarac_zns;
  /*!< libaq : aquifer characteristics structure */
  s_carac_aq *pcarac_aq;
  /*!< libwet : lake characteristics structure */
  s_carac_wet *pcarac_wet;

  /* **************************************
   *     Output management structures     *
   * **************************************/

  /*!< output structure for hydraulics */
  s_output_hyd ***outputs;
  /*!< settings for input and outputs for hydraulics*/
  s_inout_set_io *pinout;
  /*!< IO structure for all modules */
  s_out_io **pout;

  /*!< Numerical parameters */
  double general_param[NPAR_AQ]; /*!< Numerical parameters*/

  /* **************************************
   *    Transport management structures    *
   * **************************************/

  /*!< Total number of species accounted for in transport mode*/
  int nb_species;
  /*!< Physico-chemical properties values of all species. Sized over
   * NMOD_CAW, nb_species and NSPEPAR_CAW. Allows to store different values for
   * the same parameter depending on the CaWaQS module. */
  double ***chem_param;
  /*!< Table of species structure pointers. Dimensioned over the number of species. */
  s_species_cawaqs **pspecies;
  /*!< Table of TTC_carac pointers over [NMOD_CAW], all pointers set to NULL by default */
  s_carac_ttc **pcarac_ttc;
  /*!< libseb related attributes */
  s_meteodata_seb *mto_seb;
};

struct settings_cawaqs {

  /*!< Simulation type processed in NTYPE_SIM */
  int sim_type;
  /*!< defines the simulation state in [STEADY,TRANSIENT] */
  int calc_state;
  /*!< check mesh in [YES,NO] */
  int check_mesh;
  /*!< print_surf in [YES,NO] */
  int print_surf;
  /*!< idebug in [YES,NO] */
  int idebug;
  /*!< Maximum number of iterations for steady state */
  int steadynitmax;
  /*!< Maximum number of Picard iterations */
  int picnitmax;
  /*!< storage for Picard convergence criterion */
  double eps_pic;
  /*!< CaWaQS general transport activation flag in [YES,NO]. Set to NO by default */
  int transport_caw;
  /*!< Transport mode activation flags for each CaWaQS module. Table in [NMOD_CAW], all values set to NO by default.*/
  int *transport_module;
};

struct species_cawaqs {

  /*!< Intern ID of the specie */
  int id;
  /*!< Specie name */
  char name[STRING_LENGTH_LP];
  /*!< Specie type. Values in [SPETYPE_CAW]. Set to SOLUTE by default */
  int type;
};

#define new_simul_cawaqs() ((s_simul_cawaqs *)malloc(sizeof(s_simul_cawaqs)))
#define new_settings_cawaqs() ((s_settings_cawaqs *)malloc(sizeof(s_settings_cawaqs)))
#define new_species_cawaqs() ((s_species_cawaqs *)malloc(sizeof(s_species_cawaqs)))
