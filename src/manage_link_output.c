/*-------------------------------------------------------------------------------
 *
 * SOFTWARE NAME: cawaqs
 * FILE NAME: manage_link_outputs.c
 *
 * CONTRIBUTORS: Nicolas GALLOIS, Baptiste LABARTHE, Shuaitao WANG,
 *               Agnès RIVIERE, Deniz KILIC, Mathias MAILLOT,
 *               Fulvia BARATELLI, Pierre GUILLOU, Emmanuel LEDOUX, Nicolas FLIPO
 *
 * SOFTWARE BRIEF DESCRIPTION: Physically based coupled surface–subsurface
 * model for joint water, matter and energy flux balances and flow dynamics
 * simulation within all compartments of a hydrosystem (sub-surface, hydraulic
 * network, vadose zone, aquifer system and stream-aquifer exchanges).
 *
 * Software developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the cawaqs Software.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Software and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <time.h>
#include <stdarg.h>
#include <sys/time.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <malloc.h>
#include <math.h>
#include <libprint.h>
#ifdef OMP
#include <omp.h>
#endif
#include <time_series.h>
#include "libpc.h"
#include "IO.h"
#include "GC.h"
#include "reservoir.h"
#include "spa.h"
#include "FP.h"
#include "NSAT.h"
#include "CHR.h"
#include "HYD.h"
#include "MSH.h"
#include "AQ.h"
#include "WET.h"
#include "TTC_coupled.h"
#include "SEB.h"
#include "CAW.h"
#include "global_CAWAQS.h"

/**
 * \fn void CAW_print_outputs_transport_riv_balance(s_chronos_CHR *, int, double, s_chyd *, s_out_io *, int , s_species_cawaqs**, s_carac_ttc **pcarac_ttc, FILE *)
 * \brief General function launching output printing for HYD transport MB
 * \return -
 */
void CAW_print_outputs_transport_riv_balance(s_chronos_CHR *chronos, int nb_species, double t_day, s_chyd *pchyd, s_out_io *pout, int mod, s_species_cawaqs **p_species, s_carac_ttc *pcarac_ttc, FILE *flog) {
  double t_out = pout->t_out[CUR_IO];
  double t = chronos->t[CUR_CHR];
  double dt = chronos->dt;

  if (t_out <= t && t_out > t - dt) {
    if (pout->format == FORMATTED_IO) {
      CAW_print_transport_riv_mb_formatted(nb_species, pchyd, pcarac_ttc, t_day, pout, dt, flog);
    } else {
      CAW_print_transport_riv_mb_bin(nb_species, pchyd, pcarac_ttc, pout, dt, flog);
    }
  }
}

/**
 * \fn void CAW_print_outputs_transport_riv_variable(s_chronos_CHR *, int, double, s_chyd *, s_out_io *, int , s_species_cawaqs**, s_carac_ttc **pcarac_ttc, FILE *)
 * \brief General function launching printing for river transport variable only (C, T), for all species. No mass balance.
 * \return -
 */
void CAW_print_outputs_transport_riv_variable(s_chronos_CHR *chronos, int nb_species, double t_day, s_chyd *pchyd, s_out_io *pout, int mod, s_species_cawaqs **p_species, s_carac_ttc *pcarac_ttc, FILE *flog) {
  double t_out = pout->t_out[CUR_IO];
  double t = chronos->t[CUR_CHR];
  double dt = chronos->dt;

  if (t_out <= t && t_out > t - dt) {
    if (pout->format == FORMATTED_IO) {
      CAW_print_transport_riv_var_formatted(nb_species, pchyd, t_day, pout, flog);
    } else {
      CAW_print_transport_riv_var_bin(nb_species, pchyd, pcarac_ttc, pout, dt, flog);
    }
  }
}

/**
 * \fn void CAW_print_outputs_transport_aq_balance(s_chronos_CHR *, int, double, s_carac_aq *, s_out_io *, int , s_species_cawaqs**, FILE *)
 * \brief General function launching output printing for full AQ transport mass balance
 * \return -
 */
void CAW_print_outputs_transport_aq_balance(s_chronos_CHR *chronos, int nb_species, double t_day, s_carac_aq *pcarac_aq, s_out_io *pout, int mod, s_species_cawaqs **p_species, FILE *flog) {
  double t_out = pout->t_out[CUR_IO];
  double t = chronos->t[CUR_CHR];
  double dt = chronos->dt;

  if (t_out <= t && t_out > t - dt) {
    if (pout->format == FORMATTED_IO) {
      CAW_print_transport_aq_mb_formatted(nb_species, pcarac_aq, t_day, pout, dt, flog);
    } else {
      CAW_print_transport_aq_mb_bin(nb_species, pcarac_aq, pout, dt, flog);
    }
  }
}

/**
 * \fn void CAW_print_outputs_transport_aq_variable(s_chronos_CHR *, int, double, s_carac_aq *, s_out_io *, int , s_species_cawaqs**, FILE *)
 * \brief General function launching output printing for AQ transport variable only (C, T).
 * \return -
 */
void CAW_print_outputs_transport_aq_variable(s_chronos_CHR *chronos, int nb_species, double t_day, s_carac_aq *pcarac_aq, s_out_io *pout, int mod, s_species_cawaqs **p_species, FILE *flog) {
  double t_out = pout->t_out[CUR_IO];
  double t = chronos->t[CUR_CHR];
  double dt = chronos->dt;

  if (t_out <= t && t_out > t - dt) {
    if (pout->format == FORMATTED_IO) {
      CAW_print_transport_aq_var_formatted(nb_species, pcarac_aq, t_day, pout, dt, flog);
    } else {
      CAW_print_transport_aq_var_bin(nb_species, pcarac_aq, pout, dt, flog);
    }
  }
}

/**
 * \fn CAW_print_transport_header(int, int, s_species_cawaqs**, FILE*)
 * \brief Prints header of transport (HYD, AQ) output files in FORMATTED format. Accounts for the specie type
 * \return
 */
void CAW_print_transport_header(int iotype, int nb_species, s_species_cawaqs **p_species, FILE *fpout) {

  int p;

  switch (iotype) {

  case MBHYD_TRA_IO:
    fprintf(fpout, "%s", "DAY RIVER ID_INT ID_ABS ID_GIS ");
    for (p = 0; p < nb_species; p++) {
      if (p_species[p]->type == HEAT_CAW) {
        fprintf(fpout, "%s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s ", "HYD_T_C", "DOWNSTREAM_COND_W/m2", "DOWNSTREAM_ADV_W/m2", "DOWNSTREAM_TOT_W/m2", "UPSTREAM_COND_W/m2", "UPSTREAM_ADV_W/m2", "UPSTREAM_TOT_W/m2", "BANKS_COND_W/m2", "BANKS_ADV_W/m2", "BANKS_TOT_W/m2", "ATMOS_COND_W/m2", "ATMOS_ADV_W/m2", "ATMOS_TOT_W/m2", "RIVAQ_COND_W/m2", "RIVAQ_ADV_W/m2",
                "RIVAQ_TOT_W/m2", "SEB_SW", "SEB_LW", "SEB_LH", "SEB_SH", "RUNOFF", "RIVAQ_COND", "RIVAQ_ADV", "RIVBED_T", "MB_ERROR");
      } else {
        fprintf(fpout, "%s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s ", "HYD_C_g/m3", "DOWNSTREAM_COND_g/m2", "DOWNSTREAM_ADV_g/m2", "DOWNSTREAM_TOT_g/m2", "UPSTREAM_COND_g/m2", "UPSTREAM_ADV_g/m2", "UPSTREAM_TOT_g/m2", "BANKS_COND_g/m2", "BANKS_ADV_g/m2", "BANKS_TOT_g/m2", "ATMOS_COND_g/m2", "ATMOS_ADV_g/m2", "ATMOS_TOT_g/m2", "RIVAQ_COND_g/m2", "RIVAQ_ADV_g/m2",
                "RIVAQ_TOT_g/m2", "SEB_SW", "SEB_LW", "SEB_LH", "SEB_SH", "RUNOFF", "RIVAQ_COND", "RIVAQ_ADV", "RIVBED_T", "MB_ERROR");
      }
    }
    break;

  case VARHYD_TRA_IO:
    fprintf(fpout, "%s", "DAY RIVER ID_INT ID_ABS ID_GIS ");
    for (p = 0; p < nb_species; p++) {
      if (p_species[p]->type == HEAT_CAW) {
        fprintf(fpout, "%s ", "HYD_T_C");
      } else {
        fprintf(fpout, "%s ", "HYD_C_g/m3");
      }
    }
    break;

  case MBAQ_TRA_IO:
    fprintf(fpout, "%s", "DAY ID_INT ID_LAYER ID_ABS ");
    for (p = 0; p < nb_species; p++) {
      if (p_species[p]->type == HEAT_CAW) {
        fprintf(fpout, "%s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s", "AQF_T_C", "EAST_COND_W/m2", "EAST_ADV_W/m2", "EAST_TOT_W/m2", "WEST_COND_W/m2", "WEST_ADV_W/m2", "WEST_TOT_W/m2", "NORTH_COND_W/m2", "NORTH_ADV_W/m2", "NORTH_TOT_W/m2", "SOUTH_COND_W/m2", "SOUTH_ADV_W/m2", "SOUTH_TOT_W/m2", "BOTTOM_COND_W/m2", "BOTTOM_ADV_W/m2", "BOTTOM_TOT_W/m2",
                "TOP_COND_W/m2", "TOP_ADV_W/m2", "TOP_TOT_W/m2", "RIVBED_ADV_W/m2", "RIVBED_COND_W/m2", "RIVBED_T_C", "AQUITARD_ADV_W/m2", "AQUITARD_COND_W/m2", "AQUITARD_T_C", "dT/dt", "MB_TRANSPORT_(%)");
      } else {
        fprintf(fpout, "%s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s", "AQF_C_g/m3", "EAST_COND_g/m2", "EAST_ADV_g/m2", "EAST_TOT_g/m2", "WEST_COND_g/m2", "WEST_ADV_g/m2", "WEST_TOT_g/m2", "NORTH_COND_g/m2", "NORTH_ADV_g/m2", "NORTH_TOT_g/m2", "SOUTH_COND_g/m2", "SOUTH_ADV_g/m2", "SOUTH_TOT_g/m2", "BOTTOM_COND_g/m2", "BOTTOM_ADV_g/m2", "BOTTOM_TOT_g/m2",
                "TOP_COND_g/m2", "TOP_ADV_g/m2", "TOP_TOT_g/m2", "RIVBED_ADV_g/m2", "RIVBED_COND_g/m2", "RIVBED_C_g/m3", "AQUITARD_ADV_g/m2", "AQUITARD_COND_g/m2", "AQUITARD_C_g/m3", "dC/dt", "MB_TRANSPORT_(%)");
      }
    }
    break;

  case VARAQ_TRA_IO:
    fprintf(fpout, "%s", "DAY ID_INT ID_LAYER ID_ABS ");
    for (p = 0; p < nb_species; p++) {
      if (p_species[p]->type == HEAT_CAW) {
        fprintf(fpout, "%s ", "AQF_T_C");
      } else { // Solute case
        fprintf(fpout, "%s ", "AQF_C_g/m3");
      }
    }
    break;
  }
  fprintf(fpout, "\n");
}

/**
 * \fn CAW_print_transport_riv_mb_formatted(int, s_chyd*,  double, s_out_io*, double, FILE*)
 * \brief Mass balance budget calculation and print of the HYD transport output file. FORMATTED format.
 * \return -
 */
void CAW_print_transport_riv_mb_formatted(int nb_species, s_chyd *pchyd, s_carac_ttc *pcarac_ttc, double t_day, s_out_io *pout, double dt, FILE *flog) {
  int r, ne, i, p;
  int nreaches = pchyd->counter->nreaches;
  s_reach_hyd *preach;
  s_element_hyd *pele;
  s_id_io *id_out;
  s_species_ttc **p_species;

  p_species = pcarac_ttc->p_species;

  if (pout->type_out == ALL_IO) {
    for (r = 0; r < nreaches; r++) {
      preach = pchyd->p_reach[r];
      for (ne = 0; ne < preach->nele; ne++) {
        pele = preach->p_ele[ne];
        CAW_print_budget_riv_ele_transport(nb_species, pele, p_species, t_day, pout, flog);
      }
    }
  } else { // Sélection par masque d'éléments
    id_out = IO_browse_id(pout->id_output, BEGINNING_IO);

    while (id_out != NULL) {
      i = id_out->id;
      pele = pchyd->p_reach[id_out->id_lay]->p_ele[id_out->id]; // Selection par id reach et id element
      CAW_print_budget_riv_ele_transport(nb_species, pele, p_species, t_day, pout, flog);
      id_out = id_out->next;
    }
  }
}

/**
 * \void CAW_print_transport_riv_var_formatted (int, s_chyd*, double, s_out_io*, FILE*) {
 * \brief Print of the transport output file which only contains state variable (C, T) in a hydraulic network. FORMATTED format.
 * \return -
 */
void CAW_print_transport_riv_var_formatted(int nb_species, s_chyd *pchyd, double t_day, s_out_io *pout, FILE *flog) {
  int r, ne, i, p;
  int nreaches = pchyd->counter->nreaches;
  s_reach_hyd *preach;
  s_element_hyd *pele;
  s_id_io *id_out;

  if (pout->type_out == ALL_IO) {
    for (r = 0; r < nreaches; r++) {
      preach = pchyd->p_reach[r];
      for (ne = 0; ne < preach->nele; ne++) {
        pele = preach->p_ele[ne];
        fprintf(pout->fout, "%.1f %s %d %d %d ", t_day, pele->reach->river, pele->id[INTERN_HYD], pele->id[ABS_HYD], pele->id[GIS_HYD]);
        for (p = 0; p < nb_species; p++) {
          fprintf(pout->fout, "%.2f ", pele->center->hydro->transp_var[p]);
        }
        fprintf(pout->fout, "\n");
      }
    }
  } else { // Sélection par masque d'éléments
    id_out = IO_browse_id(pout->id_output, BEGINNING_IO);

    while (id_out != NULL) {
      i = id_out->id;
      pele = pchyd->p_reach[id_out->id_lay]->p_ele[id_out->id]; // Selection par id reach et id element
      fprintf(pout->fout, "%.1f %s %d %d %d ", t_day, pele->reach->river, pele->id[INTERN_HYD], pele->id[ABS_HYD], pele->id[GIS_HYD]);
      for (p = 0; p < nb_species; p++) {
        fprintf(pout->fout, "%.2f ", pele->center->hydro->transp_var[p]);
      }
      fprintf(pout->fout, "\n");
      id_out = id_out->next;
    }
  }
}

/**
 * \fn CAW_print_transport_aq_mb_formatted(int, s_carac_aq*, double, s_out_io*, double, FILE*)
 * \brief Mass balance budget print of the AQ transport output file. FORMATTED format.
 * \return -
 */
void CAW_print_transport_aq_mb_formatted(int nb_species, s_carac_aq *pcarac_aq, double t_day, s_out_io *pout, double dt, FILE *flog) {
  s_layer_msh *player;
  s_cmsh *pcmsh;
  s_ele_msh *pele;
  s_id_io *id_out;
  int nele, nlayer, i, j;

  pcmsh = pcarac_aq->pcmsh;
  nlayer = pcmsh->pcount->nlayer;

  if (pout->type_out == ALL_IO) {
    for (i = 0; i < nlayer; i++) {
      player = pcmsh->p_layer[i];
      nele = player->nele;

      for (j = 0; j < nele; j++) {
        pele = player->p_ele[j];
        CAW_print_budget_aq_ele_transport(nb_species, pele, t_day, pout, flog);
      }
    }
  } else { // Sélection par masque d'éléments
    id_out = IO_browse_id(pout->id_output, BEGINNING_IO);

    while (id_out != NULL) {
      i = id_out->id;
      pele = pcmsh->p_layer[id_out->id_lay]->p_ele[id_out->id];
      CAW_print_budget_aq_ele_transport(nb_species, pele, t_day, pout, flog);
      id_out = id_out->next;
    }
  }
}

/**
 * \fn void CAW_print_transport_aq_var_formatted (int, s_carac_aq*, double, s_out_io*, double, FILE*)
 * \brief
 * \return -
 */
void CAW_print_transport_aq_var_formatted(int nb_species, s_carac_aq *pcarac_aq, double t_day, s_out_io *pout, double dt, FILE *flog) {
  s_layer_msh *player;
  s_cmsh *pcmsh;
  s_ele_msh *pele;
  s_id_io *id_out;
  int nele, nlayer, i, j;

  pcmsh = pcarac_aq->pcmsh;
  nlayer = pcmsh->pcount->nlayer;

  if (pout->type_out == ALL_IO) {
    for (i = 0; i < nlayer; i++) {
      player = pcmsh->p_layer[i];
      nele = player->nele;

      for (j = 0; j < nele; j++) {
        pele = player->p_ele[j];
        CAW_print_var_aq_ele_transport(nb_species, pele, t_day, pout, flog);
      }
    }
  } else { // Sélection par masque d'éléments
    id_out = IO_browse_id(pout->id_output, BEGINNING_IO);

    while (id_out != NULL) {
      i = id_out->id;
      pele = pcmsh->p_layer[id_out->id_lay]->p_ele[id_out->id];
      CAW_print_var_aq_ele_transport(nb_species, pele, t_day, pout, flog);
      id_out = id_out->next;
    }
  }
}

/**
 * \fn CAW_print_transport_riv_mb_bin(int, s_chyd*, s_out_io*, double, FILE*)
 * \brief General function launching mass balance budget calculation and print of the transport output file in a hydraulic network in binary format
 * \return -
 */
void CAW_print_transport_riv_mb_bin(int nb_species, s_chyd *pchyd, s_carac_ttc *pcarac_ttc, s_out_io *pout, double dt, FILE *flog) {
  int neletot;
  double **values;
  int type_out = pout->type_out;
  int nb_rec = NB_MB_VAL_HYDNET_TRANSPORT * nb_species;
  s_id_io *id_out;
  s_species_ttc **p_species = pcarac_ttc->p_species;

  if (type_out == ALL_IO) {
    neletot = pchyd->counter->nele_tot;
  } else {
    neletot = IO_length_id(pout->id_output);
  }

  values = CAW_get_mb_val_bin_riv_transport(nb_species, pout, pchyd, p_species, neletot, type_out, dt, flog);
  IO_print_bloc_bin(values, neletot, nb_rec, pout->fout);
}

/**
 * \fn CAW_print_transport_riv_var_bin(int, s_chyd*, s_out_io*, double, FILE*)
 * \brief General function launching printing for HYD state variable only in output files. UNFORMATTED format.
 * \return -
 */
void CAW_print_transport_riv_var_bin(int nb_species, s_chyd *pchyd, s_carac_ttc *pcarac_ttc, s_out_io *pout, double dt, FILE *flog) {

  int neletot;
  double **values;
  int type_out = pout->type_out;
  s_id_io *id_out;
  s_species_ttc **p_species = pcarac_ttc->p_species;

  if (type_out == ALL_IO) {
    neletot = pchyd->counter->nele_tot;
  } else {
    neletot = IO_length_id(pout->id_output);
  }

  values = CAW_get_var_val_bin_riv_transport(nb_species, pout, pchyd, p_species, neletot, type_out, dt, flog);
  IO_print_bloc_bin(values, neletot, nb_species, pout->fout);
}

/**
 * \fn CAW_print_transport_aq_mb_bin(int, s_carac_aq*, s_out_io*, double, FILE*)
 * \brief General function launching mass balance budget calculation and print of the transport output file in case of transport in the aquifer
 * \return -
 */
void CAW_print_transport_aq_mb_bin(int nb_species, s_carac_aq *pcarac_aq, s_out_io *pout, double dt, FILE *flog) {

  int neletot;
  double **values;
  int type_out = pout->type_out;
  int nflux = NB_MB_FLUX_AQ_TRANSPORT * nb_species;
  int nvar = NB_MB_VAL_AQ_TRANSPORT * nb_species;
  int nb_rec = nflux + nvar;
  s_id_io *id_out;

  if (type_out == ALL_IO) {
    neletot = pcarac_aq->pcmsh->pcount->nele;
  } else {
    neletot = IO_length_id(pout->id_output);
  }

  values = CAW_get_mb_val_bin_aq_transport(nb_species, pout, pcarac_aq, neletot, type_out, dt, flog);
  IO_print_bloc_bin(values, neletot, nb_rec, pout->fout);
}

/**
 * \fn void CAW_print_transport_aq_var_bin (int, s_carac_aq*, s_out_io*, double, FILE*)
 * \brief Prints transport variable state only in binary format, for all or a selected group of aquifer cells
 * \return -
 */
void CAW_print_transport_aq_var_bin(int nb_species, s_carac_aq *pcarac_aq, s_out_io *pout, double dt, FILE *flog) {

  int neletot, type_out = pout->type_out;
  double **values;
  s_id_io *id_out;

  if (type_out == ALL_IO) {
    neletot = pcarac_aq->pcmsh->pcount->nele;
  } else {
    neletot = IO_length_id(pout->id_output);
  }

  values = CAW_get_var_bin_aq_transport(nb_species, pout, pcarac_aq, neletot, type_out, dt, flog);
  IO_print_bloc_bin(values, neletot, nb_species, pout->fout);
}

/**
 * \fn CAW_print_budget_riv_ele_transport(int, s_element_hyd*, double, s_out_io*, FILE*)
 * \brief Prints, in formatted format, the transport mass balance for one river calculation element
 * \return -
 */
void CAW_print_budget_riv_ele_transport(int nb_species, s_element_hyd *pele, s_species_ttc **p_species, double t_day, s_out_io *pout, FILE *fp) {
  double *mb_values;
  int j;
  int nb_rec = nb_species * NB_MB_VAL_HYDNET_TRANSPORT;

  fprintf(pout->fout, "%.1f %s %d %d %d ", t_day, pele->reach->river, pele->id[INTERN_HYD], pele->id[ABS_HYD], pele->id[GIS_HYD]);

  mb_values = CAW_river_budget_transport(nb_species, pele, p_species, fp);

  for (j = 0; j < nb_rec; j++) {
    fprintf(pout->fout, "%15.4e ", mb_values[j]);
  }

  fprintf(pout->fout, "\n");
  free(mb_values);
}

/**
 * \fn CAW_print_budget_aq_ele_transport(int, s_ele_msh*, double, s_out_io*, FILE*)
 * \brief Prints, in FORMATTED format, transport mass balance for one aquifer cell
 * \return -
 */
void CAW_print_budget_aq_ele_transport(int nb_species, s_ele_msh *pele, double t_day, s_out_io *pout, FILE *fp) {
  double *mb_values;
  int j;
  int nflux = NB_MB_FLUX_AQ_TRANSPORT * nb_species;
  int nvar = NB_MB_VAL_AQ_TRANSPORT * nb_species;
  int nb_rec = nflux + nvar;

  fprintf(pout->fout, "%.1f %d %d %d ", t_day, pele->id[INTERN_MSH], pele->player->id, pele->id[ABS_MSH]);

  mb_values = CAW_aquifer_budget_transport(nb_species, pele, fp);

  for (j = 0; j < nb_rec; j++) {
    fprintf(pout->fout, "%15.7e ", mb_values[j]);
  }

  fprintf(pout->fout, "\n");
  free(mb_values);
}

/**
 * \fn void CAW_print_var_aq_ele_transport (int, s_ele_msh*, double, s_out_io*, FILE*)
 * \brief Prints, transport state variable only in VARAQ_TRA_IO output file, in formatted format, for all species.
 * \return -
 */
void CAW_print_var_aq_ele_transport(int nb_species, s_ele_msh *pele, double t_day, s_out_io *pout, FILE *fp) { // Make sure it fits with the header

  int p;
  fprintf(pout->fout, "%.1f %d %d %d ", t_day, pele->id[INTERN_MSH], pele->player->id, pele->id[ABS_MSH]);

  for (p = 0; p < nb_species; p++) {
    fprintf(pout->fout, "%f ", pele->phydro->transp_var[p]);
  }
  fprintf(pout->fout, "\n");
}

/**
 * \fn CAW_get_mb_val_bin_riv_transport(int, s_out_io*, s_chyd*, int, int, double, FILE*)
 * \brief Function which calculates the transport mass balance budget for each river element and filles/prints the daily matrix
 * \return Matrix containing all terms of the transport mass balance budget, for all river elements
 */
double **CAW_get_mb_val_bin_riv_transport(int nb_species, s_out_io *pout, s_chyd *pchyd, s_species_ttc **p_species, int neletot, int type_out, double dt, FILE *flog) {
  double **values;
  double *mb_values;
  int i, r, ne, j, p;
  int nb_rec = NB_MB_VAL_HYDNET_TRANSPORT * nb_species;
  s_id_io *id_out;
  s_reach_hyd *preach;
  s_element_hyd *pele;
  int id_absolu;

  // #ifdef OMP
  //	int taille;
  //	int nthreads;
  //	s_smp *psmp=pchyd->settings->psmp;
  // #endif

  values = (double **)malloc(nb_rec * sizeof(double *));

  if (values == NULL)
    LP_error(flog, "CaWaQS%4.2f : Error in file %s, in function %s at line %d : Bad memory allocation while writing binary outputs.\n", NVERSION_CAW, __FILE__, __func__, __LINE__);

  for (i = 0; i < nb_rec; i++) {
    values[i] = (double *)calloc(neletot, sizeof(double));

    if (values[i] == NULL)
      LP_error(flog, "CaWaQS%4.2f : Error in file %s, in function %s at line %d : Bad memory allocation while writing binary outputs.\n", NVERSION_CAW, __FILE__, __func__, __LINE__);
  }

  if (type_out == ALL_IO) {
    // #ifdef OMP
    //	nthreads=psmp->nthreads;
    //	omp_set_num_threads(psmp->nthreads);
    //	psmp->chunk=PC_set_chunk_size_silent(flog,pchyd->counter->nreaches,nthreads);
    //	taille=psmp->chunk;
    // LP_error(flog,"neletot %d nthreads %d chunk %d\n",neletot,nthreads,taille);
    // #pragma omp parallel shared(neletot,pchyd,values,nthreads,taille) private(r,preach,ne,j,i,pele,mb_values)
    //	 {
    // #pragma omp for schedule(dynamic,taille)
    // #endif

    for (r = 0; r < pchyd->counter->nreaches; r++) {
      preach = pchyd->p_reach[r];

      for (ne = 0; ne < preach->nele; ne++) {
        pele = preach->p_ele[ne];
        id_absolu = pele->id[ABS_HYD]; // Memo NG : Correct. Starts at 0. (Although a GIS_ID starting at 1 is written in HYD_corresp_file, which may be confusing).
        mb_values = CAW_river_budget_transport(nb_species, pele, p_species, flog);

        for (j = 0; j < nb_rec; j++) {
          values[j][id_absolu] = mb_values[j];
        }

        free(mb_values);
      }
    }

    // #ifdef OMP
    //	} // Fin de section parrallélisée
    // #endif
  } else { // Selection par masque d'éléments
    id_out = IO_browse_id(pout->id_output, BEGINNING_IO);

    while (id_out != NULL) {
      i = id_out->id;
      pele = pchyd->p_reach[id_out->id_lay]->p_ele[id_out->id]; // Selection par id reach et id element
      id_absolu = pele->id[ABS_HYD];
      mb_values = CAW_river_budget_transport(nb_species, pele, p_species, flog);

      for (j = 0; j < nb_rec; j++) {
        values[j][id_absolu] = mb_values[j];
      }

      free(mb_values);
      id_out = id_out->next;
    }
  }

  return values;
}

/**
 * \fn CAW_get_var_val_bin_riv_transport(int, s_out_io*, s_chyd*, int, int, double, FILE*)
 * \brief
 * \return Matrix containing all values of the transport state variables, for all species, for all river elements
 */
double **CAW_get_var_val_bin_riv_transport(int nb_species, s_out_io *pout, s_chyd *pchyd, s_species_ttc **p_species, int neletot, int type_out, double dt, FILE *flog) {

  double **values;
  int i, r, ne, j, p;
  s_id_io *id_out;
  s_reach_hyd *preach;
  s_element_hyd *pele;
  int id_absolu;

  // #ifdef OMP
  //	int taille;
  //	int nthreads;
  //	s_smp *psmp=pchyd->settings->psmp;
  // #endif

  values = (double **)malloc(nb_species * sizeof(double *));

  if (values == NULL)
    LP_error(flog, "CaWaQS%4.2f : Error in file %s, in function %s at line %d : Bad memory allocation while writing binary outputs.\n", NVERSION_CAW, __FILE__, __func__, __LINE__);

  for (i = 0; i < nb_species; i++) {
    values[i] = (double *)calloc(neletot, sizeof(double));

    if (values[i] == NULL)
      LP_error(flog, "CaWaQS%4.2f : Error in file %s, in function %s at line %d : Bad memory allocation while writing binary outputs.\n", NVERSION_CAW, __FILE__, __func__, __LINE__);
  }

  if (type_out == ALL_IO) {
    // #ifdef OMP
    //	nthreads=psmp->nthreads;
    //	omp_set_num_threads(psmp->nthreads);
    //	psmp->chunk=PC_set_chunk_size_silent(flog,pchyd->counter->nreaches,nthreads);
    //	taille=psmp->chunk;
    // LP_error(flog,"neletot %d nthreads %d chunk %d\n",neletot,nthreads,taille);
    // #pragma omp parallel shared(neletot,pchyd,values,nthreads,taille) private(r,preach,ne,j,i,pele,mb_values)
    //	 {
    // #pragma omp for schedule(dynamic,taille)
    // #endif

    for (r = 0; r < pchyd->counter->nreaches; r++) {
      preach = pchyd->p_reach[r];

      for (ne = 0; ne < preach->nele; ne++) {
        pele = preach->p_ele[ne];
        id_absolu = pele->id[ABS_HYD];

        for (p = 0; p < nb_species; p++) {
          values[p][id_absolu] = pele->center->hydro->transp_var[p];
        }
      }
    }

    // #ifdef OMP
    //	} // Fin de section parrallélisée
    // #endif
  } else { // Selection par masque d'éléments
    id_out = IO_browse_id(pout->id_output, BEGINNING_IO);

    while (id_out != NULL) {
      i = id_out->id;
      pele = pchyd->p_reach[id_out->id_lay]->p_ele[id_out->id]; // Selection par id reach et id element
      id_absolu = pele->id[ABS_HYD];

      for (p = 0; p < nb_species; p++) {
        values[p][id_absolu] = pele->center->hydro->transp_var[p];
      }

      id_out = id_out->next;
    }
  }

  return values;
}

/**
 * \fn CAW_get_mb_val_bin_aq_transport(int, s_out_io*, s_carac_aq*, int, int, double, FILE*)
 * \brief Function which calculates the transport mass balance budget for each aquifer cell and fills/prints the matrix for each time step
 * \return Matrix containing all terms of the transport mass balance budget, for all AQ cells
 */
double **CAW_get_mb_val_bin_aq_transport(int nb_species, s_out_io *pout, s_carac_aq *pcarac_aq, int neletot, int type_out, double dt, FILE *flog) {
  double **values;
  double *mb_values;
  int i, ne, j, p, k;
  int nflux = NB_MB_FLUX_AQ_TRANSPORT * nb_species;
  int nvar = NB_MB_VAL_AQ_TRANSPORT * nb_species;
  int nb_rec = nflux + nvar;
  s_id_io *id_out;
  s_ele_msh *pele;
  int id_absolu;
  s_layer_msh *play;

  // #ifdef OMP
  //	int taille;
  //	int nthreads;
  //	s_smp *psmp=pchyd->settings->psmp;
  // #endif

  values = (double **)malloc(nb_rec * sizeof(double *));

  if (values == NULL)
    LP_error(flog, "CaWaQS%4.2f : Error in file %s, in function %s at line %d : Bad memory allocation while writing binary outputs.\n", NVERSION_CAW, __FILE__, __func__, __LINE__);

  for (i = 0; i < nb_rec; i++) {
    values[i] = (double *)calloc(neletot, sizeof(double));

    if (values[i] == NULL)
      LP_error(flog, "CaWaQS%4.2f : Error in file %s, in function %s at line %d : Bad memory allocation while writing binary outputs.\n", NVERSION_CAW, __FILE__, __func__, __LINE__);
  }

  if (type_out == ALL_IO) {
    // #ifdef OMP
    //	nthreads=psmp->nthreads;
    //	omp_set_num_threads(psmp->nthreads);
    //	psmp->chunk=PC_set_chunk_size_silent(flog,pchyd->counter->nreaches,nthreads);
    //	taille=psmp->chunk;
    // LP_error(flog,"neletot %d nthreads %d chunk %d\n",neletot,nthreads,taille);
    // #pragma omp parallel shared(neletot,pchyd,values,nthreads,taille) private(r,preach,ne,j,i,pele,mb_values)
    //	 {
    // #pragma omp for schedule(dynamic,taille)
    // #endif

    for (i = 0; i < pcarac_aq->pcmsh->pcount->nlayer; i++) {
      play = pcarac_aq->pcmsh->p_layer[i];

      for (j = 0; j < play->nele; j++) {
        pele = play->p_ele[j];
        id_absolu = pele->id[ABS_HYD] - 1; // We put -1 since index starts from 0 and neletot = ID_ABS total -1
        mb_values = CAW_aquifer_budget_transport(nb_species, pele, flog);

        for (k = 0; k < nb_rec; k++) {
          values[k][id_absolu] = mb_values[k];
        }

        free(mb_values);
      }
    }

    // #ifdef OMP
    //	} // Fin de section parrallélisée
    // #endif
  } else { // Selection par masque d'éléments
    id_out = IO_browse_id(pout->id_output, BEGINNING_IO);

    while (id_out != NULL) {
      i = id_out->id;
      pele = pcarac_aq->pcmsh->p_layer[id_out->id_lay]->p_ele[id_out->id]; // Selection par id reach et id element
      id_absolu = pele->id[ABS_HYD] - 1;                                   // We put -1 since index starts from 0 and neletot = ID_ABS total -1
      mb_values = CAW_aquifer_budget_transport(nb_species, pele, flog);

      for (j = 0; j < nb_rec; j++) {
        values[j][id_absolu] = mb_values[j];
      }
      free(mb_values);
      id_out = id_out->next;
    }
  }
  return values;
}

/**
 * \fn double **CAW_get_var_bin_aq_transport (int, s_out_io*, s_carac_aq*, int, int, double, FILE*)
 * \brief Fills transport variable state only in matrix to be written in VARAQ_TRA_IO output file, in binary format. All aquifer elements at once.
 * \return
 */
double **CAW_get_var_bin_aq_transport(int nb_species, s_out_io *pout, s_carac_aq *pcarac_aq, int neletot, int type_out, double dt, FILE *flog) {

  int i, j, p, id_absolu;
  double **var_values;
  s_layer_msh *play;
  s_ele_msh *pele;
  s_id_io *id_out;

  // #ifdef OMP
  //	int taille;
  //	int nthreads;
  //	s_smp *psmp=pchyd->settings->psmp;
  // #endif

  var_values = (double **)malloc(nb_species * sizeof(double *));

  if (var_values == NULL) {
    LP_error(flog, "CaWaQS%4.2f : Error in file %s, in function %s at line %d : Bad memory allocation while writing binary outputs.\n", NVERSION_CAW, __FILE__, __func__, __LINE__);
  }

  for (p = 0; p < nb_species; p++) {
    var_values[p] = (double *)calloc(neletot, sizeof(double));

    if (var_values[p] == NULL) {
      LP_error(flog, "CaWaQS%4.2f : Error in file %s, in function %s at line %d : Bad memory allocation while writing binary outputs.\n", NVERSION_CAW, __FILE__, __func__, __LINE__);
    }
  }

  if (type_out == ALL_IO) {
    // #ifdef OMP
    //	nthreads=psmp->nthreads;
    //	omp_set_num_threads(psmp->nthreads);
    //	psmp->chunk=PC_set_chunk_size_silent(flog,pchyd->counter->nreaches,nthreads);
    //	taille=psmp->chunk;
    // LP_error(flog,"neletot %d nthreads %d chunk %d\n",neletot,nthreads,taille);
    // #pragma omp parallel shared(neletot,pchyd,values,nthreads,taille) private(r,preach,ne,j,i,pele,mb_values)
    //	 {
    // #pragma omp for schedule(dynamic,taille)
    // #endif

    for (i = 0; i < pcarac_aq->pcmsh->pcount->nlayer; i++) {
      play = pcarac_aq->pcmsh->p_layer[i];

      for (j = 0; j < play->nele; j++) {
        pele = play->p_ele[j];
        id_absolu = pele->id[ABS_HYD] - 1; // We put -1 since index starts from 0 and neletot = ID_ABS total -1

        for (p = 0; p < nb_species; p++) {
          var_values[p][id_absolu] = pele->phydro->transp_var[p];
        }
      }
    }
    // #ifdef OMP
    //	} // Fin de section parrallélisée
    // #endif
  } else { // Selection par masque d'éléments
    id_out = IO_browse_id(pout->id_output, BEGINNING_IO);

    while (id_out != NULL) {
      i = id_out->id;
      pele = pcarac_aq->pcmsh->p_layer[id_out->id_lay]->p_ele[id_out->id]; // Selection par id reach et id element
      id_absolu = pele->id[ABS_HYD] - 1;                                   // We put -1 since index starts from 0 and neletot = ID_ABS total -1

      for (p = 0; p < nb_species; p++) {
        var_values[p][id_absolu] = pele->phydro->transp_var[p];
      }
      id_out = id_out->next;
    }
  }
  return var_values;
}

/**
 * \fn CAW_river_budget_transport(int, s_element_hyd*, FILE*)
 * \brief Calculation of the transport mass balance for one river element
 * \return
 *
 * NG : 07/06/2023 :
 * Bug fix. It was not usable without libseb.
 * Bug fix. Did not account for multiple specie.
 *
 * Some fluxes stored in transp_var commented out. Needs proper storage and rewriting.
 *
 */
double *CAW_river_budget_transport(int nb_species, s_element_hyd *pele, s_species_ttc **p_species, FILE *fp) {
  double *mb_values;
  int p, pos = 0;
  int nb_rec = nb_species * NB_MB_VAL_HYDNET_TRANSPORT;
  s_carac_seb **p_shf = NULL;
  s_species_ttc *pspecies;
  int id_abs = pele->id[ABS_HYD];

  if (Simul->pset_caw->transport_module[SEB_CAW] == YES)
    p_shf = Simul->mto_seb->p_surf_heat_flux;

  mb_values = (double *)calloc(nb_rec, sizeof(double));

  if (mb_values == NULL)
    LP_error(fp, "CaWaQS%4.2f : Error in file %s, in function %s at line %d : Bad memory allocation.\n", NVERSION_CAW, __FILE__, __func__, __LINE__);

  for (p = 0; p < nb_species; p++) {
    pspecies = p_species[p];

    mb_values[pos] = pele->center->hydro->transp_var[p]; // State variable : C (g/m3) or T (K).
    pos++;
    mb_values[pos] = pspecies->p_flux_ttc[id_abs]->condface[DOWNSTREAM_TTC][0];
    pos++;
    mb_values[pos] = pspecies->p_flux_ttc[id_abs]->advface[DOWNSTREAM_TTC][0];
    pos++;
    mb_values[pos] = pspecies->p_flux_ttc[id_abs]->totface[DOWNSTREAM_TTC][0];
    pos++;
    mb_values[pos] = pspecies->p_flux_ttc[id_abs]->condface[UPSTREAM_TTC][0];
    pos++;
    mb_values[pos] = pspecies->p_flux_ttc[id_abs]->advface[UPSTREAM_TTC][0];
    pos++;
    mb_values[pos] = pspecies->p_flux_ttc[id_abs]->totface[UPSTREAM_TTC][0];
    pos++;
    mb_values[pos] = pspecies->p_flux_ttc[id_abs]->condface[BANKS_TTC][0];
    pos++;
    mb_values[pos] = pspecies->p_flux_ttc[id_abs]->advface[BANKS_TTC][0];
    pos++;
    mb_values[pos] = pspecies->p_flux_ttc[id_abs]->totface[BANKS_TTC][0];
    pos++;
    mb_values[pos] = pspecies->p_flux_ttc[id_abs]->condface[ATMOS_TTC][0];
    pos++;
    mb_values[pos] = pspecies->p_flux_ttc[id_abs]->advface[ATMOS_TTC][0];
    pos++;
    mb_values[pos] = pspecies->p_flux_ttc[id_abs]->totface[ATMOS_TTC][0];
    pos++;
    mb_values[pos] = pspecies->p_flux_ttc[id_abs]->condface[RIVAQ_TTC][0];
    pos++;
    mb_values[pos] = pspecies->p_flux_ttc[id_abs]->advface[RIVAQ_TTC][0];
    pos++;
    mb_values[pos] = pspecies->p_flux_ttc[id_abs]->totface[RIVAQ_TTC][0];
    pos++;

    if (Simul->pset_caw->transport_module[SEB_CAW] == YES) {
      mb_values[pos] = p_shf[id_abs]->pH_budget[FLUX_SW];
      pos++;
      mb_values[pos] = p_shf[id_abs]->pH_budget[FLUX_LW];
      pos++;
      mb_values[pos] = p_shf[id_abs]->pH_budget[FLUX_LH];
      pos++;
      mb_values[pos] = p_shf[id_abs]->pH_budget[FLUX_SH];
      pos++;
      ;
    } else // In case libseb is not activated.
    {
      mb_values[pos] = 0.;
      pos++;
      mb_values[pos] = 0.;
      pos++;
      mb_values[pos] = 0.;
      pos++;
      mb_values[pos] = 0.;
      pos++;
      ;
    }

    /* NG : 07/06/2023 : WARNING : Commenting for now until its rewritten
     * using a dedicated array. Assigning to error code by default. */

    // mb_values[pos] = pele->center->hydro->transp_var[2];  // Flux runoff
    mb_values[pos] = CODE_TS;
    pos++;

    // mb_values[pos] = pele->center->hydro->transp_var[3];  // Flux rivaq conduction
    mb_values[pos] = CODE_TS;
    pos++;

    // mb_values[pos] = pele->center->hydro->transp_var[4];  // Flux rivaq advection
    mb_values[pos] = CODE_TS;
    pos++;

    // mb_values[pos] = pele->center->hydro->transp_var[5];  // Flux rivaq rivbed temperature
    mb_values[pos] = CODE_TS;
    pos++;

    // MB error term
    mb_values[pos] = CODE_TS;
    pos++;
  }
  return mb_values;
}

/**
 * \fn CAW_aquifer_budget_transport(int, s_ele_msh*, FILE*)
 * \brief Calculation of the transport mass balance for one aquifer cell
 * \return
 */
double *CAW_aquifer_budget_transport(int nb_species, s_ele_msh *pele, FILE *fp) {

  double *mb_values;
  int p, pos = 0;
  int nb_flux = NB_MB_FLUX_AQ_TRANSPORT * nb_species;           // Total number of fluxes to print out
  int nb_rec = nb_flux + (NB_MB_VAL_AQ_TRANSPORT * nb_species); // NG 28/04/2023 : To account for transp_var for each specie

  mb_values = (double *)calloc(nb_rec, sizeof(double));

  if (mb_values == NULL) {
    LP_error(fp, "CaWaQS%4.2f : Error in file %s, in function %s at line %d : Bad memory allocation.\n", NVERSION_CAW, __FILE__, __func__, __LINE__);
  }

  for (p = 0; p < nb_species; p++) {

    mb_values[pos] = pele->phydro->transp_var[p]; // Aquifer cell transport variable (C, T)
    pos++;

    for (int i = 0; i < nb_flux; i++) {
      mb_values[pos] = pele->phydro->transp_flux[i];
      // LP_printf(Simul->poutputs,"Inside %s id: %d, value %lf , pos: %d\n", __func__ ,pele->id[ABS_MSH], pele->phydro->transp_flux[i],pos);
      pos++;
      // LP_printf(fp, "in func: %s, pos: %d\n", __func__ ,pos);
    }
  }
  return mb_values;
}
