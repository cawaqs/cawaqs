/*-------------------------------------------------------------------------------
 *
 * SOFTWARE NAME: cawaqs
 * FILE NAME: manage_inputs.c
 *
 * CONTRIBUTORS: Nicolas GALLOIS, Baptiste LABARTHE, Shuaitao WANG,
 *               Agnès RIVIERE, Deniz KILIC, Mathias MAILLOT,
 *               Fulvia BARATELLI, Pierre GUILLOU, Emmanuel LEDOUX, Nicolas FLIPO
 *
 * SOFTWARE BRIEF DESCRIPTION: Physically based coupled surface–subsurface
 * model for joint water, matter and energy flux balances and flow dynamics
 * simulation within all compartments of a hydrosystem (sub-surface, hydraulic
 * network, vadose zone, aquifer system and stream-aquifer exchanges).
 *
 * Software developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the cawaqs Software.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Software and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <time.h>
#include <stdarg.h>
#include <sys/time.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <malloc.h>
#include <math.h>
#include <libprint.h>
#include <time_series.h>
#include "libpc.h"
#include "IO.h"
#include "GC.h"
#include "reservoir.h"
#include "spa.h"
#include "FP.h"
#include "NSAT.h"
#include "CHR.h"
#include "HYD.h"
#include "MSH.h"
#include "AQ.h"
#include "WET.h"
#include "TTC_coupled.h"
#include "SEB.h"
#include "CAW.h"

// SW 02/05/2016 add struct for libwet GW_LAKE
// NG 18/02/2020 second pointer towards carac_fp for hyperdermic module added
// NG 03/03/2020 modified to integrate SW_HDERM_GW sim type
void CAW_check_inputs(s_carac_aq *pcarac_aq, s_carac_fp *pcarac_fp, s_carac_zns *pcarac_zns, s_chyd *pchyd, s_settings_cawaqs *pset_caw, s_carac_wet *pcarac_wet, s_chronos_CHR *chronos, s_carac_fp *pcarac_fp_hderm, s_chyd *pchyd_hderm, FILE *fp) {
  double max_tc;
  CAW_check_sim_type(pcarac_aq, pcarac_fp, pcarac_zns, pchyd, pset_caw, pcarac_wet, pcarac_fp_hderm, pchyd_hderm, fp); // SW 02/05/2016 add struct for libwet GW_LAKE // NG 18/02/2020 hderm carac pointer added

  if (pset_caw->sim_type != SW_GW) {
    if (pset_caw->sim_type == GW_LAKE && pset_caw->calc_state == STEADY) // SW 02/05/2016 add struct for libwet GW_LAKE
    {
      LP_warning(fp, "CaWaQS%4.2f %s in %s l.%d Coupling with libwet in STEADY state, theta set to 1. instead of %4.2f, ie fully implicit. May create troubles\n", NVERSION_CAW, __func__, __FILE__, __LINE__, pcarac_aq->settings->general_param[THETA]);
      pcarac_aq->settings->general_param[THETA] = 1;
    }
  }

  if (pset_caw->sim_type != GROUND_WAT && pset_caw->sim_type != GW_LAKE) // SW 23/05/2016
  {
    FP_check_ruiss_surf(pcarac_fp, fp);

    if (pset_caw->sim_type == SW_GW || pset_caw->sim_type == SW_HDERM_GW) {
      /*NF 4/6/2017 Lorsque je lis cela j'ai peur que le calcul ne soit pas correctement effectue s'il n'y a pas de ZNS. Neanmoins je mets un controle sur pcarac_zns!=NULL, on verra bien mais il faudra ecrire d'autres routines de controle*/
      if (pcarac_zns != NULL) {
        CAW_check_infilt(pcarac_fp, pcarac_zns, fp);
        CAW_check_sout(pcarac_aq, pcarac_zns, fp);
      }
      CAW_check_surf_aq(pcarac_aq, pcarac_fp, fp);
      CAW_cAQ_simplify_cauchy(pcarac_aq, pchyd, fp); // BL attention on diminue ici le nombre d'élément rivière en contact avec l'aquifère (un élément par élément aquifère)
    }
  }
}

// SW : 02/05/2016 : add struct for libwet GA_LAKE
// NG : 20/02/2020 : pointer towards carac_fp_hderm hyperdermic module added
// NG : 03/03/2020 : modified to integrate SW_HDERM_GW sim type
void CAW_check_sim_type(s_carac_aq *pcarac_aq, s_carac_fp *pcarac_fp, s_carac_zns *pcarac_zns, s_chyd *pchyd, s_settings_cawaqs *pset_caw, s_carac_wet *pcarac_wet, s_carac_fp *pcarac_fp_hderm, s_chyd *pchyd_hderm, FILE *fp) {

  // NF 13/7/2017 Re-ecriture de cette fonction. En effet les structures fp, zns, peuvent ne pas etre allouees a ce stade, on ne peut donc pas chercher pcarac_fp->p_cprod ou pchyd->p_reach. Attention il faudra reprendre ici en cas de recharge imposee

  if (pcarac_aq != NULL) {
    if (pcarac_fp == NULL) {
      if (pcarac_wet == NULL)
        pset_caw->sim_type = GROUND_WAT;
      else {
        pset_caw->sim_type = GW_LAKE;
        if (pcarac_zns != NULL || (pcarac_fp != NULL || pchyd != NULL))
          LP_error(fp, "CaWaQS%4.2f: function %s in %s l%d, Something wrong with the GW_LAKE mode\n\t Please contact the authors of the code\n", NVERSION_CAW, __func__, __FILE__, __LINE__);
      }
    } else {
      if (pchyd != NULL && pchyd_hderm == NULL) {
        pset_caw->sim_type = SW_GW;
      } else if (pchyd != NULL && pchyd_hderm != NULL) {
        pset_caw->sim_type = SW_HDERM_GW;
      } else {
        LP_error(fp, "CaWaQS%4.2f: function %s in %s l%d, surface river network has not been defined.\n\tcheck your input files\n", NVERSION_CAW, __func__, __FILE__, __LINE__);
      }
    }
  } else {
    if (pcarac_zns == NULL)
      pset_caw->sim_type = SURF_WAT;
    else {
      if (pcarac_fp_hderm == NULL)
        pset_caw->sim_type = SURF_WAT_NSAT;
      else
        pset_caw->sim_type = SURF_WAT_NSAT_HDERM;
    }

    if (pcarac_fp == NULL || pchyd == NULL)
      LP_error(fp, "CaWaQS%4.2f: function %s in %s l%d, either Production functions or river network or both are not defined\n\tcheck your input files\n", NVERSION_CAW, __func__, __FILE__, __LINE__);
  }
}

/*NF 4/6/2017 Je ne comprends pas l'interet de cette fonction qui indique des erreurs meme quand il n'y a pas de ZNS....Apparemment pas de consideration des surfaces ZNS*/
void CAW_check_infilt(s_carac_fp *pcarac_fp, s_carac_zns *pcarac_zns, FILE *fp) {
  s_cprod_fp *pcprod;
  int nb_cprod;
  s_zns *pzns;
  s_id_io **correct_zns;
  s_id_io **correct_cp;
  s_id_io *pid_tmp;
  int nb_zns, nb_BU, i, length;
  double *check_inf;
  double area_zns, area_cprod, area_bu, prct, area_all, area_pb;
  s_id_spa *link;
  nb_BU = pcarac_fp->nb_BU;
  area_all = 0;
  area_pb = 0;
  correct_zns = (s_id_io **)malloc(nb_BU * sizeof(s_id_io *));
  bzero((char *)correct_zns, nb_BU * sizeof(s_id_io *));
  correct_cp = (s_id_io **)malloc(nb_BU * sizeof(s_id_io *));
  bzero((char *)correct_cp, nb_BU * sizeof(s_id_io *));

  nb_cprod = pcarac_fp->nb_cprod;
  check_inf = (double *)calloc(nb_BU, sizeof(double));
  if (pcarac_zns != NULL) {
    nb_zns = pcarac_zns->nb_zns;
    for (i = 0; i < nb_zns; i++) {
      // LP_printf(fp," %d on %d zns\n",i,nb_zns); //BL ti debug
      pzns = pcarac_zns->p_zns[i];
      area_zns = pzns->area;
      // LP_printf(fp," Area zns %f \n",area_zns); //BL to debug
      if (pzns->link[SURF_NS] != NULL)
        link = (s_id_spa *)SPA_browse_all_struct(IDS_SPA, pzns->link[SURF_NS], BEGINNING_SPA);
      else
        LP_error(fp, " the link pointer is null for zns %d\n", i);
      while (link != NULL) {
        check_inf[link->id] += link->prct * area_zns;
        pid_tmp = IO_create_id(0, i);
        correct_zns[link->id] = IO_secured_chain_fwd_id(correct_zns[link->id], pid_tmp);
        link = link->next;
      }
    }
  }
  for (i = 0; i < nb_cprod; i++) {
    pcprod = pcarac_fp->p_cprod[i];
    area_cprod = pcprod->area;
    link = NULL;
    // LP_printf(fp," %d on %d cprod\n",i,nb_cprod); //BL to debug;
    if (pcprod->link[NO_AQ_FP] != NULL)
      link = (s_id_spa *)SPA_browse_all_struct(IDS_SPA, pcprod->link[NO_AQ_FP], BEGINNING_SPA);
#ifdef DEBUG
    else
      LP_printf(fp, "cprod %d is fully linked with aquifer \n", pcprod->id[FP_GIS]);
#endif
    while (link != NULL) {
      check_inf[link->id] += link->prct * area_cprod;
      pid_tmp = IO_create_id(0, i);
      correct_cp[link->id] = IO_secured_chain_fwd_id(correct_cp[link->id], pid_tmp);
      link = link->next;
    }
  }
  for (i = 0; i < nb_BU; i++) {
    area_bu = pcarac_fp->p_bu[i]->area;
    area_all += area_bu;
    prct = (fabs(check_inf[i] - area_bu) / area_bu) * 100;
    // LP_printf(fp,"area_bu %f check_inf[%d] %f \n",area_bu,i,check_inf[i]); //BL to debug
    if (prct > 0.2 && prct < 1) {

      if (correct_zns[i] != NULL && correct_cp[i] == NULL) {
        pid_tmp = IO_browse_id(correct_zns[i], BEGINNING_IO);
        length = IO_length_id(pid_tmp);
        SPA_correct_intersect(NSAT_SPA, check_inf[i], area_bu, correct_zns[i], pcarac_zns, length, SURF_NS, i, fp);
      } else if (correct_cp[i] != NULL && correct_zns[i] == NULL) {
        pid_tmp = IO_browse_id(correct_cp[i], BEGINNING_IO);
        length = IO_length_id(pid_tmp);
        SPA_correct_intersect(CPROD_SPA, check_inf[i], area_bu, correct_cp[i], pcarac_fp, length, NO_AQ_FP, i, fp);

      } else if (correct_cp[i] != NULL && correct_zns[i] != NULL) {
        pid_tmp = IO_browse_id(correct_zns[i], BEGINNING_IO);
        length = IO_length_id(pid_tmp);
        pid_tmp = IO_browse_id(correct_cp[i], BEGINNING_IO);
        length += IO_length_id(pid_tmp);
        SPA_correct_intersect(NSAT_SPA, check_inf[i], area_bu, correct_zns[i], pcarac_zns, length, SURF_NS, i, fp);
        SPA_correct_intersect(CPROD_SPA, check_inf[i], area_bu, correct_cp[i], pcarac_fp, length, NO_AQ_FP, i, fp);

      } else
        LP_error(fp, "no spatial link between BU %d and infilt\n", i + 1);
    }
    if (prct > 1) {
      LP_warning(fp, "Mass conservation issue : %f prct of infiltrated water comming from surface water balance calculation unit %d is lost  \n area_bu %f recalculated bu_area from nsat %f", prct, i + 1, area_bu, check_inf[i]); // BL modifier
      area_pb += fabs(check_inf[i] - area_bu);
    }
  }
  if ((area_pb / area_all) * 100 > 0.5) {
    /*NF 4/6/2017 Je ne comprends pourquoi on a toujours des erreurs dans un cas sans ZNS. Peut-etre que rien ne marche alors??? Je mets un warning a la place de error, j'obtiens un seg fault ensuite....*/
    // LP_error(fp,"Total mass conservation issue : %f prct of infiltrated  water is lost !, the total area calculated from WBU is %f km2, the difference with area calculated from Cprod is %f km2\n",(area_pb/area_all)*100,area_all/1000000,area_pb/1000000);
    LP_warning(fp, "Total mass conservation issue : %f prct of infiltrated  water is lost !, the total area calculated from WBU is %f km2, the difference with area calculated from Cprod is %f km2\n", (area_pb / area_all) * 100, area_all / 1000000, area_pb / 1000000);
  }
  correct_zns = IO_free_tab_id(correct_zns, nb_BU, fp);
  correct_cp = IO_free_tab_id(correct_cp, nb_BU, fp);
  free(check_inf);
}

void CAW_check_sout(s_carac_aq *pcarac_aq, s_carac_zns *pcarac_zns, FILE *fp) {
  s_layer_msh *player;
  s_ele_msh *pele;
  int nb_layer, nb_ele, length, nb_ele_aq_nsat;
  s_zns *pzns;
  int nb_zns, i, j;
  double *check_sout;
  double area_ele, area_zns, prct, area_all, area_pb, area_tot_nsat_aq;
  s_id_spa *link;
  s_id_io **correct_sout;
  s_id_io *pid_tmp;
  // int back_space = 0;//BL to debug
  nb_ele_aq_nsat = 0;
  area_all = 0;
  area_pb = 0;
  area_tot_nsat_aq = 0;
  nb_layer = pcarac_aq->pcmsh->pcount->nlayer;
  nb_zns = pcarac_zns->nb_zns;
  check_sout = (double *)calloc(nb_zns, sizeof(double));
  correct_sout = (s_id_io **)malloc(nb_zns * sizeof(s_id_io *));
  bzero((char *)correct_sout, nb_zns * sizeof(s_id_io *));
  for (i = 0; i < nb_layer; i++) {
    player = pcarac_aq->pcmsh->p_layer[i];
    nb_ele = player->nele;
    for (j = 0; j < nb_ele; j++) {
      pele = player->p_ele[j];
      area_ele = pele->pdescr->surf;
      link = NULL;
      // back_space = 0; //BL to debug
      if (pele->link[NSAT_AQ] != NULL) {
        link = (s_id_spa *)SPA_browse_all_struct(IDS_SPA, pele->link[NSAT_AQ], BEGINNING_SPA);
        nb_ele_aq_nsat++;
        // LP_printf(fp,"element GIS_ID %d ABS_ID %d is linked with zns : ",pele->id[GIS_MSH],pele->id[ABS_MSH]); //BL to debug
        // back_space = 1;//BL to debug
      } else {
        if (pele->loc == TOP_MSH)
          LP_warning(fp, "The element ID_GIS %d ID_ABS %d doesn't have NSAT correspondance \n", pele->id[GIS_MSH], pele->id[ABS_MSH]);
      }

      while (link != NULL) {
        // LP_printf(fp,"%d ",link->id); //BL to debug
        check_sout[link->id] += link->prct * area_ele;
        pid_tmp = IO_create_id(i, j);
        correct_sout[link->id] = IO_secured_chain_fwd_id(correct_sout[link->id], pid_tmp);
        link = link->next;
      }
      // if(back_space==1)
      // LP_printf(fp,"\n"); //BL to debug
    }
  }

  for (i = 0; i < nb_zns; i++) {
    area_zns = pcarac_zns->p_zns[i]->area;
    area_all += area_zns;
    area_tot_nsat_aq += check_sout[i];
    if (area_zns > 0) {
      prct = (fabs(check_sout[i] - area_zns) / area_zns) * 100;

      if (prct > 0.2 && prct < 1) {
        if (correct_sout[i] != NULL) {
          pid_tmp = IO_browse_id(correct_sout[i], BEGINNING_IO);
          length = IO_length_id(pid_tmp);
          // SPA_correct_intersect(CPROD_GW_SPA,check_sout[i],area_zns,correct_sout[i],pcarac_aq,length,NSAT_AQ,i,fp);
        } else
          LP_error(fp, "no AQ correspondance with zns %d\n", i + 1);
      }
      if (prct > 1) {
        LP_warning(fp, "Mass conservation issue : %f prct of infiltrated water comming from zns unit %d is lost \n area_zns %f recalculated zns_area from aquifer %f", prct, i + 1, area_zns, check_sout[i]);
        area_pb += fabs(check_sout[i] - area_zns);
      }
    } else {

      LP_warning(fp, "No WBU correspondance with NSAT unit %d \n", i + 1);
      area_pb += fabs(check_sout[i] - area_zns);
    }
  }
  if ((area_pb / area_all) * 100 > ERROR_MAX_ON_SURF) {

    LP_warning(fp, "Total mass conservation issue : %f prct of infiltrated  water is lost !\n The total infiltration area calculated from NSAT is %f km2, total infiltration area calculated from AQ %f, the difference with area calculated from aquifer is %f km2\n, the number of ele_aq linked with nsat elements is %d", (area_pb / area_all) * 100, area_all / 1000000, area_tot_nsat_aq / 1000000,
               area_pb / 1000000, nb_ele_aq_nsat);
  }
  correct_sout = IO_free_tab_id(correct_sout, nb_zns, fp);
  free(check_sout);
}

void CAW_check_surf_aq(s_carac_aq *pchar_aq, s_carac_fp *pchar_fp, FILE *fp) {

  s_cprod_fp *pcprod;
  int nb_cprod, nb_layer, nele, no_aq;
  int nb_ele, i, j, length;
  double *check_surf;
  double area_surf, area_cprod, area_ele, prct, area_all, area_pb;
  s_id_spa *link;
  s_layer_msh *player;
  s_ele_msh *pele;
  s_id_io **correct_sout;
  s_id_io *pid_tmp;
  area_all = 0;
  area_pb = 0;
  nb_cprod = pchar_fp->nb_cprod;
  check_surf = (double *)calloc(nb_cprod, sizeof(double));
  correct_sout = (s_id_io **)malloc(nb_cprod * sizeof(s_id_io *));
  bzero((char *)correct_sout, nb_cprod * sizeof(s_id_io *));
  nb_layer = pchar_aq->pcmsh->pcount->nlayer;
  for (i = 0; i < nb_layer; i++) {
    player = pchar_aq->pcmsh->p_layer[i];
    nb_ele = player->nele;
    for (j = 0; j < nb_ele; j++) {
      pele = player->p_ele[j];
      area_ele = pele->pdescr->surf;
      link = NULL;
      // back_space = 0; //BL to debug
      if (pele->link[SURF_AQ] != NULL) {
        link = (s_id_spa *)SPA_browse_all_struct(IDS_SPA, pele->link[SURF_AQ], BEGINNING_SPA);
        // LP_printf(fp,"element GIS_ID %d ABS_ID %d is linked with zns : ",pele->id[GIS_MSH],pele->id[ABS_MSH]); //BL to debug
        // back_space = 1;//BL to debug
      } else {
        if (pele->loc == TOP_MSH)
          LP_warning(fp, "The element ID_GIS %d ID_ABS %d doesn't have SURFACE correspondance \n", pele->id[GIS_MSH], pele->id[ABS_MSH]);
      }

      while (link != NULL) {
        // LP_printf(fp,"%d ",link->id); //BL to debug
        check_surf[link->id] += link->prct * area_ele;
        pid_tmp = IO_create_id(i, j);
        correct_sout[link->id] = IO_secured_chain_fwd_id(correct_sout[link->id], pid_tmp);
        link = link->next;
      }
      // if(back_space==1)
      // LP_printf(fp,"\n"); //BL to debug
    }
  }
  for (i = 0; i < nb_cprod; i++) {
    pcprod = pchar_fp->p_cprod[i];
    area_cprod = pcprod->area;

    if (pcprod->link[NO_AQ_FP] != NULL) {
      link = (s_id_spa *)SPA_browse_all_struct(IDS_SPA, pcprod->link[NO_AQ_FP], BEGINNING_SPA);
      area_surf = 0;
      while (link != NULL) {
        area_surf += link->prct * area_cprod;
        link = link->next;
      }
      area_cprod -= area_surf;
    }
    area_all += area_cprod;
    if (fabs(area_cprod) > EPS * 100000)
      prct = (fabs(check_surf[i] - area_cprod) / area_cprod) * 100;
    else if (fabs(check_surf[i] - area_cprod) < EPS * 100000)
      prct = 0;
    else
      LP_error(fp, "error for cprod %d ID_GIS %d\n area_cprd %e recalculated cprod_area from aquifer %e", i + 1, pcprod->id[FP_GIS], area_cprod, check_surf[i]);

    if ((prct > 0.2 && prct < 1)) {
      if (correct_sout[i] != NULL) {
        pid_tmp = IO_browse_id(correct_sout[i], BEGINNING_IO);
        length = IO_length_id(pid_tmp);
        SPA_correct_intersect(CPROD_GW_SPA, check_surf[i], area_cprod, correct_sout[i], pchar_aq, length, SURF_AQ, i, fp);
      } else
        LP_error(fp, "no AQ correspondance with cprod %d\n", i + 1);
    }
    if (prct > 1) {
      LP_warning(fp, "Mass conservation issue : %f prct of exfiltrated groundwater to cprod %d, GIS_ID %d,  is lost \n area_cprd %e recalculated cprod_area from aquifer %e EPS %e", prct, i + 1, pcprod->id[FP_GIS], area_cprod, check_surf[i], EPS * 1000);
      area_pb += fabs(check_surf[i] - area_cprod);
    }
  }
  if ((area_pb / area_all) * 100 > ERROR_MAX_ON_SURF) {
    LP_warning(fp, "Total mass conservation issue : %f prct of exfiltrated  groundwater is lost !, the total exfiltration area calculated from CPROD is %f km2, the difference with area calculated from aquifer is %f km2\n", (area_pb / area_all) * 100, area_all / 1000000,
               area_pb / 1000000); // FB 23/03/17 LP_warning instead of LP_error (for estuary application where few aquifer cells are not covered by cprod)
  }
  correct_sout = IO_free_tab_id(correct_sout, nb_cprod, fp);
  free(check_surf);
}

// NG : 27/03/2020 : Initialization from file added form HDERM network
void CAW_init_from_file(char **name_out_folder, char *name_file, int npile, int mod, s_carac_aq *pcarac_aq, s_carac_fp *pcarac_fp, s_carac_zns *pcarac_zns, s_chyd *pchyd, s_out_io **pout, s_chronos_CHR *chronos, FILE *fp) {

  FILE *fp_from_file;

  fp_from_file = IO_return_file(name_out_folder, name_file, NPILE, fp);

  if (mod == NSAT_CAW) {
    if (pcarac_zns->p_zns != NULL) {
      NSAT_init_from_file(fp_from_file, pcarac_zns, fp);
      pout[NSAT_IO] = IO_init_out_struct(chronos->t[BEGINNING], chronos->t[END], chronos->dt);
      pout[NSAT_IO]->init_from_file = YES_IO;
    } else
      LP_error(fp, "ZNS strucutrs null");
  } else if (mod == HYD_CAW) {
    if (pchyd != NULL) {

      HYD_initialize_Q_from_file_caw(fp_from_file, pchyd, fp);
      pout[HYD_Q_IO] = IO_init_out_struct(chronos->t[BEGINNING], chronos->t[END], chronos->dt);
      pout[HYD_Q_IO]->init_from_file = YES_IO;
    } else
      LP_error(fp, "HYD structur is null\n");
  } else if (mod == HDERM_CAW) {
    if (pchyd != NULL) {
      HYD_initialize_Q_from_file_caw(fp_from_file, pchyd, fp);
      pout[HDERM_Q_IO] = IO_init_out_struct(chronos->t[BEGINNING], chronos->t[END], chronos->dt);
      pout[HDERM_Q_IO]->init_from_file = YES_IO;
    } else
      LP_error(fp, "HDERM structur is null\n");
  } else {
    LP_warning(fp, "Initialization from file is not avalaible for module %s\n", CAW_name_mod(mod));
  }
}
