/*-------------------------------------------------------------------------------
 *
 * SOFTWARE NAME: cawaqs
 * FILE NAME: functions_fp_coupled.h
 *
 * CONTRIBUTORS: Nicolas GALLOIS, Baptiste LABARTHE, Shuaitao WANG,
 *               Agnès RIVIERE, Deniz KILIC, Mathias MAILLOT,
 *               Fulvia BARATELLI, Pierre GUILLOU, Emmanuel LEDOUX, Nicolas FLIPO
 *
 * SOFTWARE BRIEF DESCRIPTION: Physically based coupled surface–subsurface
 * model for joint water, matter and energy flux balances and flow dynamics
 * simulation within all compartments of a hydrosystem (sub-surface, hydraulic
 * network, vadose zone, aquifer system and stream-aquifer exchanges).
 *
 * Software developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the cawaqs Software.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Software and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

// in FP_manage_cprod_coupled.c
// void FP_calc_watbal_cprod_coupled(s_carac_fp *,s_carac_aq *,double,double,double,int,FILE *);                                // NG : 12/06/2023 : Obsolete BL function.
void FP_calc_watbal_cprod_sout(s_carac_fp *, s_carac_aq *, FILE *);
void FP_calc_watbal_cprod_sout_layer(s_layer_msh *, s_carac_fp *, double, FILE *);
// void FP_calc_watbal_cprod_sout_ele(s_ele_msh *,s_carac_fp *,double,FILE *);                                                  // NG : 12/06/2023 : Obsolete BL function.
void CAW_cFP_calc_watbal_cprod_coupled_hyperdermic(s_cprod_fp *, s_carac_fp *, s_carac_zns *, double, double, FILE *);             // NG : 20/02/2020 : Calcul des flux d'eau agrégés par cprod hyperdermique depuis la sortie du NSAT
void CAW_cFP_calc_watbal_cprod_coupled_hyperdermic_all(s_carac_fp *, s_carac_zns *, double, double, FILE *);                       // NG : 20/02/2020
void CAW_cFP_check_catchment_chasm_river(s_carac_fp *, s_chyd *, FILE *);                                                          // NG : 24/01/2020
void CAW_cFP_assign_catchment_cprod_outlet(s_chyd *, s_carac_fp *, FILE *);                                                        // NG : 22/02/2020
void CAW_cFP_calc_matter_fluxes_cprod_coupled_hyperdermic_all(s_carac_fp *, s_carac_zns *, int, double, double, FILE *);           // NG : 14/08/2020 : Calcul des flux et concentrations agrégés par cprod HDERM depuis le NONSAT
void CAW_cFP_calc_matter_fluxes_cprod_coupled_hyperdermic(s_cprod_fp *, s_carac_fp *, s_carac_zns *, int, double, double, FILE *); // NG : 14/08/2020
