/*-------------------------------------------------------------------------------
 *
 * SOFTWARE NAME: cawaqs
 * FILE NAME: NSAT_coupled.c
 *
 * CONTRIBUTORS: Nicolas GALLOIS, Baptiste LABARTHE, Shuaitao WANG,
 *               Agnès RIVIERE, Deniz KILIC, Mathias MAILLOT,
 *               Fulvia BARATELLI, Pierre GUILLOU, Emmanuel LEDOUX, Nicolas FLIPO
 *
 * SOFTWARE BRIEF DESCRIPTION: Physically based coupled surface–subsurface
 * model for joint water, matter and energy flux balances and flow dynamics
 * simulation within all compartments of a hydrosystem (sub-surface, hydraulic
 * network, vadose zone, aquifer system and stream-aquifer exchanges).
 *
 * Software developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the cawaqs Software.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Software and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include "libprint.h"
#include "time_series.h"
#include "libpc.h"
#include "IO.h"
#include "reservoir.h"
#include "spa.h"
#include "CHR.h"
#include "FP.h"
#include "NSAT.h"
#include "GC.h"
#include "HYD.h"
#include "MSH.h"
#include "AQ.h"
#include "WET.h"
#include "TTC_coupled.h"
#include "SEB.h"
#include "CAW.h"
#ifdef OMP
#include "omp.h"
#endif

/**
 * \fn void CAW_cNSAT_dynNsat_init (s_carac_zns*, s_carac_aq*, double, FILE*)
 * \brief Projects initial hydraulic heads onto the NSAT grid. Turns off dynamic
 * thickness behavior if NSAT cells not actually linked to the AQ system.
 * \return -
 */
void CAW_cNSAT_dynNsat_init(s_carac_zns *pcarac_zns, s_carac_aq *pcarac_aq, double t, FILE *flog) {

  s_zns *pzns;
  s_id_spa *link;
  s_ele_msh *pele_aq;
  int i, nb_zns = pcarac_zns->nb_zns;
  double hh, pcover_zns;
  double *values;
  int nzns_off = 0;

  for (i = 0; i < nb_zns; i++) {
    pzns = pcarac_zns->p_zns[i];
    link = pzns->link[AQ_NS];

    hh = 0.;
    pcover_zns = 0.;

    if (link == NULL) {
      pzns->dyn_io = NO;
      nzns_off++;
    } else {
      pzns->hydhead = (double *)calloc(NS_TSTEP, sizeof(double));
      link = (s_id_spa *)SPA_browse_all_struct(IDS_SPA, pzns->link[AQ_NS], BEGINNING_SPA);

      while (link != NULL) {
        pele_aq = MSH_get_ele_from_abs(pcarac_aq->pcmsh, link->id, flog);
        values = SPA_dynamic_nonsat_values(AQ_NSAT_HHYD_SPA, pele_aq, pzns, link->prct, t, 0, flog);
        hh += values[0];
        pcover_zns += values[1];
        link = link->next;
        free(values);
      }

      // Checking cover fraction in case of partially covered ZNS by AQ cells
      if (fabs(pcover_zns - 1.0) > EPS_CAW)
        hh /= pcover_zns;

      pzns->hydhead[PREVTS_NS] = hh;
      pzns->hydhead[CURRTS_NS] = hh;
      // LP_printf(flog, "ZNS cell INT_ID %d Hhyd %f [m] at time t %f\n", pzns->id[NS_INTERN], pzns->hydhead, t); // NG check
    }
  }
  if (nzns_off != 0) {
    LP_warning(flog, "In %s : %d ZNS cells not linked to the AQ system : Ignored for dynamic thickness calculations.\n", __func__, nzns_off);
  }
}

/**
 * \fn void CAW_cNSAT_update_dynamic_nsat (s_carac_zns*, s_carac_aq*, s_chyd*, double, int, int, FILE*)
 * \brief Launched all data updates needed for NSAT dynamic thickness behavior (i.e. hydraulic head, transport variable,
 * number of reservoirs. Performs projection of those values onto the NSAT grid.
 * \return -
 */
void CAW_cNSAT_update_dynamic_nsat(s_carac_zns *pcarac_zns, s_carac_aq *pcarac_aq, s_chyd *pchyd, double t, int flag_transport, int nb_species, FILE *flog) {

  int i, nb_res = pcarac_zns->nb_zns;
  s_zns *pzns;

  for (i = 0; i < nb_res; i++) {
    pzns = pcarac_zns->p_zns[i];
    CAW_cNSAT_define_soil_elevation(pzns, pcarac_aq, pchyd, t, flog); // Re-applied at each update because of time-dependant Cauchy conditions
    CAW_cNSAT_update_hydraulic_head(pzns, pcarac_aq, t, flog);

    if (flag_transport == YES) {
      CAW_cNSAT_update_aquifer_tr_variable(pzns, pcarac_aq, t, nb_species, flog);
    }

    // Number of reservoirs update according to the chosen method
    CAW_cNSAT_update_nres(pzns, pcarac_zns->nres_method, t, flog);
  }
}

/**
 * \fn void CAW_cNSAT_define_soil_elevation (s_zns_zns*, s_carac_aq*, double, FILE*)
 * \brief Projects soil elevation, initially declared at the aq msh scale onto the NSAT grid.
 * So far, to do so, only considers values declared as CAUCHY drainage value in libaq.
 * \return -
 */
void CAW_cNSAT_define_soil_elevation(s_zns *pzns, s_carac_aq *pcarac_aq, s_chyd *pchyd, double t, FILE *flog) {

  s_id_spa *link;
  s_ele_msh *pele_aq;
  s_bound_aq *pbound;
  // s_element_hyd *pele_riv;
  double soil_elev, pcover_zns;
  double *values;

  link = pzns->link[AQ_NS];

  soil_elev = 0.;
  pcover_zns = 0.;

  if (link != NULL) {
    link = (s_id_spa *)SPA_browse_all_struct(IDS_SPA, pzns->link[AQ_NS], BEGINNING_SPA);

    while (link != NULL) {
      pele_aq = MSH_get_ele_from_abs(pcarac_aq->pcmsh, link->id, flog);
      pbound = pele_aq->phydro->pbound;
      // LP_printf(flog, "Link ZNS %d <--> AQ cell ABS %d\n", pzns->id[NS_INTERN]+1, pele_aq->id[ABS_MSH]);

      if (pbound != NULL) {
        if (pbound->type == CAUCHY) {
          if (pbound->freq == CST) {
            values = SPA_dynamic_nonsat_values(AQ_NSAT_SOIL_SPA, pele_aq, pzns, link->prct, t, 0, flog);
            // LP_printf(flog, "id %d CAUCHY Freq %s value %f\n", pele_aq->id[ABS_MSH], AQ_name_freq(pbound->freq), pbound->pft->ft);
          } else {
            values = SPA_dynamic_nonsat_values(AQ_NSAT_CAUCHY_SPA, pele_aq, pzns, link->prct, t, 0, flog);
          }
        }
        soil_elev += values[0];
        pcover_zns += values[1]; // values[1] is s_aq/s_zns ratio
        free(values);
      }
      link = link->next;
    }

    // Checking cover fraction in case of partially covered ZNS by AQ cells
    if (fabs(pcover_zns - 1.) > EPS_CAW)
      soil_elev /= pcover_zns;

    pzns->soil_elevation = soil_elev;

    // LP_printf(flog, "ZNS cell INT_ID %d Soil_elevation %f [m] Connection fraction with aquifer %f\n", pzns->id[NS_INTERN], pzns->soil_elevation, pcover_zns); // NG check
  }

  // Je garde pour memo si on doit prendre en compte le zbot pour les mailles en loc RIV
  // switch (pele_aq->loc) {
  //  case RIV_MSH :
  //    // Fetching river bottom elevation in connection with the aquifer cell
  //    pele_riv = HYD_find_element_by_id_io(pele_aq->id_riv->id_lay, pele_aq->id_riv->id, pchyd, flog);
  //    pzns->soil_elevation = pele_riv->center->description->Zbottom;
  //    // LP_printf(flog, "Aquifer cell ABS %d connected to river element GIS %d Zbottom = %f\n", pele_aq->id[ABS_MSH], pele_riv->id[GIS_HYD],pele_riv->center->description->Zbottom); // NG check
  //    break;
}

/**
 * \fn void CAW_cNSAT_update_hydraulic_head (s_carac_zns*, s_carac_aq*, double, FILE*)
 * \brief Updates and projects hydraulic head onto the NSAT grid.
 * \return -
 */
void CAW_cNSAT_update_hydraulic_head(s_zns *pzns, s_carac_aq *pcarac_aq, double t, FILE *flog) {

  s_id_spa *link;
  s_ele_msh *pele_aq;
  double hhead, pcover_zns;
  double *values;

  hhead = 0.;
  pcover_zns = 0.;

  if (pzns->dyn_io == YES) {
    link = pzns->link[AQ_NS];
    link = (s_id_spa *)SPA_browse_all_struct(IDS_SPA, pzns->link[AQ_NS], BEGINNING_SPA);

    while (link != NULL) {
      pele_aq = MSH_get_ele_from_abs(pcarac_aq->pcmsh, link->id, flog);
      values = SPA_dynamic_nonsat_values(AQ_NSAT_HHYD_SPA, pele_aq, pzns, link->prct, t, 0, flog);
      hhead += values[0];
      pcover_zns += values[1];
      link = link->next;
      free(values);
    }

    // Checking cover pct in case of partially covered ZNS cells
    if (fabs(pcover_zns - 1.0) > EPS_CAW)
      hhead /= pcover_zns;

    // Updating hydraulic heads
    pzns->hydhead[PREVTS_NS] = pzns->hydhead[CURRTS_NS];
    pzns->hydhead[CURRTS_NS] = hhead;
    // LP_printf(flog, "ZNS cell INT_ID %d Hhyd(curr) %f [m] Hhyd(prev) %f [m]\n", pzns->id[NS_INTERN], pzns->hydhead[CURRTS_NS], pzns->hydhead[PREVTS_NS]); // NG check
  }
}

/**
 * \fn void CAW_cNSAT_update_nres (s_zns*, double, FILE*) {
 * \brief Defines the new number of reservoir of a nash cascade in case of dynamic thickness
 * behavior, according to the chosen method.
 * \return -
 */
void CAW_cNSAT_update_nres(s_zns *pzns, int method, double t, FILE *flog) {

  int var_res = 0;
  double dpiez = 0.;

  if (pzns->dyn_io == YES) {

    switch (method) {

    case (DYNSAT_SOIL_NS): // Recalculation of new number of reservoirs based on (soil-piezo)/height_res
      pzns->nres[PREVTS_NS] = pzns->nres[CURRTS_NS];
      pzns->nres[CURRTS_NS] = (int)(floor((pzns->soil_elevation - pzns->hydhead[CURRTS_NS]) / (pzns->pnash->carac_ZNS[NS_HEIGHT] * 1e-3)));
      break;

    case (DYNSAT_HH_NS): // Update of number of reservoirs based on hydhead variation only
      dpiez = pzns->hydhead[CURRTS_NS] - pzns->hydhead[PREVTS_NS];
      if (dpiez > 0) {
        var_res = (int)(floor(dpiez) / (pzns->pnash->carac_ZNS[NS_HEIGHT] * 1e-3));
      } else {
        var_res = (int)(ceil(dpiez) / (pzns->pnash->carac_ZNS[NS_HEIGHT] * 1e-3));
      }
      pzns->nres[PREVTS_NS] = pzns->nres[CURRTS_NS];
      pzns->nres[CURRTS_NS] += var_res;
      break;
    default:
      LP_error(flog, "In CaWaQS%4.2f : In file %s, function %s : Unknown reservoir update method.\n", NVERSION_CAW, __FILE__, __func__);
    }

    if (pzns->nres[CURRTS_NS] < 0)
      pzns->nres[CURRTS_NS] = 0;

    LP_printf(flog, "ID_NSAT %d new_nres %d\n", pzns->id[NS_GIS] + 1, pzns->nres[CURRTS_NS]); // NG check
  }
}

/**
 * \fn void CAW_cNSAT_update_aquifer_tr_variable (s_zns*, s_carac_aq*, double, int, FILE*)
 * \brief Updates and projects transport variable values onto the NSAT grid. Accounts for multi species.
 * \return -
 */
void CAW_cNSAT_update_aquifer_tr_variable(s_zns *pzns, s_carac_aq *pcarac_aq, double t, int nb_species, FILE *flog) {

  s_id_spa *link;
  s_ele_msh *pele_aq;
  int p;
  double *trvar, pcover_zns;
  double *values;

  trvar = (double *)calloc(nb_species, sizeof(double));
  pcover_zns = 0.;

  if (pzns->dyn_io == YES) {
    link = pzns->link[AQ_NS];
    link = (s_id_spa *)SPA_browse_all_struct(IDS_SPA, pzns->link[AQ_NS], BEGINNING_SPA);

    while (link != NULL) {
      pele_aq = MSH_get_ele_from_abs(pcarac_aq->pcmsh, link->id, flog);

      for (p = 0; p < nb_species; p++) {
        values = SPA_dynamic_nonsat_values(AQ_NSAT_TRVAR_SPA, pele_aq, pzns, link->prct, t, p, flog);
        trvar[p] += values[0];
        if (p == 0)
          pcover_zns += values[1];
        link = link->next;
        free(values);
      }
    }

    // Checking cover pct in case of partially covered ZNS cells
    for (p = 0; p < nb_species; p++) {
      if (fabs(pcover_zns - 1.0) > EPS_CAW)
        trvar[p] /= pcover_zns;
    }

    // Updating transport variable
    for (p = 0; p < nb_species; p++) {
      pzns->trvar_init[p] = trvar[p];
      LP_printf(flog, "Specie %d ZNS cell INT_ID %d Trvar(curr) %f [g/m3]\n", p, pzns->id[NS_INTERN], pzns->trvar_init[p]); // NG check
    }

    free(trvar);
  }
}
