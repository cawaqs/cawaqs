/*-------------------------------------------------------------------------------
 *
 * SOFTWARE NAME: cawaqs
 * FILE NAME: manage_pic.c
 *
 * CONTRIBUTORS: Nicolas GALLOIS, Baptiste LABARTHE, Shuaitao WANG,
 *               Agnès RIVIERE, Deniz KILIC, Mathias MAILLOT,
 *               Fulvia BARATELLI, Pierre GUILLOU, Emmanuel LEDOUX, Nicolas FLIPO
 *
 * SOFTWARE BRIEF DESCRIPTION: Physically based coupled surface–subsurface
 * model for joint water, matter and energy flux balances and flow dynamics
 * simulation within all compartments of a hydrosystem (sub-surface, hydraulic
 * network, vadose zone, aquifer system and stream-aquifer exchanges).
 *
 * Software developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the cawaqs Software.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Software and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <time.h>
#include <stdarg.h>
#include <sys/time.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <malloc.h>
#include <math.h>
#include <libprint.h>
#include <time_series.h>
#include "libpc.h"
#include "IO.h"
#include "GC.h"
#include "reservoir.h"
#include "spa.h"
#include "FP.h"
#include "NSAT.h"
#include "CHR.h"
#include "HYD.h"
#include "MSH.h"
#include "AQ.h"
#include "WET.h"
#include "TTC_coupled.h"
#include "SEB.h"
#include "CAW.h"

double CAW_solve_hydrosyst(s_carac_aq *pchar_aq, s_carac_fp *pchar_fp, s_chyd *pchyd, s_chronos_CHR *chronos, s_clock_CHR *clock, s_out_io **pout, s_mb_hyd *pmb, s_mb_hyd *pmb2, s_inout_set_io *pinout, s_output_hyd ***p_outputs, s_settings_cawaqs *pset_caw, int *iit, int *ts_count, FILE *fp, s_carac_fp *pchar_fp_hderm, s_chyd *pchyd_hderm, s_simul_cawaqs *psim) {

  FILE *fmb;
  double t, dt;
  double pic_max = 0;
  int sim_type, nit_max;
  sim_type = pset_caw->sim_type;
  if (pinout != NULL) {
    fmb = pinout->fmb;
  }
  t = chronos->t[CUR_CHR];
  dt = chronos->dt;

  switch (sim_type) {

  case SW_GW: {
    nit_max = pchar_aq->ppic->nitmax;
    pic_max = AQ_do_one_pic_it(iit, t, dt, pchar_aq, chronos, clock, pset_caw->idebug, ts_count, fp);
    // CAW_cAQ_check_cauchy_riv(dt,t,pchar_aq,pchyd,fp);//Checks connexion status for riv-aquifer cells
    CAW_calculate_aq_riv_flux(dt, t, pchar_aq, fp);
    CAW_update_AQ_MB(pchar_aq, iit, fp); // FB 12/04/2018 Update mass balance (for enr) before the libhyd calculation
    FP_calc_watbal_cprod_sout(pchar_fp, pchar_aq, fp);
    HYD_solve_hydro_coupled(t, pchyd, clock, chronos, pchar_fp, pchar_aq, sim_type, PIC_HYD, ts_count, fp);
    CAW_cAQ_check_cauchy_riv(dt, t, pchar_aq, pchyd, fp); // Checks connexion status for riv-aquifer cells
    // CAW_calculate_Q_cauchy_discrepancy(pchar_aq,pchyd,delta_q_cauchy,fp);//Remove if it works
    break;
  }

  case SURF_WAT: {
    LP_printf(fp, "Calculating flow in river system \n");
    HYD_solve_hydro_coupled(t, pchyd, clock, chronos, pchar_fp, pchar_aq, sim_type, T_HYD, ts_count, fp);
    break;
  }

  case GROUND_WAT: {
    nit_max = pchar_aq->ppic->nitmax;
    pic_max = AQ_do_one_pic_it(iit, t, dt, pchar_aq, chronos, clock, pset_caw->idebug, ts_count, fp);
    break;
  }

  case SURF_WAT_NSAT: {

    LP_printf(fp, "Calculating flow in river system \n");
    HYD_solve_hydro_coupled(t, pchyd, clock, chronos, pchar_fp, pchar_aq, sim_type, T_HYD, ts_count, fp);
    break;
  }

  case SURF_WAT_NSAT_HDERM: {

    LP_printf(fp, "Calculating flow in river system \n");
    HYD_solve_hydro_coupled(t, pchyd, clock, chronos, pchar_fp, pchar_aq, sim_type, T_HYD, ts_count, fp);
    LP_printf(fp, "\nCalculating flow in hyperdermic river system \n");
    HYD_solve_hydro_coupled(t, pchyd_hderm, clock, chronos, pchar_fp_hderm, pchar_aq, sim_type, T_HYD, ts_count, fp);
    break;
  }

  case SW_HDERM_GW: {

    LP_printf(fp, "\nCalculating flow in hyperdermic river system \n");
    HYD_solve_hydro_coupled(t, pchyd_hderm, clock, chronos, pchar_fp_hderm, pchar_aq, sim_type, T_HYD, ts_count, fp);
    nit_max = pchar_aq->ppic->nitmax;
    pic_max = AQ_do_one_pic_it(iit, t, dt, pchar_aq, chronos, clock, pset_caw->idebug, ts_count, fp);
    // CAW_cAQ_check_cauchy_riv(dt,t,pchar_aq,pchyd,fp);//Checks connexion status for riv-aquifer cells
    CAW_calculate_aq_riv_flux(dt, t, pchar_aq, fp);
    CAW_update_AQ_MB(pchar_aq, iit, fp); // FB 12/04/2018 Update mass balance (for enr) before the libhyd calculation
    FP_calc_watbal_cprod_sout(pchar_fp, pchar_aq, fp);

    LP_printf(fp, "Calculating flow in river system \n");
    HYD_solve_hydro_coupled(t, pchyd, clock, chronos, pchar_fp, pchar_aq, sim_type, PIC_HYD, ts_count, fp);
    CAW_cAQ_check_cauchy_riv(dt, t, pchar_aq, pchyd, fp); // Checks connexion status for riv-aquifer cells
    break;
  }
  }
  return pic_max;
}