#-------------------------------------------------------------------------------
# 
# SOFTWARE NAME: cawaqs
# FILE NAME: compil_lib.sh
# 
# CONTRIBUTORS: Nicolas GALLOIS, Baptiste LABARTHE, Shuaitao WANG,
#               Agnès RIVIERE, Deniz KILIC, Mathias MAILLOT,
#               Fulvia BARATELLI, Pierre GUILLOU, Emmanuel LEDOUX, Nicolas FLIPO
# 
# SOFTWARE BRIEF DESCRIPTION: Physically based coupled surface–subsurface model 
# for joint water, matter and energy flux balances and flow dynamics simulation
# within all compartments of a hydrosystem (sub-surface, hydraulic network, 
# vadose zone, aquifer system and stream-aquifer exchanges).
#
# Software developed at the Geosciences Center, joint research center 
# of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
#
# COPYRIGHT: (c) 2022 Contributors to the cawaqs Software.
# CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
#          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
#
# All rights reserved. This Software and the accompanying materials
# are made available under the terms of the Eclipse Public License v2.0
# which accompanies this distribution, and is available at
# http://www.eclipse.org/legal/epl-v20.html
# 
#------------------------------------------------------------------------------*/


PATH_INST=$3

rm $PATH_INST/lib$1$2_gcc.a
cd $PATH_INST/lib$1/src
awk -F= -f $PATH_INST/modify_lib_path.awk -v path=$PATH_INST Makefile
mv awk.out Makefile_tmp
make -f Makefile_tmp clean
make -f Makefile_tmp lib
