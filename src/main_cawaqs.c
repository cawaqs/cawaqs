/*
 * -------------------------------------------------------------------------------------- *
 *                      CaWaQS (CAtchment WAter Quality Simulator)                        *
 * -------------------------------------------------------------------------------------- *
 *
 * PROGRAM NAME : CaWaQS
 * FULL NAME    : Catchment Water Quality Simulator
 * VERSION ID   : 3.x
 *
 * Software developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * PROGRAM DESCRIPTION: Physically based coupled surface-subsurface model for
 *                      joint water, matter and energy flux balances and flow dynamics
 *                      simulation within all compartments of a hydrosystem
 *                      (sub-surface, hydraulic network, vadose zone, aquifer system
 *                      and stream-aquifer exchanges).
 *
 * CONTRIBUTORS: Nicolas GALLOIS (NG), Baptiste LABARTHE (BL), Shuaitao WANG (SW),
 *               Agnes RIVIERE (AR), Deniz KILIC (DK), Mathias MAILLOT (MM),
 *               Fulvia BARATELLI (FB), Pierre GUILLOU (PG), Emmanuel LEDOUX (EL)(**),
 *               Nicolas FLIPO (NF)(*)
 *
 * (*)  : Project Manager
 * (**) : Blueprint of CaWaQS through the MODCOU software, benchmarking
 *        MODCOU/CaWaQS with Nicolas GALLOIS
 *
 * -------------------------------------------------------------------------------------- *
 *                              CAWAQS ATTACHED REPOSITORIES                              *
 *    All attached repositories are made available under Eclipse Public License v2.0      *
 * -------------------------------------------------------------------------------------- *
 *
 * libfp      : Atmosphere-Soil water balance calculations using a
 *              conceptual 7-parameter reservoir model.
 *
 * libaq      : Aquifer systems (saturated) with finite volume using a semi-implicit
 *              temporal scheme: confined-unconfined aquifer units, 2D diffusivity
 *              equation in each layer, 1D vertical exchanges between layers with a
 *              linear drainance model, 1D vertical exchanges at the soil surface or
 *              for river with a conductance model, subcell lithology.
 *
 * libnsat    : Unsaturated zone simulated using a Nash cascade of reservoirs.
 *
 * libhyd     : 1D river network simulation solving in 0D with Muskigum or 1D with Barre
 *              de Saint Venant equation, 1D finite volume using a semi-implicit temporal
 *              scheme, 0D approach is tuned into a 0.5D one as long as geometries are defined,
 *              1D approach handles islands, tributaries, and hydraulics works such as dams.
 *
 * libwet     : Surface water bodies mass balance calculations (lakes, gravel quarries, etc.)
 *              accounting for exchanges with atmosphere and aquifer system.
 *
 * libttc     : Conservative transport (solute, heat) for porous media (horizontal 2D)
 *              or free surface flow (longitudinal 1D).
 *
 * libseb     : Atmosphere-water energy balance calculations.
 *
 * libprint   : Joined printing in stderr and files, warning messages and error messages.
 *
 * libts      : Management of time series including linear interpolation for holes
 *              as well as convertions back and forth between julian days and calendar dates.
 *
 * libpc      : Parallel computing management using open MP (memory shared)
 *
 * libchronos : Checks CPU time.
 *
 * libio      : Management of inputs and outputs.
 *
 * libspa     : Linking of meshes either regular, nested or irregular.
 *
 * libgc      : Linear solver based either on sparse (https://www.netlib.org/sparse/readme,
 *              (User guide available at https://www.netlib.org/sparse/spdoc),
 *              or modulef (Copyright (c) 1999 INRIA, https://www.rocq.inria.fr/modulef/english.html),
 *              both EPL v2.0 friendly.
 *
 * librsv     : Simulation of a reservoir 0.5D.
 *
 * libmesh    : Creation and management of nested mesh of squared cells.
 *
 * scripts    : Install scripts.
 *
 *-------------------------------------------------------------------------------
 *
 * COPYRIGHT    : (c) 2022 Contributors to the cawaqs software.
 *
 * CONTACT      : Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *                Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 *--------------------------------------------------------------------------------
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <time.h>
#include <stdarg.h>
#include <sys/time.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <malloc.h>
#include <math.h>
#include <libprint.h>
#include <time_series.h>
#include "libpc.h"
#include "IO.h"
#include "GC.h"
#include "reservoir.h"
#include "spa.h"
#include "FP.h"
#include "NSAT.h"
#include "CHR.h"
#include "HYD.h"
#include "MSH.h"
#include "AQ.h"
#include "WET.h"
#include "TTC_coupled.h"
#include "SEB.h"
#include "CAW.h"
// #include "global_CAWAQS.h"
#include "ext_CAWAQS.h"

int main(int argc, char **argv) {
  /* Date of the beginning of the simulation */
  char *date_of_simulation;
  /* Structure from time.h containing date, time, etc */
  static struct timeval current_time;
  /* Structure containing computer clock time */
  struct tm *clock_time;
  /* Current time in seconds when programm starts */
  time_t clock;
  /* Error report */
  int timi;
  /* Current time in simulation */
  double t, dt;
  /* Time of end of the simulation in s */
  double tend;
  /* Curent Time  of the simulation in days (julian) */
  double t_day;
  /* variables for loop on total years and total days*/
  int y, d;
  /* loop to print output in [AQ,NSAT,FP] for now, HYD outputs are printed separately */
  int i, nj_simu = 0;
  int nit_wet, nitwet = 0, nit_inter_wet; // SW 02/06/2016
  int nit_day;
  double test_out;
  /* year number and day number in the simulation  */
  int nj, year, nit = 0;
  double pic_max = 9999;
  s_mb_hyd *pmb;  // Mass balance structure containing the mass balance at t if asked for and chained with the previous mass balance pmb2
  s_mb_hyd *pmb2; // Mass balance structure containing the mass balance at t-1 if asked for
  int nday = 0;
  int nstep = 0;
  s_date_ts *date_curent;
  char *month;
  int *spe_types;
  int flag_dyn_nsat = NO;
  /* SW 19/07/2021 libseb variables */
  double t_abs, dt_day;
  int *id_meteo;
  s_rs_input **p_rside;
  int nele_libhyd, ne, ns, nr;
  int it_elehyd = 0;
  s_reach_hyd *preach;

  pmb = NULL;
  pmb2 = NULL;

  /* Creates the simulation structure */
  Simul = CAW_init_simul();

  /* Beginning of the simulation */
  Simul->clock->begin = time(NULL);

  /* Date */
  timi = gettimeofday(&current_time, NULL);
  clock = (time_t)current_time.tv_sec;
  ctime(&clock);
  clock_time = localtime(&clock);
  date_of_simulation = asctime(clock_time);

  /* Opening of command file */
  if (argc != 3) {
    LP_error(stderr, "WRONG COMMAND LINE : CaWaQS.%4.2f CommandFileName DebugFileName\n", NVERSION_CAW);
  }

  if ((Simul->poutputs = fopen(argv[2], "w")) == 0) {
    LP_error(stderr, "Impossible to open the log file %s\n", argv[2]);
  }

  LP_log_file_header(stderr, "CaWaQS", NVERSION_CAW, date_of_simulation, argv[1]);
  LP_log_file_header(Simul->poutputs, "CaWaQS", NVERSION_CAW, date_of_simulation, argv[1]);

  /********************************************************/
  /*    Now input files are read  --> see file input.y    */
  /********************************************************/

  CHR_begin_timer(); // LV 3/09/2012
  lecture(argv[1], Simul->poutputs);

  /********************************************************/
  /*          Verifications before calculations           */
  /********************************************************/

  if (Simul->pset_caw->calc_state == STEADY) {
    CHR_reinit_chronos(Simul->chronos);
  } // NF 4/12/2017

  t = Simul->chronos->t[BEGINNING];
  tend = Simul->chronos->t[END];
  dt = Simul->chronos->dt;
  CHR_calculate_simulation_date(Simul->chronos, t);

  nstep = tend / dt;
  LP_printf(Simul->poutputs, "Total number of time-steps over the simulation period = %d\n", nstep);

  LP_printf(Simul->poutputs, "Checking inputs...\n");
  CAW_check_compatibility_output_hydro_module(Simul); // NG : 04/01/2021 Check compatibility of requested outputs and defined modules
  CAW_check_compatibility_outputs_transport(Simul);
  LP_printf(Simul->poutputs, "Consistency between inputs and outputs checked !\n");

  CAW_check_inputs(Simul->pcarac_aq, Simul->pcarac_fp, Simul->pcarac_zns, Simul->pchyd, Simul->pset_caw, Simul->pcarac_wet, Simul->chronos, Simul->pcarac_fp_hderm, Simul->pchyd_hderm, Simul->poutputs);

  if (Simul->pset_caw->sim_type == SW_GW || Simul->pset_caw->sim_type == SW_HDERM_GW) {
    CAW_cAQ_print_nappe_riv(Simul->pcarac_aq, Simul->pchyd, Simul->poutputs);

    if (Simul->pcarac_zns->dynzns_io == YES) { // NG : 27/06/2023 : Launching dynamic ZNS init if activated.
      CAW_cNSAT_dynNsat_init(Simul->pcarac_zns, Simul->pcarac_aq, 0, Simul->poutputs);
      LP_printf(Simul->poutputs, "Dynamic NONSAT : Initial hydraulic heads projected onto unsaturated zone grid.\n");
    }
  }

  LP_printf(Simul->poutputs, "Done \n");
  CAW_print_characteristics(Simul->pset_caw, Simul->pcarac_aq, Simul->pcarac_fp, Simul->pcarac_zns, Simul->pchyd, Simul->pcarac_fp_hderm, Simul->pchyd_hderm, Simul->poutputs);
  Simul->clock->time_spent[LEC_CHR] += CHR_end_timer();
  LP_printf(Simul->poutputs, "********************************************************\n");
  LP_printf(Simul->poutputs, "\t --> SIMULATION IN %s MODE\n", CAW_sim_name(Simul->pset_caw->sim_type, Simul->poutputs));
  LP_printf(Simul->poutputs, "********************************************************\n");

  /********************************************************/
  /*           Starting HYDRO calculations                */
  /********************************************************/

  CHR_begin_timer();                                                                   // LV 3/09/2012
  if (Simul->pset_caw->sim_type != GROUND_WAT && Simul->pset_caw->sim_type != GW_LAKE) // SW 28/04/2016 add GW_LAKE
  {                                                                                    // NF 27/9/2017 This part has to be put directly in input.y otherwise the conductance of the river is not calculated properly (conduc=0). Moreover it is a typical operation to be performed during reading of input files. Except HYD_initialize_GC(Simul->pchyd,Simul->chronos,Simul->poutputs);
    LP_printf(Simul->poutputs, "Initializing hydraulics structures\n");
    HYD_initialize_hydro(Simul->pchyd, Simul->pinout, Simul->pout[HYD_Q_IO], Simul->chronos, Simul->poutputs);
    HYD_assign_river(Simul->pchyd);                                   // LV nov2014
    HYD_initialize_GC(Simul->pchyd, Simul->chronos, Simul->poutputs); // LV nov2014 : initialisation des coefs de la matrice de calcul de l'hydro
    LP_printf(Simul->poutputs, "Done \n");
  }
  // NG : 03/03/2020 : Hyperdermic network initialization
  if (Simul->pset_caw->sim_type == SURF_WAT_NSAT_HDERM || Simul->pset_caw->sim_type == SW_HDERM_GW) {
    LP_printf(Simul->poutputs, "Initializing hydraulics structures for hyperdermic river network\n");
    HYD_initialize_hydro(Simul->pchyd_hderm, Simul->pinout, Simul->pout[HDERM_Q_IO], Simul->chronos, Simul->poutputs);
    HYD_assign_river(Simul->pchyd_hderm);
    HYD_initialize_GC(Simul->pchyd_hderm, Simul->chronos, Simul->poutputs);
    LP_printf(Simul->poutputs, "Done \n");
  }

  // NG : 12/08/2020 : Full TTC transport initialization in every CaWaQS module it is called for
  if (Simul->pset_caw->transport_caw == YES) {
    /* First, we fetch all declared specie types in an int*, so we can easily pass it on to
     * any library to differentiate SOLUTE and HEAT type species */
    spe_types = CAW_fetch_specie_types(Simul, Simul->poutputs);

    for (i = 0; i < NMOD_CAW; i++) {
      if (((i == HYD_CAW) || (i == HDERM_CAW) || (i == AQ_CAW)) && Simul->pset_caw->transport_module[i] == YES) {
        CAW_initialize_TTC_all(Simul, i, nstep, Simul->poutputs);
        LP_printf(Simul->poutputs, " TTC library has been fully initialized for all species in order to run with %s CaWaQS module.\n", CAW_name_mod(i));
      }

      /* SW 19/07/2021 add here libseb initialization */
      /*** Meteorological parameters: filling, linking and calculating ***/
      if ((i == HYD_CAW) && (Simul->pset_caw->transport_module[SEB_CAW] == YES)) {
        nele_libhyd = Simul->pcarac_ttc[i]->count[NELE_TTC];

        /****************************************************************************/
        /*** Table of size nele, that links id_abs (of the elements) to id_meteo: ***/
        /*** the indices correspond to id_abs and the values are int id_meteo. ******/
        /*** This is an important function... ***************************************/
        /****************************************************************************/

        id_meteo = CAW_link_icell_to_imet_SEB(nele_libhyd, Simul->mto_seb->n_meteocell, Simul->pchyd->counter->nreaches, Simul->mto_seb->p_rts, Simul->pchyd->p_reach, Simul->mto_seb->p_met); // SW 14/06/2021 add p_met
        /*** p_rts est libérée à fin de la routine. ***/

        /***************************/
        /*** Initialization: t=0 ***/
        /***************************/

        // Here some initializations are made by libseb and libttc for the river transport.
        // Ask to SW and reorganize these functions in a proper location
        CAW_init_TTC_val_one_species_riv(Simul->pcarac_ttc[i], Simul->pchyd, nele_libhyd, 0, Simul->poutputs);

        Simul->mto_seb->p_surf_heat_flux = SEB_alloc_shf(nele_libhyd);
        p_rside = SEB_alloc_rs_input(Simul->pchyd->counter->nreaches);
        Simul->mto_seb->H_flux_ttc = (double *)malloc(nele_libhyd * sizeof(double));
        for (ns = 0; ns < Simul->pcarac_ttc[i]->count[NSPECIES_TTC]; ns++) {
          if (Simul->pcarac_ttc[i]->p_species[ns]->pset->type == HEAT_TTC) {
            for (ne = 0; ne < nele_libhyd; ne++) {
              p_rside[0]->val[VTS] = Simul->mto_seb->viewToSky; /*** !!! To be filled in through the input.y !!! --> parameter of vegetal cover ***/
              Simul->mto_seb->p_surf_heat_flux[ne]->pH_inputs->r_side = p_rside[0]->val;
              // Currently we index at 0 not the 'ne' because the dimensions are not correct at the moment. r_side should have number of elements of each reach (id_int)
            }

            /* test initialization temperature */ // DK This has to be removed after input.y addition
            for (nr = 0; nr < Simul->pchyd->counter->nreaches; nr++) {
              preach = Simul->pchyd->p_reach[nr];
              for (ne = 0; ne < preach->nele; ne++) {
                // DK test
                Simul->mto_seb->p_surf_heat_flux[it_elehyd]->pH_inputs->Tw = preach->p_ele[ne]->center->hydro->transp_var[ns]; // Initialzie Twater in libseb (Kelvin)
                                                                                                                               //                      LP_printf(Simul->poutputs, "Simul river: nreach: %d, nele: %d, transp_var[%d]: %lf\n", nr, ne, ns, preach->p_ele[ne]->center->hydro->transp_var[ns] );
                it_elehyd++;
              }
            }
          }
        }
        // SW 14/06/2021 add here allocation and initialization of meteo
        CAW_alloc_inputs_meteo_all(nele_libhyd, Simul->mto_seb->p_surf_heat_flux, NMETEO);

        t_abs = Simul->mto_seb->t_extrema[BEGINNING_CHR]; // beginning julian day for the simulation
        dt_day = Simul->chronos->dt / NSEC_DAY_TS;
      }
    }
  }

  if (Simul->pset_caw->sim_type != SURF_WAT && Simul->pset_caw->sim_type != SURF_WAT_NSAT && Simul->pset_caw->sim_type != SURF_WAT_NSAT_HDERM) {
    if (Simul->pset_caw->sim_type == GW_LAKE) {
      WET_print_carac_wet(Simul->pcarac_wet, Simul->poutputs);
      WET_calc_surf_wet(Simul->pcarac_wet, Simul->poutputs);
      WET_define_dirichlet(Simul->pcarac_aq->pcmsh, Simul->pcarac_wet, Simul->poutputs);
      LP_printf(Simul->poutputs, "Setting BC DIRICHLET for lake\n"); // SW 28/04/2016 set bc cauchy for faces under lake
    }
    LP_printf(Simul->poutputs, "Initializing aquifer structures\n");
    MSH_link_mesh_SIG(Simul->pcarac_aq->pcmsh, Simul->poutputs);
    if (Simul->pset_caw->sim_type == GW_LAKE) {

      WET_define_condu_wet(Simul->pcarac_aq->pcmsh, Simul->pcarac_wet, Simul->chronos, Simul->poutputs);
      LP_printf(Simul->poutputs, "Done \n");
      WET_define_face_ele_aq_wet(Simul->pcarac_aq, Simul->pcarac_wet, Simul->poutputs);
      if (Simul->pset_caw->calc_state == STEADY) {
        if (Simul->pcarac_wet->fpmto != NULL) {
          WET_calc_penman_steady_mto(Simul->pcarac_wet, Simul->poutputs);
          WET_print_evap_penman_to_file(Simul->pcarac_wet, Simul->poutputs);
          WET_series_to_mean_mto(Simul->pcarac_wet, Simul->poutputs);
          WET_free_mto_wet(Simul->pcarac_wet, Simul->poutputs);
        }
      }
    }

    AQ_finalize_count_hydro(Simul->pcarac_aq->pcmsh, Simul->poutputs);
    AQ_tab_eletype_all(Simul->pcarac_aq->pcmsh, Simul->poutputs);

    if (Simul->pset_caw->calc_state == STEADY) {
      // NF 26/11/2018 For STEADY STATE, the only numerical scheme that works is implicit, otherwise there are issues with the cauchy term. DO NOT COMMENT THIS LINE
      AQ_init_steady(Simul->pcarac_aq);
      LP_printf(Simul->poutputs, "cawaqs%4.2f : Taking an implicit scheme for steady state\n", NVERSION_CAW); // NF 5/1/2018 To have a steady state with a proper solution. SEMI IMPLICIT DOESN'T WORK
    }

    AQ_optimise_pic(Simul->pcarac_aq->pcmsh, Simul->pcarac_aq->ppic, Simul->pset_caw->calc_state, Simul->poutputs); // NF 18/10/2017
    AQ_check_compatibility_DIRICHLET_initial_state(Simul->pcarac_aq, Simul->poutputs);                              // NF 5/1/2018
    AQ_init_cauchy_qlim(Simul->pcarac_aq, Simul->poutputs);                                                         // NF 18/1/2018 Initialising of the pele->phydro->Qlim to the pbound->qlim. This is indispensable for a proper filling of RHS in the case of a non linear CAUCHY value (QLIM)
    AQ_init_mat_int(Simul->pcarac_aq->pcmsh, Simul->pcarac_aq->calc_aq, Simul->poutputs);
    GC_check_consistency(Simul->pcarac_aq->calc_aq, Simul->poutputs); // NF 7/10/2017 Check the consistency of the matricial system
    AQ_init_sol_vec(Simul->pcarac_aq->calc_aq);
    AQ_init_RHS(Simul->pcarac_aq->calc_aq);

    for (i = 0; i < Simul->pcarac_aq->pcmsh->pcount->nlayer; i++) {
      if (Simul->pcarac_aq->pcmsh->p_layer[i]->type == UNCONFINED_AQ) // NF 6/12/2018 There was a tricky bug before. Indeed the check was on the global mesh, and not per layer !
      {
        AQ_calc_param_unconfined(Simul->pcarac_aq->settings, Simul->pcarac_aq->pcmsh->p_layer[i], Simul->poutputs);
        AQ_calc_face_transm_layer(Simul->pcarac_aq->pcmsh->p_layer[i], Simul->poutputs);
        AQ_calc_face_cond_layer(Simul->pcarac_aq->settings, Simul->pcarac_aq->pcmsh->p_layer[i], Simul->chronos, Simul->poutputs); // MM 24/10/2018//NF 6/12/2018 Transm need to be calculated befor cond
      }
    }

    Simul->pcarac_aq->calc_aq->mat6 = GC_create_mat_double(Simul->pcarac_aq->calc_aq->lmat5);                                                                        // NF 9/10/2018 create d'allocation de mat6 comme cela AQ_calc_mat6 peut être appele de n'importe ou
    AQ_calc_mat6(Simul->pcarac_aq->pcmsh, Simul->chronos, Simul->pcarac_aq->settings, Simul->pcarac_aq->calc_aq, Simul->chronos->t[BEGINNING_CHR], Simul->poutputs); // BL ATTENTION peut ne pas calculer les paramètres de mat6 à tous les pas de temps uniquement si non nappe libre et que ttes les bounds sont cst!!

    if (Simul->pset_caw->sim_type == SW_GW || Simul->pset_caw->sim_type == SW_HDERM_GW) {
      CAW_cAQ_init_z_riv(Simul->pcarac_aq, Simul->pchyd, Simul->poutputs);
    }
    LP_printf(Simul->poutputs, "Done \n");
  }

  if (Simul->pset_caw->sim_type == GW_LAKE) {
    WET_ini_hini_wet(Simul->pcarac_aq, Simul->pcarac_wet, Simul->poutputs);
  }

  Simul->clock->time_spent[INIT_CHR] += CHR_end_timer(); // LV 3/09/2012

  if (Simul->pset_caw->sim_type != GROUND_WAT && Simul->pset_caw->sim_type != GW_LAKE) // SW 28/04/2016 add GW_LAKE
  {
    CHR_begin_timer(); // LV 3/09/2012
    if (Simul->pinout != NULL && Simul->outputs != NULL) {
      if (Simul->pset_caw->sim_type == SURF_WAT_NSAT_HDERM || Simul->pset_caw->sim_type == SW_HDERM_GW) // NG : 03/03/2020  : Volcanic sim type added
        HYD_create_output_files(Simul->pinout, Simul->outputs, Simul->pchyd_hderm, Simul->poutputs);    // NG : 21/02/2020  : HDERM added
      else
        HYD_create_output_files(Simul->pinout, Simul->outputs, Simul->pchyd, Simul->poutputs);
    }
    Simul->clock->time_spent[OUTPUTS_CHR] += CHR_end_timer(); // LV 3/09/2012
  }

  if (Simul->pset_caw->calc_state == TRANSIENT) {
    LP_printf(Simul->poutputs, "Inside transient solution \n");
    if (Simul->pset_caw->sim_type != GROUND_WAT && Simul->pset_caw->sim_type != GW_LAKE) // SW 28/04/2016 add GW_LAKE
    {
      if (Simul->outputs != NULL && Simul->pinout != NULL) {
        CHR_begin_timer();                                                                                                               // LV 3/09/2012
        HYD_print_outputs_old(Simul->chronos->t[CUR_CHR], Simul->outputs, Simul->pinout, Simul->pchyd, Simul->chronos, Simul->poutputs); // LV test 26/07/2012
        LP_printf(Simul->poutputs, " dt out print output : %f \n", Simul->chronos->dt);
        Simul->clock->time_spent[OUTPUTS_CHR] += CHR_end_timer(); // LV 3/09/2012
      }
    }
    // ###################
    //  START SIMULATION
    // ###################
    // ###################
    //  START YEAR LOOP //
    // ###################
    for (y = 0; y < Simul->chronos->nyear; y++) {
      CHR_refresh_year(Simul->chronos, y);
      LP_printf(Simul->poutputs, "Initializing files\n");

      CAW_init_files(Simul->pout, Simul->pset_caw, Simul->pcarac_fp, Simul->pcarac_zns, Simul->pchyd, Simul->pcarac_aq, Simul->pcarac_fp_hderm, Simul->pchyd_hderm, Simul->chronos, Simul->nb_species, Simul->pspecies, spe_types, Simul->poutputs);

      LP_printf(Simul->poutputs, "Done\n");

      if (Simul->pset_caw->sim_type != GROUND_WAT && Simul->pset_caw->sim_type != GW_LAKE) {
        LP_printf(Simul->poutputs, "Reading MTO inputs\n");
        CHR_begin_timer(); // LV 3/09/2012
        FP_read_all_MTO(Simul->pcarac_fp, Simul->chronos->year[CUR_CHR], Simul->poutputs);
        if (Simul->pcarac_fp->input_lai == YES) { // NG : 26/11/2019
          LP_printf(Simul->poutputs, "Reading LAI inputs\n");
          FP_read_all_LAI(Simul->pcarac_fp, Simul->chronos->year[CUR_CHR], Simul->poutputs);
        }

        if (Simul->pset_caw->transport_caw == YES && Simul->pset_caw->transport_module[FP_CAW] == YES) { // NG : 17/07/2020
          LP_printf(Simul->poutputs, "Reading transport forcing inputs\n");
          FP_read_all_transport_inputs(Simul->pcarac_fp, Simul->nb_species, Simul->chronos->year[CUR_CHR], Simul->poutputs);
        }

        Simul->clock->time_spent[LEC_CHR] += CHR_end_timer(); // LV 3/09/2012
        LP_printf(Simul->poutputs, "Done\n");
      }

      // ##################
      //  START DAY LOOP //
      // ##################

      nj = TS_nb_jour_an(Simul->chronos->year[CUR_CHR] + 1);

      for (d = 0; d < nj; d++) {
        t_day = (double)TS_calculate_jour_julien_j_nbday(Simul->chronos->year[CUR_CHR], CODE_FP, d);
        ++nday;
        flag_dyn_nsat = NO;
        test_out = (d + 1) * Simul->chronos->dt / Simul->chronos->dt_output;
        date_curent = TS_convert_julian2date(t_day, CODE_CAW, Simul->poutputs);
        month = TS_name_month(date_curent->mm, Simul->poutputs);
        LP_printf(Simul->poutputs, "\n\n************simul day : %d    simul date : %d %s %d *************\n\n", nday, date_curent->dd, month, date_curent->yyyy);
        free(date_curent);
        free(month);
        CHR_refresh_t(Simul->chronos);

        if (Simul->pset_caw->sim_type != GROUND_WAT && Simul->pset_caw->sim_type != GW_LAKE) {
          CHR_begin_timer();
          LP_printf(Simul->poutputs, "Calculating surface water balance\n");
          FP_calc_wat_bal_BU_all(Simul->pcarac_fp, t_day, CHR_convert_time(dt, S_CHR, DAYS_CHR), Simul->poutputs);
          LP_printf(Simul->poutputs, "Done\n");
          Simul->clock->time_spent[FP_CHR] += CHR_end_timer();
          CHR_begin_timer();

          // NG : Transport 21/07/2020
          if (Simul->pset_caw->transport_caw == YES && Simul->pset_caw->transport_module[FP_CAW] == YES) {
            LP_printf(Simul->poutputs, "TRANSPORT : Calculating transport surface inputs\n");
            FP_calc_transport_all_surface_fluxes(Simul->pcarac_fp, Simul->nb_species, spe_types, Simul->chem_param[FP_CAW][MMASS_SPECIES_CAW], Simul->chem_param[FP_CAW][MMASS_ELEMENT_CAW], t_day, CHR_convert_time(dt, S_CHR, DAYS_CHR), Simul->poutputs);
            LP_printf(Simul->poutputs, "Done\n");
            Simul->clock->time_spent[FP_TRANS_CHR] += CHR_end_timer();
            CHR_begin_timer();
          }
          LP_printf(Simul->poutputs, "Calculating water balance on cell surface\n");
          CHR_begin_timer();
          FP_calc_watbal_cprod_all(Simul->pcarac_fp, CHR_convert_time(dt, S_CHR, DAYS_CHR), t_day, Simul->poutputs);
          Simul->clock->time_spent[FP_CHR] += CHR_end_timer();
          LP_printf(Simul->poutputs, "Done\n");
          CHR_begin_timer();

          // SW : libseb fluxes 19/07/2021

          if (Simul->pset_caw->transport_module[SEB_CAW] == YES && Simul->pset_caw->transport_module[HYD_CAW] == YES) {
            /*****************************************/
            // if(Simul->calc_mode[EB_HEAT] == YES) // SW 04/05/2021 add energy balance, need SEB
            // CAW_calc_energy_init(t, Simul->chronos->dt, Simul->pcarac_heat_ttc->p_species[0], Simul->mb_heat, Simul->pchyd, Simul->poutputs);

            // CHR_begin_timer();
            //  calculation of energy fluxes by libseb

            for (ns = 0; ns < Simul->pcarac_ttc[HYD_CAW]->count[NSPECIES_TTC]; ns++) {
              if (Simul->pcarac_ttc[HYD_CAW]->p_species[ns]->pset->type == HEAT_TTC)
                CAW_update_meteo_for_HT(Simul->pcarac_ttc[HYD_CAW]->p_species[ns], Simul->pcarac_ttc[HYD_CAW]->count[NELE_TTC], Simul->mto_seb->n_meteocell, t_abs, Simul->pchyd, Simul->mto_seb->p_surf_heat_flux, Simul->mto_seb->p_met, id_meteo, Simul->mto_seb->H_flux_ttc);
            }

            // Simul->clock->time_spent[SEB_CHR] += CHR_end_timer();

            // if(Simul->calc_mode[EB_HEAT] == YES) // SW 04/05/2021 add energy balance, need SEB
            // CAW_calc_flux_from_seb(t, Simul->chronos->dt, Simul->pcarac_heat_ttc->p_species[0], Simul->mb_heat, Simul->pchyd, p_surf_heat_flux, Simul->poutputs);

            // CAW_update_heat_transport(Simul->pcarac_heat_ttc->p_species[0], Simul->pchyd, Simul->mto_seb->H_flux_ttcH_flux_ttc, nele, t, Simul->poutputs);

            // if(Simul->calc_mode[EB_HEAT] == YES) // SW 04/05/2021 add energy balance, need SEB
            // CAW_calc_all_ebs_temperature_and_energy_end(Simul->pcarac_heat_ttc, t, dt, H_flux_ttc, Simul->poutputs);

            // PROSE_print_outputs_heat(t, p_surf_heat_flux, Simul->outputs, Simul->pinout, Simul->pchyd, Simul->chronos, Simul->poutputs);

            t_abs = t_abs + dt_day;
          }
          // SW : libseb fluxes end 19/07/2021

          // NG : Transport FP 27/07/2020 - Il est capital d'imbriquer hydro et transport dans cet ordre => pas touche !
          if (Simul->pset_caw->transport_caw == YES && Simul->pset_caw->transport_module[FP_CAW] == YES) {
            LP_printf(Simul->poutputs, "TRANSPORT : Calculating transport flux balance on cell surface\n");
            FP_calc_transport_fluxes_cprod_all(Simul->pcarac_fp, Simul->nb_species, spe_types, CHR_convert_time(dt, S_CHR, DAYS_CHR), t_day, Simul->poutputs);
            Simul->clock->time_spent[FP_TRANS_CHR] += CHR_end_timer();
            LP_printf(Simul->poutputs, "Done\n");
            CHR_begin_timer();
          }

          if (Simul->pset_caw->sim_type == SW_GW) {
            if (Simul->pset_caw->transport_caw == NO) {
              LP_printf(Simul->poutputs, "Calculating ZNS water flow\n");

              // NG : 27/06/2023 : Launching ZNS data update if dynamic behavior activated.
              if (Simul->pcarac_zns->dynzns_io == YES) {
                if (((int)Simul->chronos->t[CUR_CHR] % (int)Simul->pcarac_zns->dyn_refresh_dt) == 0) {
                  flag_dyn_nsat = YES;
                  CAW_cNSAT_update_dynamic_nsat(Simul->pcarac_zns, Simul->pcarac_aq, Simul->pchyd, t_day, Simul->pset_caw->transport_caw, Simul->nb_species, Simul->poutputs);
                }
              }

              CAW_cHYD_calc_runoff_catchment_chasm_all(Simul->pcarac_zns, Simul->pchyd, Simul->poutputs); // NG : 24/01/2020 : Calcul du champ pcatchment->Q_chasm pour les catchments de type chasm
              NSAT_calculate_all_zns_coupled(Simul->pcarac_zns, CHR_convert_time(dt, S_CHR, DAYS_CHR), Simul->pcarac_fp, Simul->poutputs, flag_dyn_nsat, t_day);
              LP_printf(Simul->poutputs, "Done\n");
              Simul->clock->time_spent[NSAT_CHR] += CHR_end_timer();
              CHR_begin_timer();
            } else // NG : 31/07/2020 : Eau et transport simultanés NSAT
            {
              if (Simul->pset_caw->transport_module[NSAT_CAW] == YES) {
                LP_printf(Simul->poutputs, "TRANSPORT : Calculating ZNS coupled water and transport flows\n");
                CAW_cHYD_calc_runoff_catchment_chasm_all(Simul->pcarac_zns, Simul->pchyd, Simul->poutputs);

                // NG : 27/06/2023 : Launching ZNS data update if dynamic behavior activated.
                if (Simul->pcarac_zns->dynzns_io == YES) {
                  if (((int)Simul->chronos->t[CUR_CHR] % (int)Simul->pcarac_zns->dyn_refresh_dt) == 0) {
                    flag_dyn_nsat = YES;
                    CAW_cNSAT_update_dynamic_nsat(Simul->pcarac_zns, Simul->pcarac_aq, Simul->pchyd, t_day, Simul->pset_caw->transport_caw, Simul->nb_species, Simul->poutputs);
                  }
                }
                NSAT_calculate_all_zns_transport(Simul->nb_species, spe_types, Simul->pcarac_zns, CHR_convert_time(dt, S_CHR, DAYS_CHR), Simul->pcarac_fp, Simul->poutputs, flag_dyn_nsat, t_day);
                LP_printf(Simul->poutputs, "Done\n");
                Simul->clock->time_spent[NSAT_TRANS_CHR] += CHR_end_timer();
                CHR_begin_timer();
              }
            }

            LP_printf(Simul->poutputs, "Calculating flow in hydro-system \n");
            CAW_cAQ_calculate_recharge_coupled(Simul->chronos->t[CUR_CHR], Simul->chronos->dt, NSAT_CAW, Simul->pcarac_aq, Simul->pcarac_zns, Simul->pchyd_hderm, Simul->poutputs); // NG : 07/03/2020
            if (Simul->pcarac_aq->settings->cascade == YES)
              AQ_transfer_derivation_discharge(Simul->chronos->t[CUR_CHR], Simul->chronos->dt, Simul->pcarac_aq, CASCADE_AQ, CASCADE_DISCHARGE_AQ, Simul->poutputs); // NG : 19/04/2021
            if (Simul->pcarac_aq->settings->bypass == YES)
              AQ_transfer_derivation_discharge(Simul->chronos->t[CUR_CHR], Simul->chronos->dt, Simul->pcarac_aq, BYPASS_AQ, BYPASS_DISCHARGE_AQ, Simul->poutputs);

            LP_printf(Simul->poutputs, "Done\n");
            Simul->clock->time_spent[AQ_HIN_CHR] += CHR_end_timer();
            CHR_begin_timer();

            // AQ_print_elesource(Simul->pcarac_aq->pcmsh,Simul->poutputs); // BL to debug
            AQ_fill_state(Simul->pcarac_aq, Simul->chronos, Simul->pout, INI_AQ, Simul->poutputs); // NF 29/11/2018
          }

          else if (Simul->pset_caw->sim_type == SURF_WAT_NSAT) {
            if (Simul->pset_caw->transport_caw == NO) {
              LP_printf(Simul->poutputs, "Calculating ZNS water flow\n");
              CAW_cHYD_calc_runoff_catchment_chasm_all(Simul->pcarac_zns, Simul->pchyd, Simul->poutputs); // NG : 24/01/2020 : Calcul du champ pcatchment->Q_chasm pour les catchments de type chasm
              NSAT_calculate_all_zns_coupled(Simul->pcarac_zns, CHR_convert_time(dt, S_CHR, DAYS_CHR), Simul->pcarac_fp, Simul->poutputs, flag_dyn_nsat, t_day);
              LP_printf(Simul->poutputs, "Done\n");
              Simul->clock->time_spent[NSAT_CHR] += CHR_end_timer();
              CHR_begin_timer();
            } else {
              if (Simul->pset_caw->transport_module[NSAT_CAW] == YES) {
                LP_printf(Simul->poutputs, "TRANSPORT : Calculating ZNS coupled water and transport flows\n");
                CAW_cHYD_calc_runoff_catchment_chasm_all(Simul->pcarac_zns, Simul->pchyd, Simul->poutputs);
                NSAT_calculate_all_zns_transport(Simul->nb_species, spe_types, Simul->pcarac_zns, CHR_convert_time(dt, S_CHR, DAYS_CHR), Simul->pcarac_fp, Simul->poutputs, flag_dyn_nsat, t_day);
                LP_printf(Simul->poutputs, "Done\n");
                Simul->clock->time_spent[NSAT_TRANS_CHR] += CHR_end_timer();
                CHR_begin_timer();
              }
            }
          }

          else if (Simul->pset_caw->sim_type == SURF_WAT_NSAT_HDERM) // NG : 20/02/2020 : hyperdermique
          {
            if (Simul->pset_caw->transport_caw == NO) {
              LP_printf(Simul->poutputs, "Calculating ZNS water flow\n");
              CAW_cHYD_calc_runoff_catchment_chasm_all(Simul->pcarac_zns, Simul->pchyd, Simul->poutputs); // NG : 24/01/2020 : Calcul du champ pcatchment->Q_chasm pour les catchments de type chasm
              NSAT_calculate_all_zns_coupled(Simul->pcarac_zns, CHR_convert_time(dt, S_CHR, DAYS_CHR), Simul->pcarac_fp, Simul->poutputs, flag_dyn_nsat, t_day);
              LP_printf(Simul->poutputs, "Done\n");
              Simul->clock->time_spent[NSAT_CHR] += CHR_end_timer();
              CHR_begin_timer();
            } else // NG : 31/07/2020 : Eau et transport simultanés NSAT
            {
              if (Simul->pset_caw->transport_module[NSAT_CAW] == YES) {
                LP_printf(Simul->poutputs, "TRANSPORT : Calculating ZNS coupled water and transport flows\n");
                CAW_cHYD_calc_runoff_catchment_chasm_all(Simul->pcarac_zns, Simul->pchyd, Simul->poutputs);
                NSAT_calculate_all_zns_transport(Simul->nb_species, spe_types, Simul->pcarac_zns, CHR_convert_time(dt, S_CHR, DAYS_CHR), Simul->pcarac_fp, Simul->poutputs, flag_dyn_nsat, t_day);
                LP_printf(Simul->poutputs, "Done\n");
                Simul->clock->time_spent[NSAT_TRANS_CHR] += CHR_end_timer();
                CHR_begin_timer();
              }
            }

            LP_printf(Simul->poutputs, "Calculating water flow from ZNS to hyperdermic zone\n");
            CAW_cFP_calc_watbal_cprod_coupled_hyperdermic_all(Simul->pcarac_fp_hderm, Simul->pcarac_zns, CHR_convert_time(dt, S_CHR, DAYS_CHR), t_day, Simul->poutputs);
            LP_printf(Simul->poutputs, "Done\n");
            HYD_refresh_qapp_coupled(Simul->pchyd_hderm, Simul->poutputs);
            Simul->clock->time_spent[HDERM_CHR] += CHR_end_timer();
            CHR_begin_timer();

            // NG : 14/08/2020 : Apports en matière au réseau hyperdermique
            if (Simul->pset_caw->transport_module[HDERM_CAW] == YES) {
              LP_printf(Simul->poutputs, "TRANSPORT : Calculating transport fluxes from ZNS to hyperdermic zone\n");
              CAW_cFP_calc_matter_fluxes_cprod_coupled_hyperdermic_all(Simul->pcarac_fp_hderm, Simul->pcarac_zns, Simul->nb_species, CHR_convert_time(dt, S_CHR, DAYS_CHR), t_day, Simul->poutputs);
              LP_printf(Simul->poutputs, "Done\n");
              Simul->clock->time_spent[HDERM_TRANS_CHR] += CHR_end_timer();
              CHR_begin_timer();
            }

          } else if (Simul->pset_caw->sim_type == SW_HDERM_GW) // NG : 03/03/2020 : hydrosystème volcanique complet
          {
            if (Simul->pset_caw->transport_caw == NO) {
              LP_printf(Simul->poutputs, "Calculating ZNS water flow\n");
              CAW_cHYD_calc_runoff_catchment_chasm_all(Simul->pcarac_zns, Simul->pchyd, Simul->poutputs); // NG : 24/01/2020 : Calcul du champ pcatchment->Q_chasm pour les catchments de type chasm
              // NG : 27/06/2023 : Launching ZNS data update if dynamic behavior activated.
              if (Simul->pcarac_zns->dynzns_io == YES) {
                if (((int)Simul->chronos->t[CUR_CHR] % (int)Simul->pcarac_zns->dyn_refresh_dt) == 0) {
                  flag_dyn_nsat = YES;
                  CAW_cNSAT_update_dynamic_nsat(Simul->pcarac_zns, Simul->pcarac_aq, Simul->pchyd, t_day, Simul->pset_caw->transport_caw, Simul->nb_species, Simul->poutputs);
                }
              }
              NSAT_calculate_all_zns_coupled(Simul->pcarac_zns, CHR_convert_time(dt, S_CHR, DAYS_CHR), Simul->pcarac_fp, Simul->poutputs, flag_dyn_nsat, t_day);
              LP_printf(Simul->poutputs, "Done\n");
              Simul->clock->time_spent[NSAT_CHR] += CHR_end_timer();
              CHR_begin_timer();
            } else // NG : 31/07/2020 : Eau et transport simultanés NSAT
            {
              if (Simul->pset_caw->transport_module[NSAT_CAW] == YES) {
                LP_printf(Simul->poutputs, "TRANSPORT : Calculating ZNS coupled water and transport flows\n");
                CAW_cHYD_calc_runoff_catchment_chasm_all(Simul->pcarac_zns, Simul->pchyd, Simul->poutputs);
                // NG : 27/06/2023 : Launching ZNS data update if dynamic behavior activated.
                if (Simul->pcarac_zns->dynzns_io == YES) {
                  if (((int)Simul->chronos->t[CUR_CHR] % (int)Simul->pcarac_zns->dyn_refresh_dt) == 0) {
                    flag_dyn_nsat = YES;
                    CAW_cNSAT_update_dynamic_nsat(Simul->pcarac_zns, Simul->pcarac_aq, Simul->pchyd, t_day, Simul->pset_caw->transport_caw, Simul->nb_species, Simul->poutputs);
                  }
                }
                NSAT_calculate_all_zns_transport(Simul->nb_species, spe_types, Simul->pcarac_zns, CHR_convert_time(dt, S_CHR, DAYS_CHR), Simul->pcarac_fp, Simul->poutputs, flag_dyn_nsat, t_day);
                LP_printf(Simul->poutputs, "Done\n");
                Simul->clock->time_spent[NSAT_TRANS_CHR] += CHR_end_timer();
                CHR_begin_timer();
              }
            }

            LP_printf(Simul->poutputs, "Calculating flow from ZNS to hyperdermic zone\n");
            CAW_cFP_calc_watbal_cprod_coupled_hyperdermic_all(Simul->pcarac_fp_hderm, Simul->pcarac_zns, CHR_convert_time(dt, S_CHR, DAYS_CHR), t_day, Simul->poutputs);
            LP_printf(Simul->poutputs, "Done\n");
            Simul->clock->time_spent[HDERM_CHR] += CHR_end_timer();
            CHR_begin_timer();

            // NG : 14/08/2020 : Apports en matière au réseau hyperdermique
            if (Simul->pset_caw->transport_module[HDERM_CAW] == YES) {
              LP_printf(Simul->poutputs, "TRANSPORT : Calculating matter fluxes from ZNS to hyperdermic zone\n");
              CAW_cFP_calc_matter_fluxes_cprod_coupled_hyperdermic_all(Simul->pcarac_fp_hderm, Simul->pcarac_zns, Simul->nb_species, CHR_convert_time(dt, S_CHR, DAYS_CHR), t_day, Simul->poutputs);
              LP_printf(Simul->poutputs, "Done\n");
              Simul->clock->time_spent[HDERM_TRANS_CHR] += CHR_end_timer();
              CHR_begin_timer();
            }

            LP_printf(Simul->poutputs, "Calculating aquifer system recharge \n");
            CAW_cAQ_calculate_recharge_coupled(Simul->chronos->t[CUR_CHR], Simul->chronos->dt, NSAT_CAW, Simul->pcarac_aq, Simul->pcarac_zns, Simul->pchyd_hderm, Simul->poutputs);  // NG : 07/03/2020
            CAW_cAQ_calculate_recharge_coupled(Simul->chronos->t[CUR_CHR], Simul->chronos->dt, HDERM_CAW, Simul->pcarac_aq, Simul->pcarac_zns, Simul->pchyd_hderm, Simul->poutputs); // NG : 07/03/2020
            if (Simul->pcarac_aq->settings->cascade == YES)
              AQ_transfer_derivation_discharge(Simul->chronos->t[CUR_CHR], Simul->chronos->dt, Simul->pcarac_aq, CASCADE_AQ, CASCADE_DISCHARGE_AQ, Simul->poutputs); // NG : 19/04/2021
            if (Simul->pcarac_aq->settings->bypass == YES)
              AQ_transfer_derivation_discharge(Simul->chronos->t[CUR_CHR], Simul->chronos->dt, Simul->pcarac_aq, BYPASS_AQ, BYPASS_DISCHARGE_AQ, Simul->poutputs);

            LP_printf(Simul->poutputs, "Done\n");
            // AQ_print_elesource(Simul->pcarac_aq->pcmsh,Simul->poutputs);  // check NG
            AQ_fill_state(Simul->pcarac_aq, Simul->chronos, Simul->pout, INI_AQ, Simul->poutputs);
            HYD_refresh_qapp_coupled(Simul->pchyd_hderm, Simul->poutputs);
            Simul->clock->time_spent[AQ_HIN_CHR] += CHR_end_timer();
            CHR_begin_timer();
          }

          HYD_refresh_qapp_coupled(Simul->pchyd, Simul->poutputs);

        } else if (Simul->pset_caw->sim_type == GROUND_WAT) {
          // LP_printf(Simul->poutputs,"Inside aquifer transport.\n");

          AQ_fill_state(Simul->pcarac_aq, Simul->chronos, Simul->pout, INI_AQ, Simul->poutputs); // NF 29/11/2018
          if (Simul->pcarac_aq->settings->cascade == YES)
            AQ_transfer_derivation_discharge(Simul->chronos->t[CUR_CHR], Simul->chronos->dt, Simul->pcarac_aq, CASCADE_AQ, CASCADE_DISCHARGE_AQ, Simul->poutputs); // NG : 19/04/2021
          if (Simul->pcarac_aq->settings->bypass == YES)
            AQ_transfer_derivation_discharge(Simul->chronos->t[CUR_CHR], Simul->chronos->dt, Simul->pcarac_aq, BYPASS_AQ, BYPASS_DISCHARGE_AQ, Simul->poutputs);
        }

        nit = 0;
        pic_max = (double)CODE_TS;
        if (Simul->pset_caw->sim_type != GW_LAKE) {
          int nitmax = 1, nitmin = 1;
          double eps_pic;
          // Initialising Picard counters
          if (Simul->pcarac_aq != NULL) {
            nitmax = Simul->pcarac_aq->ppic->nitmax;
            nitmin = Simul->pcarac_aq->ppic->nitmin;
            eps_pic = Simul->pcarac_aq->ppic->eps_pic;
          } else
            eps_pic = (double)CODE_TS;

          // Iterative Picard loop
          if (nitmin > 1) { // CASE with CAUCHY BC, only for the GROUND_WATER and SW_GW type of simul
            while (nit < nitmin) {
              pic_max = CAW_solve_hydrosyst(Simul->pcarac_aq, Simul->pcarac_fp, Simul->pchyd, Simul->chronos, Simul->clock, Simul->pout, pmb, pmb2, Simul->pinout, Simul->outputs, Simul->pset_caw, &nit, &nday, Simul->poutputs, Simul->pcarac_fp_hderm, Simul->pchyd_hderm, Simul);
            }
            while ((nit < nitmax) && (fabs(pic_max) > eps_pic)) {
              pic_max = CAW_solve_hydrosyst(Simul->pcarac_aq, Simul->pcarac_fp, Simul->pchyd, Simul->chronos, Simul->clock, Simul->pout, pmb, pmb2, Simul->pinout, Simul->outputs, Simul->pset_caw, &nit, &nday, Simul->poutputs, Simul->pcarac_fp_hderm, Simul->pchyd_hderm, Simul);
            }
            if (nit == nitmax)
              LP_warning(Simul->poutputs, "Picard failed to converge after %d iterations. Error on piezometric head = %7.2f m\n", nit, pic_max);
          }    // End Picard loop
          else // No CAUCHY Boundaries
            pic_max = CAW_solve_hydrosyst(Simul->pcarac_aq, Simul->pcarac_fp, Simul->pchyd, Simul->chronos, Simul->clock, Simul->pout, pmb, pmb2, Simul->pinout, Simul->outputs, Simul->pset_caw, &nit, &nday, Simul->poutputs, Simul->pcarac_fp_hderm, Simul->pchyd_hderm, Simul);
        }

        if (Simul->pset_caw->sim_type == GW_LAKE) // SW 02/06/2016  function to refresh Cauchy for all lakes and refresh h_lake
        {

          if (Simul->pcarac_wet->fpmto != NULL) // Si fichier de forcage meteo demandé dans le fichier de commande
          {
            if (Simul->pcarac_wet->read_evap == YES)
              WET_read_evap_mto(Simul->pcarac_wet, nday, Simul->poutputs);
            else
              WET_calc_penman_transient_mto(Simul->pcarac_wet, nday, Simul->poutputs);
          }

          AQ_fill_state(Simul->pcarac_aq, Simul->chronos, Simul->pout, INI_AQ, Simul->poutputs);                                                                                       // TV 07/02/2020 Pour corriger les flux et error
          WET_solve_diffusiv(Simul->pcarac_aq, Simul->pcarac_wet, nday, Simul->pset_caw->calc_state, Simul->chronos, Simul->pout[AQ_CAW], Simul->clock, Simul->pout, Simul->poutputs); // SW 05/07/2016

          AQ_fill_state(Simul->pcarac_aq, Simul->chronos, Simul->pout, END_AQ, Simul->poutputs);
          AQ_calc_mb_end(Simul->pcarac_aq, Simul->chronos, Simul->pout, Simul->poutputs);
        }

        if (Simul->pset_caw->sim_type == SW_GW) {
          AQ_refresh_state_variable(Simul->pcarac_aq, Simul->chronos, Simul->clock, Simul->pout, Simul->poutputs);
          HYD_extract_sol_t(Simul->pchyd, Simul->clock, Simul->chronos, Simul->outputs, Simul->pinout, Simul->pout, Simul->poutputs);
          CAW_cAQ_refresh_cauchy(Simul->chronos, Simul->pchyd, Simul->pcarac_aq, Simul->poutputs); // FB 12/03/18
        }
        if ((Simul->pset_caw->transport_caw == YES) && (Simul->pset_caw->transport_module[AQ_CAW] == YES)) {
          LP_printf(Simul->poutputs, "TRANSPORT : Calculating transport in aquifer system \n");
          CAW_solve_TTC_all_species(Simul, AQ_CAW, Simul->chronos->dt, Simul->chronos->t[CUR_CHR], &nday, Simul->poutputs);
        }

        if ((Simul->pset_caw->transport_caw == YES) && (Simul->pset_caw->transport_module[HYD_CAW] == YES)) {
          LP_printf(Simul->poutputs, "TRANSPORT : Calculating transport in river system \n");
          CAW_solve_TTC_all_species(Simul, HYD_CAW, Simul->chronos->dt, Simul->chronos->t[CUR_CHR], &nday, Simul->poutputs);
        }

        if (Simul->pset_caw->sim_type == GROUND_WAT) {
          AQ_refresh_state_variable(Simul->pcarac_aq, Simul->chronos, Simul->clock, Simul->pout, Simul->poutputs);
        }

        if (Simul->pset_caw->sim_type == SURF_WAT || Simul->pset_caw->sim_type == SURF_WAT_NSAT) {
          CHR_begin_timer();
          HYD_extract_sol_t(Simul->pchyd, Simul->clock, Simul->chronos, Simul->outputs, Simul->pinout, Simul->pout, Simul->poutputs);
          Simul->clock->time_spent[OUTPUTS_CHR] += CHR_end_timer(); // LV 3/09/2012
        }

        if (Simul->pset_caw->sim_type == SURF_WAT_NSAT_HDERM) {
          CHR_begin_timer();
          HYD_extract_sol_t(Simul->pchyd, Simul->clock, Simul->chronos, Simul->outputs, Simul->pinout, Simul->pout, Simul->poutputs);
          HYD_extract_sol_t_hyperdermic(Simul->pchyd_hderm, Simul->clock, Simul->chronos, Simul->outputs, Simul->pinout, Simul->pout, Simul->poutputs);
          Simul->clock->time_spent[OUTPUTS_CHR] += CHR_end_timer();
        }

        if (Simul->pset_caw->sim_type == SW_HDERM_GW) {
          AQ_refresh_state_variable(Simul->pcarac_aq, Simul->chronos, Simul->clock, Simul->pout, Simul->poutputs);
          CHR_begin_timer();
          HYD_extract_sol_t(Simul->pchyd, Simul->clock, Simul->chronos, Simul->outputs, Simul->pinout, Simul->pout, Simul->poutputs);
          HYD_extract_sol_t_hyperdermic(Simul->pchyd_hderm, Simul->clock, Simul->chronos, Simul->outputs, Simul->pinout, Simul->pout, Simul->poutputs);
          CAW_cAQ_refresh_cauchy(Simul->chronos, Simul->pchyd, Simul->pcarac_aq, Simul->poutputs); // FB 12/03/18
          Simul->clock->time_spent[OUTPUTS_CHR] += CHR_end_timer();
        }

        CHR_begin_timer();

        if (test_out == floor(test_out)) {
          CAW_print_outputs(t_day, Simul);
        }
        Simul->clock->time_spent[OUTPUTS_CHR] += CHR_end_timer(); // LV 3/09/2012
        //  }	//endif ((t_day>=Simul->chronos->t[BEGINNING])&&(t_day<Simul->chronos->t[END]) //NF 26/11/2020
      } // End for day

      CAW_close_files(Simul->pout, Simul->chronos);
      LP_printf(Simul->poutputs, "Done\n");
    }
  }

  // #################
  //   STEADY STATE
  // #################
  else {                                                     // STEADY STATE if (Simul->pset_caw->calc_state == STEADY)
    LP_printf(Simul->poutputs, "Inside steady solution \n"); // There is no soluton of the transport in the steady case yet. it has to be added DK 11 09 2021
    if (Simul->pset_caw->sim_type == SURF_WAT) {
      if (Simul->pchyd->settings->schem_type != MUSKINGUM) {
        HYD_steady_state_coupled(dt, pmb, pmb2, Simul->pinout->fmb, Simul->pinout, Simul->outputs, Simul->pchyd, Simul->clock, Simul->chronos, Simul->pcarac_fp, Simul->pcarac_aq, Simul->pset_caw->sim_type, Simul->poutputs); // hydraulique_permanent(dt_calc) in ProSe

        CHR_begin_timer();
        HYD_print_outputs_old(Simul->chronos->t[CUR_CHR], Simul->outputs, Simul->pinout, Simul->pchyd, Simul->chronos, Simul->poutputs);

        Simul->clock->time_spent[OUTPUTS_CHR] += CHR_end_timer();
      } else
        LP_error(Simul->poutputs, "No steady state for %s scheme \n check input files", CAW_sim_name(Simul->pset_caw->sim_type, Simul->poutputs));
    }
    if ((Simul->pset_caw->sim_type == GROUND_WAT) || (Simul->pset_caw->sim_type == GW_LAKE)) {
      CAW_init_files(Simul->pout, Simul->pset_caw, Simul->pcarac_fp, Simul->pcarac_zns, Simul->pchyd, Simul->pcarac_aq, Simul->pcarac_fp_hderm, Simul->pchyd_hderm, Simul->chronos, Simul->nb_species, Simul->pspecies, spe_types, Simul->poutputs);
      CHR_refresh_t(Simul->chronos);
      // #####
      //  GW
      // #####
      if (Simul->pset_caw->sim_type == GROUND_WAT) {
        AQ_solve_diffusiv_steady(Simul->pcarac_aq, Simul->chronos, Simul->clock, Simul->pout, Simul->pset_caw->idebug, Simul->poutputs);

      } else {
        // ##########
        //  GW_LAKE
        // ##########
        pic_max = (double)-CODE_TS;
        while (fabs(pic_max) > Simul->pcarac_wet->eps_wet) {
          pic_max = WET_solve_diffusiv_pic(Simul->pcarac_aq, Simul->pcarac_wet, nday, Simul->pset_caw->calc_state, Simul->chronos, Simul->pout[AQ_CAW], Simul->clock, Simul->poutputs);
        }

        AQ_fill_state(Simul->pcarac_aq, Simul->chronos, Simul->pout, END_AQ, Simul->poutputs);
        AQ_calc_mb_end(Simul->pcarac_aq, Simul->chronos, Simul->pout, Simul->poutputs);

        WET_get_leakage_all(Simul->pcarac_wet, Simul->pset_caw->calc_state, Simul->chronos->dt, Simul->poutputs);
        // WET_write_mb_wet(Simul->chronos->dt, t_day, Simul->pout[MBWET_IO], Simul->pcarac_wet, Simul->poutputs); // NG : 05/11/2024. Useless as it is integrated in CAW_print_outputs() now
      }
      CAW_print_outputs(t_day, Simul);
      CAW_close_files(Simul->pout, Simul->chronos);
    }
    if (Simul->pset_caw->transport_module[AQ_CAW] == YES) {
      LP_printf(Simul->poutputs, "TRANSPORT : Calculating transport in aquifer system\n");
      CAW_solve_TTC_all_species(Simul, AQ_CAW, dt, 0, &nday, Simul->poutputs);

      LP_printf(Simul->poutputs, "Done\n");
    }
    LP_printf(Simul->poutputs, "End steady state \n");
  } // END STEADY STATE

  CHR_begin_timer(); // LV 3/09/2012
  CAW_print_final_state(Simul->pchyd, Simul->chronos, Simul->pout, Simul->pcarac_fp, Simul->pcarac_zns, Simul->pcarac_aq, Simul->pchyd_hderm, Simul->poutputs);

  Simul->clock->time_spent[OUTPUTS_CHR] += CHR_end_timer(); // LV 3/09/2012

  t_day = (double)TS_calculate_jour_julien_j_nbday(Simul->chronos->year[CUR_CHR], CODE_FP, 0);
  date_curent = TS_convert_julian2date(t_day, CODE_CAW, Simul->poutputs);
  LP_printf(Simul->poutputs, "Date of end of simulation : %d %s %d\n", date_curent->dd, TS_name_month(date_curent->mm, Simul->poutputs), date_curent->yyyy);

  /* ****************************************************** */
  /*           End, summary of computation length           */
  /* ****************************************************** */

  CHR_calculate_calculation_length(Simul->clock, Simul->poutputs);
  printf("Time of calculation : %f s\n", Simul->clock->time_length);

  CHR_print_times(Simul->poutputs, Simul->clock);
  GC_clean_gc();
  fclose(Simul->poutputs);

  return 0;
}
