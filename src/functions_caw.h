/*-------------------------------------------------------------------------------
 *
 * SOFTWARE NAME: cawaqs
 * FILE NAME: functions_caw.h
 *
 * CONTRIBUTORS: Nicolas GALLOIS, Baptiste LABARTHE, Shuaitao WANG,
 *               Agnès RIVIERE, Deniz KILIC, Mathias MAILLOT,
 *               Fulvia BARATELLI, Pierre GUILLOU, Emmanuel LEDOUX, Nicolas FLIPO
 *
 * SOFTWARE BRIEF DESCRIPTION: Physically based coupled surface–subsurface
 * model for joint water, matter and energy flux balances and flow dynamics
 * simulation within all compartments of a hydrosystem (sub-surface, hydraulic
 * network, vadose zone, aquifer system and stream-aquifer exchanges).
 *
 * Software developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the cawaqs Software.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Software and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

/* in manage_simulation.h */
s_simul_cawaqs *CAW_init_simul();
void CAW_print_transport_activation_status(s_settings_cawaqs *, FILE *); // NG : 18/08/2020

/* in manage_output.c */
void CAW_print_outputs(double, s_simul_cawaqs *);
void CAW_close_files(s_out_io **, s_chronos_CHR *);
void CAW_init_files(s_out_io **, s_settings_cawaqs *, s_carac_fp *, s_carac_zns *, s_chyd *, s_carac_aq *, s_carac_fp *, s_chyd *, s_chronos_CHR *, int, s_species_cawaqs **, int *, FILE *);
void CAW_init_bin(s_carac_fp *, s_carac_zns *, s_chyd *, s_carac_aq *, s_carac_fp *, s_chyd *, int, s_out_io **, int, s_species_cawaqs **, FILE *);
void CAW_init_for(s_out_io **, int, int, int *, s_species_cawaqs **, FILE *);
void CAW_print_characteristics(s_settings_cawaqs *, s_carac_aq *, s_carac_fp *, s_carac_zns *, s_chyd *, s_carac_fp *, s_chyd *, FILE *);
void CAW_link_mb(s_out_io **, s_chronos_CHR *, FILE *);
void CAW_print_final_state(s_chyd *, s_chronos_CHR *, s_out_io **, s_carac_fp *, s_carac_zns *, s_carac_aq *, s_chyd *, FILE *);
void CAW_check_compatibility_output_hydro_module(s_simul_cawaqs *);
void CAW_print_corresp_files_hyperdermic(s_carac_fp *, s_chyd *, int, FILE *); // NG : 21/02/2020
void CAW_check_compatibility_outputs_transport(s_simul_cawaqs *);              // NG : 31/07/2020

/* in manage_link_output.c  */ // NG : 09/10/2021
void CAW_print_outputs_transport_riv_balance(s_chronos_CHR *, int, double, s_chyd *, s_out_io *, int, s_species_cawaqs **, s_carac_ttc *, FILE *);
void CAW_print_outputs_transport_riv_variable(s_chronos_CHR *, int, double, s_chyd *, s_out_io *, int, s_species_cawaqs **, s_carac_ttc *, FILE *);
void CAW_print_outputs_transport_aq_balance(s_chronos_CHR *, int, double, s_carac_aq *, s_out_io *, int, s_species_cawaqs **, FILE *);
void CAW_print_outputs_transport_aq_variable(s_chronos_CHR *, int, double, s_carac_aq *, s_out_io *, int, s_species_cawaqs **, FILE *);
void CAW_print_transport_header(int, int, s_species_cawaqs **, FILE *);
void CAW_print_transport_riv_mb_formatted(int, s_chyd *, s_carac_ttc *, double, s_out_io *, double, FILE *);
void CAW_print_transport_riv_var_formatted(int, s_chyd *, double, s_out_io *, FILE *);
void CAW_print_transport_riv_mb_bin(int, s_chyd *, s_carac_ttc *, s_out_io *, double, FILE *);
void CAW_print_transport_riv_var_bin(int, s_chyd *, s_carac_ttc *, s_out_io *, double, FILE *);
void CAW_print_budget_riv_ele_transport(int, s_element_hyd *, s_species_ttc **, double, s_out_io *, FILE *);
double **CAW_get_mb_val_bin_riv_transport(int, s_out_io *, s_chyd *, s_species_ttc **, int, int, double, FILE *);
double **CAW_get_var_val_bin_riv_transport(int, s_out_io *, s_chyd *, s_species_ttc **, int, int, double, FILE *);
double *CAW_river_budget_transport(int, s_element_hyd *, s_species_ttc **, FILE *);
void CAW_print_transport_aq_mb_formatted(int, s_carac_aq *, double, s_out_io *, double, FILE *);
void CAW_print_transport_aq_var_formatted(int, s_carac_aq *, double, s_out_io *, double, FILE *);
void CAW_print_budget_aq_ele_transport(int, s_ele_msh *, double, s_out_io *, FILE *);
double *CAW_aquifer_budget_transport(int, s_ele_msh *, FILE *);
void CAW_print_transport_aq_mb_bin(int, s_carac_aq *, s_out_io *, double, FILE *);
void CAW_print_transport_aq_var_bin(int, s_carac_aq *, s_out_io *, double, FILE *);
double **CAW_get_mb_val_bin_aq_transport(int, s_out_io *, s_carac_aq *, int, int, double, FILE *);
void CAW_print_var_aq_ele_transport(int, s_ele_msh *, double, s_out_io *, FILE *);
double **CAW_get_var_bin_aq_transport(int, s_out_io *, s_carac_aq *, int, int, double, FILE *);

/* in manage_pic.c */
double CAW_solve_hydrosyst(s_carac_aq *, s_carac_fp *, s_chyd *, s_chronos_CHR *, s_clock_CHR *, s_out_io **, s_mb_hyd *, s_mb_hyd *, s_inout_set_io *, s_output_hyd ***, s_settings_cawaqs *, int *, int *, FILE *, s_carac_fp *, s_chyd *, s_simul_cawaqs *);

/* in manage_inputs.c */
void CAW_check_inputs(s_carac_aq *, s_carac_fp *, s_carac_zns *, s_chyd *, s_settings_cawaqs *, s_carac_wet *, s_chronos_CHR *, s_carac_fp *, s_chyd *, FILE *);
void CAW_check_infilt(s_carac_fp *, s_carac_zns *, FILE *);
void CAW_check_sout(s_carac_aq *, s_carac_zns *, FILE *);
void CAW_check_sim_type(s_carac_aq *, s_carac_fp *, s_carac_zns *, s_chyd *, s_settings_cawaqs *, s_carac_wet *, s_carac_fp *, s_chyd *, FILE *);
void CAW_check_surf_aq(s_carac_aq *, s_carac_fp *, FILE *);
void CAW_init_from_file(char **, char *, int, int, s_carac_aq *, s_carac_fp *, s_carac_zns *, s_chyd *, s_out_io **, s_chronos_CHR *, FILE *);

/* in manage_pc.c */
s_smp *CAW_set_OMP_param(int);

/* in manage_species.c */
double ***CAW_init_chem_param(FILE *);
double ***CAW_manage_memory_pchem_param(double ***, int, FILE *);
s_species_cawaqs *CAW_init_species(int);
s_species_cawaqs **CAW_manage_memory_species(s_species_cawaqs **, int, FILE *);
void CAW_display_species_properties(s_simul_cawaqs *, FILE *);
int *CAW_fetch_specie_types(s_simul_cawaqs *, FILE *);

/* in itos.c */
char *CAW_sim_name(int, FILE *);
char *CAW_sim_outputs(int);
char *CAW_name_mod(int);
char *CAW_species_type(int);      // NG : 01/08/2020
char *CAW_specie_chem_param(int); // NG : 12/08/2020
char *CAW_species_variable(int);  // NG : 09/10/2021

/* in HYD_hydraulics_coupled.c */
s_reach_hyd *HYD_create_reaches_musk_coupled(s_lec_tmp_hyd **, s_chyd *, int, char **, s_carac_fp *, FILE *);
void HYD_finalyse_link_reach_coupled(int, int, double, int, s_chyd *, FILE *);

/* in manage_link_master.c */ // NG : 08/2020
void CAW_initialize_TTC_carac_species(s_simul_cawaqs *, int, int, int, int, FILE *);
void CAW_initialize_TTC_all(s_simul_cawaqs *, int, int, FILE *);
void CAW_fill_TTC_param_base_all_species(s_simul_cawaqs *, int, int, FILE *);
void CAW_fill_TTC_param_base_one_species(s_simul_cawaqs *, int, int, int, FILE *);
void CAW_alloc_TTC_u_dist_deltaL_all_species(s_simul_cawaqs *psim, int modname, FILE *flog);
void CAW_alloc_TTC_u_dist_deltaL_one_species(s_simul_cawaqs *psim, s_species_ttc *pspecies, s_link_ttc *plink, int nele, int modname, int id_species, FILE *flog);
void CAW_fill_TTC_neigh_all_species(s_simul_cawaqs *, int, int, FILE *);
void CAW_init_neighbor_one_species(s_link_ttc *, int, int, FILE *);
void CAW_fill_neighbor_one_species(s_simul_cawaqs *, s_link_ttc *, s_species_ttc *, int, int, int, FILE *);
void CAW_print_neighbor_attributes_one_specie(s_link_ttc *, int, int, FILE *);
void CAW_solve_TTC_all_species(s_simul_cawaqs *, int, double, double, int *, FILE *);
void CAW_fill_var_one_species(s_simul_cawaqs *, s_species_ttc *, int, int, FILE *);
void CAW_build_TTC_var_all_species(s_simul_cawaqs *, int, FILE *);
void CAW_fill_u_one_species(s_simul_cawaqs *, s_link_ttc *, int, int, FILE *);
void CAW_fill_inflows_one_species(s_simul_cawaqs *, double, s_species_ttc *, int, int, double, FILE *);
void CAW_fill_wetsurf_one_specie(s_simul_cawaqs *, s_link_ttc *, int, int, FILE *);
void CAW_transient_solve_ttc(s_carac_ttc *, s_param_calc_ttc **, s_species_ttc *, int, double, double, int *, FILE *);
void CAW_transient_solve_libaq_ttc(s_carac_ttc *, s_param_calc_ttc **, s_species_ttc *, int, double, double, int *, FILE *); // DK AR 06 09 2021 correction of path after refactor of libttc
void CAW_update_variable_one_specie(s_simul_cawaqs *, s_species_ttc *, int, int, FILE *);                                    // NG : 09/10/2021 : Now updates either C or T depending on the specie type
void CAW_print_timestep_transport_variable(s_simul_cawaqs *, int, int, FILE *);
void CAW_steady_solve_TTC(s_carac_ttc *, s_param_calc_ttc **, s_species_ttc *, FILE *);

/* in manage_link_aquifer.c */
void CAW_build_TTC_param_syst_aq(s_cmsh *, s_species_ttc *, int, FILE *);    // DK AR 10 09 2021 fill system parameters (dispersivity, porosity)
void CAW_build_TTC_param_therm(s_cmsh *, s_species_ttc *, int, int, FILE *); // DK AR 10 09 2021 fill system parameters (dispersivity, porosity)
void CAW_fill_dist_one_species_aq(s_cmsh *, s_species_ttc *, FILE *);        // DK AR 10 09 2021 fill size of each mesh element
void CAW_build_deltaL_aq(s_cmsh *, s_species_ttc *, s_link_ttc *, FILE *);   // DK AR 10 09 2021 fill the distance between each neighbouring mesh element
void CAW_fill_neighbor_one_species_aq(s_cmsh *, s_link_ttc *, FILE *);       // DK AR 07 09 2021 AQ function
void CAW_fill_nsub_one_species_aq(s_cmsh *, s_link_ttc *, FILE *);           // DK AR 10 09 2021 fill number of subfaces for each face of mesh element
void CAW_fill_var_one_species_aq(s_cmsh *, s_species_ttc *, FILE *);
void CAW_build_var_one_species_aq(s_cmsh *, s_species_ttc *, FILE *); // DK AR 13 10 2021 this function is used to pass the initial value read from input.y to pspecies
void CAW_init_TTC_aquitard(s_ele_msh *, int, int, int, double, FILE *);
void CAW_build_TTC_param_aquitard_aq(s_cmsh *, s_species_ttc *, int, FILE *);
void CAW_build_TTC_var_aquitard_aq(s_cmsh *, s_species_ttc *, FILE *);
void CAW_build_TTC_temp_aquitard_aq(s_cmsh *, s_species_ttc *, FILE *);
void CAW_init_TTC_aquitard_cells(s_carac_ttc *, FILE *);
void CAW_init_TTC_define_bound(s_cmsh *, s_id_io *, s_ft *, int, int, double, double, double, double, FILE *);
s_ele_msh *CAW_init_TTC_boundaries(s_ele_msh *, int, s_boundary_ttc *, s_id_io **, FILE *);
void CAW_init_TTC_source_cells(s_cmsh *, s_id_io *, s_ft *, int, double, FILE *);
void CAW_add_TTC_source_to_element(s_ele_msh *, int, s_source_ttc *, FILE *);
void CAW_print_TTC_elesource(s_cmsh *, FILE *);
void CAW_build_TTC_source_imposed(s_cmsh *, s_species_ttc *, FILE *);
void CAW_calc_TTC_b_i_source(s_ele_msh *, double, double, double, int, FILE *);                         // Function to fill RHS with source term of transport.
void CAW_build_TTC_face_source_transient(s_cmsh *, s_link_ttc *, s_species_ttc *pspecies, int, FILE *); // Function to fill RHS with source term of transport.
void CAW_init_TTC_riverbed(s_face_hyd *, int, int, int, double, FILE *);
void CAW_init_TTC_riverbed_cells(s_carac_ttc *, FILE *);
void CAW_build_TTC_param_riverbed_aq(s_chyd *, s_species_ttc *, int, int, FILE *);
void CAW_build_TTC_var_riverbed_aq(s_cmsh *, s_chyd *, s_species_ttc *, int, FILE *);
void CAW_build_TTC_neighbours_riverbed_aq(s_chyd *, s_species_ttc *, int, int, FILE *);
void CAW_build_TTC_temp_riverbed_aq(s_cmsh *, s_chyd *, s_species_ttc *, int, FILE *);
void CAW_update_TTC_velocity_riverbed_aq(s_chyd *, s_species_ttc *, int, FILE *);
void CAW_cAQ_TTC_calculate_transport_fluxes(double, double, int, int, s_carac_aq *, s_carac_zns *, s_chyd *, s_carac_ttc *, FILE *); // Function to link and pass matter fluxes from NSAT to Aquifer DK AR 15022022 // Rewritten NG 12/04/2023
void CAW_cAQ_TTC_river_impact_on_aq(double, double, int, s_carac_aq *, s_chyd *, s_carac_ttc *, FILE *);
void CAW_update_variable_aquitard_one_specie_aq(s_species_ttc *, s_carac_aq *, s_carac_ttc *, int, FILE *);
void CAW_update_velocities_TTC(s_cmsh *, s_carac_ttc *, int, FILE *);
void CAW_update_velocity_directions_TTC(s_cmsh *pcmsh, s_carac_ttc *, int, FILE *);

// functionalities in input.y for the transport problem
void CAW_init_TTC_ele_NPARAM_SYST(s_ele_msh *, int, double, FILE *);            // DK AR 10 09 2021 pass system parameter from input file to ele_mesh structure
void CAW_init_TTC_ele_INIT(s_layer_msh *, int, double, FILE *);                 // DK AR 10 09 2021 pass initial condition variable from input file to ele_mesh structure
void CAW_init_TTC_ele_NPARAM_THERM(s_ele_msh *, int, int, int, double, FILE *); // DK AR 10 09 2021 pass thermal parameters variable from input file to ele_mesh structure
void CAW_init_TTC_ele_nbound(s_cmsh *, FILE *);                                 // DK AR 10 09 2021 pass initial condition variable from input file to ele_mesh structure
int CAW_init_TTC_calc_nbc(s_cmsh *, FILE *);                                    // DK AR 10 09 2021 pass number of boundary conditions variable from input file to ele_mesh structure
void CAW_init_TTC_face_bound(s_cmsh *, FILE *);                                 // DK AR 10 09 2021 initialise the type and value of the BC variable from input file to ele_mesh structure
void CAW_init_TTC_face_bound_transient(s_layer_msh *, int, int, int, int, double, double);
void CAW_build_TTC_face_bound_transient(s_cmsh *, s_link_ttc *, s_species_ttc *, int, FILE *);
int CAW_TTC_type_by_card_aq(int);                                // DK AR 10 09 2021 Returns the type of the card from the card
int CAW_TTC_dir_by_card_aq(int);                                 // DK AR 10 09 2021 Returns the dir of the card from the card
int CAW_TTC_card_aq(int, int);                                   // DK AR 10 09 2021 Returns the card value
void CAW_build_TTC_tab_neigh_aq(s_cmsh *, s_link_ttc *, FILE *); // DK AR 10 09 2021 fill neighbours table for aquifers
int CAW_calc_TTC_nvois_ele_aq(s_cmsh *, FILE *);                 // DK AR 10 09 2021 calculate the number of neighbours of each element
void CAW_update_variable_one_specie_aq(s_species_ttc *, s_carac_aq *, s_carac_ttc *, int, FILE *);

/* in manage_link_river.c */
void CAW_alloc_TTC_u_dist_one_species_riv(s_carac_ttc *, s_chyd *, int, int, FILE *);
void CAW_init_TTC_val_one_species_riv(s_carac_ttc *, s_chyd *, int, int, FILE *); // DK AR reading init file for the river 18 11 2021
void CAW_fill_neighbor_one_species_riv(s_link_ttc *, s_chyd *, FILE *);
void CAW_fill_nsub_one_species_riv(s_link_ttc *, int, s_chyd *, FILE *);
void CAW_fill_var_one_species_riv(s_species_ttc *, s_chyd *, int, FILE *);
void CAW_build_var_one_species_riv(s_species_ttc *, s_chyd *, int, FILE *);
void CAW_fill_Q_one_species_riv(s_carac_ttc *, s_chyd *, int, int, FILE *);
void CAW_fill_inflows_one_species_riv_aq(s_species_ttc *, double, s_chyd *, s_carac_fp *, s_carac_aq *, int, double, FILE *);
void CAW_fill_inflows_one_species_riv(s_species_ttc *, double, s_chyd *, s_carac_fp *, int, double, FILE *);
void CAW_fill_wetsurf_one_specie_riv(s_link_ttc *, s_chyd *, int, FILE *);
void CAW_update_variable_one_specie_riv(s_species_ttc *, s_chyd *, int, FILE *);
void CAW_cFP_get_fp_fluxes_to_riv(s_species_ttc *, s_chyd *, s_carac_fp *, int, double, double, FILE *); // NG : 19/04/2023
void CAW_cSEB_get_seb_fluxes_to_riv(s_species_ttc *, s_chyd *, FILE *);                                  // NG : 19/04/2023
void CAW_cAQ_get_rivaq_fluxes_to_riv(s_species_ttc *, s_chyd *, s_cmsh *, FILE *);                       // NG : 19/04/2023
void CAW_cFP_calc_overflows_cprod_sout_all(s_carac_fp *, s_carac_aq *, double, int, FILE *);             // NG : 12/06/2023
void CAW_cFP_calc_overflows_cprod_layer(s_layer_msh *, s_carac_fp *, double, double, int, FILE *);       // NG : 12/06/2023

/* in manage_meteo.c */
void CAW_Patm_reading_in_inputy(int, FILE *, long, long, int, s_rts **, s_met **, FILE *);
void CAW_safransreading_in_inputy(int, FILE *, long, long, int, double, double, s_rts **, s_met **, FILE *);
int *CAW_link_icell_to_imet_SEB(int, int, int, s_rts **, s_reach_hyd **, s_met **);
void CAW_alloc_inputs_meteo_all(int, s_carac_seb **, int);
void CAW_update_meteo_for_HT(s_species_ttc *, long, long, double, s_chyd *, s_carac_seb **, s_met **, int *, double *);