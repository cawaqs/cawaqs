/*-------------------------------------------------------------------------------
 *
 * SOFTWARE NAME: cawaqs
 * FILE NAME: manage_simulation.c
 *
 * CONTRIBUTORS: Nicolas GALLOIS, Baptiste LABARTHE, Shuaitao WANG,
 *               Agnès RIVIERE, Deniz KILIC, Mathias MAILLOT,
 *               Fulvia BARATELLI, Pierre GUILLOU, Emmanuel LEDOUX, Nicolas FLIPO
 *
 * SOFTWARE BRIEF DESCRIPTION: Physically based coupled surface–subsurface
 * model for joint water, matter and energy flux balances and flow dynamics
 * simulation within all compartments of a hydrosystem (sub-surface, hydraulic
 * network, vadose zone, aquifer system and stream-aquifer exchanges).
 *
 * Software developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the cawaqs Software.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Software and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include "libprint.h"
#include "time_series.h"
#include "libpc.h"
#include "IO.h"
#include "reservoir.h"
#include "spa.h"
#include "CHR.h"
#include "FP.h"
#include "NSAT.h"
#include "GC.h"
#include "HYD.h"
#include "MSH.h"
#include "AQ.h"
#include "WET.h"
#include "TTC_coupled.h"
#include "SEB.h"
#include "CAW.h"

/**\fn s_simul_aq *CAW_init_simul()
 *\brief initialize the simulation structure
 *\return psimul
 */
s_simul_cawaqs *CAW_init_simul() {
  /* Structure containing all global variables */
  s_simul_cawaqs *psimul;
  int i;

  psimul = new_simul_cawaqs();
  bzero((char *)psimul, sizeof(s_simul_cawaqs));
  bzero((char *)psimul->general_param, sizeof(NPAR_AQ * sizeof(double))); // NF 4/6/2017

  psimul->pset_caw = new_settings_cawaqs();
  bzero((char *)psimul->pset_caw, sizeof(s_settings_cawaqs));
  psimul->pset_caw->check_mesh = NO;
  psimul->pset_caw->print_surf = NO;
  psimul->pset_caw->idebug = NO_TS;
  psimul->pset_caw->picnitmax = PICNITMAX_AQ;
  psimul->pset_caw->steadynitmax = STEADYNITMAX_AQ;
  psimul->pset_caw->eps_pic = EPS_TS;

  psimul->chronos = CHR_init_chronos();
  psimul->pchyd = NULL; // NG : 20/02/2020
  psimul->pchyd_hderm = NULL;

  // NF 4/6/2017 Mauvaise procedure d'initialisation, les pcarac ne doivent etre alloues qu'en cas de lecture des champs ad hoc dans le fichiers de commande!
  psimul->pout = (s_out_io **)calloc(NMOD_IO, sizeof(s_out_io *));
  // psimul->param=FP_create_sim_param();

  psimul->clock = CHR_new_clock();
  bzero((char *)psimul->clock, sizeof(s_clock_CHR));
  psimul->clock->begin = time(NULL);

  psimul->plec = (s_lec_tmp_hyd **)malloc(NB_LEC_MAX * sizeof(s_lec_tmp_hyd *));
  for (i = 0; i < NB_LEC_MAX; i++) {
    psimul->plec[i] = NULL;
  }

  psimul->plec_hderm = (s_lec_tmp_hyd **)malloc(NB_LEC_MAX * sizeof(s_lec_tmp_hyd *));
  for (i = 0; i < NB_LEC_MAX; i++) {
    psimul->plec_hderm[i] = NULL;
  }

  /** Transport-related attributes **/
  psimul->nb_species = 0;
  psimul->chem_param = NULL;
  psimul->pspecies = NULL;
  psimul->pset_caw->transport_caw = NO;

  psimul->pset_caw->transport_module = (int *)malloc(NMOD_CAW * sizeof(int));
  for (i = 0; i < NMOD_CAW; i++)
    psimul->pset_caw->transport_module[i] = NO;

  psimul->pcarac_ttc = (s_carac_ttc **)malloc(NMOD_CAW * sizeof(s_carac_ttc *));
  for (i = 0; i < NMOD_CAW; i++)
    psimul->pcarac_ttc[i] = NULL;

  return psimul;
}

/**\fn void CAW_print_transport_activation_status(s_settings_cawaqs*, FILE*)
 *\brief Displays the current CaWaQS general transport activation status as well as status for each module
 *\return -
 */
void CAW_print_transport_activation_status(s_settings_cawaqs *pset_caw, FILE *fpout) {
  int i;
  LP_printf(fpout, "\nCaWaQS transport activation status : %s\n", LP_answer(pset_caw->transport_caw, fpout));
  for (i = 0; i < NMOD_CAW; i++)
    LP_printf(fpout, "Module %s : Transport initialization status : %s\n", CAW_name_mod(i), LP_answer(pset_caw->transport_module[i], fpout));
  LP_printf(fpout, "\n");
}