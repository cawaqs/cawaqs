/*-------------------------------------------------------------------------------
 *
 * SOFTWARE NAME: cawaqs
 * FILE NAME: manage_output.c
 *
 * CONTRIBUTORS: Nicolas GALLOIS, Baptiste LABARTHE, Shuaitao WANG,
 *               Agnès RIVIERE, Deniz KILIC, Mathias MAILLOT,
 *               Fulvia BARATELLI, Pierre GUILLOU, Emmanuel LEDOUX, Nicolas FLIPO
 *
 * SOFTWARE BRIEF DESCRIPTION: Physically based coupled surface–subsurface
 * model for joint water, matter and energy flux balances and flow dynamics
 * simulation within all compartments of a hydrosystem (sub-surface, hydraulic
 * network, vadose zone, aquifer system and stream-aquifer exchanges).
 *
 * Software developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the cawaqs Software.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Software and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include "libprint.h"
#include "time_series.h"
#include "libpc.h"
#include "IO.h"
#include "reservoir.h"
#include "spa.h"
#include "CHR.h"
#include "FP.h"
#include "NSAT.h"
#include "GC.h"
#include "HYD.h"
#include "MSH.h"
#include "AQ.h"
#include "WET.h"
#include "TTC_coupled.h"
#include "SEB.h"
#include "CAW.h"

/**
 * @brief
 * @return
 */
void CAW_print_outputs(double t_day, s_simul_cawaqs *psimul) {

  int i;
  double dt, t;
  char *name;

  s_output_hyd ***outputs_hyd = psimul->outputs;
  s_inout_set_io *pinout = psimul->pinout;
  s_chyd *pchyd = psimul->pchyd;
  s_chronos_CHR *chronos = psimul->chronos;
  s_out_io **pout = psimul->pout;
  s_carac_fp *pcarac_fp = psimul->pcarac_fp;
  s_carac_zns *pcarac_zns = psimul->pcarac_zns;
  s_carac_aq *pcarac_aq = psimul->pcarac_aq;
  s_chyd *pchyd_hderm = psimul->pchyd_hderm;
  s_carac_wet *pcarac_wet = psimul->pcarac_wet;
  s_species_cawaqs **p_species = psimul->pspecies;
  s_carac_ttc **pcarac_ttc = psimul->pcarac_ttc;

  int sim_type = psimul->pset_caw->sim_type;
  int nb_species = psimul->nb_species;
  FILE *fp = psimul->poutputs;

  t = chronos->t[CUR_CHR];
  dt = chronos->dt;

  if ((pout[HYD_H_IO] == NULL && pout[MBHYD_IO] == NULL) && pout[HYD_Q_IO] == NULL) {
    if (sim_type != GROUND_WAT && sim_type != GW_LAKE) { // 02/05/2016 SW add lake simul type
      LP_printf(fp, " print hyd old version \n");
      HYD_print_outputs_old(t, outputs_hyd, pinout, pchyd, chronos, fp); // LV test 26/07/2012
    }
  }

  for (i = 0; i < NMOD_IO; i++) {
    if (pout[i] != NULL && pout[i]->write_out == YES) { // NG : 31/07/2020 : Output files printing can now be turned off, even if the output setting sub-block remains written in the output file.
      if ((i == FP_IO) && (pcarac_fp != NULL)) {
        if (pout[i]->t_out[CUR_IO] <= (t - dt))
          IO_refresh_t_out(pout[i]);

        // NG : 23/07/2020 : Je remplace la fonction de BL par une nouvelle prenant en argument le 'spatial_scale' pour sortir le mbfile soit à l'ele_bu soit à la cprod.
        // Les deux bilans sont écrits en m3/s. // BL attention ne marche pas pour des pas de temps > à dt il faudrait pouvoir interpoler et sommer (ajout d'un q_ruis interpol)
        FP_write_mb_hydro(pout[i]->spatial_scale, t, dt, t_day, pout[i], pcarac_fp, fp);
      }

      // NG : 22/07/2020 : WATBAL_TRANSPORT output
      if ((i == FP_TRA_IO) && (pcarac_fp != NULL)) {
        if (pout[i]->t_out[CUR_IO] <= (t - dt))
          IO_refresh_t_out(pout[i]);
        FP_write_mb_transport(nb_species, pout[i]->spatial_scale, t, dt, t_day, pout[i], pcarac_fp, fp);
      }

      if ((i == NSAT_IO) && (pcarac_zns != NULL)) {
        if (pout[i]->t_out[CUR_IO] <= (t - dt))
          IO_refresh_t_out(pout[i]);
        NSAT_write_mb_zns(t, dt, t_day, pout[i], pcarac_zns, fp); // BL attention ne marche pas pour des pas de temps > à dt il faudrait pouvoir interpoler et sommer (ajout d'un q_sout interpol)
      }

      // NG : 30/07/2020
      if ((i == NSAT_TRA_IO) && (pcarac_zns != NULL)) {
        if (pout[i]->t_out[CUR_IO] <= (t - dt))
          IO_refresh_t_out(pout[i]);
        NSAT_write_mb_zns_transport(nb_species, t, dt, t_day, pout[i], pcarac_zns, fp); // BL attention ne marche pas pour des pas de temps > à dt il faudrait pouvoir interpoler et sommer (ajout d'un q_sout interpol)
      }

      if ((i == MASS_IO) && (pcarac_aq != NULL)) {
        if (pout[i]->t_out[CUR_IO] <= (t - dt))
          IO_refresh_t_out(pout[i]);
        AQ_print_output(chronos, t_day, pcarac_aq, pout[i], i, fp);
      }

      // NG : 04/01/2021 : Extracting and printing hydraulic head only
      if ((i == AQ_H_IO) && (pcarac_aq != NULL)) {
        if (pout[i]->t_out[CUR_IO] <= (t - dt))
          IO_refresh_t_out(pout[i]);
        AQ_print_hydhead_only(chronos, t_day, pcarac_aq, pout[i], i, fp);
      }

      if ((i == AQ_THRESH_IO) && (pcarac_aq != NULL)) {
        if (pout[i]->t_out[CUR_IO] <= (t - dt))
          IO_refresh_t_out(pout[i]);
        AQ_fill_threshold_output(chronos, t_day, pcarac_aq, pout[i], i, fp);
      }

      if (((i == HYD_Q_IO) || (i == HYD_H_IO) || (i == MBHYD_IO)) && (pchyd != NULL)) {
        if (pout[i]->t_out[CUR_IO] <= (t - dt))
          IO_refresh_t_out(pout[i]);
        HYD_print_outputs_all(chronos, t_day, pchyd, pout, i, fp);
      }

      // NG : 01/11/2024 : Adding launch of libwet outputs
      if ((i == MBWET_IO) || (i == HWET_IO)) {
        if (pout[i]->t_out[CUR_IO] <= (t - dt))
          IO_refresh_t_out(pout[i]);
        WET_print_outputs(chronos, t_day, pcarac_wet, pout[i], i, fp);
      }

      if (psimul->pset_caw->transport_caw == YES) { // NG : 31/10/2024 : Fix. Has to be conditional to general transport activation.

        if ((i == MBHYD_TRA_IO) && (pchyd != NULL)) {
          if (pout[i]->t_out[CUR_IO] <= (t - dt))
            IO_refresh_t_out(pout[i]);
          CAW_print_outputs_transport_riv_balance(chronos, nb_species, t_day, pchyd, pout[i], i, p_species, pcarac_ttc[HYD_CAW], fp);
        }

        if (((i == HDERM_Q_IO) || (i == HDERM_MB_IO)) && (pchyd_hderm != NULL)) { // NG : 23/02/2020 : Outputs for Hderm module added
          if (pout[i]->t_out[CUR_IO] <= (t - dt))
            IO_refresh_t_out(pout[i]);
          HYD_print_outputs_all(chronos, t_day, pchyd_hderm, pout, i, fp);
        }

        if ((i == MBAQ_TRA_IO) && (pcarac_aq != NULL)) {
          if (pout[i]->t_out[CUR_IO] <= (t - dt))
            IO_refresh_t_out(pout[i]);
          CAW_print_outputs_transport_aq_balance(chronos, nb_species, t_day, pcarac_aq, pout[i], i, p_species, fp);
        }

        // NG : 12/04/2023 : Lighter AQ transport added. Only contains transport state variable for aquifer (C, T). No mass balance.
        if ((i == VARAQ_TRA_IO) && (pcarac_aq != NULL)) {
          if (pout[i]->t_out[CUR_IO] <= (t - dt))
            IO_refresh_t_out(pout[i]);
          CAW_print_outputs_transport_aq_variable(chronos, nb_species, t_day, pcarac_aq, pout[i], i, p_species, fp);
        }

        // NG : 18/04/2023 : Lighter HYD transport added. Only contains transport state variable for river (C, T). No mass balance.
        if ((i == VARHYD_TRA_IO) && (pchyd != NULL)) {
          if (pout[i]->t_out[CUR_IO] <= (t - dt))
            IO_refresh_t_out(pout[i]);
          CAW_print_outputs_transport_riv_variable(chronos, nb_species, t_day, pchyd, pout[i], i, p_species, pcarac_ttc[HYD_CAW], fp);
        }
      }
    }
  }
}

/**
 * @brief
 * @return
 */
void CAW_close_files(s_out_io **pout, s_chronos_CHR *chronos) {

  int i;

  for (i = 0; i < NMOD_IO; i++) {
    if (pout[i] != NULL && pout[i]->write_out == YES) { // NG : 31/07/2020 : To avoid segfaults. Can't close the file if it hasn't been written.
      fclose(pout[i]->fout);
    }
  }
}

/**
 * @brief
 * @return
 * @internal NG : 31/07/2020 : output files printing can now be turned off although the output block is still written in the command file.
 * But also turn off prints of the *corresp files in binary mode !
 * NG : 18/04/2023 : Update for transport : CAW_init_for() : Transport headers now accounts for specie type.
 * New lighter VAR_AQ_TRA_IO and VAR_HYD_TRA_IO types introduced as well. @endinternal
 */
void CAW_init_files(s_out_io **pout, s_settings_cawaqs *pset_caw, s_carac_fp *pcarac_fp, s_carac_zns *pcarac_zns, s_chyd *pchyd, s_carac_aq *pcarac_aq, s_carac_fp *pcarac_fp_hderm, s_chyd *pchyd_hderm, s_chronos_CHR *chronos, int nb_species, s_species_cawaqs **p_species, int *spe_types, FILE *fp) {

  int i, year2, year;

  for (i = 0; i < NMOD_IO; i++) {
    if (pout[i] != NULL && pout[i]->write_out == YES) {
      if (pset_caw->calc_state == TRANSIENT) { // FB 23/04/2018 Now for all the modules we print one file per year.
        year = chronos->year[CUR_CHR];
        year2 = year + 1;
        IO_init_out_file(pout[i], i, year, year2, fp);

        if (pout[i]->format == FORMATTED_IO) {
          CAW_init_for(pout, i, nb_species, spe_types, p_species, fp);
        } else if (chronos->year[CUR_CHR] == chronos->year[BEGINNING_CHR])
          CAW_init_bin(pcarac_fp, pcarac_zns, pchyd, pcarac_aq, pcarac_fp_hderm, pchyd_hderm, i, pout, nb_species, p_species, fp);
      } else { // FB 23/04/2018 Now for all the modules we print one file per year. In the STEADY state case we obviously have one output file only.
        year = chronos->year[BEGINNING_CHR];
        year2 = chronos->year[END_CHR];
        IO_init_out_file(pout[i], i, year, year2, fp);

        if (pout[i]->format == FORMATTED_IO) {
          CAW_init_for(pout, i, nb_species, spe_types, p_species, fp);
        } else {
          CAW_init_bin(pcarac_fp, pcarac_zns, pchyd, pcarac_aq, pcarac_fp_hderm, pchyd_hderm, i, pout, nb_species, p_species, fp);
        }
      }
    }
  }
}

/**
 * @brief
 * @return
 */
void CAW_init_bin(s_carac_fp *pcarac_fp, s_carac_zns *pcarac_zns, s_chyd *pchyd, s_carac_aq *pcarac_aq, s_carac_fp *pcarac_fp_hderm, s_chyd *pchyd_hderm, int i, s_out_io **pout, int nb_species, s_species_cawaqs **p_species, FILE *fp) {

  if (i == FP_IO) {
    if ((pcarac_fp != NULL) && (pout[i]->fout != NULL)) {
      FP_print_corresp(pcarac_fp, i, pout[i]->spatial_scale, fp); // NG : 31/08/2021 : Info in WATBAL_corresp file is now consistent with the spatial scale the user asked for.
    } else {
      LP_warning(fp, "CaWaQS%4.2f: function %s in %s l%d, FYI No %s input declared\n", NVERSION_CAW, __func__, __FILE__, __LINE__, IO_name_mod(i));
    }
  }

  // Transport
  if (i == FP_TRA_IO) {
    if ((pcarac_fp != NULL) && (pout[i]->fout != NULL)) {
      FP_print_corresp_all_species(pcarac_fp, nb_species, i, fp);
    } else {
      LP_warning(fp, "CaWaQS%4.2f: function %s in %s l%d, FYI No %s input declared\n", NVERSION_CAW, __func__, __FILE__, __LINE__, IO_name_mod(i));
    }
  }

  if (i == NSAT_IO) {
    if ((pcarac_zns != NULL) && (pout[i]->fout != NULL)) {
      NSAT_print_corresp(pcarac_zns, i, fp);
    } else {
      LP_warning(fp, "CaWaQS%4.2f: function %s in %s l%d, FYI No %s input declared\n", NVERSION_CAW, __func__, __FILE__, __LINE__, IO_name_mod(i));
    }
  }

  if (i == MASS_IO || i == AQ_H_IO || i == MBAQ_TRA_IO || i == VARAQ_TRA_IO) {
    if ((pcarac_aq != NULL) && (pout[i]->fout != NULL)) {
      AQ_print_corresp(pcarac_aq->pcmsh, i, fp);
    } // NG : 13/04/2023 : Adding cells surfaces in file
    else {
      LP_warning(fp, "CaWaQS%4.2f: function %s in %s l%d, FYI No %s input declared\n", NVERSION_CAW, __func__, __FILE__, __LINE__, IO_name_mod(i));
    }
  }

  // NG : 18/02/2020 : Printing correspondances for both cprod cells and river elements for hyperdermic zone
  if (i == HDERM_Q_IO || i == HDERM_MB_IO) {
    if ((pcarac_fp_hderm != NULL) && (pchyd_hderm != NULL) && (pout[i]->fout != NULL)) {
      CAW_print_corresp_files_hyperdermic(pcarac_fp_hderm, pchyd_hderm, i, fp);
    } else {
      LP_warning(fp, "CaWaQS%4.2f: function %s in %s l%d, FYI No %s input declared\n", NVERSION_CAW, __func__, __FILE__, __LINE__, IO_name_mod(i));
    }
  }

  if (i == HYD_Q_IO || i == HYD_H_IO || i == MBHYD_IO || i == MBHYD_TRA_IO || i == VARHYD_TRA_IO) {
    if ((pchyd != NULL) && (pout[i]->fout != NULL)) {
      HYD_print_corresp(pchyd, i, fp);
    } else {
      LP_warning(fp, "CaWaQS%4.2f: function %s in %s l%d, FYI No %s input declared\n", NVERSION_CAW, __func__, __FILE__, __LINE__, IO_name_mod(i));
    }
  }
}

/**
 * @brief
 * @return
 */
void CAW_init_for(s_out_io **pout, int i, int nb_species, int *spe_types, s_species_cawaqs **p_species, FILE *fp) {

  if (i == FP_IO) {
    if (pout[i]->fout != NULL) {
      FP_print_header(pout[i]->fout);
    } else {
      LP_error(fp, " The output file %s has not been properly opened.\n", IO_name_out(i));
    }
  }

  if (i == FP_TRA_IO) {
    if (pout[i]->fout != NULL) {
      FP_print_header_transport(nb_species, spe_types, pout[i]->fout);
    } else {
      LP_error(fp, " The output file %s has not been properly opened.\n", IO_name_out(i));
    }
  }

  if (i == NSAT_IO) {
    if (pout[i]->fout != NULL) {
      NSAT_print_header(pout[i]->fout, fp);
    } else {
      LP_error(fp, " The output file %s has not been properly opened.\n", IO_name_out(i));
    }
  }

  if (i == NSAT_TRA_IO) {
    if (pout[i]->fout != NULL) {
      NSAT_print_transport_header(nb_species, spe_types, pout[i]->fout);
    } else {
      LP_error(fp, " The output file %s has not been properly opened.\n", IO_name_out(i));
    }
  }

  // NG : 04/01/2021 : AQ_print_header() now prints the appropriate header according to the output file type which has been requested.
  if ((i == MASS_IO) || (i == AQ_H_IO) || (i == AQ_THRESH_IO)) {
    if (pout[i]->fout != NULL) {
      AQ_print_header(pout[i]->fout, i, fp);
    } else {
      LP_error(fp, " The output file %s has not been properly opened.\n", IO_name_out(i));
    }
  }

  if ((i == HYD_Q_IO) || (i == HYD_H_IO) || (i == MBHYD_IO) || (i == HDERM_Q_IO) || (i == HDERM_MB_IO)) {
    if (pout[i]->fout != NULL) {
      HYD_print_header(pout[i]->fout, i);
    } else {
      LP_error(fp, " The output file %s has not been properly opened.\n", IO_name_out(i));
    }
  }

  if ((i == MBHYD_TRA_IO) || (i == MBAQ_TRA_IO) || (i == VARAQ_TRA_IO) || (i == VARHYD_TRA_IO)) {
    if (pout[i]->fout != NULL) {
      CAW_print_transport_header(i, nb_species, p_species, pout[i]->fout);
    } else {
      LP_error(fp, " The output file %s has not been properly opened.\n", IO_name_out(i));
    }
  }

  // NG 01/11/2024
  if ((i == MBWET_IO) || (i == HWET_IO)) {
    if (pout[i]->fout != NULL) {
      WET_print_headers(pout[i]->fout, i, fp);
    } else {
      LP_error(fp, " The output file %s has not been properly opened.\n", IO_name_out(i));
    }
  }
}

/**
 * @brief
 * @return
 */
void CAW_link_mb(s_out_io **pout, s_chronos_CHR *chronos, FILE *fplog) {

  int i;

  for (i = 0; i < NMOD_IO; i++) {
    if (pout[i] != NULL) {
      if ((i == MASS_IO || i == NSAT_IO) || (i == FP_IO || i == MBHYD_IO))
        IO_link_mb(pout[i], i, chronos->year[BEGINNING_CHR], chronos->year[END_CHR], fplog);
    }
  }
}

/**
 * @brief
 * @return
 * @internal NG : 21/02/2020 : Printing cprod cells and river elements' corresp files pour the hyperdermic module
 */
void CAW_print_corresp_files_hyperdermic(s_carac_fp *pcarac_fp_hderm, s_chyd *pchyd_hderm, int imodule, FILE *fp) {

  int i, j, nb_cprod, nb_reaches, nb_ele;
  char *name_mod, *name_cprod, *name_river_ele;
  s_cprod_fp *cprod;
  s_reach_hyd *preach;
  s_element_hyd *pele;
  FILE *fpout_cprod, *fpout_river;

  name_mod = IO_name_mod(imodule);
  name_cprod = (char *)malloc((strlen(getenv("RESULT")) + strlen(name_mod) + ALLOCSCD_LP) * sizeof(char));
  name_river_ele = (char *)malloc((strlen(getenv("RESULT")) + strlen(name_mod) + ALLOCSCD_LP) * sizeof(char));

  sprintf(name_cprod, "%s/%s_cprod_corresp_file.txt", getenv("RESULT"), name_mod);
  sprintf(name_river_ele, "%s/%s_river_corresp_file.txt", getenv("RESULT"), name_mod);

  fpout_cprod = fopen(name_cprod, "w");
  fpout_river = fopen(name_river_ele, "w");

  if (fpout_cprod == NULL || fpout_river == NULL)
    LP_error(fp, "Cannot open either Cprod or River element corresp file for hyperdermic module. Error in function %s, file %s at line %d.\n", __func__, __FILE__, __LINE__);

  fprintf(fpout_cprod, "ID_INTERN ID_ABS ID_GIS\n");
  fprintf(fpout_river, "ID_INTERN ID_ABS ID_GIS\n");

  nb_cprod = pcarac_fp_hderm->nb_cprod;

  for (i = 0; i < nb_cprod; i++) {
    cprod = pcarac_fp_hderm->p_cprod[i];
    fprintf(fpout_cprod, "%d %d %d\n", cprod->id[FP_INTERN], cprod->id[FP_ABS], cprod->id[FP_GIS]);
  }

  nb_reaches = pchyd_hderm->counter->nreaches;

  for (i = 0; i < nb_reaches; i++) {
    preach = pchyd_hderm->p_reach[i];
    nb_ele = preach->nele;

    for (j = 0; j < nb_ele; j++) {
      pele = preach->p_ele[j];
      fprintf(fpout_river, "%d %d %d\n", pele->id[INTERN_HYD], pele->id[ABS_HYD], pele->id[GIS_HYD]);
    }
  }

  fclose(fpout_cprod);
  fclose(fpout_river);
  free(name_mod);
  free(name_cprod);
  free(name_river_ele);
}

// NG : 21/02/2020 : pointers towards carac_fp_hderm and pchyd_hderm added
void CAW_print_characteristics(s_settings_cawaqs *pset_caw, s_carac_aq *pchar_aq, s_carac_fp *pchar_fp, s_carac_zns *pchar_zns, s_chyd *pchyd, s_carac_fp *pchar_fp_hderm, s_chyd *pchyd_hderm, FILE *flog) {
  char *name;
  int i;

  for (i = 0; i < NMOD_CAW; i++) {
    name = CAW_name_mod(i);
    switch (i) {
    case AQ_CAW: {
      if (pset_caw->sim_type == GROUND_WAT || pset_caw->sim_type == SW_GW) {
        LP_printf(flog, "Printing %s CHARACTERISTICS and INPUT PARAMETERS : \n", name);
        AQ_print_abstract(pchar_aq, flog);
        AQ_print_param_overview(pchar_aq, i, flog); // NG : 11/08/2021 : New function - prints out an overview output file of all input AQ parameters.
        LP_printf(flog, "\n\n");
      }
      break;
    }

    case HYD_CAW: {
      if ((pset_caw->sim_type != GW_LAKE) && (pset_caw->sim_type != GROUND_WAT)) { // SW 02/05/2016 add WET_CAW
        LP_printf(flog, "Printing %s CHARACTERISTICS : \n", name);
        HYD_print_abstract(pchyd, flog);
        HYD_print_ficvid(pchyd, "HYD", flog);
        // if(pset_caw->sim_type==SW_GW)
        HYD_print_Zbot(pchyd, flog);
        LP_printf(flog, "\n\n");
      }
      break;
    }

    case FP_CAW: {
      if (pset_caw->sim_type != GROUND_WAT && pset_caw->sim_type != GW_LAKE) { // SW 02/05/2016 add WET_CAW
        LP_printf(flog, "Printing %s CHARACTERISTICS : \n", name);
        FP_print_abstract(pchar_fp, flog);
        LP_printf(flog, "\n\n");
      }
      break;
    }

    case NSAT_CAW: {
      if (pset_caw->sim_type == SW_GW || pset_caw->sim_type == SURF_WAT_NSAT) {
        LP_printf(flog, "Printing %s CHARACTERISTICS : \n", name);
        if (pchar_zns != NULL) {
          NSAT_print_abstract(pchar_zns, flog);
        } else {
          LP_printf(flog, "none\n");
          LP_error(flog, "In %s l%d : No link between surface and subsurface defined. This must be done either through a zns or through another  link table TBD. This is a major misconception of the code that needs to be corrected\n", __FILE__, __LINE__);
        }
        LP_printf(flog, "\n\n");
      }
      break;
    }

    case WET_CAW: // SW 02/05/2016 add WET_CAW
    {
      if (pset_caw->sim_type == GW_LAKE) {
        LP_printf(flog, "Printing %s CHARACTERISTICS : \n", name);
        AQ_print_abstract(pchar_aq, flog);
        CAW_cAQ_print_nappe_riv(pchar_aq, pchyd, flog);
        LP_printf(flog, "\n\n");
      }
      break;
    }

    case HDERM_CAW: // NG 18/02/2020 Hyperdermic module added
    {
      if (pset_caw->sim_type == SURF_WAT_NSAT_HDERM || pset_caw->sim_type == SW_HDERM_GW) {
        LP_printf(flog, "Printing %s CHARACTERISTICS : \n", name);
        LP_printf(flog, "\n----- Cells properties -----\n");
        FP_print_FP_hyperdermic_abstract(pchar_fp_hderm, flog);
        LP_printf(flog, "\n----- River network properties -----\n");
        HYD_print_abstract(pchyd_hderm, flog);
        HYD_print_ficvid(pchyd_hderm, "HDERM", flog);
        LP_printf(flog, "\n\n");
      }
      break;
    }
    }
    free(name);
  }
}

// NG : 24/06/2023 : Restructuring function using case select.
void CAW_print_final_state(s_chyd *pchyd, s_chronos_CHR *chronos, s_out_io **pout, s_carac_fp *pcarac_fp, s_carac_zns *pcarac_zns, s_carac_aq *pcarac_aq, s_chyd *pchyd_hderm, FILE *fp) {

  int i, nb_out = 0;
  double dt, t;
  char *name;

  for (i = 0; i < NMOD_IO; i++) {
    if (pout[i] != NULL) {

      switch (i) {
      case (HYD_Q_IO):
        if (pchyd != NULL) {
          if (pout[i]->final_state == YES) {
            IO_init_final_state_file(pout[i], i, fp);
            HYD_print_Q_fin_state(pout[i], pchyd, fp);
            fclose(pout[i]->fout);
          }
        }
        break;

      case (NSAT_IO):
        if (pcarac_zns != NULL) {
          if (pout[i]->final_state == YES) {
            IO_init_final_state_file(pout[i], i, fp);
            NSAT_print_final_state(pcarac_zns, pout[i], fp);
            fclose(pout[i]->fout);
          }
        }
        break;

      case MASS_IO:
        if (pcarac_aq != NULL) {
          if (pout[i]->final_state == YES) {
            //  IO_init_final_state_file(pout[i],i,fp );
            AQ_print_final_state(pcarac_aq, fp);
            //  fclose(pout[i]->fout);
          }
        }
        break;

      case HDERM_Q_IO:
        if (pchyd_hderm != NULL) {
          if (pout[i]->final_state == YES) {
            IO_init_final_state_file(pout[i], i, fp);
            HYD_print_Q_fin_state(pout[i], pchyd_hderm, fp);
            fclose(pout[i]->fout);
          }
        }
        break;
      default:
        if (pout[i]->final_state == YES) {
          LP_warning(fp, "In CaWaQS%4.2f : In file %s, function %s : Final state file not available yet for output file type : %s", NVERSION_CAW, __FILE__, __func__, IO_name_out(i));
        }
      }
    }
  }
}

// NG : 23/02/2020 : Update to integrate HDERM module outputs declarations. Updated 04/01/2021
void CAW_check_compatibility_output_hydro_module(s_simul_cawaqs *Simul) {
  int i;
  double dt, t;
  int nb_out = 0;
  char *name;
  s_carac_fp *pcarac_fp;
  s_carac_zns *pcarac_zns;
  s_carac_aq *pcarac_aq;
  s_chyd *pchyd;
  s_chyd *pchyd_hderm;
  s_out_io *pio;
  s_out_io **pout;
  FILE *fp;

  pout = Simul->pout;
  fp = Simul->poutputs;
  pcarac_fp = Simul->pcarac_fp;
  pcarac_zns = Simul->pcarac_zns;
  pcarac_aq = Simul->pcarac_aq;
  pchyd_hderm = Simul->pchyd_hderm;
  pchyd = Simul->pchyd;

  for (i = 0; i < NMOD_IO; i++) {
    if (pout[i] != NULL) {
      if ((((i == HYD_Q_IO) || (i == HYD_H_IO)) && (pchyd == NULL)) || ((i == NSAT_IO) && (pcarac_zns == NULL)) || (((i == MASS_IO) || (i == AQ_H_IO)) && (pcarac_aq == NULL)) || ((i == FP_IO) && (pcarac_fp == NULL)) || (((i == HDERM_Q_IO) || (i == HDERM_MB_IO)) && (pchyd_hderm == NULL))) {
        LP_error(fp, "CaWaQS%4.2f: in function %s in file %s at line %d : %s-related output with no associated hydro module declared.\n\t Please check the definition of your modules input in respect of the requested outputs.\n", NVERSION_CAW, __func__, __FILE__, __LINE__, IO_name_mod(i));

        pio = pout[i];

        if (pio->fout != NULL)
          fclose(pio->fout);

        if (pio->name != NULL)
          free(pio->name);

        if (pio->id_output != NULL)
          free(pio->id_output);

        free(pout[i]);
        pout[i] = NULL;
      }
    }

    /* NG : 04/01/2021 : From the 2.91 version on, two AQ output file types are now available (full MB or H only).
       Any output setting configuration defined by the user need to include the definition of the MB_AQ part in the command file (although actual writing can still be turned off in unecessary).
       An undeclared MB_AQ part will imply a segfault in AQ_refresh_state_var(), thus the systematic additional checkpoint below ! */
    if (i == MASS_IO) {
      if ((pcarac_aq != NULL) && (pout[i] == NULL))
        LP_error(fp, "CaWaQS%4.2f : Error in file %s, function %s at line %d : It seems the %s settings have not been declared in the command file. Check your outputs options !\n", NVERSION_CAW, __FILE__, __func__, __LINE__, IO_name_out(i));
    }
  }
}

/**
 * \brief Checks if transport module outputs declared are coherent transport calculations
 * \return -
 */
void CAW_check_compatibility_outputs_transport(s_simul_cawaqs *Simul) {
  int i;
  int *transmod = Simul->pset_caw->transport_module;
  s_out_io **pout = Simul->pout;
  s_out_io *pio;

  for (i = 0; i < NMOD_IO; i++) {
    if (pout[i] != NULL && pout[i]->write_out == YES) {
      if (((i == FP_TRA_IO) && (transmod[FP_CAW] == NO)) || ((i == NSAT_TRA_IO) && (transmod[NSAT_CAW] == NO)) || ((i == HDERM_TRA_IO) && (transmod[HDERM_CAW] == NO)) || ((i == MBHYD_TRA_IO) && (transmod[HYD_CAW] == NO)) || ((i == MBAQ_TRA_IO) && (transmod[AQ_CAW] == NO))) {
        LP_error(Simul->poutputs, "CaWaQS%4.2f: in function %s in file %s at line %d : %s-related output with no declared (or deactivated) transport module.\n\t Please check the definition of your module inputs in respect of the requested outputs.\n", NVERSION_CAW, __func__, __FILE__, __LINE__, IO_name_mod(i));
        pio = pout[i];

        if (pio->fout != NULL)
          fclose(pio->fout);
        if (pio->name != NULL)
          free(pio->name);
        if (pio->id_output != NULL)
          free(pio->id_output);

        free(pout[i]);
        pout[i] = NULL;
      }
    }
  }
}
