/*-------------------------------------------------------------------------------
 *
 * SOFTWARE NAME: cawaqs
 * FILE NAME: FP_manage_cprod_coupled.c
 *
 * CONTRIBUTORS: Nicolas GALLOIS, Baptiste LABARTHE, Shuaitao WANG,
 *               Agnès RIVIERE, Deniz KILIC, Mathias MAILLOT,
 *               Fulvia BARATELLI, Pierre GUILLOU, Emmanuel LEDOUX, Nicolas FLIPO
 *
 * SOFTWARE BRIEF DESCRIPTION: Physically based coupled surface–subsurface
 * model for joint water, matter and energy flux balances and flow dynamics
 * simulation within all compartments of a hydrosystem (sub-surface, hydraulic
 * network, vadose zone, aquifer system and stream-aquifer exchanges).
 *
 * Software developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the cawaqs Software.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Software and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include "libprint.h"
#include "time_series.h"
#include "libpc.h"
#include "IO.h"
#include "reservoir.h"
#include "spa.h"
#include "CHR.h"
#include "FP.h"
#include "NSAT.h"
#include "GC.h"
#include "HYD.h"
#include "MSH.h"
#include "AQ.h"
#include "WET.h"
#include "TTC_coupled.h"
#include "SEB.h"
#include "CAW.h"
#ifdef OMP
#include "omp.h"
#endif

// NG : 12/06/2023 : Obsolete BL function. Does not appear to be called anywhere. Commenting out.
/*void FP_calc_watbal_cprod_coupled (s_carac_fp *pcarac_fp, s_carac_aq *pcarac_aq, double dt, double t, double theta, int sim_type, FILE *fpout) {

  FP_calc_watbal_cprod_all(pcarac_fp, dt, t, fpout);
  if (sim_type == SW_GW) {
    FP_calc_watbal_cprod_sout(pcarac_fp, pcarac_aq, fpout);
  }
}*/

void FP_calc_watbal_cprod_sout(s_carac_fp *pcarac_fp, s_carac_aq *pcarac_aq, FILE *fpout) {
  int l, nlayer;
  double theta;
  s_layer_msh **p_layer;
  p_layer = pcarac_aq->pcmsh->p_layer;

  FP_reinit_q_cprod(pcarac_fp, RSV_SOUT, fpout);

  theta = pcarac_aq->settings->general_param[THETA];
  nlayer = pcarac_aq->pcmsh->pcount->nlayer;

  for (l = 0; l < nlayer; l++) {
    FP_calc_watbal_cprod_sout_layer(p_layer[l], pcarac_fp, theta, fpout);
  }
}

void FP_calc_watbal_cprod_sout_layer(s_layer_msh *player, s_carac_fp *pcarac_fp, double theta, FILE *fpout) {

  int e, nele;
  s_ele_msh **p_ele;
  s_id_spa *link;
  double *output_SURF;
  s_cprod_fp *pcprod;
  char *name;
  s_ele_msh *pele;
#ifdef OMP
  s_smp *psmp;
  psmp = pcarac_fp->psmp;
#endif
  p_ele = player->p_ele;
  // BL pour parrallelisation orivate(e,pele) shared(pcarac_fp,theta,fpout)
  nele = player->nele;
  /*
#ifdef OMP
  int taille; //BL résultats différents si OMP je pense que c'est du à pcprod qui doit être partagé même en faisant ça pas même résultats. comprends pas trop ....
  int nthreads;
  nthreads=psmp->nthreads;
  omp_set_num_threads(psmp->nthreads);

  psmp->chunk=PC_set_chunk_size_silent(fpout,player->nele,nthreads);
  taille=psmp->chunk;

#pragma omp parallel shared(p_ele,nthreads,taille) private(e,link,pele,output_SURF,link->prct)
  {
#pragma omp for schedule(dynamic,taille)
#endif
  */
  for (e = 0; e < nele; e++) {
    pele = p_ele[e];
    link = pele->link[SURF_AQ];
    if (link != NULL) {
      link = (s_id_spa *)SPA_browse_all_struct(IDS_SPA, link, BEGINNING_FP);

    } else {
      // if(pele->loc==TOP)//FB 23/03/2017
      if (pele->loc == TOP_MSH && pele->phydro->pbound == NULL) // FB 23/03/2017 chaque maille souterraine est soit condition limite soit connectee a la surface (en mode couple)
      {
        LP_error(fpout, "the TOP element GIS_ID %d ABS_ID %d is not linked with surface", pele->id[GIS_MSH], pele->id[ABS_MSH]);
      }
    }

    while (link != NULL) {
      pcprod = pcarac_fp->p_cprod[link->id];
#ifdef DEBUG
      LP_printf(fpout, "ele INTERN %d ABS %d is linked with cprod %d", pele->id[INTERN_MSH], pele->id[ABS_MSH], pcprod->id[FP_GIS]);

#endif

      if (pele->loc == TOP_MSH && (pele->phydro->pbound != NULL && pele->phydro->pbound->type == CAUCHY)) {
        if (pele->modif[MODIF_CALC_AQ] == NO_LIM_AQ) {
          output_SURF = SPA_calcul_values(CPROD_GW_SPA, pele, link->prct, theta, 0., fpout);
#ifdef DEBUG
          LP_printf(fpout, " debit from sout %e m3/s H[SURF] %f H[t] %f prct %f \n", output_SURF[0], pele->phydro->pbound->pft->ft, pele->phydro->h[PIC], prct);
#endif
          if (output_SURF[0] > 0) {                  // Cas ou la nappe deborde
            if (pele->phydro->pderivation != NULL) { // NG : 22/01/2021 : Pas de prise en compte du débit de Cauchy si maille AQ en dérivation. Dans ce cas, le flux est déja renvoyé vers une autre maille AQ !!
              pcprod->pwat_bal->q[RSV_SOUT] += output_SURF[0];
            } else {
              pcprod->pwat_bal->q[RSV_SOUT] += 0.;
            }
          }
          free(output_SURF);
        } else {
#ifdef DEBUG
          LP_printf(fpout, " pele->modif is YES debit from sout %f m3/s H[SURF] %f H[t] %f \n", 0., pele->phydro->pbound->pft->ft, pele->phydro->h[PIC]);
#endif
          pcprod->pwat_bal->q[RSV_SOUT] += 0;
        }
      }
      link = link->next;
    }
  }
  /*
 #ifdef OMP
} // end of parallel section
#endif
*/
}

// NG : 12/06/2023 : Obsolete BL function. Does not appear to be called anywhere. Commenting out.
/*void FP_calc_watbal_cprod_sout_ele (s_ele_msh *pele, s_carac_fp *pcarac_fp, double theta, FILE *fpout) {
  s_id_spa *link;
  double prct;
  double *output_SURF;
  s_cprod_fp *pcprod;
  char *name;
  link = pele->link[SURF_AQ];
  if (link != NULL) {
    link = (s_id_spa *) SPA_browse_all_struct(IDS_SPA, link, BEGINNING_FP);

  }
  else {
    if (pele->loc == TOP_MSH) {
      LP_error(fpout, "the TOP element GIS_ID %d ABS_ID %d is not linked with surface", pele->id[GIS_MSH], pele->id[ABS_MSH]);
    }
  }

  while (link != NULL) {

    pcprod = pcarac_fp->p_cprod[link->id];
#ifdef DEBUG
    LP_printf(fpout,"ele INTERN %d ABS %d is linked with cprod %d",pele->id[INTERN_MSH],pele->id[ABS_MSH],pcprod->id[FP_GIS]);

#endif

    prct = link->prct;
    if (pele->loc == TOP_MSH && (pele->phydro->pbound != NULL && pele->phydro->pbound->type == CAUCHY)) {
      if (pele->modif[MODIF_CALC_AQ] == NO_LIM_AQ) {
        output_SURF = SPA_calcul_values(CPROD_GW_SPA, pele, prct, theta, 0., fpout);
#ifdef DEBUG
        LP_printf(fpout," debit from sout %e m3/s H[SURF] %f H[t] %f prct %f \n",output_SURF[0],pele->phydro->pbound->pft->ft,pele->phydro->h[PIC],prct);
#endif
        if (output_SURF[0] > 0) {//Cas ou la nappe deborde
          pcprod->pwat_bal->q[RSV_SOUT] += output_SURF[0];
        }
        free(output_SURF);
      }
      else {
#ifdef DEBUG
        LP_printf(fpout," pele->modif is YES debit from sout %f m3/s H[SURF] %f H[t] %f \n",0.,pele->phydro->pbound->pft->ft,pele->phydro->h[PIC]);
#endif
        pcprod->pwat_bal->q[RSV_SOUT] += 0;
      }
    }
    link = link->next;
  }
}*/

// NG : 20/02/2020 : Calcul parraléllisé des flux d'eau par Cprod hyperdermique depuis le nonsat
void CAW_cFP_calc_watbal_cprod_coupled_hyperdermic_all(s_carac_fp *pcarac_fp, s_carac_zns *pcarac_zns, double dt, double t, FILE *fpout) {

  int i, j, nb_cprod = pcarac_fp->nb_cprod;
  s_cprod_fp **p_cprod = pcarac_fp->p_cprod;

#ifdef OMP
  int taille;
  int nthreads;
  s_smp *psmp;
  psmp = pcarac_fp->psmp;
  nthreads = psmp->nthreads;
  omp_set_num_threads(psmp->nthreads);
  psmp->chunk = PC_set_chunk_size_silent(fpout, pcarac_fp->nb_cprod, nthreads);
  taille = psmp->chunk;

  // LP_printf(fpout,"print du nombre de threads requis : %d. taille du chunk de cprod : %d\n",nthreads,taille); // NG check

#pragma omp parallel shared(p_cprod, nthreads, taille) private(i)
  {
#pragma omp for schedule(dynamic, taille)
#endif
    for (i = 0; i < nb_cprod; i++) {
      CAW_cFP_calc_watbal_cprod_coupled_hyperdermic(p_cprod[i], pcarac_fp, pcarac_zns, dt, t, fpout);
    }
#ifdef OMP
  }
#endif
}

// NG : 20/02/2020 : Calcul des flux agrégés par Cprod (ruissellement) du module hyperdermique depuis la base de la zone non saturée
void CAW_cFP_calc_watbal_cprod_coupled_hyperdermic(s_cprod_fp *cprod, s_carac_fp *pcarac_fp, s_carac_zns *pcarac_zns, double dt, double t, FILE *fpout) {
  int i;
  double *output_HDERM, prct;
  double cp_area;
  s_id_spa *link;
  s_zns *pzns;

  cp_area = cprod->area;

  link = cprod->link[NSAT_HDERM_FP];

  if (link != NULL) {
    link = (s_id_spa *)SPA_browse_all_struct(IDS_SPA, link, BEGINNING_FP);
  } else {
    LP_error(fpout, "Link pointer is Null. Error in function %s, in file %s at line %d.\n", __func__, __FILE__, __LINE__);
  }

  RSV_reinit_wat_bal(cprod->pwat_bal);
  FP_reinit_stock_cprod(cprod->stock);

  while (link != NULL) {
    pzns = pcarac_zns->p_zns[link->id];

    prct = (link->prct * cp_area) / (pzns->area); /* NG : ici "prct" représente non pas le % de couverture de la Cprod par une NSAT unit donnée
                                                          mais le % de surface de la NSAT unit tombant dans la Cprod pour laquelle on fait le calcul !!!!   */

    output_HDERM = SPA_calcul_values(CPROD_NSAT_HDERM_SPA, pzns, prct, 0., 0., fpout); // output_HDERM alloué ici est en m3/s

    if (isnan(output_HDERM[RSV_RUIS]) || isinf(output_HDERM[RSV_RUIS])) {
      LP_error(fpout, "Abnormal value while calculating %s fluxes at cprod %d. Error in function %s, in file %s at line %d.\n", RSV_name_res(RSV_RUIS), cprod->id[FP_GIS], __func__, __FILE__, __LINE__);
    }

    cprod->pwat_bal->q[RSV_RUIS] += output_HDERM[RSV_RUIS];

    //	LP_printf(fpout,"nsat %d is linked to cprod %d with percent %f\n",link->id,cprod->id[FP_GIS],prct); // Check NG

    link = link->next;
    free(output_HDERM);
  }
  // LP_printf(fpout,"Input_Cprod %d Day %f Input %25.19f\n",cprod->id[FP_GIS],t,cprod->pwat_bal->q[RSV_RUIS]); // Check NG
}

// NG : 24/01/2020 : On vérifie ici que les catchments gouffres déclarés (ie. en ruissellement pur) ne sont effectivement pas liés à un troncon rivière. Sinon ils sont ignorés avec display d'un warning
void CAW_cFP_check_catchment_chasm_river(s_carac_fp *pcarac_fp, s_chyd *pchyd, FILE *fpout) {

  s_catchment_fp *pcatch = pcarac_fp->p_catchment;
  s_catchment_fp *ptmp;
  s_reach_hyd *preach;
  int nb_reaches = pchyd->counter->nreaches;
  int i, id_cprod_out;

  ptmp = FP_browse_catchment_chain(pcatch, BEGINNING_IO, fpout);

  while (ptmp != NULL) {

    if (ptmp->catchment_type == FP_CATCH_CHASM && ptmp->pcprod_outlet->cell_prod_type == FP_CATCH_CHASM) {

      id_cprod_out = ptmp->pcprod_outlet->id[FP_GIS];
      // On check si l'on trouve un reach avec le meme id gis.
      for (i = 0; i < nb_reaches; i++) {
        preach = pchyd->p_reach[i];
        if (preach->id[GIS_HYD] == id_cprod_out) {
          LP_warning(fpout, "Chasm-type catchment %s is linked to the river reach GIS ID %d. It can't be assigned as a chasm catchment. Catchment type ignored - switched back to a river catchment.\n", ptmp->catchment_name, preach->id[GIS_HYD]);
          // Repassage du catchment en rivière
          ptmp->catchment_type = FP_CATCH_RIV;
          ptmp->pcprod_outlet->cell_prod_type = FP_CATCH_RIV;
        }
      }
    }
    ptmp = ptmp->next;
  }
}

/*\fn void CAW_cFP_assign_catchment_cprod_outlet (s_chyd*, s_carac_fp*, FILE*)
 *\brief Déterminatation des cprod_outlet des catchments après lecture du réseau hydro
 *\return -
 */
void CAW_cFP_assign_catchment_cprod_outlet(s_chyd *pchyd, s_carac_fp *pcarac_fp, FILE *fpout) {

  s_cprod_fp *pcprod_st = NULL;
  int i, d, id_reach;

  for (d = 0; d < pchyd->counter->ndownstr_limits; d++) {
    id_reach = pchyd->downstr_reach[d]->id[GIS_HYD];
    for (i = 0; i < pcarac_fp->nb_cprod; i++) {
      if (pcarac_fp->p_cprod[i]->id[FP_GIS] == id_reach) {
        pcprod_st = pcarac_fp->p_cprod[i];
        // p_catch peut ne pas être défini si la cprod identifiée comme étant à l'aval d'un catchment n'a pas été listé comme cprod d'un catchment !!!!
        if (pcarac_fp->p_cprod[i]->p_catch == NULL)
          LP_error(fpout, "Cprod GIS ID %d has been identified as a catchment outlet but isn't listed as part of any catchment in the input file !\n", pcarac_fp->p_cprod[i]->id[FP_GIS]);
        pcarac_fp->p_cprod[i]->p_catch->pcprod_outlet = pcprod_st;
        break;
      }
    }
  }
}

/*\fn void CAW_cFP_calc_matter_fluxes_cprod_coupled_hyperdermic_all(s_carac_fp*, s_carac_zns*,double,double,FILE *)
 *\brief Launches matter flux and concentration input computation at the HDERM cprod cell scale from NSAT outputs
 *\return -
 */
void CAW_cFP_calc_matter_fluxes_cprod_coupled_hyperdermic_all(s_carac_fp *pcarac_fp, s_carac_zns *pcarac_zns, int nb_species, double dt, double t, FILE *fpout) {
  int i, j, nb_cprod = pcarac_fp->nb_cprod;
  s_cprod_fp **p_cprod = pcarac_fp->p_cprod;

#ifdef OMP
  int taille;
  int nthreads;
  s_smp *psmp;
  psmp = pcarac_fp->psmp;
  nthreads = psmp->nthreads;
  omp_set_num_threads(psmp->nthreads);
  psmp->chunk = PC_set_chunk_size_silent(fpout, pcarac_fp->nb_cprod, nthreads);
  taille = psmp->chunk;

#pragma omp parallel shared(p_cprod, nthreads, nb_species, taille) private(i)
  {
#pragma omp for schedule(dynamic, taille)
#endif
    for (i = 0; i < nb_cprod; i++) {
      CAW_cFP_calc_matter_fluxes_cprod_coupled_hyperdermic(p_cprod[i], pcarac_fp, pcarac_zns, nb_species, dt, t, fpout);
    }
#ifdef OMP
  }
#endif
}

/*\fn CAW_cFP_calc_matter_fluxes_cprod_coupled_hyperdermic(s_cprod_fp*, s_carac_fp*, s_carac_zns*, double, double, FILE*)
 *\brief Computes matter fluxes and concentration inputs for each Cprod cell of the HDERM hydraulic network, for all species
 *\return -
 */
void CAW_cFP_calc_matter_fluxes_cprod_coupled_hyperdermic(s_cprod_fp *cprod, s_carac_fp *pcarac_fp, s_carac_zns *pcarac_zns, int nb_species, double dt, double t, FILE *fpout) {
  int i, p;
  double *output_HDERM;
  s_id_spa *link;
  s_zns *pzns;
  s_wat_bal_rsv *pwatbal = cprod->pwat_bal;

  for (p = 0; p < nb_species; p++) {
    RSV_reinit_matter_fluxes_bal(pwatbal, p); // L'hydro est réinit dans CAW_cFP_calc_watbal_cprod_coupled_hyperdermic()
  }

  link = cprod->link[NSAT_HDERM_FP];

  if (link != NULL) {
    link = (s_id_spa *)SPA_browse_all_struct(IDS_SPA, link, BEGINNING_FP);
  } else {
    LP_error(fpout, "CaWaQS%4.2f : Link pointer is Null. Error in function %s, in file %s at line %d.\n", NVERSION_CAW, __func__, __FILE__, __LINE__);
  }

  while (link != NULL) {
    pzns = pcarac_zns->p_zns[link->id];

    for (p = 0; p < nb_species; p++) {
      output_HDERM = SPA_calcul_transport_fluxes(CPROD_NSAT_HDERM_SPA, pzns, p, link->prct, 0., 0., fpout); // flux en g/m2
      pwatbal->trflux[p][RSV_RUIS] += output_HDERM[0];

      if (isnan(output_HDERM[0]) || isinf(output_HDERM[0])) {
        LP_error(fpout, "CaWaQS%4.2f : Error in file %s, in function %s at line %d. Abnormal value while calculating HDERM input fluxes for cprod GIS id %d.\n", NVERSION_CAW, __FILE__, __func__, __LINE__, cprod->id[FP_GIS]);
      }

      free(output_HDERM);
    }
    link = link->next;
  }

  // Remplissage des concentrations pour toutes les spécies
  for (p = 0; p < nb_species; p++) {
    if (pwatbal->q[RSV_RUIS] > EPS_FP) { // eau ici en m3/s
      pwatbal->trvar[p][RSV_RUIS] = (pwatbal->trflux[p][RSV_RUIS] * cprod->area) / (pwatbal->q[RSV_RUIS] * dt * TS_unit2sec(DAY_TS, fpout));
    }
  }
}