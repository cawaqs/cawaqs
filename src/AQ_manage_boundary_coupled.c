/*-------------------------------------------------------------------------------
 *
 * SOFTWARE NAME: cawaqs
 * FILE NAME: AQ_manage_boundary_coupled.c
 *
 * CONTRIBUTORS: Nicolas GALLOIS, Baptiste LABARTHE, Shuaitao WANG,
 *               Agnès RIVIERE, Deniz KILIC, Mathias MAILLOT,
 *               Fulvia BARATELLI, Pierre GUILLOU, Emmanuel LEDOUX, Nicolas FLIPO
 *
 * SOFTWARE BRIEF DESCRIPTION: Physically based coupled surface–subsurface
 * model for joint water, matter and energy flux balances and flow dynamics
 * simulation within all compartments of a hydrosystem (sub-surface, hydraulic
 * network, vadose zone, aquifer system and stream-aquifer exchanges).
 *
 * Software developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the cawaqs Software.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Software and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include "libprint.h"
#include "time_series.h"
#include "libpc.h"
#include "IO.h"
#include "reservoir.h"
#include "spa.h"
#include "CHR.h"
#include "FP.h"
#include "NSAT.h"
#include "GC.h"
#include "HYD.h"
#include "MSH.h"
#include "AQ.h"
#include "WET.h"
#include "TTC_coupled.h"
#include "SEB.h"
#include "CAW.h"

/**
 * \fn void CAW_cAQ_calculate_recharge_coupled(double,double,int, s_carac_aq *,s_carac_zns *, s_chyd*, FILE *fp)
 * \brief define the aquifer recharge from a linked module (either ZNS and/or HDERM)
 * \return -
 *
 * NG : 07/03/2020 : Rewritten function to account for new ***psource format type. It also keeps track of the origin of the input fluxes.
 * Function managing links with ZNS and/or HDERM modules
 *
 */
void CAW_cAQ_calculate_recharge_coupled(double t, double dt, int linked_module, s_carac_aq *pchar_aq, s_carac_zns *pchar_zns, s_chyd *pchyd_hderm, FILE *fp) {
  s_id_io *id_list;
  s_ele_msh *pele;
  s_zns *pzns;
  s_id_spa *link;
  s_ft *pft;
  int k, nb_rech, r, nb_bound, pos_pbound;
  double ft_old, t_old, area_tot, q_tot, area_onaq;
  double q_sout, q, prct;
  char *modname = NULL;
  modname = CAW_name_mod(linked_module);

  nb_rech = 0;
  area_tot = 0;
  q_tot = 0;

  id_list = pchar_aq->pcmsh->pcount->pcount_hydro->elesource[RECHARGE_AQ]; // Liste de TOUS les éléments soumis à une recharge (i.e multiples recharges isolées et/ou recharge depuis le NSAT et/ou depuis le HDERM)
  id_list = IO_browse_id(id_list, BEGINNING_IO);

  while (id_list != NULL) {
    pele = pchar_aq->pcmsh->p_layer[id_list->id_lay]->p_ele[id_list->id];
    nb_bound = pele->phydro->nsource_type[RECHARGE_AQ]; // Nombre total de recharge inputs déclarés sur l'élément, tous types confondus.
    pos_pbound = CODE_AQ;                               // Position de la pbound dans le tableau **psource[RECHARGE_AQ]

    switch (linked_module) {
    case NSAT_CAW: {
      if (pele->link[NSAT_AQ] != NULL) // On isole ici que les mailles effectivement liées au module en question.
      {
        link = (s_id_spa *)SPA_browse_all_struct(IDS_SPA, pele->link[NSAT_AQ], BEGINNING_SPA);
        q = 0;
        prct = 0;
        nb_rech++;

        while (link != NULL) {
          pzns = pchar_zns->p_zns[link->id];
          area_onaq = pele->pdescr->surf * link->prct;
          area_tot += area_onaq;
          if (pzns->area > 0)
            prct = area_onaq / pzns->area;
          else
            prct = 0;

          if ((isnan(pzns->wat_bal->q[RSV_SOUT]) || isinf(pzns->wat_bal->q[RSV_SOUT])) || pzns->wat_bal->q[RSV_SOUT] > RECH_MAX_CAW)
            LP_error(fp, "Abnormal AQ recharge value calculated : %f for AQ cell %d layer %d and ZNS cell INTERN_ID %d ABS_ID %d", pzns->wat_bal->q[RSV_SOUT], pele->id[ABS_MSH], id_list->id_lay + 1, link->id, link->id + 1);

          q += (pzns->wat_bal->q[RSV_SOUT] * prct);

          link = link->next;
        }

        // On cible la position de la pbound destinée à recevoir la recharge du module
        for (k = 0; k < nb_bound; k++) {
          if (pele->phydro->psource[RECHARGE_AQ][k]->origin == ZNS_SOURCE_AQ) {
            pos_pbound = k;
            break;
          }
        }
        if (pos_pbound == CODE_AQ)
          LP_error(fp, "Aquifer recharge can't be calculated for element %d layer %d : dedicated pbound pointer not declared yet. %s-AQ link file might be missing in the command file.\n", pele->id[ABS_MSH], id_list->id_lay + 1, CAW_name_mod(linked_module));
      }

      break;
    }

    case HDERM_CAW: {
      if (pele->link[HDERM_AQ] != NULL) {
        q = 0.; // m3/s
        nb_rech++;
        area_tot += pele->pdescr->surf;
        link = (s_id_spa *)SPA_browse_all_struct(IDS_SPA, pele->link[HDERM_AQ], BEGINNING_SPA);

        while (link != NULL) {
          q += HYD_extract_q_element(pchyd_hderm, link->id, fp); // ici link->id est le GIS ID de l'élément riviere
          link = link->next;
        }

        for (k = 0; k < nb_bound; k++) {
          if (pele->phydro->psource[RECHARGE_AQ][k]->origin == HDERM_SOURCE_AQ) {
            pos_pbound = k;
            break;
          }
        }
        if (pos_pbound == CODE_AQ)
          LP_error(fp, "Aquifer recharge can't be calculated for element %d layer %d : dedicated pbound pointer not declared yet. %s-AQ link file might be missing in the command file.\n", pele->id[ABS_MSH], id_list->id_lay + 1, CAW_name_mod(linked_module));
      }
      break;
    }
    default:
      LP_error(fp, "Aquifer recharge calculations not implemented yet for module %s. Error in function %s, file %s at line %d.\n", CAW_name_mod(linked_module), __func__, __FILE__, __LINE__);
    } // Fin du switch

    if (pos_pbound != CODE_AQ) {
      q_tot += q;
      q = q / pele->pdescr->surf; // Passage en m/s
      ft_old = pele->phydro->psource[RECHARGE_AQ][pos_pbound]->pft->ft;
      pele->phydro->psource[RECHARGE_AQ][pos_pbound]->pft = TS_free_ts(pele->phydro->psource[RECHARGE_AQ][pos_pbound]->pft, fp);
      t_old = t - dt;
      pft = TS_create_function(t, -q);
      pele->phydro->psource[RECHARGE_AQ][pos_pbound]->pft = TS_create_function(t_old, ft_old);
      pele->phydro->psource[RECHARGE_AQ][pos_pbound]->pft = TS_secured_chain_fwd_ts(pele->phydro->psource[RECHARGE_AQ][pos_pbound]->pft, pft);
      pele->phydro->psource[RECHARGE_AQ][pos_pbound]->freq = VAR;
    }

    id_list = id_list->next;
  }

  // LP_printf(fp,"\t --> Number of aquifer elements with %s recharge : %d. Total surface of recharge application %f km2. Total discharge %f m3/s\n", modname,nb_rech,area_tot/1000000,q_tot);
  if (q_tot > RECH_MAX_CAW)
    AQ_print_elesource(pchar_aq->pcmsh, fp);
  free(modname); // DK free allocated modname variable after printing 31 01 2022
}

void CAW_cAQ_create_riv_aq(s_chyd *pchyd, s_carac_aq *pchar_aq, s_chronos_CHR *chronos, FILE *fp) {

  int r, e, f;
  int nb_faces, nb_reaches, nb_ele;
  s_ele_msh *pele_aq;
  s_face_msh *pface_aq;
  s_element_hyd *pele_hyd;
  s_reach_hyd *preach;
  double Z_max, surf_riv;
  s_id_io *id_list_tmp;
  s_ft *pft;
  nb_reaches = pchyd->counter->nreaches;
  for (r = 0; r < nb_reaches; r++) {
    preach = pchyd->p_reach[r];
    nb_ele = preach->nele;
    for (e = 0; e < nb_ele; e++) {
      pele_hyd = preach->p_ele[e];
      if (pele_hyd->face[X_MSH][ONE]->id_gw != NULL && pele_hyd->face[X_MSH][ONE]->id_gw->id_lay != CODE) {
        pele_aq = pchar_aq->pcmsh->p_layer[pele_hyd->face[X_MSH][ONE]->id_gw->id_lay]->p_ele[pele_hyd->face[X_MSH][ONE]->id_gw->id];
        id_list_tmp = IO_create_id(r, e);
        pele_aq->id_riv = IO_secured_chain_fwd_id(pele_aq->id_riv, id_list_tmp);
        Z_max = pele_hyd->face[X_MSH][ONE]->geometry->p_pointsAbscZ[0]->z + pele_hyd->face[X_MSH][TWO]->geometry->p_pointsAbscZ[0]->z / 2;
        surf_riv = pele_hyd->length * preach->width;

        pele_aq->loc = RIV_MSH;
        pface_aq = pele_aq->face[Z_MSH][TWO];
        pface_aq->loc = RIV_MSH;

        if (pele_aq->phydro->pbound == NULL) { // NF 27/3/2017 Les autres conditions limites aquifer sont toujours prioritaires sur la condition de conductance riviere
          AQ_define_cauchy_coupled(pchar_aq, pele_aq, Z_max, surf_riv, pele_hyd->face[X_MSH][ONE]->hyporeic_sets[EP_HYD], pele_hyd->face[X_MSH][ONE]->hyporeic_sets[TZ_HYD], fp);
        } else if (fabs(pele_hyd->face[X_MSH][ONE]->hyporeic_sets[TZ_HYD] - CODE_TS) > EPS) {
          AQ_define_pdescr_cauchy(pele_aq, surf_riv, pele_hyd->face[X_MSH][ONE]->hyporeic_sets[EP_HYD], pele_hyd->face[X_MSH][ONE]->hyporeic_sets[TZ_HYD], fp);
        }

        pele_hyd->center->hydro->Qlim = preach->Qlim * pele_hyd->length / HYD_LIN_QLIM; // NF 3/10/2017 L'unite de definition du qlim network est en m3/S/km (HYD_LIN_QLIM exprime cette unite
        pele_aq->phydro->Qlim = pele_hyd->center->hydro->Qlim;
        //        LP_printf(fp,"\t -->TEST In func: %s, nreach: %d  pele_hyd: %d pele_aq: %d , qlim: %lf\n", __func__ ,r, e, pele_hyd->id, pele_aq->id , pele_hyd->center->hydro->Qlim );

        pele_aq->modif[MODIF_CALC_AQ] = NO_LIM_AQ;
        pele_aq->modif[MODIF_MB_AQ] = NO_LIM_AQ;
        pele_aq->phydro->pbound->pft = TS_free_ts(pele_aq->phydro->pbound->pft, fp);
        pele_aq->phydro->pbound->freq = VAR;
        pft = TS_create_function(chronos->t[CUR_CHR] + chronos->dt, pele_aq->phydro->h[T]);
        pele_aq->phydro->pbound->pft = TS_create_function(chronos->t[CUR_CHR], pele_aq->phydro->h[T]);
        pele_aq->phydro->pbound->pft = TS_secured_chain_fwd_ts(pele_aq->phydro->pbound->pft, pft);
        pele_aq->phydro->pbound->qlim = pele_aq->phydro->Qlim; // NF 3/10/2017 Otherwise qlim de la boundary non initialise!
        // pele_hyd->center->hydro->Z[PIC_HYD]=pele_aq->phydro->h[T];//FB 16/04/2018
        pele_hyd->center->hydro->Z[PIC_HYD] = CODE_TS; // FB 16/04/2018 River water level is initialized to CODE_TS instead of pele_aq->phydro->h[T]
      }
    }
  }
}

void CAW_cAQ_simplify_cauchy(s_carac_aq *pchar_aq, s_chyd *pchyd, FILE *fp) {

  s_id_io *cauchy, *id_riv, *id_tmp = NULL;
  s_ele_msh *pele_aq;
  s_face_msh *pface_aq;
  s_element_hyd *pele_hyd;
  int length_id;
  double check_length, length;
  cauchy = pchar_aq->pcmsh->pcount->pcount_hydro->elebound[CAUCHY];
  cauchy = IO_browse_id(cauchy, BEGINNING_TS);
  while (cauchy != NULL) {
    pele_aq = pchar_aq->pcmsh->p_layer[cauchy->id_lay]->p_ele[cauchy->id];
    // LP_printf(fp,"loc %s \n",MSH_name_position_cell(pele_aq->loc,fp));
    if (pele_aq->loc == RIV_MSH) {
      id_riv = IO_browse_id(pele_aq->id_riv, BEGINNING_TS);
      length_id = IO_length_id(pele_aq->id_riv);
      if (length_id > 1) {
        if (HYD_check_reach(pele_aq->id_riv, pchyd, fp) == YES) {
          id_riv = IO_browse_id(pele_aq->id_riv, BEGINNING_TS);
          check_length = -9999;
          while (id_riv != NULL) {
            pele_hyd = pchyd->p_reach[id_riv->id_lay]->p_ele[id_riv->id];
            length = pele_hyd->length;
            if (length > check_length) {
              if (id_tmp != NULL)
                id_tmp = IO_free_id_serie(id_tmp, fp);
              id_tmp = IO_create_id(id_riv->id_lay, id_riv->id);
              check_length = length;
            }
            id_riv = id_riv->next;
          }
          pele_aq->id_riv = IO_free_id_serie(pele_aq->id_riv, fp);
          pele_aq->id_riv = IO_create_id(id_tmp->id_lay, id_tmp->id);
          id_tmp = IO_free_id_serie(id_tmp, fp);
          // LP_printf(fp,"ele_aq %d ele_hyd reach %d ele %d max length %f\n",pele_aq->id[ABS_MSH],pele_aq->id_riv->id_lay,pele_aq->id_riv->id,check_length);
        } else {
          id_tmp = HYD_get_id_riv_down(pchyd, pele_aq->id_riv, length_id, fp);
          pele_aq->id_riv = IO_free_id_serie(pele_aq->id_riv, fp);
          pele_aq->id_riv = IO_create_id(id_tmp->id_lay, id_tmp->id);
          // LP_printf(fp,"ele_aq %d ele_reach AVAL reach %d ele %d reach_id_gis %d ele_hyd GIS %d\n",pele_aq->id[ABS_MSH],pele_aq->id_riv->id_lay,pele_aq->id_riv->id,pchyd->p_reach[pele_aq->id_riv->id_lay]->id[GIS_HYD],pchyd->p_reach[pele_aq->id_riv->id_lay]->p_ele[pele_aq->id_riv->id]->id[GIS_HYD]);
          id_tmp = IO_free_id_serie(id_tmp, fp);
        }
      }
    }
    cauchy = cauchy->next;
  }
}

// NF 3/10/2017 This fonction refreshes the values of Cauchy BC between two time step before starting picard iteration
void CAW_cAQ_refresh_cauchy(s_chronos_CHR *chronos, s_chyd *pchyd, s_carac_aq *pchar_aq, FILE *fp) {

  int r, e, f;
  int nb_faces, nb_reaches, nb_ele;
  s_ele_msh *pele_aq;
  s_face_msh *pface_aq;
  s_element_hyd *pele_hyd;
  s_ft *pft, *ptmp, *pft_old;
  s_reach_hyd *preach;
  double h_old;
  s_id_io *cauchy, *id_riv;
  double length, ft, t_old, ft_old, t, deltat_calc;
  t = chronos->t[CUR_CHR];
  deltat_calc = chronos->dt;
  // BL faire boucle sur elbound_cauchy, regarder les ele=RIV_MSH et sur ces ele chercher les ele_hyd pour refresh cauchy !!!
  cauchy = pchar_aq->pcmsh->pcount->pcount_hydro->elebound[CAUCHY];
  cauchy = IO_browse_id(cauchy, BEGINNING_TS);
  while (cauchy != NULL) {
    pele_aq = pchar_aq->pcmsh->p_layer[cauchy->id_lay]->p_ele[cauchy->id];

    if ((pele_aq->loc == RIV_MSH)) {
      pele_aq->phydro->Qlim = pele_aq->phydro->pbound->qlim; // NF 3/10/2017 besoin d'etre reinitialise apres les iterations de Picart qui modifient cette valeur
    }
    cauchy = cauchy->next;
  }
}

void CAW_cAQ_init_z_riv(s_carac_aq *pchar_aq, s_chyd *pchyd, FILE *fp) {
  s_element_hyd *pele_riv;
  s_hydro_hyd *phyd;
  s_ele_msh *pele_aq;
  s_id_io *id_riv, *cauchy;
  s_face_msh *pface_aq;

  cauchy = pchar_aq->pcmsh->pcount->pcount_hydro->elebound[CAUCHY];
  cauchy = IO_browse_id(cauchy, BEGINNING_TS);
  while (cauchy != NULL) {
    pele_aq = pchar_aq->pcmsh->p_layer[cauchy->id_lay]->p_ele[cauchy->id];
    pface_aq = pele_aq->face[Z_MSH][TWO];
    if (pface_aq->loc == RIV_MSH) {
      id_riv = IO_browse_id(pele_aq->id_riv, BEGINNING_TS);
      while (id_riv != NULL) {
        pele_riv = pchyd->p_reach[id_riv->id_lay]->p_ele[id_riv->id];
        phyd = pele_riv->center->hydro;
        if (phyd->Z[PIC_HYD] == CODE_TS) // FB 16/04/2016 If river water level is CODE_TS, then we set it at the initial piezometric head. Normally phyd->Z[PIC_HYD] is not CODE_TS as it is initialized upstream and downstream a reach with the values in network_musk file. Those values are then linearly interpolated in order to initialize each element.
          phyd->Z[PIC_HYD] = pele_aq->phydro->h[T];
        id_riv = id_riv->next;
      }
    }
    cauchy = cauchy->next;
  }
}

/*Calculates stream-aquifer flux after Libaq. This is the value used to solve the linear system of Libaq.*/
void CAW_calculate_aq_riv_flux(double dt, double t, s_carac_aq *pchar_aq, FILE *fp) {
  s_id_io *cauchy;
  s_ele_msh *pele_aq;
  s_face_msh *pface_aq;
  double h_riv0, h_riv1;
  s_ft *pft;

  cauchy = pchar_aq->pcmsh->pcount->pcount_hydro->elebound[CAUCHY];
  cauchy = IO_browse_id(cauchy, BEGINNING_TS);
  while (cauchy != NULL) {
    pele_aq = pchar_aq->pcmsh->p_layer[cauchy->id_lay]->p_ele[cauchy->id];
    pface_aq = pele_aq->face[Z_MSH][TWO];
    if (pface_aq->loc == RIV_MSH) {
      pft = TS_browse_ft(pele_aq->phydro->pbound->pft, BEGINNING_TS);
      // LP_printf(fp,"calculate_aq_riv_flux (apres resol syst): idabs=%d h_riv1=%f\n",pele_aq->id[ABS_MSH],h_riv0);
      h_riv0 = TS_interpolate_ts(t - dt, pft);
      // LP_printf(fp,"calculate_aq_riv_flux (apres resol syst): idabs=%d h_riv1=%f\n",pele_aq->id[ABS_MSH],h_riv0);
      pft = TS_browse_ft(pele_aq->phydro->pbound->pft, BEGINNING_TS);
      h_riv1 = TS_interpolate_ts(t, pft);
      // LP_printf(fp,"calculate_aq_riv_flux (apres resol syst): idabs=%d h_riv1=%f\n",pele_aq->id[ABS_MSH],h_riv1);
      if (pele_aq->modif[MODIF_CALC_AQ] == NO_LIM_AQ)
        pface_aq->phydro->pcond->q = pface_aq->phydro->pcond->conductance * (pele_aq->phydro->h[PIC] - h_riv1);
      else
        pface_aq->phydro->pcond->q = pele_aq->phydro->Qlim;
      //	LP_printf(fp,"q=%f\n",pface_aq->phydro->pcond->q);
    }
    cauchy = cauchy->next;
  }
}

/*Sets stream-aquifer flux in mass balance structure of Libaq before the calculation in Libhyd. */
void CAW_update_AQ_MB(s_carac_aq *pchar_aq, int *iit, FILE *fp) {
  s_id_io *cauchy;
  s_ele_msh *pele_aq;
  s_face_msh *pface_aq;
  int pos = CODE_TS;

  cauchy = pchar_aq->pcmsh->pcount->pcount_hydro->elebound[CAUCHY];
  cauchy = IO_browse_id(cauchy, BEGINNING_TS);
  while (cauchy != NULL) {
    pele_aq = pchar_aq->pcmsh->p_layer[cauchy->id_lay]->p_ele[cauchy->id];
    pface_aq = pele_aq->face[Z_MSH][TWO];
    if (pface_aq->loc == RIV_MSH) {
      if (*iit == 1) {
        pos = INI_AQ;
      } else {
        pos = END_AQ;
      }
      pele_aq->phydro->pmb->pbound[CAUCHY][pos] = -pface_aq->phydro->pcond->q; // - because in mass balance exfiltration is negative and infiltration is positive (which is the inverse of the convention during the calculation)
    }
    cauchy = cauchy->next;
  }
}

/*Checks connexion status between the aquifer and the river and corrects the LHS if needed. Not used for TOP_MSH cells*/
void CAW_cAQ_check_cauchy_riv(double dt, double t, s_carac_aq *pchar_aq, s_chyd *pchyd, FILE *fp) {

  s_id_io *cauchy;
  s_ele_msh *pele_aq;
  s_face_msh *pface_aq;
  s_gc *pgc;
  double theta;
  theta = pchar_aq->settings->general_param[THETA];
  pgc = pchar_aq->calc_aq;
  cauchy = pchar_aq->pcmsh->pcount->pcount_hydro->elebound[CAUCHY];
  cauchy = IO_browse_id(cauchy, BEGINNING_TS);
  while (cauchy != NULL) {
    pele_aq = pchar_aq->pcmsh->p_layer[cauchy->id_lay]->p_ele[cauchy->id];
    pface_aq = pele_aq->face[Z_MSH][TWO];
    if (pface_aq->loc == RIV_MSH)
      CAW_cAQ_check_qlim_riv(pele_aq, pgc, pchyd, dt, theta, t, fp);
    cauchy = cauchy->next;
  }
}

/*cette fonction permet de vérifier après le calcul de la charge en aquifère si le débit échangé avec la rivière est bien supérieur à Qlim*/
void CAW_cAQ_check_qlim_riv(s_ele_msh *pele_aq, s_gc *pgc, s_chyd *pchyd, double dt, double theta, double t, FILE *fp) {
  s_element_hyd *pele_riv;
  s_hydro_hyd *phyd, *phyd_face;
  int length_id, nb_qinf = 0, nb_qsup = 0;
  s_id_io *id_riv;
  s_face_msh *pface_aq;
  int id_abs, id_mat6, nb_inf, modif_old;
  double q, length, ft, Q_sout, Z_bot;
  double h_riv0, h_riv1, Q_inraq;
  s_ft *pft, *ptmp, *pft_old;
  int dry = NO_TS, k;

  pface_aq = pele_aq->face[Z_MSH][TWO];

  /*Update in libaq of river water level*/
  pft = TS_browse_ft(pele_aq->phydro->pbound->pft, BEGINNING_TS);
  // LP_printf(fp,"riv0 = %d\n",h_riv0);
  h_riv0 = TS_interpolate_ts(t - dt, pft);
  // LP_printf(fp,"riv0 = %d\n",h_riv0);
  length = 0; // FB 11/04/2018 Il faut initialiser
  ft = 0;     // FB 11/04/2018 Il faut initialiser
  Q_sout = 0;
  id_riv = IO_browse_id(pele_aq->id_riv, BEGINNING_TS);
  if (pchyd != NULL) {
    while (id_riv != NULL) // NF 3/10/2017 Calcul de la cote de l'eau sur une maille sout
    {
      pele_riv = pchyd->p_reach[id_riv->id_lay]->p_ele[id_riv->id];
      length += pele_riv->length;
      phyd = pele_riv->center->hydro;
      ft += phyd->Z[PIC_HYD] * pele_riv->length;
      Q_sout += phyd->Q[PIC_HYD];
      id_riv = id_riv->next;
    }
  } else { // NF 30/9/2017 Cas TOP_MSH
    ft = h_riv1;
    length = 1.;
    LP_error(fp, "cawaqs%4.2f %s in %s l.%d : We shouldn't be here. AQ ele GIS_ID %d, ABS_ID %d should be a RIV_MSH cell and not a TOP_MSH cell\n", NVERSION_CAW, __func__, __FILE__, __LINE__, pele_aq->id[GIS_MSH], pele_aq->id[ABS_MSH]);
  } // end if(pchyd!=NULL)
  pele_aq->phydro->pbound->pft = TS_free_ts(pele_aq->phydro->pbound->pft, fp);
  pele_aq->phydro->pbound->pft = TS_create_function(t - dt, h_riv0);
  pft = TS_create_function(t, ft / length); // NF 30/9/2017 je ne suis pas certain de suivre le calcul de length pour le cas RIV_MSH
  pele_aq->phydro->pbound->pft = TS_secured_chain_fwd_ts(pele_aq->phydro->pbound->pft, pft);
  h_riv1 = ft / length;

  modif_old = pele_aq->modif[MODIF_CALC_AQ];

  id_riv = IO_browse_id(pele_aq->id_riv, BEGINNING_TS);
  while (id_riv != NULL) {
    pele_riv = pchyd->p_reach[id_riv->id_lay]->p_ele[id_riv->id];
    if (fabs(pele_riv->center->hydro->Z[PIC_HYD] - pele_riv->center->description->Zbottom) < EPS_TS) {
      dry = YES_TS;
      pele_aq->modif[MODIF_CALC_AQ] = QLIM_AQ;
      pele_aq->phydro->Qlim = 0;
      pface_aq->phydro->pcond->q = 0;
      // LP_printf(fp,"DRY idabs=%d\n",pele_aq->id[ABS_MSH]);//FB 11/04/2018 debug
    }
    id_riv = id_riv->next;
  } // end while(id_riv!=NULL)

  if (dry != YES_TS) {
    // Q_inraq=pface_aq->phydro->pcond->conductance*(theta*(pele_aq->phydro->h[PIC]-h_riv1)+(1-theta)*(pele_aq->phydro->h[T]-h_riv0));
    Q_inraq = pface_aq->phydro->pcond->conductance * (pele_aq->phydro->h[PIC] - h_riv1); // FB 11/03/18 Cauchy term implicit
    if (Q_inraq < pele_aq->phydro->Qlim) {
      pele_aq->modif[MODIF_CALC_AQ] = QLIM_AQ;
      pface_aq->phydro->pcond->q = pele_aq->phydro->Qlim;
    } else {
      pele_aq->modif[MODIF_CALC_AQ] = NO_LIM_AQ;
      pface_aq->phydro->pcond->q = Q_inraq;
    }
    if (-pface_aq->phydro->pcond->q > Q_sout) {
      pface_aq->phydro->pcond->q = -Q_sout;
      if (pele_aq->modif[MODIF_CALC_AQ] == QLIM_AQ)
        pele_aq->phydro->Qlim = -Q_sout;
    }

  } // end if(dry!=YES_TS)

  if (pele_aq->modif[MODIF_CALC_AQ] == QLIM_AQ && modif_old == NO_LIM_AQ) // Cauchy deactivated in LHS
  {
    id_abs = pele_aq->id[ABS_MSH];
    id_mat6 = pgc->mat4[id_abs];
    AQ_correct_LHS(pgc, id_mat6, theta, dt, pface_aq, COR_NEG_AQ);
  } else if (pele_aq->modif[MODIF_CALC_AQ] == NO_LIM_AQ && modif_old == QLIM_AQ) // Cauchy activated in LHS
  {
    id_abs = pele_aq->id[ABS_MSH];
    id_mat6 = pgc->mat4[id_abs];
    AQ_correct_LHS(pgc, id_mat6, theta, dt, pface_aq, COR_POS_AQ);
  }

  pele_aq->modif[MODIF_MB_AQ] = pele_aq->modif[MODIF_CALC_AQ];

  if (pele_aq->phydro->Qlim > 0)
    LP_warning(fp, "Qlim> 0 idabs=%d Qlim=%f\n", pele_aq->id[ABS_MSH], pele_aq->phydro->Qlim); // FB 11/04/2018 debug
}

void CAW_cAQ_print_nappe_riv(s_carac_aq *pchar_aq, s_chyd *pchyd, FILE *flog) {

  int i, j, nriv;
  int nlayer, nele, nsize;
  s_id_io *id_list;
  s_layer_msh *player;
  s_ele_msh *pele;
  s_element_hyd *pele_riv;
  char *name;
  FILE *fp;
  s_cmsh *pcmsh;
  nriv = 0;
  pcmsh = pchar_aq->pcmsh;
  nlayer = pchar_aq->pcmsh->pcount->nlayer;

  name = (char *)malloc((strlen(getenv("RESULT")) + ALLOCSCD_LP) * sizeof(char));
  sprintf(name, "%s/lien_nappe_riv.txt", getenv("RESULT"));
  fp = fopen(name, "w");
  free(name);
  for (i = 0; i < nlayer; i++) {
    player = pcmsh->p_layer[i];
    nele = player->nele;
    for (j = 0; j < nele; j++) {
      pele = player->p_ele[j];
      if (pele->loc == RIV_MSH && pele->id_riv != NULL) {
        id_list = IO_browse_id(pele->id_riv, BEGINNING_TS);
        while (id_list != NULL) {
          nriv++;
          // pele_riv=pchyd->p_reach[pele->id_riv->id_lay]->p_ele[pele->id_riv->id];
          pele_riv = pchyd->p_reach[id_list->id_lay]->p_ele[id_list->id]; // to print for the whole river network
          fprintf(fp, "%d %d %d 1.000\n", nriv, pele->id[ABS_MSH], pele_riv->id[ABS_HYD] + 1);
          id_list = id_list->next;
        }
      }
    }
  }
  fclose(fp);
}
