/*-------------------------------------------------------------------------------
 *
 * SOFTWARE NAME: cawaqs
 * FILE NAME: manage_link_master.c
 *
 * CONTRIBUTORS: Nicolas GALLOIS, Baptiste LABARTHE, Shuaitao WANG,
 *               Agnès RIVIERE, Deniz KILIC, Mathias MAILLOT,
 *               Fulvia BARATELLI, Pierre GUILLOU, Emmanuel LEDOUX, Nicolas FLIPO
 *
 * SOFTWARE BRIEF DESCRIPTION: Physically based coupled surface–subsurface
 * model for joint water, matter and energy flux balances and flow dynamics
 * simulation within all compartments of a hydrosystem (sub-surface, hydraulic
 * network, vadose zone, aquifer system and stream-aquifer exchanges).
 *
 * Software developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the cawaqs Software.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Software and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <time.h>
#include <stdarg.h>
#include <sys/time.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <malloc.h>
#include <math.h>
#include <libprint.h>
#ifdef OMP
#include <omp.h>
#endif
#include <time_series.h>
#include "libpc.h"
#include "IO.h"
#include "GC.h"
#include "reservoir.h"
#include "spa.h"
#include "FP.h"
#include "NSAT.h"
#include "CHR.h"
#include "HYD.h"
#include "MSH.h"
#include "AQ.h"
#include "WET.h"
#include "TTC_coupled.h"
#include "SEB.h"
#include "CAW.h"
#include "global_CAWAQS.h"

/**
 * \fn void CAW_initialize_TTC_carac_species(s_simul_cawaqs*, int, int, int, int, FILE*)
 * \brief Initializes TTC_carac attributes but also ttc_species each time a new species
 *        is being declared in input.y
 * \return -
 */
void CAW_initialize_TTC_carac_species(s_simul_cawaqs *psim, int modname, int nb_species, int iappli, int nstep, FILE *flog) {

  int nele, id_species = nb_species - 1; // Fetching the specie ID

  if (psim->pcarac_ttc[modname] == NULL) {
    psim->pcarac_ttc[modname] = TTC_init_carac();

    // Putting TTC and CaWaQS regimes in adequacy
    if (psim->pset_caw->calc_state == TRANSIENT)
      psim->pcarac_ttc[modname]->regime = TRANSIENT_TTC;
    else
      psim->pcarac_ttc[modname]->regime = STEADY_TTC;

    /* NG : 02/10/2021 : Forcing TTC calculation to full implicit mode, otherwise transport equation is not properly resolved.
                         CaWaQS theta value is not considered. Do not change !   */
    psim->pcarac_ttc[modname]->theta = THET_TTC;
    LP_warning(flog, "CaWaQS%4.2f: CaWaQS theta (%4.2f) value ignored. Full-implicit mode forced for %s transport calculations.\n", NVERSION_CAW, psim->general_param[THETA_AQ], CAW_name_mod(modname), psim->pcarac_ttc[modname]->theta);

    // Filling TTC counters
    switch (modname) {

    case HYD_CAW: {
      nele = psim->pchyd->counter->nele_tot;
      psim->pcarac_ttc[modname]->count[NVOIS_ELE_TTC] = 0;
      break;
    }

    case AQ_CAW: {
      nele = psim->pcarac_aq->pcmsh->pcount->nele;
      psim->pcarac_ttc[modname]->count[NVOIS_ELE_TTC] = CAW_calc_TTC_nvois_ele_aq(psim->pcarac_aq->pcmsh, flog);
      break;
    }

    case HDERM_CAW: {
      nele = psim->pchyd_hderm->counter->nele_tot;
      psim->pcarac_ttc[modname]->count[NVOIS_ELE_TTC] = 0;
      break;
    }

    default:
      LP_error(flog, "In CaWaQS%4.2f, File %s in %s line %d : No coupling between CaWaQS module %s and TTC library.\n", NVERSION_CAW, __FILE__, __func__, __LINE__, CAW_name_mod(modname));
      break;
    }

    psim->pcarac_ttc[modname]->count[NELE_TTC] = nele;
    psim->pcarac_ttc[modname]->count[NBC_TTC] = 0;

    // Allocating specie table
    psim->pcarac_ttc[modname]->count[NSPECIES_TTC] = nb_species;
    psim->pcarac_ttc[modname]->p_species = (s_species_ttc **)malloc(nb_species * sizeof(s_species_ttc *));
    psim->pcarac_ttc[modname]->p_link = (s_link_ttc **)malloc(nb_species * sizeof(s_link_ttc *));

    if (psim->pcarac_ttc[modname]->p_species == NULL || psim->pcarac_ttc[modname]->p_link == NULL)
      LP_error(flog, "In CaWaQS%4.2f : Error in file %s, in function %s at line %d : Bad memory allocation.\n", NVERSION_CAW, __FILE__, __func__, __LINE__);
  } else // Re-allocations in case of a new specie
  {
    // Number of specie update
    psim->pcarac_ttc[modname]->count[NSPECIES_TTC] = nb_species;
    psim->pcarac_ttc[modname]->p_species = (s_species_ttc **)realloc(psim->pcarac_ttc[modname]->p_species, nb_species * sizeof(s_species_ttc *));
    psim->pcarac_ttc[modname]->p_link = (s_link_ttc **)realloc(psim->pcarac_ttc[modname]->p_link, nb_species * sizeof(s_link_ttc *));
  }

  // Initializing the newly allocated specie structure
  psim->pcarac_ttc[modname]->p_species[id_species] = TTC_init_species(psim->pcarac_ttc[modname]->count[NELE_TTC], id_species, flog);

  psim->pcarac_ttc[modname]->p_species[id_species]->name = psim->pspecies[id_species]->name;
  psim->pcarac_ttc[modname]->p_species[id_species]->pset->iappli_gc = iappli;
  psim->pcarac_ttc[modname]->p_species[id_species]->pgc->sw_int[APPL_NB] = iappli;

  psim->pcarac_ttc[modname]->p_link[id_species] = TTC_init_link(psim->pcarac_ttc[modname], psim->pcarac_ttc[modname]->p_species[id_species], psim->pcarac_ttc[modname]->count[NELE_TTC]);

  // Filling specie type
  if (psim->pspecies[id_species]->type == HEAT_CAW)
    psim->pcarac_ttc[modname]->p_species[id_species]->pset->type = HEAT_TTC;
  else
    psim->pcarac_ttc[modname]->p_species[id_species]->pset->type = SOLUTE_TTC;

  // Filling specie media_type
  if (modname == HYD_CAW || modname == HDERM_CAW)
    psim->pcarac_ttc[modname]->p_species[id_species]->pset->media_type = WATER_TTC;
  else
    psim->pcarac_ttc[modname]->p_species[id_species]->pset->media_type = POROUS_TTC;

  psim->pcarac_ttc[modname]->pchronos = CHR_copy_chronos(psim->chronos);
}

/**
 * \fn void CAW_initialize_TTC_all(s_simul_cawaqs*, int, FILE*)
 * \brief General function launching TTC initialization for all species for a given CaWaQS module
               (boundaries initialization, distances between elements, neighbours tables, etc.)
 * \return -
 */
void CAW_initialize_TTC_all(s_simul_cawaqs *psim, int modname, int nstep, FILE *flog) {

  LP_printf(flog, "Entering TTC initialization procedure for %s module...\n", CAW_name_mod(modname));

  if (modname == AQ_CAW) {
    // NG : 17/06/2023 : First two functions should be conditional to effective state of aquitard/riverbed declaration in .COMM.
    CAW_init_TTC_aquitard_cells(psim->pcarac_ttc[modname], psim->poutputs);
    CAW_init_TTC_riverbed_cells(psim->pcarac_ttc[modname], psim->poutputs);

    // Aquifer cells bounds initialization
    CAW_init_TTC_face_bound(psim->pcarac_aq->pcmsh, flog);
    CAW_init_TTC_ele_nbound(psim->pcarac_aq->pcmsh, flog);
    psim->pcarac_ttc[modname]->count[NBC_TTC] = CAW_init_TTC_calc_nbc(psim->pcarac_aq->pcmsh, flog);
    // LP_printf(flog,"Total number of bounds : %i\n",psim->pcarac_ttc[modname]->count[NBC_TTC]); // NG check
  }

  CAW_fill_TTC_param_base_all_species(psim, modname, psim->nb_species, psim->poutputs);
  CAW_fill_TTC_neigh_all_species(psim, modname, nstep, psim->poutputs);
  CAW_alloc_TTC_u_dist_deltaL_all_species(psim, modname, psim->poutputs);
  CAW_build_TTC_var_all_species(psim, modname, psim->poutputs);

  LP_printf(flog, "Filling TTC parameters, geometry and initial values for all species done.\n");
}

/**
 * \fn void CAW_fill_ttc_param_base_all_species(s_simul_cawaqs*, int, int, FILE*)
 * \brief Fill TTC param base struct for all species, for a given CaWaQS module
 * \return -
 */
void CAW_fill_TTC_param_base_all_species(s_simul_cawaqs *psim, int modname, int nb_species, FILE *flog) {

  int p;
  s_carac_ttc *pcarac = psim->pcarac_ttc[modname];

  for (p = 0; p < nb_species; p++)
    CAW_fill_TTC_param_base_one_species(psim, pcarac->count[NELE_TTC], modname, p, flog);
}

/**
 * \fn void CAW_fill_TTC_param_base_one_species(s_species_ttc*, int, int, int, FILE*)
 * \brief Fill TTC param base struct for one single species, for a given CaWaQS module
 * \return -
 */
void CAW_fill_TTC_param_base_one_species(s_simul_cawaqs *psim, int nele, int modname, int id_species, FILE *flog) {

  int n, npa, naquitard, nriverbed;
  s_carac_ttc *pcarac_ttc = psim->pcarac_ttc[modname];
  s_chyd *pchyd = psim->pchyd;
  s_species_ttc *pspecies = pcarac_ttc->p_species[id_species];
  s_link_ttc *plink = pcarac_ttc->p_link[id_species];
  s_cmsh *pcmsh;

  LP_printf(flog, "Filling TTC param base for module %s, specie %s (ID %d)\n", CAW_name_mod(modname), psim->pspecies[id_species]->name, id_species);

  if (modname == HYD_CAW || modname == HDERM_CAW) {
    for (n = 0; n < nele; n++) {
      pspecies->p_param_ttc[n]->param_syst[POROSITY_TTC] = 1.; // Default values for all river calculation elements
      pspecies->p_param_ttc[n]->param_syst[DISPERSIVITY_TTC] = 0.;

      switch (pspecies->pset->type) {
      case HEAT_CAW: {
        pspecies->p_param_ttc[n]->param_thermic[WATER_TTC][RHO_TTC] = RHOW_TTC;
        pspecies->p_param_ttc[n]->param_thermic[WATER_TTC][HEAT_CAP_TTC] = HEAT_CAPW_TTC;
        pspecies->p_param_ttc[n]->param_thermic[WATER_TTC][LAMBDA_TTC] = LAMBDAW_TTC;
        break;
      }
      case SOLUTE_CAW:
        pspecies->p_param_ttc[n]->diff_mol_solute = Simul->chem_param[modname][DIFF_MOL_CAW][id_species];
        break;
      }
    }
  } else if (modname == AQ_CAW) {

    pcmsh = psim->pcarac_aq->pcmsh;
    naquitard = pcarac_ttc->count[NAQUITARD_TTC];
    nriverbed = pcarac_ttc->count[NRIVERBED_TTC];

    for (n = 0; n < nele; n++) {
      switch (pspecies->pset->type) {
      case HEAT_CAW: {
        pspecies->p_param_ttc[n]->param_thermic[WATER_TTC][RHO_TTC] = RHOW_TTC;
        pspecies->p_param_ttc[n]->param_thermic[WATER_TTC][HEAT_CAP_TTC] = HEAT_CAPW_TTC;
        pspecies->p_param_ttc[n]->param_thermic[WATER_TTC][LAMBDA_TTC] = LAMBDAW_TTC;
        break;
      }
      case SOLUTE_CAW:
        pspecies->p_param_ttc[n]->diff_mol_solute = Simul->chem_param[modname][DIFF_MOL_CAW][id_species];
        break;
      }
    }

    /* NG : 06/04/2023. At this point, read parameters in COMM file have been stored in libmesh.
     * Here, we've lost track of which parameter has been actually declared/read.
     * It's making the assumption that all five params are declared which is not always the case (ex. for SOLUTE).
     * --> At least, I'm making therm param conditional to HEAT specie type only. */

    // Initializing TTC base parameters (POROSITY and DISPERSIVITY)
    for (npa = 1; npa < NPARAM_SYST_TTC; npa++) {
      CAW_build_TTC_param_syst_aq(pcmsh, pspecies, npa, flog);
      LP_printf(flog, "TTC system parameter %s linked for the aquifer system.\n", TTC_param_syst(npa));
    }

    if (psim->pspecies[id_species]->type == HEAT_CAW) { // NG : 06/04/2023 : adding HEAT if case
      // Initializing THERM parameters (LAMBDA, HEAT CAPACITY, RHO)
      for (npa = 0; npa < NPARAM_THERM_TTC; npa++) {
        CAW_build_TTC_param_therm(pcmsh, pspecies, SOLID_TTC, npa, flog);
        LP_printf(flog, "TTC thermic parameter %s linked for the aquifer system.\n", TTC_thermic_param(npa));
      }
    }

    if (pspecies->pset->riverbed == AQUITARD_ON_TTC) {
      CAW_build_TTC_neighbours_riverbed_aq(pchyd, pspecies, nriverbed, npa, flog);
    }

    for (npa = 0; npa < NPARAM_AQUITARD_TTC; npa++) {

      if (pspecies->pset->aquitard == AQUITARD_ON_TTC) {
        CAW_build_TTC_param_aquitard_aq(pcmsh, pspecies, npa, flog);
        LP_printf(flog, "Aquitard parameter %s linked for the aquifer system.\n", TTC_param_aquitard(npa));
      }

      if (pspecies->pset->riverbed == AQUITARD_ON_TTC) {
        CAW_build_TTC_param_riverbed_aq(pchyd, pspecies, nriverbed, npa, flog);
        LP_printf(flog, "Riverbed parameter %s linked for the aquifer system.\n", TTC_param_aquitard(npa));
      }
    }

    if (pspecies->pset->aquitard == AQUITARD_ON_TTC) {
      CAW_build_TTC_var_aquitard_aq(pcmsh, pspecies, flog);
    }
    if (pspecies->pset->riverbed == AQUITARD_ON_TTC) {
      CAW_build_TTC_var_riverbed_aq(pcmsh, pchyd, pspecies, nriverbed, flog);
    }
  } else {
    LP_error(flog, "CaWaQS%4.2f, File %s in %s line %d : No coupling between CaWaQS module %s and TTC library.\n", NVERSION_CAW, __FILE__, __func__, __LINE__, CAW_name_mod(modname));
  }
}

/**
 * \fn void CAW_alloc_TTC_u_dist_deltaL_all_species(s_simul_cawaqs*, int, FILE*)
 * \brief Launches CAW_alloc_TTC_u_dist_deltaL_one_species() function for all species
 * \return -
 */
void CAW_alloc_TTC_u_dist_deltaL_all_species(s_simul_cawaqs *psim, int modname, FILE *flog) {

  int p, nb_species = psim->nb_species;
  s_carac_ttc *pcarac = psim->pcarac_ttc[modname];
  s_species_ttc *pspecies;
  s_link_ttc *plink;

  for (p = 0; p < nb_species; p++) {
    pspecies = pcarac->p_species[p];
    plink = pcarac->p_link[p];
    CAW_alloc_TTC_u_dist_deltaL_one_species(psim, pspecies, plink, pcarac->count[NELE_TTC], modname, p, flog);
  }
}

/**
 * \fn void CAW_alloc_TTC_u_dist_deltaL_one_species(s_simul_cawaqs*, s_link_ttc*, int, int, int, FILE*)
 * \brief Launches CAW_alloc_TTC_u_dist_deltaL_one_species() function depending on the CaWaQS module its called for
 * \return -
 */
void CAW_alloc_TTC_u_dist_deltaL_one_species(s_simul_cawaqs *psim, s_species_ttc *pspecies, s_link_ttc *plink, int nele, int modname, int id_species, FILE *flog) {

  s_cmsh *pcmsh;

  switch (modname) {

  case HYD_CAW: {
    CAW_alloc_TTC_u_dist_one_species_riv(psim->pcarac_ttc[modname], psim->pchyd, nele, id_species, flog);
    psim->pcarac_ttc[modname]->dVdt = TTC_allocate_dvdt(nele);
    break;
  }
  case HDERM_CAW: {
    CAW_alloc_TTC_u_dist_one_species_riv(psim->pcarac_ttc[modname], psim->pchyd_hderm, nele, id_species, flog);
    break;
  }
  case AQ_CAW: {
    pcmsh = psim->pcarac_aq->pcmsh;
    // DK AR 13 09 2021 divide u and dist function into two sub-functions
    CAW_fill_dist_one_species_aq(pcmsh, pspecies, flog);
    CAW_build_deltaL_aq(pcmsh, pspecies, plink, flog);
    psim->pcarac_ttc[modname]->uface = TTC_allocate_uface(nele); // DK AR 06 09 2021 allocate uface
    psim->pcarac_ttc[modname]->dVdt = TTC_allocate_dvdt(nele);
    psim->pcarac_ttc[modname]->qSink = TTC_allocate_SinkSource(nele);
    psim->pcarac_ttc[modname]->qSource = TTC_allocate_SinkSource(nele);
    break;
  }
  default:
    LP_error(flog, "In CaWaQS%4.2f, File %s in %s line %d : No coupling between CaWaQS module %s and TTC library.\n", NVERSION_CAW, __FILE__, __func__, __LINE__, CAW_name_mod(modname));
    break;
  }
}

/**
 * \fn void CAW_fill_TTC_neigh_all_species(s_simul_cawaqs*, int, FILE*)
 * \brief Launches TTC neighbour initialization and filling procedures for all species
 * \return -
 */
void CAW_fill_TTC_neigh_all_species(s_simul_cawaqs *psim, int modname, int nstep, FILE *flog) {

  int p, nb_species = psim->nb_species;
  s_carac_ttc *pcarac = psim->pcarac_ttc[modname];

  for (p = 0; p < nb_species; p++) {
    CAW_init_neighbor_one_species(pcarac->p_link[p], pcarac->count[NELE_TTC], modname, flog);
    CAW_fill_neighbor_one_species(psim, pcarac->p_link[p], pcarac->p_species[p], modname, pcarac->count[NELE_TTC], nstep, flog);
  }
}

/**
 * \fn void CAW_build_TTC_neigh_all_species(s_simul_cawaqs*, int, FILE*)
 * \brief Fill the initial value to the TTC pspecies structure
 * \return -
 */
void CAW_build_TTC_var_all_species(s_simul_cawaqs *psim, int modname, FILE *flog) {

  int p, nb_species = psim->nb_species;
  s_carac_ttc *pcarac = psim->pcarac_ttc[modname];

  if (modname == AQ_CAW) {
    for (p = 0; p < nb_species; p++) {
      CAW_build_var_one_species_aq(psim->pcarac_aq->pcmsh, pcarac->p_species[p], flog);
    }
  } else if (modname == HYD_CAW) {
    for (p = 0; p < nb_species; p++) {
      CAW_build_var_one_species_riv(pcarac->p_species[p], psim->pchyd, p, flog);
    }
  } else if (modname == HDERM_CAW) {
    for (p = 0; p < nb_species; p++) {
      CAW_build_var_one_species_riv(pcarac->p_species[p], psim->pchyd_hderm, p, flog);
    }
  }
}

/**
 * \fn void CAW_init_neighbor_one_species(s_link_ttc*, int, int, FILE*)
 * \brief Initialize neighbors and boundaries attributes for all calculation elements, for one single specie
 * \return -
 */
void CAW_init_neighbor_one_species(s_link_ttc *plink, int nele, int modname, FILE *flog) {

  int e, icard, sub_icard;

  for (e = 0; e < nele; e++) {

    plink->p_neigh_ttc[e]->nvois = 0.;

    for (icard = 0; icard < NB_CARD_TTC; icard++) {
      for (sub_icard = 0; sub_icard < SUB_CARD_TTC; sub_icard++) {
        plink->p_neigh_ttc[e]->ivois[icard][sub_icard] = NONE_TTC;

        if ((modname == HYD_CAW) || (modname == HDERM_CAW)) {
          plink->pspecies->p_boundary_ttc[e]->icl[icard][sub_icard] = NO_BOUND_TTC;
          plink->pspecies->p_boundary_ttc[e]->valcl[icard][sub_icard] = 0.;
        }

        if (modname == AQ_CAW) {
          plink->pspecies->p_boundary_ttc[e]->icl[icard][sub_icard] = NEU_FACE_TTC;
          plink->pspecies->p_boundary_ttc[e]->valcl[icard][sub_icard] = 0.;
        }
        //  LP_printf(flog,"id_ele %d dir %s sub %d cond %s\n",e,TTC_param_card(icard),sub_icard,TTC_name_bound(plink->pspecies->p_boundary_ttc[e]->icl[icard][sub_icard]));
      }
    }
  }
}

/**
 * \fn void CAW_fill_neighbor_one_species(s_simul_cawaqs*, s_link_ttc*, int, int, FILE*)
 * \brief Launches CAW_fill_neighbor_one_species_[riv,aq]() function depending on the CaWaQS module its called for
 * \return -
 */
void CAW_fill_neighbor_one_species(s_simul_cawaqs *psim, s_link_ttc *plink, s_species_ttc *pspecies, int modname, int nele, int nstep, FILE *flog) {

  switch (modname) {
  case HYD_CAW: {
    CAW_fill_neighbor_one_species_riv(plink, psim->pchyd, flog);
    CAW_fill_nsub_one_species_riv(plink, nele, psim->pchyd, flog);
    break;
  }

  case HDERM_CAW: {
    CAW_fill_neighbor_one_species_riv(plink, psim->pchyd_hderm, flog);
    CAW_fill_nsub_one_species_riv(plink, nele, psim->pchyd_hderm, flog);
    break;
  }

  case AQ_CAW: {
    CAW_fill_neighbor_one_species_aq(psim->pcarac_aq->pcmsh, plink, flog);
    CAW_fill_nsub_one_species_aq(psim->pcarac_aq->pcmsh, plink, flog);

    // LP_printf(flog,"Input status : Bounds = %s, Source = %s\n",TTC_answer(plink->pspecies->pset->boundaries),TTC_answer(plink->pspecies->pset->source));

    if (psim->pcarac_ttc[modname]->regime == TRANSIENT_TTC) {

      if (plink->pspecies->pset->boundaries == YES_TTC)
        // NG : 21/06/2023 : DK function badly coded. Impose that the length of pft is equal to TSTEP.
        CAW_build_TTC_face_bound_transient(psim->pcarac_aq->pcmsh, plink, pspecies, nstep, flog); // DK AR 08 09 2021 fill neighbours of the studied mesh

      if (plink->pspecies->pset->source == YES_TTC)
        // NG : 21/06/2023 : DK function badly coded. Impose the direction. Can't assure the browsing of pft is OK either. Haven't checked.
        CAW_build_TTC_face_source_transient(psim->pcarac_aq->pcmsh, plink, pspecies, nstep, flog);
    } else if (psim->pcarac_ttc[modname]->regime == STEADY_TTC) {
      LP_error(flog, "In CaWaQS%4.2f : File %s, function %s at line %d : Entering steady simulation face boundary setup. Dead end for now.\n", NVERSION_CAW, __FILE__, __func__, __LINE__);
    }
    break;
  }
  default:
    LP_error(flog, "In CaWaQS%4.2f, File %s in %s line %d : No coupling between CaWaQS module %s and TTC library.\n", NVERSION_CAW, __FILE__, __func__, __LINE__, CAW_name_mod(modname));
    break;
  }

  // NG check : making sure that neighbours and face boundaries have been stored and initialized properly
  if (psim->pset_caw->idebug == YES) {
    CAW_print_neighbor_attributes_one_specie(plink, nele, modname, flog);
  }
}

/**
 * \fn void CAW_print_neighbor_attributes_one_specie(s_link_ttc*, int, int, FILE*)
 * \brief Prints neighbor related attributes after being transferred to TTC. Debug
          function to make sure its has been defined and stored properly.
 * \return -
 *
 * NG : 19/06/2023 : Revised function for a more complete display
 *
 */
void CAW_print_neighbor_attributes_one_specie(s_link_ttc *plink, int nele, int modname, FILE *flog) {

  int n, icar, isub, nneigh, bound_type;
  double deltaX, bound_val;

  LP_printf(flog, " \n *** NEIGHBORS ATTRIBUTES [%s module]*** \n\n", CAW_name_mod(modname));

  for (n = 0; n < nele; n++) {
    LP_printf(flog, "TTC specie %s id_abs %d Nb_neigh = %d :", plink->pspecies->name, n + 1, plink->p_neigh_ttc[n]->nvois);
    for (icar = 0; icar < NB_CARD_TTC; icar++) {
      LP_printf(flog, " [%s - %d sbf] ", TTC_param_card(icar), plink->nsub[n][icar]);
      for (isub = 0; isub < SUB_CARD_TTC; isub++) {
        nneigh = plink->p_neigh_ttc[n]->ivois[icar][isub];
        // deltaX = plink->p_neigh_ttc[n]->delta_L[icar][isub]; // Not calculated for AQ at this stage.
        if (nneigh != NONE_TTC)
          LP_printf(flog, "%d ", nneigh);
      }
    }
    LP_printf(flog, "\n");
  }

  LP_printf(flog, " \n *** FACES INITIALIZATION [%s module]*** \n\n", CAW_name_mod(modname));
  for (n = 0; n < nele; n++) {
    LP_printf(flog, "TTC specie %s id_abs %d Nb_neigh = %d\n", plink->pspecies->name, n + 1, plink->p_neigh_ttc[n]->nvois);
    for (icar = 0; icar < NB_CARD_TTC; icar++) {
      LP_printf(flog, " [%s - %d sbf] ", TTC_param_card(icar), plink->nsub[n][icar]);
      if (plink->nsub[n][icar] == 0) {
        bound_type = plink->pspecies->p_boundary_ttc[n]->icl[icar][0];
        bound_val = plink->pspecies->p_boundary_ttc[n]->valcl[icar][0];
        LP_printf(flog, " (sbf0 : %s) \n", TTC_name_bound(bound_type));
      } else {
        for (isub = 0; isub < plink->nsub[n][icar]; isub++) {
          bound_type = plink->pspecies->p_boundary_ttc[n]->icl[icar][isub];
          bound_val = plink->pspecies->p_boundary_ttc[n]->valcl[icar][isub];
          LP_printf(flog, " (sbf%d : %s) ", isub, TTC_name_bound(bound_type));
        }
        LP_printf(flog, "\n");
      }
    }
    LP_printf(flog, "\n");
  }
}

/**
 * \fn void CAW_fill_var_one_species(s_simul_cawaqs*, s_species_ttc*, int, int, FILE*)
 * \brief Launches TTC filling procedure of previous time step computed variable (concentration, temperature) for all elements
 * \return -
 */
void CAW_fill_var_one_species(s_simul_cawaqs *psim, s_species_ttc *pspecies, int modname, int id_species, FILE *flog) {

  switch (modname) {
  case HYD_CAW: {
    CAW_fill_var_one_species_riv(pspecies, psim->pchyd, id_species, flog);
    break;
  }
  case HDERM_CAW: {
    CAW_fill_var_one_species_riv(pspecies, psim->pchyd_hderm, id_species, flog);
    break;
  }
  case AQ_CAW: {
    CAW_fill_var_one_species_aq(psim->pcarac_aq->pcmsh, pspecies, flog);
    break;
  }
  default:
    LP_error(flog, "In CaWaQS%4.2f, File %s in %s line %d : No coupling between CaWaQS module %s and TTC library.\n", NVERSION_CAW, __FILE__, __func__, __LINE__, CAW_name_mod(modname));
    break;
  }
}

/**
 * \fn void CAW_fill_u_one_species(s_simul_cawaqs*, s_link_ttc*, int, int, FILE*)
 * \brief Launches TTC filling procedure of libhyd discharges or libaq velocities for all calculation elements
 * \return -
 */
void CAW_fill_u_one_species(s_simul_cawaqs *psim, s_link_ttc *plink, int modname, int id_species, FILE *flog) {

  switch (modname) {
  case HYD_CAW: {
    CAW_fill_Q_one_species_riv(psim->pcarac_ttc[modname], psim->pchyd, modname, id_species, flog);
    break;
  }
  case HDERM_CAW: {
    CAW_fill_Q_one_species_riv(psim->pcarac_ttc[modname], psim->pchyd_hderm, modname, id_species, flog);
    break;
  }
  case AQ_CAW: {
    CAW_update_velocities_TTC(psim->pcarac_aq->pcmsh, psim->pcarac_ttc[modname], id_species, flog);
    CAW_update_velocity_directions_TTC(Simul->pcarac_aq->pcmsh, psim->pcarac_ttc[modname], id_species, flog);

    if (psim->pset_caw->sim_type == SW_GW) {
      if (psim->pcarac_ttc[modname]->p_species[id_species]->pset->riverbed == AQUITARD_ON_TTC) {
        CAW_update_TTC_velocity_riverbed_aq(psim->pchyd, psim->pcarac_ttc[modname]->p_species[id_species], psim->pcarac_ttc[modname]->count[NRIVERBED_TTC], flog);
      }
    }
    break;
  }
  default:
    LP_error(flog, "In CaWaQS%4.2f, File %s in %s line %d : No coupling between CaWaQS module %s and TTC library.\n", NVERSION_CAW, __FILE__, __func__, __LINE__, CAW_name_mod(modname));
    break;
  }
}

/**
 * \fn void CAW_fill_inflows_one_species(s_simul_cawaqs*, double, s_species_ttc*, int, int, double, FILE*)
 * \brief Launches calculation procedure of total lateral inflows to river elements
 * \return -
 */
void CAW_fill_inflows_one_species(s_simul_cawaqs *psim, double t, s_species_ttc *pspecies, int modname, int id_species, double dt, FILE *flog) {

  switch (modname) {

  case HYD_CAW: {
    if (psim->pset_caw->sim_type == SW_GW) {
      CAW_fill_inflows_one_species_riv_aq(pspecies, t, psim->pchyd, psim->pcarac_fp, psim->pcarac_aq, id_species, dt, flog);
    } else {
      CAW_fill_inflows_one_species_riv(pspecies, t, psim->pchyd, psim->pcarac_fp, id_species, dt, flog);
    }
    break;
  }

  case HDERM_CAW: {
    if (psim->pset_caw->sim_type == SW_GW) {
      CAW_fill_inflows_one_species_riv_aq(pspecies, t, psim->pchyd_hderm, psim->pcarac_fp_hderm, psim->pcarac_aq, id_species, dt, flog);
    } else {
      CAW_fill_inflows_one_species_riv(pspecies, t, psim->pchyd_hderm, psim->pcarac_fp_hderm, id_species, dt, flog);
    }
    break;
  }

  case AQ_CAW: {
    // Calculation of transport inputs to aquifer system from linked modules (i.e. NSAT, HDERM (not coded yet) for example)
    if (psim->pset_caw->sim_type == SW_GW) { // NG : 22/06/2023 : Adding condition to sim_type
      CAW_cAQ_TTC_calculate_transport_fluxes(t, dt, NSAT_CAW, id_species, psim->pcarac_aq, psim->pcarac_zns, psim->pchyd, psim->pcarac_ttc[modname], flog);
    }
    // Update of the top boundary condition of the aquifer with the impact of river
    if (pspecies->pset->type == HEAT_TTC) {
      CAW_cAQ_TTC_river_impact_on_aq(t, dt, id_species, psim->pcarac_aq, psim->pchyd, psim->pcarac_ttc[modname], flog);
    }

    break;
  }
  default:
    LP_error(flog, "In CaWaQS%4.2f, File %s in %s line %d : No coupling between CaWaQS module %s and TTC library.\n", NVERSION_CAW, __FILE__, __func__, __LINE__, CAW_name_mod(modname));
    break;
  }
}

/**
 * \fn void CAW_fill_wetsurf_one_specie(s_simul_cawaqs*, s_link_ttc*, int, int, FILE*)
 * \brief Launches wet surfaces calculations procedures for each element
 * \return -
 */
void CAW_fill_wetsurf_one_specie(s_simul_cawaqs *psim, s_link_ttc *plink, int modname, int id_species, FILE *flog) {

  switch (modname) {
  case HYD_CAW: {
    CAW_fill_wetsurf_one_specie_riv(plink, psim->pchyd, id_species, flog);
    break;
  }
  case HDERM_CAW: {
    CAW_fill_wetsurf_one_specie_riv(plink, psim->pchyd_hderm, id_species, flog);
    break;
  }
  default:
    LP_error(flog, "In CaWaQS%4.2f, File %s in %s line %d : No coupling between CaWaQS module %s and TTC library.\n", NVERSION_CAW, __FILE__, __func__, __LINE__, CAW_name_mod(modname));
    break;
  }
}

/**
 * \fn void CAW_update_variable_one_specie(s_simul_cawaqs*, s_carac_ttc*, s_species_ttc*, int, int, FILE*);
 * \brief Refreshes transport variable for all calculation elements (river ele, aq cell) after TTC calculation at current time step
 * \return -
 */
void CAW_update_variable_one_specie(s_simul_cawaqs *psim, s_species_ttc *pspecies, int modname, int id_species, FILE *flog) {
  switch (modname) {

  case HYD_CAW: {
    CAW_update_variable_one_specie_riv(pspecies, psim->pchyd, id_species, flog);
    break;
  }
  case HDERM_CAW: {
    CAW_update_variable_one_specie_riv(pspecies, psim->pchyd_hderm, id_species, flog);
    break;
  }
  case AQ_CAW: {
    CAW_update_variable_aquitard_one_specie_aq(pspecies, psim->pcarac_aq, psim->pcarac_ttc[modname], id_species, flog); // Should be conditionnal to aquitard declaration
    CAW_update_variable_one_specie_aq(pspecies, psim->pcarac_aq, psim->pcarac_ttc[modname], id_species, flog);
    break;
  }
  default:
    LP_error(flog, "CaWaQS%4.2f, File %s in %s line %d : No coupling between CaWaQS module %s and TTC library.\n", NVERSION_CAW, __FILE__, __func__, __LINE__, CAW_name_mod(modname));
    break;
  }
}

/**
 * \fn void CAW_print_timestep_transport_variable(s_simul_cawaqs*, int, int, FILE*)
 * \brief Debug function to check transport variable values for all calculations elements at current time step
 * (when passing through targeted library (i.e. libhyd, or libaq).
 * \return -
 */
void CAW_print_timestep_transport_variable(s_simul_cawaqs *psim, int modname, int id_species, FILE *flog) {
  switch (modname) {

  case HYD_CAW: {
    HYD_print_ele_transport_variable(psim->pchyd, id_species, flog);
    break;
  }
  case HDERM_CAW: {
    HYD_print_ele_transport_variable(psim->pchyd_hderm, id_species, flog);
    break;
  }
  case AQ_CAW: {
    AQ_print_ele_transport_variable(psim->pcarac_aq, id_species, flog);
    break;
  }
  default:
    LP_error(flog, "CaWaQS%4.2f, File %s in %s line %d : No coupling between CaWaQS module %s and TTC library.\n", NVERSION_CAW, __FILE__, __func__, __LINE__, CAW_name_mod(modname));
    break;
  }
}

/**
 * \fn void CAW_solve_TTC_all_species(s_simul_cawaqs*, int, double, double, FILE*)
 * \brief Launches current time step resolution of transport equation for all species and for one given CaWaQS module
 * \return -
 */
void CAW_solve_TTC_all_species(s_simul_cawaqs *psim, int modname, double dt, double t, int *ts_count, FILE *flog) {
  s_carac_ttc *pcarac = psim->pcarac_ttc[modname];
  int i, ns;
  int nb_species = pcarac->count[NSPECIES_TTC];
  int nele = pcarac->count[NELE_TTC];
  int naquitard = pcarac->count[NAQUITARD_TTC];
  int nriverbed = pcarac->count[NRIVERBED_TTC];
  s_species_ttc *pspecies;
  s_param_calc_ttc **pcalc_ttc;
  s_link_ttc *plink;
  s_element_hyd *pele;
  s_ele_msh *pele_aq;

  // int ns_out = 0; // NG to debug

  for (ns = 0; ns < nb_species; ns++) {

    pspecies = pcarac->p_species[ns];
    plink = pcarac->p_link[ns];
    pcalc_ttc = plink->p_param_calc_ttc;
    // LP_printf(flog,"Inside = %s : modname : %d, id_species %d\n",__func__ ,modname, ns); // NG check

    // Fetching transport state variable (C,T) calculated at previous time step, stored in either lbhyd or libaq
    CAW_fill_var_one_species(psim, pspecies, modname, ns, flog);

    // Fetching velocities (for AQ) or discharge (HYD) at previous time step
    CAW_fill_u_one_species(psim, plink, modname, ns, flog);

    if (modname != AQ_CAW) {
      CAW_fill_wetsurf_one_specie(psim, plink, modname, ns, flog); // For hydraulic systems : Computing and filling wet surfaces
    }

    // Filling of inflows
    CAW_fill_inflows_one_species(psim, t, pspecies, modname, ns, dt, flog);

    if (modname == AQ_CAW) {
      switch (pcarac->regime) {
      case STEADY_TTC:
        // LP_printf(flog,"Before entering to the steady state solution\n");
        CAW_steady_solve_TTC(pcarac, pcalc_ttc, pspecies, flog);
        break;
      case TRANSIENT_TTC:
        // LP_printf(flog,"Before entering to the transient state solution for the aquifer\n");
        CAW_transient_solve_libaq_ttc(pcarac, pcalc_ttc, pspecies, ns, dt, t, ts_count, flog);
        break;
      }
    } else {
      switch (pcarac->regime) {
      case STEADY_TTC:
        // LP_printf(flog,"Before entering to the steady state solution\n");
        CAW_steady_solve_TTC(pcarac, pcalc_ttc, pspecies, flog);
        break;
      case TRANSIENT_TTC:
        // LP_printf(flog,"Before entering to the transient state solution\n");
        CAW_transient_solve_ttc(pcarac, pcalc_ttc, pspecies, ns, dt, t, ts_count, flog);
        break;
      }
    }

    CAW_update_variable_one_specie(psim, pspecies, modname, ns, flog);

    // River system/libseb connection
    if (modname == HYD_CAW) {
      if ((psim->pset_caw->transport_module[SEB_CAW] == YES) && (pspecies->pset->type == HEAT_TTC)) {
        for (i = 0; i < nele; i++)
          psim->mto_seb->p_surf_heat_flux[i]->pH_inputs->Tw = pspecies->pgc->x[i];
      }
    }
    // if (ns == ns_out) CAW_print_timestep_transport_variable(psim,modname,ns_out,flog);  // NG check
  }
}

/**
 * \fn void CAW_transient_solve_ttc(s_carac_ttc*, s_param_calc_ttc*, s_species_ttc*, int, double, double, int*, FILE*)
 * \brief General loop for transient-type resolution of the RIVER transport equation
 * \return -
 */
void CAW_transient_solve_ttc(s_carac_ttc *pcarac_ttc, s_param_calc_ttc **pcalc_ttc, s_species_ttc *pspecies, int id_species, double dt, double t, int *ts_count, FILE *flog) {

  int i, nele = pcarac_ttc->count[NELE_TTC];
  int naquitard = pcarac_ttc->count[NAQUITARD_TTC];
  s_gc *pgc = pspecies->pgc;

  if (t - dt < 86400) {                                             // TODO : Simul->chronos->t[BEGINNING] is variant and does not hold a good reference for conditional case
    TTC_init_mat5_mat4(pcarac_ttc->p_link[id_species], nele, flog); // Initialization : Creation and filling of mat4 and mat5 according to the system geometry
  }

  // Free or creation of b vector
  TTC_init_RHS(pgc);

  // Free or creation of x solution vector
  TTC_init_sol_vec(pgc);
  TTC_free_mat6(pgc);

  pgc->mat6 = GC_create_mat_double(pgc->lmat5);

  // Filling of mat6 and RHS
  TTC_fill_mat6_and_b_cawaqs(pcarac_ttc, pcalc_ttc, pspecies, dt, flog);

  // TTC_print_LHS(pgc,flog);
  // TTC_print_RHS(pgc,flog);
  // TTC_print_LHS_RHS(pgc,flog);

  GC_configure_gc(pgc, ts_count, flog);
  GC_solve_gc(pgc, TTC_GC, flog);

  // NG 05/04/2023 : DK NASTY FIX BELOW --> making it conditional to HEAT and adding lots of WARNINGS to inform user to deal with this properly.
  if (pspecies->pset->type == HEAT_TTC) {
    for (i = 0; i < nele; i++) {
      LP_warning(flog, "In CaWaQS%4.2f : In %s : Entering a NASTY QUICK FIX loop for %s transport in hydraulic network. CODE CHECK NEEDED !\n", NVERSION_CAW, __func__, TTC_name_equa_type(pspecies->pset->type));

      // DK : Here we limit the river temperatures to a minimum and maximum temperature to by-pass initialization of the river T simulation
      if (pspecies->pgc->x[i] < 273.15) {
        pspecies->pgc->x[i] = 273.15;
      } else if (pspecies->pgc->x[i] > 310.15) {
        pspecies->pgc->x[i] = 310.15;
      }
    }
  }

  for (i = 0; i < nele; i++) {
    if (pspecies->pset->calc_process[DIFFUSION_TTC] == YES_TS) {
      TTC_cal_condflux(pcarac_ttc, pcalc_ttc, pspecies, i, flog); // NO COND FLUX RIV ???
      // LP_printf(flog, "Inside convection processes, calculating conductive fluxes ...\n");
    }
    if (pspecies->pset->calc_process[CONVECTION_TTC] == YES_TS) {
      TTC_cal_advflux_riv(pcarac_ttc, pcalc_ttc, pspecies, i, flog);
      // LP_printf(flog, "Inside advection processes, calculating advective fluxes ...\n");
    }
    TTC_cal_totflux_hyd(pcarac_ttc, pcalc_ttc, pspecies, i, flog); // NG : SOLUTE CASES STILL TO BE WRITTEN ALTHOUGH TOTFLUX IN MB /OUTPUT FILES IS KINDA SUPERFLUOUS
  }
}

/**
 * \fn void CAW_transient_solve_libaq_ttc (s_carac_ttc*, s_param_calc_ttc**, s_species_ttc*, int, double, double, int*, FILE*)
 * \brief General loop for transient-type resolution of the AQUIFER transport equation
 * \return -
 */
void CAW_transient_solve_libaq_ttc(s_carac_ttc *pcarac_ttc, s_param_calc_ttc **pcalc_ttc, s_species_ttc *pspecies, int id_species, double dt, double t, int *ts_count, FILE *flog) {

  int i, nele = pcarac_ttc->count[NELE_TTC];
  int naquitard = pcarac_ttc->count[NAQUITARD_TTC];
  int nriverbed = pcarac_ttc->count[NRIVERBED_TTC];
  int one = 1;
  int lmat4, lmat5;
  s_gc *pgc = pspecies->pgc;

  // LP_printf(flog,"Verif : dans %s : iappli = %d, total cells in aquifer %d\n",__func__,pspecies->pgc->appl_nb, nele);

  if (pspecies->pset->riverbed == AQUITARD_ON_TTC) {
    // LP_printf(flog,"Inside %s, Riverbed calculation is on: %d \n",__func__ , pspecies->pset->riverbed);
    CAW_build_TTC_temp_riverbed_aq(Simul->pcarac_aq->pcmsh, Simul->pchyd, pspecies, nriverbed, flog);
  }
  if (pspecies->pset->aquitard == AQUITARD_ON_TTC) {
    CAW_build_TTC_temp_aquitard_aq(Simul->pcarac_aq->pcmsh, pspecies, flog);
  }

  // Initialisations
  if (t - dt < 86400) { // DK : The Simul->chronos->t[BEGINNING] is variant and does not hold a good reference for conditional case

    if (pspecies->pset->aquitard == AQUITARD_ON_TTC || pspecies->pset->riverbed == AQUITARD_ON_TTC) {
      // We include nriverbed twice to include the river temperatures in the solution

      pgc->ndl = nele + naquitard + nriverbed;
      pgc->mat4 = GC_create_mat_int(pgc->ndl + one);
      pgc->lmat5 = TTC_calc_lmat5_test(pcarac_ttc->p_link[id_species], nele, naquitard, nriverbed, flog);
      pgc->mat5 = GC_create_mat_int(pgc->lmat5);
      TTC_init_mat5_mat4_libaq_test(pcarac_ttc->p_link[id_species], nele, naquitard, nriverbed, flog); // Remplissage de mat4 et mat5 selon la géométrie du système
    } else {
      // pgc->ndl = nele;                                // NG : 22/06/2023 : Bug fix. Allocation of mat4,mat5 is already done in TTC_init_mat5_mat4_libaq() !
      // pgc->mat4 = GC_create_mat_int(pgc->ndl + one);
      // pgc->lmat5 = TTC_calc_lmat5(pcarac_ttc->p_link[id_species], nele, flog);
      // pgc->mat5 = GC_create_mat_int(pgc->lmat5);
      TTC_init_mat5_mat4_libaq(pcarac_ttc->p_link[id_species], nele, flog); // Création ET remplissage de mat4 et mat5 selon la géométrie du système
      // LP_printf(flog,"MAT4\n"); TTC_print_mat4_mat5(pgc->ndl+1,pgc->mat4,flog);
      // LP_printf(flog,"MAT5\n"); TTC_print_mat4_mat5(pgc->lmat5,pgc->mat5,flog);
    }
    // LP_printf(flog, "In %s : Filling mat4, mat5, nele: %d, naquitard : %d, nriverbed: %d\n", __func__,nele, naquitard, nriverbed);
  }

  // LP_printf(flog,"Verif : dans %s : length of lmat is %d\n",__func__,pspecies->pgc->lmat5);

  TTC_init_RHS(pgc);     // Free et/ou création de b
  TTC_init_sol_vec(pgc); // Free et/ou création du vecteur solution
  TTC_free_mat6(pgc);

  pgc->mat6 = GC_create_mat_double(pspecies->pgc->lmat5);
  // LP_printf(flog,"[Entering to fill mat6 and b , lmat is %d]\n", pspecies->pgc->lmat5);

  // CAW_print_TTC_elesource(Simul->pcarac_aq->pcmsh,flog);

  // LP_printf(flog, "[Entering %s Entering fill mat6 & b]\n", __func__);
  TTC_fill_mat6_and_b_libaq_test(pcarac_ttc, pcarac_ttc->p_link[id_species], pcalc_ttc, pspecies, dt, flog); // Remplissage de mat6 et du RHS
  // LP_printf(flog, "[Entering %s Filled mat6 & b]\n", __func__);

  // LP_printf(flog, "[Entering %s Starting to solve GC]\n", __func__);
  GC_configure_gc(pgc, ts_count, flog);
  GC_solve_gc(pgc, TTC_AQ_GC, flog);
  //  for (i=0;i<nele;i++) LP_printf(flog,"In %s after GCsolve : Var[%d] = %f, pgc(i): %f \n",__func__,i,pspecies->pval_ttc->val[i],pgc->x[i] );
  // LP_printf(flog, "[Entering %s Solved GC]\n", __func__);

  // Update dans TTC pour l'itération suivante
  for (i = 0; i < nele; i++) {
    pspecies->pval_ttc->val_old[i] = pspecies->pval_ttc->val[i];
    pspecies->pval_ttc->val[i] = pgc->x[i];
  }

  if (pspecies->pset->aquitard == AQUITARD_ON_TTC) {
    for (int j = 0; j < naquitard; j++) {
      pspecies->p_aquitard_ttc[j]->temperature_old = pgc->x[nele + j];
      pspecies->p_aquitard_ttc[j]->temperature[VAR_AQUITARD_TTC][0] = pgc->x[nele + j];
      // LP_printf(flog, "j is %d id: %d q = %e \t T_aquitard: %f T_old = %lf \n",j, pspecies->p_aquitard_ttc[j]->id, pspecies->p_aquitard_ttc[j]->q,
      // pspecies->p_aquitard_ttc[j]->temperature[VAR_AQUITARD_TTC][0], pspecies->p_aquitard_ttc[j]->temperature_old );
      Calc_aquitard_fluxes(pspecies, j, AQUITARD_TTC, flog);
    }
  }

  if (pspecies->pset->riverbed == AQUITARD_ON_TTC) {
    for (int j = 0; j < nriverbed; j++) {
      pspecies->p_riverbed_ttc[j]->temperature_old = pspecies->p_riverbed_ttc[j]->temperature[VAR_AQUITARD_TTC][0];
      pspecies->p_riverbed_ttc[j]->temperature[VAR_AQUITARD_TTC][0] = pgc->x[nele + naquitard + j];
      Calc_aquitard_fluxes(pspecies, j, RIVERBED_TTC, flog);
      // LP_printf(flog, "q = %e\tT_rivb:%f\n",pspecies->p_riverbed_ttc[j]->q, pspecies->p_riverbed_ttc[j]->temperature[VAR_AQUITARD_TTC][0]);
    }
  }

  for (i = 0; i < nele; i++) {
    if (pspecies->pset->calc_process[DIFFUSION_TTC] == YES_TS) {
      TTC_cal_condflux(pcarac_ttc, pcalc_ttc, pspecies, i, flog);
      // LP_printf(flog, "Inside convection processes, calculating conductive fluxes \n");
    }
    if (pspecies->pset->calc_process[CONVECTION_TTC] == YES_TS) {
      TTC_cal_advflux(pcarac_ttc, pcalc_ttc, pspecies, i, flog);
      // LP_printf(flog, "Inside advection processes, calculating advective fluxes \n");
    }

    TTC_cal_totflux(pcarac_ttc, pcalc_ttc, pspecies, i, flog);
    TTC_calc_transport_mb(pcarac_ttc, pcalc_ttc, pspecies, i, flog);
    // LP_printf(flog, "Inside total processes, calculating total fluxes \n");
  }
  // LP_printf(flog, "[%s cycle completed]\n", __func__);
}

/**
 * \fn void CAW_steady_solve_TTC(s_carac_ttc*, s_param_calc_ttc**, s_species_ttc*, FILE*)
 * \brief
 * \return -
 */
void CAW_steady_solve_TTC(s_carac_ttc *pcarac_ttc, s_param_calc_ttc **p_param_calc_ttc, s_species_ttc *pspecies, FILE *fpout) {
  s_gc *pgc;
  s_link_ttc *plink = pcarac_ttc->p_link[pspecies->id]; // DK AR 26 08 2021 adding plink into the function
  int i, regime, type, nele;
  double dt = 0;
  FILE *fpvar = NULL;
  FILE *fpmb = NULL;
  nele = pcarac_ttc->count[NELE_TTC];
  regime = pcarac_ttc->regime;
  type = pspecies->pset->type;
  pgc = pspecies->pgc;
  int ts_count = 0;

  // TTC_header_output_steady(fpvar,type);
  // TTC_header_output_MB_steady(fpmb,type);
  // TTC_init_tab_gc
  TTC_print_carac_properties(pcarac_ttc, fpout); // NG : 24/11/2020

  TTC_init_mat5_mat4_libaq(plink, nele, fpout); // NG : 24/11/2020 Bug sur les sub-faces corrigé.
  pgc->mat6 = GC_create_mat_double(pspecies->pgc->lmat5);
  TTC_init_RHS(pgc);
  pgc->x = GC_create_mat_double(pspecies->pgc->ndl);
  LP_printf(fpout, "Entering steady solution in aquifeferf\n");                                                     // NG check
  LP_printf(fpout, "Thermic parameter  ABS ele id 0= %f\n", pspecies->p_param_ttc[0]->param_thermic[SOLID_TTC][0]); // NG check
  // On remplit mat6 qui sera utile pour le solveur libgc
  TTC_fill_mat6_and_b_libaq(pcarac_ttc, plink, p_param_calc_ttc, pspecies, dt, fpout);
  LP_printf(fpout, "Disp  ABS ele id 0= %f\n", plink->p_param_calc_ttc[0]->disp_face[0][0]); // NG check

  /* NG : 11/06/2021 : New GC_configure_gc() function used to update solver param, depending on the iteration number (steady) or time step we're at,
                       in order to save some calculation time. 'ts_count' (int*) is the counter of time steps or iteration. */

  LP_printf(fpout, "lmat is %d, appl num : %d\n", pgc->lmat5, pgc->sw_int[APPL_NB]);
  for (int j = 0; j < pgc->lmat5; j++) {

    LP_printf(fpout, "id is %d, mat6 is %e \n", j, pgc->mat6[j]);
  }
  // Pour le moment, je rends ça inerte (ts_count=0) pour ne pas avoir à modifier les arguments.
  GC_configure_gc(pgc, &ts_count, fpout);
  GC_solve_gc(pgc, TTC_GC, fpout);
  // --------------------------------------------

  for (i = 0; i < nele; i++) {
    plink->pspecies->pval_ttc->val_old[i] = plink->pspecies->pval_ttc->val[i];
    plink->pspecies->pval_ttc->val[i] = pspecies->pgc->x[i];
    LP_printf(fpout, "%i \t %lf \t %lf \t %lf \t %lf\n", i, pspecies->pval_ttc->val[i], pspecies->pgc->x[i], pspecies->pgc->b[i], pspecies->pgc->mat6[i]);

    // LP_printf(fpout,"Specie %s : T[%d] = %f\n",pspecies->name,i,plink->pspecies->pval_ttc->var[i]); // NG check
  }

  // for(i=0;i<nele;i++)
  //{
  // TTC_cal_condflux(pcarac_ttc,pparam_calc_ttc,pspecies,i);
  // TTC_cal_advflux(pcarac_ttc,pparam_calc_ttc,pspecies,i);
  // TTC_cal_totflux(pcarac_ttc,pparam_calc_ttc,pspecies,i);
  // }
  // TTC_print_output_var_steady(pcarac_ttc,plink->pspecies->pval_ttc,fpvar);
  // TTC_print_output_MB_steady(pcarac_ttc,pparam_calc_ttc,pspecies,fpmb);
}
