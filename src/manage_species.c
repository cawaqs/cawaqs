/*-------------------------------------------------------------------------------
 *
 * SOFTWARE NAME: cawaqs
 * FILE NAME: manage_species.c
 *
 * CONTRIBUTORS: Nicolas GALLOIS, Baptiste LABARTHE, Shuaitao WANG,
 *               Agnès RIVIERE, Deniz KILIC, Mathias MAILLOT,
 *               Fulvia BARATELLI, Pierre GUILLOU, Emmanuel LEDOUX, Nicolas FLIPO
 *
 * SOFTWARE BRIEF DESCRIPTION: Physically based coupled surface–subsurface
 * model for joint water, matter and energy flux balances and flow dynamics
 * simulation within all compartments of a hydrosystem (sub-surface, hydraulic
 * network, vadose zone, aquifer system and stream-aquifer exchanges).
 *
 * Software developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the cawaqs Software.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Software and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <time.h>
#include <stdarg.h>
#include <sys/time.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <malloc.h>
#include <math.h>
#include <libprint.h>
#include <time_series.h>
#include "libpc.h"
#include "IO.h"
#include "GC.h"
#include "reservoir.h"
#include "spa.h"
#include "FP.h"
#include "NSAT.h"
#include "CHR.h"
#include "HYD.h"
#include "MSH.h"
#include "AQ.h"
#include "WET.h"
#include "TTC_coupled.h"
#include "SEB.h"
#include "CAW.h"

/**
 * \fn s_species_cawaqs *CAW_init_species(int)
 * \brief Species structur initialization for transport simulation
 * \return pointer towards newly allocated structure
 */
s_species_cawaqs *CAW_init_species(int id) {

  int i, j;
  s_species_cawaqs *pspecies;

  pspecies = new_species_cawaqs();
  bzero((char *)pspecies, sizeof(s_species_cawaqs));

  pspecies->id = id;
  pspecies->type = SOLUTE_CAW;
  // NG : 31/10/2024 : Fix : Default name initalization if not specified by the user in the COMM file
  sprintf(pspecies->name, "%s%d", "unspecified_", id);

  return pspecies;
}

/**
 * \fn s_species_cawaqs** CAW_manage_memory_species(s_species_cawaqs**, int, FILE*)
 * \brief Allocates or reallocates pspecies pointer tables each time a new specie is being read
 * \return newly re/allocated **pspecies
 */
s_species_cawaqs **CAW_manage_memory_species(s_species_cawaqs **pspecies, int nb_species, FILE *flog) {

  int i, id_species = nb_species - 1; // Differenciating current number of specie and its position in the pointer array

  if (pspecies != NULL) {
    pspecies = (s_species_cawaqs **)realloc(pspecies, nb_species * sizeof(s_species_cawaqs *));
    if (pspecies == NULL) {
      LP_error(flog, "In CaWaQS%4.2f, File %s in %s line %d : Bad memory reallocation. \n", NVERSION_CAW, __FILE__, __func__, __LINE__);
    }
  } else {
    pspecies = (s_species_cawaqs **)malloc(nb_species * sizeof(s_species_cawaqs *));
    if (pspecies == NULL) {
      LP_error(flog, "In CaWaQS%4.2f, File %s in %s line %d : Bad memory allocation. \n", NVERSION_CAW, __FILE__, __func__, __LINE__);
    }
    bzero((char *)pspecies, nb_species * sizeof(s_species_cawaqs *));
  }

  pspecies[id_species] = CAW_init_species(id_species);
  return pspecies;
}

/**
 * \fn double*** CAW_init_chem_param(FILE*)
 * \brief Initialization procedure of Simul->chem_param over all CaWaQS module and all properties
 * \return Allocated and initialized ***double chem_param
 */
double ***CAW_init_chem_param(FILE *flog) {

  int i, j;
  double ***param = NULL;

  param = (double ***)malloc(NMOD_CAW * sizeof(double **));

  if (param == NULL) {
    LP_error(flog, "In CaWaQS%4.2f : Error in file %s, function %s at line %d : Bad memory allocation.\n", NVERSION_CAW, __FILE__, __func__, __LINE__);
  }

  for (i = 0; i < NMOD_CAW; i++) {
    param[i] = (double **)malloc(NSPEPAR_CAW * sizeof(double *));
    if (param[i] == NULL)
      LP_error(flog, "CaWaQS%4.2f : Error in file %s, function %s at line %d : Bad memory allocation.\n", NVERSION_CAW, __FILE__, __func__, __LINE__);

    for (j = 0; j < NSPEPAR_CAW; j++)
      param[i][j] = NULL; // Default initialization
  }

  LP_printf(flog, "Chemical properties matrix has properly been allocated.\n");

  return param;
}

/**
 * \fn double*** CAW_manage_memory_pchem_param(double***, int, FILE*)
 * \brief (Re)-allocated sub double* vectors according to the current number of species being read
 * \return Re-allocated ***chem_param matrix
 */
double ***CAW_manage_memory_pchem_param(double ***param, int nb_species, FILE *flog) {

  int id_species = nb_species - 1;
  int i, j;

  for (i = 0; i < NMOD_CAW; i++) {
    for (j = 0; j < NSPEPAR_CAW; j++) {
      if (param[i][j] == NULL) {
        param[i][j] = (double *)malloc(nb_species * sizeof(double));
      }

      if (param[i][j] == NULL) {
        LP_error(flog, "In CaWaQS%4.2f : Error in file %s, function %s at line %d : Bad memory allocation.\n", NVERSION_CAW, __FILE__, __func__, __LINE__);
      } else {
        param[i][j] = (double *)realloc(param[i][j], nb_species * sizeof(double));
      }

      param[i][j][id_species] = CODE_CAW; // Initialization to default code. Allows to bypass it for HEAT transport (useless in the case).
    }
  }
  return param;
}

/**
 * \fn CAW_display_species_properties(s_simul_cawaqs*, FILE*)
 * \brief Verification function to make sure species attributes are properly defined and stored, for all CaWaQS module
 * \return -
 */
void CAW_display_species_properties(s_simul_cawaqs *psim, FILE *flog) {
  int i, j, k;
  int *transport_module = psim->pset_caw->transport_module;
  s_species_cawaqs *pspecies;
  char *name_mod = NULL;
  char *name_species = NULL;

  // General attributes

  LP_printf(flog, "\n *********** CaWaQS TRANSPORT ATTRIBUTES  *********** \n");
  for (i = 0; i < psim->nb_species; i++) {
    pspecies = psim->pspecies[i];
    LP_printf(flog, "\nSpecies ID : %d - Name : %s - Type : %s\n", pspecies->id, pspecies->name, CAW_species_type(pspecies->type));
    LP_printf(flog, "Physico-chemical properties : \n");

    for (j = 0; j < NMOD_CAW; j++) {
      name_mod = CAW_name_mod(j);
      LP_printf(flog, "CaWaQS module : %s - ", name_mod);
      for (k = 0; k < NSPEPAR_CAW; k++) {
        name_species = CAW_specie_chem_param(k);
        LP_printf(flog, "%s = %9.4f  ", name_species, psim->chem_param[j][k][i]);
        free(name_species);
      }
      LP_printf(flog, "\n");
      free(name_mod);
    }
  }

  // CaWaQS modules-related species attributes

  for (i = 0; i < NMOD_CAW; i++) {

    if (i == FP_CAW && transport_module[i] == YES) {
      LP_printf(flog, "\n ***** %s MODULE - Species attributes  ***** \n", CAW_name_mod(i));
      FP_print_species_properties(psim->pcarac_fp, psim->nb_species, flog);
    }

    if (i == HYD_CAW && transport_module[i] == YES) {
      LP_printf(flog, "\n ***** %s MODULE - Species attributes ***** \n", CAW_name_mod(i));
      TTC_print_carac_properties(psim->pcarac_ttc[i], flog);
    }

    if (i == HDERM_CAW && transport_module[i] == YES) {
      LP_printf(flog, "\n ***** %s MODULE - Species attributes ***** \n", CAW_name_mod(i));
      TTC_print_carac_properties(psim->pcarac_ttc[i], flog);
    }

    if (i == NSAT_CAW && transport_module[i] == YES) {
      LP_printf(flog, "\n ***** %s MODULE - Species attributes ***** \n", CAW_name_mod(i));
      LP_printf(flog, "\n Nothing specific to display yet.\n");
    }

    if (i == AQ_CAW && transport_module[i] == YES) {
      LP_printf(flog, "\n ***** %s MODULE - Species attributes ***** \n", CAW_name_mod(i));
      TTC_print_carac_properties(psim->pcarac_ttc[i], flog);
    }
  }
}

/**
 * \fn CAW_fetch_specie_types(s_simul_cawaqs*, FILE*)
 * \brief Stores all declared species types into an int*, so it can easily be passed onto any library whenever its needed.
 * \return integer array with specie types indicators.
 */
int *CAW_fetch_specie_types(s_simul_cawaqs *psim, FILE *flog) {

  int p, nb_species = psim->nb_species;
  int *p_spetypes;

  p_spetypes = (int *)malloc(nb_species * sizeof(int));

  for (p = 0; p < nb_species; p++)
    p_spetypes[p] = psim->pspecies[p]->type;

  return p_spetypes;
}