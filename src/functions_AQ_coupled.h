/*-------------------------------------------------------------------------------
 *
 * SOFTWARE NAME: cawaqs
 * FILE NAME: functions_AQ_coupled.h
 *
 * CONTRIBUTORS: Nicolas GALLOIS, Baptiste LABARTHE, Shuaitao WANG,
 *               Agnès RIVIERE, Deniz KILIC, Mathias MAILLOT,
 *               Fulvia BARATELLI, Pierre GUILLOU, Emmanuel LEDOUX, Nicolas FLIPO
 *
 * SOFTWARE BRIEF DESCRIPTION: Physically based coupled surface–subsurface
 * model for joint water, matter and energy flux balances and flow dynamics
 * simulation within all compartments of a hydrosystem (sub-surface, hydraulic
 * network, vadose zone, aquifer system and stream-aquifer exchanges).
 *
 * Software developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the cawaqs Software.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Software and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

// AQ_manage_boundary_coupled.c

// Coupling with other modules - Account for coupling with ZNS and/or HDERM
void CAW_cAQ_calculate_recharge_coupled(double, double, int, s_carac_aq *, s_carac_zns *, s_chyd *, FILE *); // NG : 07/03/2020

// Coupling with river network
void CAW_cAQ_create_riv_aq(s_chyd *, s_carac_aq *, s_chronos_CHR *, FILE *);
void CAW_cAQ_refresh_cauchy(s_chronos_CHR *, s_chyd *, s_carac_aq *, FILE *);
void CAW_cAQ_simplify_cauchy(s_carac_aq *, s_chyd *, FILE *);
void CAW_cAQ_check_qlim_riv(s_ele_msh *, s_gc *, s_chyd *, double, double, double, FILE *);
void CAW_cAQ_check_cauchy_riv(double, double, s_carac_aq *, s_chyd *, FILE *);
void CAW_cAQ_init_z_riv(s_carac_aq *, s_chyd *, FILE *);
void CAW_cAQ_print_nappe_riv(s_carac_aq *, s_chyd *, FILE *);
void CAW_update_AQ_MB(s_carac_aq *pchar_aq, int *iit, FILE *fp);
void CAW_calculate_aq_riv_flux(double dt, double t, s_carac_aq *pchar_aq, FILE *fp);
