/*-------------------------------------------------------------------------------
 *
 * SOFTWARE NAME: cawaqs
 * FILE NAME: HYD_hydraulics_coupled.c
 *
 * CONTRIBUTORS: Nicolas GALLOIS, Baptiste LABARTHE, Shuaitao WANG,
 *               Agnès RIVIERE, Deniz KILIC, Mathias MAILLOT,
 *               Fulvia BARATELLI, Pierre GUILLOU, Emmanuel LEDOUX, Nicolas FLIPO
 *
 * SOFTWARE BRIEF DESCRIPTION: Physically based coupled surface–subsurface
 * model for joint water, matter and energy flux balances and flow dynamics
 * simulation within all compartments of a hydrosystem (sub-surface, hydraulic
 * network, vadose zone, aquifer system and stream-aquifer exchanges).
 *
 * Software developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the cawaqs Software.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Software and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include "libprint.h"
#include "time_series.h"
#include "libpc.h"
#include "IO.h"
#include "reservoir.h"
#include "spa.h"
#include "CHR.h"
#include "FP.h"
#include "NSAT.h"
#include "GC.h"
#include "HYD.h"
#include "MSH.h"
#include "AQ.h"
#include "WET.h"
#include "TTC_coupled.h"
#include "SEB.h"
#include "CAW.h"
#ifdef OMP
#include "omp.h"
#endif

/* NG : 02/10/2020 : Function has been updated in order to integrate supplementary dicharges by exogenous inflows.
   (Previous BL version created a mass balance error when inflows were added. Now corrected.)

/**\fn HYD_calc_Qapp_coupled_reach(s_reach_hyd*, double, s_carac_fp*, FILE*)
 *\brief Computes total linear discharge for all river elements of a river reach, when coupled with libfp.
 *\return -
 */
void HYD_calc_Qapp_coupled_reach(s_reach_hyd *preach, double t, s_carac_fp *pchar_fp, FILE *fp) {
  int e, i, iface;
  s_element_hyd *pele;
  s_hydro_hyd *phyd;
  s_id_spa *link_fp;
  double qapp, q_lat, q_upstr, q_inflow;
  double qtot;
  double *input_fp;
  double verif, verif_upstr;
  double temp;
  link_fp = preach->link_fp;

  q_lat = 0;
  verif = 0;    // verif comptabilise le débit total en m3/s apporté au reach
  q_inflow = 0; // NG : 02/10/2020 : pour pouvoir isoler le débit apporté au reach via seulement les inflows (m3/s)
  verif_upstr = 0;

  while (link_fp != NULL) {
    input_fp = SPA_calcul_values(RIV_SPA, pchar_fp->p_cprod[link_fp->id], link_fp->prct, 0., 0., fp);

#ifdef DEBUG
    LP_printf(fp, "debit lateral pour reach %s id_gis %d, q_lat = %f q[Ruiss] %f q[Sout] %f q[Direct] %f \n", preach->river, preach->id[GIS_HYD], input_fp[0], pchar_fp->p_cprod[link_fp->id]->pwat_bal->q[RSV_RUIS], pchar_fp->p_cprod[link_fp->id]->pwat_bal->q[RSV_SOUT], pchar_fp->p_cprod[link_fp->id]->pwat_bal->q[RSV_DIRECT]);
#endif

    q_lat += input_fp[0] / preach->length; // q_lat (m2/s) = apports par ruissellement
    free(input_fp);
    link_fp = link_fp->next;
  }

  /* NG : muskingum calcule uniquement le débit à l'aval de l'élément. Pour que le 1er élement
      ne soit pas nul, 50% du q_lat de l'élément amont est reporté en [X_HYD][ONE]. */
  if (preach->limits[ONE]->nupstr_reaches != 0)
    q_upstr = 0;
  else {
    q_upstr = 0.5 * q_lat * preach->length;
    q_lat = 0.5 * q_lat;
  }

  for (e = 0; e < preach->nele; e++) {
    pele = preach->p_ele[e];
    phyd = pele->center->hydro;

    qapp = 0.; // pour chaque element, qapp additionne les apports latéraux totaux em m2/s (ruisselement + inflows)

    // NG : 02/10/2020 : Intégration des inflows
    for (iface = 0; iface < NELEMENT_HYD; iface++) {
      for (i = 0; i < pele->face[Y][iface]->ninflows; i++) {
        qapp += TS_function_value_t(t, pele->face[Y][iface]->pt_inflows[i]->discharge, fp) / pele->length;
        q_inflow += TS_function_value_t(t, pele->face[Y][iface]->pt_inflows[i]->discharge, fp); // m3/s
      }
    }

    qapp += q_lat;

    if (e == 0)
      qapp += q_upstr / pele->length;

    //  NG : 02/10/2020 : RUNOFF_HYD combine apports par ruissellement et inflows -> impossible de les dissocier dans le MB :(
    phyd->qapp[RUNOFF_HYD][T_HYD] = qapp;
    phyd->qapp[RUNOFF_HYD][INTERPOL_HYD] = phyd->qapp[RUNOFF_HYD][T_HYD];
    // LP_printf(fp,"ele %d qapp %f \n",pele->id[INTERN_HYD],qapp); // BL to debug

    if (e != 0)
      verif += phyd->qapp[RUNOFF_HYD][T_HYD] * pele->length;
    else {
      verif += (phyd->qapp[RUNOFF_HYD][T_HYD] - q_upstr / pele->length) * pele->length;
      // verif_upstr=(phyd->qapp[RUNOFF_HYD][T_HYD]-q_lat)*pele->length;
      verif_upstr = (phyd->qapp[RUNOFF_HYD][T_HYD] - q_lat - (q_inflow / pele->length)) * pele->length; // NG : soustraction de débit linéaire lié à l'inflow sinon -> MB erreur si inflow sur l'élément amont du réseau !
    }
  }

  preach->q_upstr = q_upstr;

  qtot = 0.;
  qtot = (q_inflow + q_lat * preach->length); // qtot en m3/s

  if (fabs(verif - qtot) > 0.00001)
    LP_warning(fp, "Mass conservation error for reach %s : Q_ruis %e [m3/s] Q_inflow %e [m3/s] Qapp_tot %e [m3/s] Verif %e [m3/s] Error %e [m3/s] EPS %e\n", preach->river, q_lat * preach->length, q_inflow, qtot, verif, fabs(verif - qtot), 0.00001);

  if (fabs(verif_upstr - preach->q_upstr) > 0.00001)
    LP_warning(fp, "Mass conservation error at reach upstream %s : Q_upstr %e [m3/s] Verif %e [m3/s]", preach->river, preach->q_upstr, verif_upstr);
}

/* Calculation of the linear lateral discharge that enters each element */
void HYD_calc_Qapp_coupled(double t, s_chyd *pchyd, s_carac_fp *pchar_fp, FILE *fp) {
  /* Reach, element, inflow nb */
  int r, nreaches;
  /* Current element */

  s_reach_hyd **p_reach;
#ifdef OMP
  s_smp *psmp;
  psmp = pchyd->settings->psmp;
#endif
  nreaches = pchyd->counter->nreaches;
  p_reach = pchyd->p_reach;
#ifdef OMP
  int taille;
  int nthreads;
  nthreads = psmp->nthreads;
  omp_set_num_threads(psmp->nthreads);

  psmp->chunk = PC_set_chunk_size_silent(fp, pchyd->counter->nreaches, nthreads);
  taille = psmp->chunk;
#pragma omp parallel shared(p_reach, nthreads, taille) private(r)
  {
#pragma omp for schedule(dynamic, taille)
#endif
    for (r = 0; r < pchyd->counter->nreaches; r++) {

      HYD_calc_Qapp_coupled_reach(p_reach[r], t, pchar_fp, fp);
    }
#ifdef OMP
  } /* end of parallel section */
#endif
}

/*Update stream-aquifer exchange before libhyd*/
void HYD_calc_Qapp_GW(s_chyd *pchyd, s_carac_aq *pchar_aq, double theta, double t, double deltat, FILE *fp) {
  // Reach, element, inflow nb
  int r, e, i;
  // Current element
  s_element_hyd *pele_hyd;
  s_ele_msh *pele_aq;
  s_face_msh *pface_aq;
  s_hydro_hyd *phyd;
  s_id_io *id_riv, *cauchy;
  // Total linear lateral discharge in one element
  double q_sout = 0;

  cauchy = pchar_aq->pcmsh->pcount->pcount_hydro->elebound[CAUCHY];
  cauchy = IO_browse_id(cauchy, BEGINNING_TS);

  while (cauchy != NULL) {
    pele_aq = pchar_aq->pcmsh->p_layer[cauchy->id_lay]->p_ele[cauchy->id];
    pface_aq = pele_aq->face[Z_MSH][TWO]; // Above face of each aquifer
                                          //        LP_printf(fp,"UPDATE_QAPP aquifer cauchy_layer %d, cauchy_ele: %d \n",cauchy->id_lay,cauchy->id); // BL to debug

    if (pele_aq->loc == RIV_MSH) {

      id_riv = IO_browse_id(pele_aq->id_riv, BEGINNING_TS);

      while (id_riv != NULL) {
        pele_hyd = pchyd->p_reach[id_riv->id_lay]->p_ele[id_riv->id];
        phyd = pele_hyd->center->hydro;

        q_sout = pface_aq->phydro->pcond->q;

        phyd->qapp[INR_HYD][T_HYD] = q_sout / (pele_hyd->length); // Why is it saved in m2/s?
        phyd->qapp[INR_HYD][INTERPOL_HYD] = phyd->qapp[INR_HYD][T_HYD];
        //             LP_printf(fp,"UPDATE_QAPP ele %d qapp %f \n",pele_hyd->id[GIS_HYD],phyd->qapp[INR_HYD][T_HYD] ); // BL to debug

        //          LP_printf(fp,"UPDATE_QAPP ele_river reach_id %d, id_ele: %d, qapp: %lf \n",pele_hyd->reach->id[GIS_HYD], pele_hyd->id[GIS_HYD], phyd->qapp[INR_HYD][T_HYD]); // BL to debug

        id_riv = id_riv->next;
      }
    }
    cauchy = cauchy->next;
  }
  // LP_printf(fp,"error_max sur Q_inr %e soit %e prct a l'element ID_ABS %d \n",error_Qinr,error_prct,id_aq_error);
  //  LP_printf(fp,"ele %d qapp %f \n",pele->id[INTERN_HYD],qapp); // BL to debug
}

/* Steady-state calculation
 * Iterative transient calculation (transient()) until convergence
 */
void HYD_steady_state_coupled(double dt, s_mb_hyd *pmb, s_mb_hyd *pmb2, FILE *fpmb, s_inout_set_io *pinout, s_output_hyd ***p_outputs, s_chyd *pchyd, s_clock_CHR *clock, s_chronos_CHR *chronos, s_carac_fp *pchar_fp, s_carac_aq *pchar_aq, int sim_type, FILE *pout) {
  /* Nb of the iteration, id of the element */
  int n, numf, nume, f;
  /* Calculation errors */
  double err1, err2;
  /* Iteration file */
  FILE *f_iter;
  /* Convergence criteria defined by the user */
  double eps_q, eps_z;
  s_face_hyd *pface_hyd;
  eps_q = pchyd->settings->general_param[EPS_Q];
  eps_z = pchyd->settings->general_param[EPS_Z];

  /* Opening of the iteration file */
  if (pinout->calc[ITERATIONS] == YES)
    f_iter = HYD_open_iterations_file(p_outputs, pinout, pout);

  /* Initialization of the hydraulic characteristics in the network */
  // begin_timer();//LV 3/09/2012
  // initialize_hydro();
  // assign_river();//LV nov2014
  // Simul->clock->time_spent[INITIALISATION] += end_timer();//LV 3/09/2012

  // calculate_Qapp(Simul->chronos->t[BEGINNING]);
  HYD_calc_Qapp_coupled(chronos->t[BEGINNING], pchyd, pchar_fp, pout); // LV nov2014

  CHR_begin_timer(); // LV 3/09/2012
  if (pinout->calc[ITERATIONS] == YES)
    fprintf(f_iter, "0\n"), HYD_print_hydraulic_state(f_iter, pchyd);
  clock->time_spent[OUTPUTS_CHR] += CHR_end_timer(); // LV 3/09/2012

  n = 0;
  err1 = err2 = 1.;

  /* Loop until convergence isn't reached */
  while (((err1 > eps_q) || (err2 > eps_z)) && (n < MAX_CONV)) {

    if (pmb != NULL) {                 // LV 27/09/2012
      pmb = HYD_init_mass_balance();   // NF 27/3/2012
      if (pmb2 != NULL)                // LV 27/09/2012
        pmb = HYD_chain_mb(pmb, pmb2); // NF 27/3/2012
    }

    if (pmb != NULL)                                                                                                                                     // LV 27/09/2012
      pmb = HYD_solve_hydro_coupled_old(chronos->t[BEGINNING], fpmb, pmb, pchyd, clock, chronos, p_outputs, pinout, pchar_fp, pchar_aq, sim_type, pout); // NF 27/3/2012
    else
      HYD_solve_hydro_coupled_old(chronos->t[BEGINNING], fpmb, pmb, pchyd, clock, chronos, p_outputs, pinout, pchar_fp, pchar_aq, sim_type, pout); // NF 27/3/2012
    // Chain t-1 and t mass balance
    if (pmb != NULL) // LV 27/09/2012
      pmb2 = pmb;
    err1 = err2 = 0.0;

    HYD_calc_cv_criteria(&err1, &err2, &nume, &numf, pchyd);

    n++;

    if (pinout->calc[ITERATIONS] == YES)
      fprintf(f_iter, "\n%d\n", n), HYD_print_hydraulic_state(f_iter, pchyd);

    LP_printf(pout, "%d : error on Q = %f at face nb %d\n\n", n, err1, numf);
    LP_printf(pout, "%d : error on Z = %f at element center nb %d\n\n", n, err2, nume);
  }

  pchyd->counter->niter = n;

  if ((err1 <= eps_q) && (err2 <= eps_z)) {

    LP_printf(pout, "Steady-state calculated with %d iterations.\n", n);
  } else {

    LP_printf(pout, "No convergence of the steady-state.\n");
    exit(2);
  }

  if (pinout->calc[ITERATIONS] == YES)
    fclose(f_iter);
}

/* Calculation of the transient state during one time step
 * Calls main subroutine transient() after initialization of the matrixes
 */
// s_mb *transient_hydraulics(double *t,
//			  double dt,s_mb *pmb,s_mb *pmb2,FILE *fpmb)
void HYD_transient_hydraulics_coupled(double t, double dt, s_mb_hyd *pmb, s_mb_hyd *pmb2, FILE *fpmb, s_chyd *pchyd, s_clock_CHR *clock, s_chronos_CHR *chronos, s_output_hyd ***p_outputs, s_inout_set_io *pinout, s_carac_fp *pchar_fp, s_carac_aq *pchar_aq, int sim_type, FILE *fpout) {

  // s_gc *pgc;
  // pgc=pchyd->calcul_hydro;
  // zero_hydro();

  pmb = HYD_init_mass_balance(); // LV 22/05/2012
  pmb = HYD_chain_mb(pmb, pmb2); // LV 22/05/2012

  pmb = HYD_solve_hydro_coupled_old(t, fpmb, pmb, pchyd, clock, chronos, p_outputs, pinout, pchar_fp, pchar_aq, sim_type, fpout); // NF 27/3/2012
  // return pmb;//NF 27/3/2012 soit cela, soit un chainage avec pmb2
  pmb2 = pmb; // LV 22/05/2012
}

/* Transient hydraulics
 * Fills the matrix for resolution of St-Venant equations
 * and calls the solving functions
 */
s_mb_hyd *HYD_solve_hydro_coupled_old(double t, FILE *fpmb, s_mb_hyd *pmb, s_chyd *pchyd, s_clock_CHR *clock, s_chronos_CHR *chronos, s_output_hyd ***p_outputs, s_inout_set_io *pinout, s_carac_fp *pchar_fp, s_carac_aq *pchar_aq, int sim_type, FILE *fp) {

  int schem_type;
  double dt;
  double theta;
  int *ts_count;

  // s_gc *pgc;
  // pgc=pchyd->calcul_hydro;
  dt = chronos->dt;

  schem_type = pchyd->settings->schem_type;
  theta = pchyd->settings->general_param[THETA];
  if (pmb != NULL) {                                       // LV 27/09/2012
    pmb->time = t;                                         // NF 27/3/2012
    pmb = HYD_set_state_for_mass_balance(pmb, INI, pchyd); // NF 27/3/2012
  }

  // print_outputs(t);//LV test 26/07/2012

  CHR_begin_timer(); // LV 3/09/2012

  /* Calculation of all the matrixes in the system */
  HYD_calc_Qapp_coupled(t, pchyd, pchar_fp, fp);
  if (sim_type == SW_GW) {

    HYD_calc_Qapp_GW(pchyd, pchar_aq, theta, t, dt, fp);
  }
  if (schem_type == MUSKINGUM) {
    HYD_calc_RHS_Musk(t, dt, pchyd, fp);
  } else {
    HYD_calc_coefs_ele(t, dt, pchyd, fp);
    HYD_calc_coefs_faces(t, dt, pchyd, fp);
  }

  clock->time_spent[MATRIX_FILL_CHR] += CHR_end_timer(); // LV 3/09/2012
  CHR_begin_timer();                                     // LV 3/09/2012

  /* Solving of the equations */
  // calculate_Q(t);
  // calculate_Z(t);
  HYD_calc_sol_hydro(pchyd, ts_count, fp); // LV nov2014 seul appel dans hydraulics_v2.c aux fonctions de libraire libgc (cf. calc_hyd_gc.c)

  clock->time_spent[MATRIX_SOLVE_CHR] += CHR_end_timer(); // LV 3/09/2012

  CHR_begin_timer(); // LV 3/09/2012
  /* Calculation of the new hydraulic characteristics at each face and element center */

  HYD_interpolate_Q_Z(t, pchyd, T_HYD, fp);

  clock->time_spent[MATRIX_GET_CHR] += CHR_end_timer(); // LV 3/09/2012
  // pele->hydro->H = assess_infilt(pele);//NF 11/19/06 Pour memoire quand on traitera les fonds secs

  if (pmb != NULL) // LV 27/09/2012
  {

    pmb = HYD_set_state_for_mass_balance(pmb, END, pchyd);
  }
  // if(test_print_time(*t,HYDRAULIC) == YES) {//NF 2/23/07 put HYDRAULIC as an argument of test
  CHR_begin_timer(); // LV 3/09/2012
  if (pmb != NULL)   // LV 27/09/2012
  {

    pmb = HYD_print_mass_balance(pmb, t, fpmb, chronos, fp); // NF 5/15/06
  }
  if (pinout->calc[MB_ELE] == YES) // LV 15/06/2012
    HYD_print_mb_at_elements(t, p_outputs, pchyd, chronos, fp);
  // build_outputs_hydro(*t);
  // }
  clock->time_spent[OUTPUTS_CHR] += CHR_end_timer(); // LV 3/09/2012

  return pmb;
}

/* Transient hydraulics
 * Fills the matrix for resolution of St-Venant equations
 * and calls the solving functions
 */
// NG : 11/06/2021 : ts_count added
void HYD_solve_hydro_coupled(double t, s_chyd *pchyd, s_clock_CHR *clock, s_chronos_CHR *chronos, s_carac_fp *pchar_fp, s_carac_aq *pchar_aq, int sim_type, int type_sol, int *ts_count, FILE *fp) {

  int schem_type;
  double dt;
  double theta;

  // s_gc *pgc;
  // pgc=pchyd->calcul_hydro;
  dt = chronos->dt;

  schem_type = pchyd->settings->schem_type;
  theta = pchyd->settings->general_param[THETA];

  CHR_begin_timer(); // LV 3/09/2012

  /* Calculation of all the matrixes in the system */
  HYD_calc_Qapp_coupled(t, pchyd, pchar_fp, fp);
  if (sim_type == SW_GW) {

    HYD_calc_Qapp_GW(pchyd, pchar_aq, theta, t, dt, fp);
  }
  if (schem_type == MUSKINGUM) {
    HYD_calc_RHS_Musk(t, dt, pchyd, fp);
  } else {
    HYD_calc_coefs_ele(t, dt, pchyd, fp);
    HYD_calc_coefs_faces(t, dt, pchyd, fp);
  }

  clock->time_spent[MATRIX_FILL_CHR] += CHR_end_timer(); // LV 3/09/2012
  CHR_begin_timer();                                     // LV 3/09/2012

  HYD_calc_sol_hydro(pchyd, ts_count, fp); // LV nov2014 seul appel dans hydraulics_v2.c aux fonctions de libraire libgc (cf. calc_hyd_gc.c)

  clock->time_spent[MATRIX_SOLVE_CHR] += CHR_end_timer(); // LV 3/09/2012

  CHR_begin_timer(); // LV 3/09/2012
  /* Calculation of the new hydraulic characteristics at each face and element center */
  if (type_sol == PIC_HYD) {
    HYD_extract_solgc_pic(pchyd, fp);
    HYD_interpolate_Q_Z(t, pchyd, type_sol, fp);
  } else // SW 07/10/2021 add extraction for T_HYD
  {
    HYD_extract_solgc(pchyd, fp);
    HYD_interpolate_Q_Z(t, pchyd, type_sol, fp);
  }
  clock->time_spent[MATRIX_GET_CHR] += CHR_end_timer(); // LV 3/09/2012
  // pele->hydro->H = assess_infilt(pele);//NF 11/19/06 Pour memoire quand on traitera les fonds secs
}

void HYD_refresh_qapp_coupled(s_chyd *pchyd, FILE *fp) {
  int r, e, i;
  s_reach_hyd *preach;
  s_element_hyd *pele;

  for (r = 0; r < pchyd->counter->nreaches; r++) {

    preach = pchyd->p_reach[r];

    for (e = 0; e < preach->nele; e++) {
      pele = preach->p_ele[e];

      for (i = 0; i < NQAPP; i++) {
        pele->center->hydro->qapp[i][ITER_HYD] = pele->center->hydro->qapp[i][T_HYD];
        pele->center->hydro->qapp[i][ITER_INTERPOL_HYD] = pele->center->hydro->qapp[i][INTERPOL_HYD];
      }
    }
  }
}

int HYD_check_reach(s_id_io *id_riv, s_chyd *pchyd, FILE *fp) {
  s_id_io *pid;
  int id_reach, id_check;
  int answ = YES;
  s_element_hyd *pele_hyd;
  pid = id_riv;
  if (pid != NULL)
    pid = IO_browse_id(pid, BEGINNING_IO);
  pele_hyd = pchyd->p_reach[pid->id_lay]->p_ele[pid->id];
  id_check = pele_hyd->reach->id[GIS_HYD];
  while (pid != NULL) {
    pele_hyd = pchyd->p_reach[pid->id_lay]->p_ele[pid->id];
    id_reach = pele_hyd->reach->id[GIS_HYD];
    // LP_printf(fp,"id_reach %d id_check %d \n",id_reach,id_check);
    if (id_check != id_reach) {
      answ = NO;
      break;
    }
    pid = pid->next;
  }
  return (answ);
}

s_id_io *HYD_get_id_riv_down(s_chyd *pchyd, s_id_io *id_riv, int length_id, FILE *fp) {

  int nreach, idreach, ndown, answ, niddown, id_max;
  int *id_reach, *id_down;
  s_id_io *downstream;
  double *width, *length;
  double max_width, max_length;
  s_id_io *pid;
  int i = 0;
  s_element_hyd *pele_hyd;
  s_reach_hyd *preach;
  s_singularity_hyd *psing;
  id_reach = (int *)malloc(length_id * sizeof(int));
  id_down = (int *)malloc(length_id * sizeof(int));
  width = (double *)malloc(length_id * sizeof(double));
  length = (double *)malloc(length_id * sizeof(double));
  pid = IO_browse_id(id_riv, BEGINNING_IO);
  for (i = 0; i < length_id; i++) {
    id_down[i] = CODE;
    pele_hyd = pchyd->p_reach[pid->id_lay]->p_ele[pid->id];
    id_reach[i] = pele_hyd->reach->id[GIS_HYD];
    width[i] = pele_hyd->reach->width;
    length[i] = pele_hyd->length;
    pid = pid->next;
  }
  pid = IO_browse_id(id_riv, BEGINNING_IO);
  idreach = 0;
  niddown = 0;
  while (pid != NULL) {

    pele_hyd = pchyd->p_reach[pid->id_lay]->p_ele[pid->id];
    preach = pele_hyd->reach;
    psing = preach->limits[DOWNSTREAM];
    ndown = psing->ndownstr_reaches;
    nreach = 0;
    answ = YES;
    while (ndown > 0 && nreach < 10) {
      preach = psing->p_reach[DOWNSTREAM][0];
      psing = preach->limits[DOWNSTREAM];
      ndown = psing->ndownstr_reaches;
      for (i = 0; i < length_id; i++) {
        if (preach->id[GIS_HYD] == id_reach[i]) {
          answ = NO;
          break;
        }
      }
      nreach++;
    }
    if (answ == YES) {
      id_down[niddown] = idreach;
      niddown++;
    }
    idreach++;
    pid = pid->next;
  }
  if (niddown > 1) {
    // LP_printf(fp,"niddown %d \n",niddown);
    max_width = -9999;
    max_length = -9999;
    for (i = 0; i < niddown; i++) {
      if (width[id_down[i]] >= max_width && length[id_down[i]] >= max_length) {
        max_width = width[id_down[i]];
        max_length = length[id_down[i]];
        id_max = id_down[i];
      }
    }
  } else {
    id_max = id_down[0];
  }
  if (id_max == CODE)
    LP_error(fp, "CODE is returned for id_max !!\n");
  i = 0;
  pid = IO_browse_id(id_riv, BEGINNING_IO);
  while (i < id_max) {
    pid = pid->next;
    i++;
  }
  free(width);
  free(id_down);
  free(id_reach);
  free(length);
  if (pid != NULL) {
    downstream = IO_create_id(pid->id_lay, pid->id);
    return (downstream);
  } else
    LP_error(fp, "The id_down is Null !!\n");
}

s_reach_hyd *HYD_create_reaches_musk_coupled(s_lec_tmp_hyd **plec, s_chyd *pchyd, int dim_tmp_lec, char **FnodeTnode, s_carac_fp *pchar_fp, FILE *fp) {
  int i;
  s_reach_hyd *preach, *preach_tmp;
  int id_cprod;
  preach = NULL;
  for (i = 0; i < dim_tmp_lec; i++) {
    if (plec[i] != NULL) {
      // LP_printf(fp,"Creating reach : upstr sing %s down sing %s, Fnode %d,Tnode %d \n",FnodeTnode[plec[i]->Fnode],FnodeTnode[plec[i]->Tnode],plec[i]->Fnode,plec[i]->Tnode); //BL to debug
      preach_tmp = HYD_create_reach(FnodeTnode[plec[i]->Fnode], FnodeTnode[plec[i]->Tnode], 1, pchyd, fp);

      HYD_set_reach_param(preach_tmp, plec[i]->id_gis, plec[i]->Alt_Fnode, plec[i]->Alt_Tnode, plec[i]->slope, plec[i]->strahler, plec[i]->length, plec[i]->Wed_Area, plec[i]->k_musk, plec[i]->alpha, plec[i]->width, plec[i]->strickler, plec[i]->Qlim, fp);
      id_cprod = FP_search_id_cprod(pchar_fp, preach_tmp->id[GIS_HYD], GIS_HYD, INTERN_HYD, fp);
      // LP_printf(fp,"id_cprod %d linked with reach %s id %d\n",id_cprod,preach_tmp->limits[UPSTREAM]->name,preach_tmp->id[GIS_HYD]); //BL to debug
      if (fabs(id_cprod - CODE) < EPS_HYD)
        LP_error(fp, "cannot find id %d in cprod fp check your input files !\n", preach_tmp->id[GIS_HYD]);
      preach_tmp->link_fp = SPA_create_ids(id_cprod, 1.);
      preach = HYD_chain_reaches_fwd(preach, preach_tmp);
    }
  }

  preach = HYD_rewind_reach(preach);
  return preach;
}
void HYD_finalyse_link_reach_coupled(int id_reach, int id_cprod, double prct, int nb_lec, s_chyd *pchyd, FILE *fp) {
  // BL nb_lec doit etre le nombre de fois que le id_reach a été lu!!
  s_reach_hyd *preach;
  s_id_spa *plink_tmp;
  preach = pchyd->p_reach[id_reach - 1];
  if (nb_lec > 1)
    preach->link_fp = SPA_free_ids(preach->link_fp, fp);
  plink_tmp = SPA_create_ids(id_cprod, prct);
  preach->link_fp = SPA_secured_chain_id_fwd(preach->link_fp, plink_tmp);
}

// NG : 24/01/2020 : Calcul de l'attribut Q_runoff (débit rivière ou ruissellement) en fonction du type de catchment et du type de la cprod aval.
void CAW_cHYD_calc_runoff_catchment_chasm_all(s_carac_zns *pcarac_zns, s_chyd *pchyd, FILE *fpout) {

  int nb_zns = pcarac_zns->nb_zns;
  s_zns *pzns;
  int i;

  for (i = 0; i < nb_zns; i++) {
    pzns = pcarac_zns->p_zns[i];
    if (pzns->pcatchment_chasm != NULL && pzns->pcatchment_chasm->catchment_type == FP_CATCH_CHASM) {
      CAW_cHYD_calc_runoff_catchment_chasm(pzns, pchyd, fpout);
    }
  }
}

// NG : 10/03/2020 : Ajout de la possibilité de n'infiltrer que la fraction catchment->infil_coef du débit du chasm
void CAW_cHYD_calc_runoff_catchment_chasm(s_zns *pzns, s_chyd *pchyd, FILE *fpout) {

  int nb_reach = pchyd->counter->nreaches;
  int i, j, id_cprod_outlet, nele;
  int is_outlet = NO;
  int id_eleriv;
  s_reach_hyd *p_reach_st;
  s_element_hyd *pele;
  double Q = 0.;

  if (pzns->pcatchment_chasm->pcprod_outlet->cell_prod_type == FP_CATCH_RIV) { // la cprod outlet du catchment est de type riv : on infiltre le débit rivière à l'aval du catchment
    id_cprod_outlet = pzns->pcatchment_chasm->pcprod_outlet->id[FP_GIS];

    for (i = 0; i < nb_reach; i++) {
      if (pchyd->p_reach[i]->id[GIS_HYD] == id_cprod_outlet) { // On cherche le reach aval du bassin
        p_reach_st = pchyd->p_reach[i];
        break;
      }
    }

    // On détermine si le reach contient bien un element outlet, si oui, on récupère son débit, et on l'assiqgne à Q_chasm.
    nele = p_reach_st->nele;
    for (j = 0; j < nele; j++) {
      pele = p_reach_st->p_ele[j];
      id_eleriv = pele->id[GIS_HYD];
      is_outlet = HYD_check_is_element_outlet(pchyd, id_eleriv, fpout);
      if (is_outlet == YES)
        break;
    }

    if (is_outlet == NO)
      LP_error(fpout, "There's no outlet river element in the river reach GIS ID %d. Check your input files. Error in file %s, function %s at line.\n", p_reach_st->id[GIS_HYD], __FILE__, __func__, __LINE__);

    pzns->pcatchment_chasm->Q_chasm = pele->face[X_HYD][TWO]->hydro->Q[T_HYD] * pzns->pcatchment_chasm->infil_coef;

    //   LP_printf(fpout,"RIV catch %d %20.9f\n",pzns->pcatchment_chasm->id_gis,pzns->pcatchment_chasm->Q_chasm);

  } else {                                                                                                                               // la cprod outlet du catchment est de type chasm : on infiltre le ruissellement de la cprod aval
    pzns->pcatchment_chasm->Q_chasm = pzns->pcatchment_chasm->pcprod_outlet->pwat_bal->q[RSV_RUIS] * pzns->pcatchment_chasm->infil_coef; // (m3/s)
    //   LP_printf(fpout,"Ruis_cprod_GIS %d %f catch %s.\n",pzns->pcatchment_chasm->pcprod_outlet->id[FP_GIS],pzns->pcatchment_chasm->Q_chasm,pzns->pcatchment_chasm->catchment_name);
    //   LP_printf(fpout,"RUIS catch %d %20.9f\n",pzns->pcatchment_chasm->id_gis,pzns->pcatchment_chasm->Q_chasm);
  }
}

/* NG : 25/02/2020 :
     --> Calcul des Kmusk par reach rivière en utilisant le Tc des catchments pour la méthode TTRA
     --> Calcul des Itr_max par catchments
  NG : 29/02/2020
     --> Modifications dans les fonctions utilisées par FORMULA pour prendre en compte les Tc des catchments   */

void CAW_cHYD_set_K_basin_by_Tc_catchments(s_chyd *pchyd, s_carac_fp *pcarac_fp, FILE *fpout) {

  int Kdef, nb_catch, c;
  s_catchment_fp *pcatch;
  Kdef = pchyd->settings->Kdef;
  nb_catch = pcarac_fp->nb_catchment;

  switch (Kdef) {
  case FORMULA: {
    HYD_calc_TC_reach(pchyd, fpout);                     // Calcul des Ki musk pour tous les reachs
    CAW_cHYD_get_TC_Catchments(pchyd, pcarac_fp, fpout); // Calcul des Tc_Formula pour tous les catchments

    pcatch = FP_browse_catchment_chain(pcarac_fp->p_catchment, BEGINNING_FP, fpout);

    while (pcatch != NULL) {
      if (pcatch->concentration_time > 0 && pcatch->Tc_Formula > pcatch->concentration_time) {
        LP_warning(fpout, "Calculated Tc (FORMULA method) and input Tc for catchment %s are different (Input = %f, Calc = %f) --> Tc catchment is set to %f.\n", pcatch->catchment_name, pcatch->concentration_time, pcatch->Tc_Formula, pcatch->concentration_time);
        CAW_cHYD_set_TC_reach_Catchment(pchyd, pcatch, fpout);
      }
      pcatch = pcatch->next;
    }
    break;
  }
  case TTRA: {
    CAW_cHYD_set_K_TTRA_Bassin_by_Tc_catchments(pchyd, pcarac_fp, fpout);
    break;
  }
  default:
    LP_error(fpout, "In CaWaQS%.2f : Error in file %s, function %s : Unknown Kdef method.\n", NVERSION_CAW, __FILE__, __func__);
  }
}

void CAW_cHYD_set_K_TTRA_Bassin_by_Tc_catchments(s_chyd *pchyd, s_carac_fp *pcarac_fp, FILE *fpout) {

  int nreaches, r, c, idreach;
  int SDA = 0, ITRI = 1;
  double **Itr;
  double Tc_catch, Itr_max_catch;
  s_reach_hyd *preach;
  s_catchment_fp *pcatch, *pcatch_tmp;

  nreaches = pchyd->counter->nreaches;

  Itr = (double **)malloc(nreaches * sizeof(double *));
  if (Itr == NULL)
    LP_error(fpout, "Bad memory allocation for Itr matrix in file %s, function %s at line %d.\n", __FILE__, __func__, __LINE__);

  for (r = 0; r < nreaches; r++) {
    Itr[r] = (double *)malloc(2 * sizeof(double));
    if (Itr[r] == NULL)
      LP_error(fpout, "Bad memory allocation for Itr matrix in file %s, function %s at line %d.\n", __FILE__, __func__, __LINE__);
    preach = pchyd->p_reach[r];
    Itr[r][SDA] = preach->pmusk->param[AREA] / 1000000.; // Surface drainée par le reach en km2 (= surface de la cprod associée)
    Itr[r][ITRI] = 0;
  }

  HYD_calc_SDA(pchyd, Itr, fpout);
  CAW_cHYD_calc_Itr_i_outlets(pchyd, pcarac_fp, Itr, fpout); // Détermine le Itr_max du catchment !

  //	for (r=0;r<nreaches;r++) LP_printf(fpout,"Surface amont drainée totale pour le reach %d = %f.\n",pchyd->p_reach[r]->id[GIS_HYD],Itr[r][SDA]);   // NG check

  pcatch_tmp = FP_browse_catchment_chain(pcarac_fp->p_catchment, BEGINNING_FP, fpout);
  while (pcatch_tmp != NULL) {
    // LP_printf(fpout,"Itr_max = %f pour le catchment %s.\n",pcatch_tmp->Itr_max,pcatch_tmp->catchment_name); // NG check

    if (pcatch_tmp->catchment_type == FP_CATCH_RIV && pcatch_tmp->Itr_max < 0) {
      //  LP_printf(fpout,"Itr_max %f\n",pcatch_tmp->Itr_max);
      LP_error(fpout, "Itr_max hasn't been calculated for river catchment %s.\n", pcatch_tmp->catchment_name);
    }

    pcatch_tmp = pcatch_tmp->next;
  }

  // Calcul des K musk
  // LP_printf(fpout," \n ********** Muskingum river reaches attributes ********** \n");

  for (r = 0; r < nreaches; r++) {
    preach = pchyd->p_reach[r];
    idreach = preach->id[GIS_HYD];
    pcatch_tmp = FP_browse_catchment_chain(pcarac_fp->p_catchment, BEGINNING_FP, fpout);
    while (pcatch_tmp != NULL) {
      for (c = 0; c < pcatch_tmp->nb_cprod_catchment; c++) {
        if (pcatch_tmp->cprod_catchment[c]->id[FP_GIS] == idreach) {
          Tc_catch = pcatch_tmp->concentration_time;
          Itr_max_catch = pcatch_tmp->Itr_max;
          break;
        }
      }
      pcatch_tmp = pcatch_tmp->next;
    }

    preach->pmusk->param[K] = (Itr[r][ITRI] / Itr_max_catch) * Tc_catch;
    // LP_printf(fpout,"TTRA_Reach %d  Tc %f Itr_max %f Kmusk %f\n",idreach,Tc_catch,Itr_max_catch,preach->pmusk->param[K]); // NG check
    // LP_printf(fpout,"River reach GIS_ID %d : K_musk = %f\n",idreach,preach->pmusk->param[K]);
  }

  for (r = 0; r < nreaches; r++) {
    free(Itr[r]);
    Itr[r] = NULL;
  }
  free(Itr);
  Itr = NULL;
}

// NG : 26/02/2020 : Fonction dérivée de HYD_calc_Itr_i (BL) pour calculer directement le Itr_max par catchment
void CAW_cHYD_calc_Itr_i_outlets(s_chyd *pchyd, s_carac_fp *pcarac_fp, double **Itr, FILE *fp) {
  s_reach_hyd *preach;
  s_singularity_hyd *psing;
  int SDA = 0, ITRI = 1;
  double Area_up, betha = 0.25, Itr_max = 0;
  int r, i, ndown, nreaches, c;
  int nb_cprod;
  s_catchment_fp *pcatch;

  nreaches = pchyd->counter->nreaches;

  for (r = 0; r < nreaches; r++) {
    preach = pchyd->p_reach[r];
    Itr[r][ITRI] = (preach->length * 0.001) / (sqrt(preach->pmusk->param[SLOPE]) * pow(Itr[r][SDA], betha));

    psing = preach->limits[UPSTREAM];
    if (psing->nupstr_reaches == 0) {
      psing = preach->limits[DOWNSTREAM];
      ndown = psing->ndownstr_reaches;
      i = 1;
      Itr_max = Itr[r][ITRI];

      while (ndown > 0) {
        preach = psing->p_reach[DOWNSTREAM][0];
        if (Itr[preach->id[INTERN_HYD]][ITRI] == 0)
          Itr[preach->id[INTERN_HYD]][ITRI] = (preach->length * 0.001) / (sqrt(preach->pmusk->param[SLOPE]) * pow(Itr[preach->id[INTERN_HYD]][SDA], betha));
        Itr_max += Itr[preach->id[INTERN_HYD]][ITRI];
        psing = preach->limits[DOWNSTREAM];
        ndown = psing->ndownstr_reaches;
        i++;
      }

      //	LP_printf(fp,"Itr at outlet %s id %d is %f [km]\n",psing->name,preach->id[GIS_HYD],Itr_max);  // NG check

      pcatch = FP_browse_catchment_chain(pcarac_fp->p_catchment, BEGINNING_FP, fp);

      // On ne peut pas passer par pcatch->pcprod_outlet car à ce stade elle ne sont pas définies. Les cprod outlet le sont après lecture finalisée d'un réseau hydro !!
      // J'utilise le critère d'appartenance de la cprod au catchment pour actualiser et stocker la valeur de Itr_max

      while (pcatch != NULL) {
        nb_cprod = pcatch->nb_cprod_catchment;
        for (c = 0; c < nb_cprod; c++) {
          if (pcatch->cprod_catchment[c]->id[FP_GIS] == preach->id[GIS_HYD]) { // a ce stade, preach->id[GIS_HYD] est un outlet !
            if (Itr_max > pcatch->Itr_max)
              pcatch->Itr_max = Itr_max;
          }
        }
        pcatch = pcatch->next;
      }
    }
  }
}

// NG : 29/02/2020 : Fonction dérivée de HYD_get_TC_Bassin (BL) pour calculer directement le TC max par catchment quand la méthode FORMULA est sélectionnée
void CAW_cHYD_get_TC_Catchments(s_chyd *pchyd, s_carac_fp *pcarac_fp, FILE *fp) {
  double TC = 0;
  int nreaches, r, ndown, i, c, nb_cprod;
  s_reach_hyd *preach;
  s_singularity_hyd *psing;
  s_catchment_fp *pcatch;

  nreaches = pchyd->counter->nreaches;

  for (r = 0; r < nreaches; r++) {
    preach = pchyd->p_reach[r];
    psing = preach->limits[UPSTREAM];
    if (psing->nupstr_reaches == 0) {
      // LP_printf(fp,"\nTete Bassin at reach %s id %d\n",psing->name,preach->id[GIS_HYD]);
      psing = preach->limits[DOWNSTREAM];
      ndown = psing->ndownstr_reaches;
      if (ndown == 0)
        LP_warning(fp, "reach %s id %d is don't have downstream\n", psing->name, preach->id[GIS_HYD]);
      TC = preach->pmusk->param[K];
      i = 1;
      while (ndown > 0) {
        if (psing->ndownstr_reaches > 1)
          LP_warning(fp, "More than one downstream at singularity %s id %d\n", psing->name, psing->id[GIS_HYD]);
        preach = psing->p_reach[DOWNSTREAM][0];
        // LP_printf(fp," To debug : id_reach %d\n",preach->id[GIS_HYD]);
        TC += preach->pmusk->param[K];
        psing = preach->limits[DOWNSTREAM];
        ndown = psing->ndownstr_reaches;
        i++;
      }

      pcatch = FP_browse_catchment_chain(pcarac_fp->p_catchment, BEGINNING_FP, fp);

      while (pcatch != NULL) {
        nb_cprod = pcatch->nb_cprod_catchment;
        for (c = 0; c < nb_cprod; c++) {
          if (pcatch->cprod_catchment[c]->id[FP_GIS] == preach->id[GIS_HYD]) {
            if (TC > pcatch->Tc_Formula)
              pcatch->Tc_Formula = TC;
          }
        }
        // LP_printf(fp, "FORMULA method : Catchment %s. Input Tc : %f. Calculated Tc : %f.\n",pcatch->catchment_name,pcatch->concentration_time,pcatch->Tc_Formula); // NG check
        pcatch = pcatch->next;
      }
    }
  }
}

// NG : 29/02/2020 : Fonction dérivée de HYD_set_TC_reach (BL) pour modifier la valeur de K pour tous les reach d'un catchment
void CAW_cHYD_set_TC_reach_Catchment(s_chyd *pchyd, s_catchment_fp *pcatch, FILE *fp) {
  int r, c;
  s_cprod_fp *pcprod;
  s_reach_hyd *preach;
  int nreaches = pchyd->counter->nreaches;
  int nb_cprod = pcatch->nb_cprod_catchment;
  double Tc_frac = pcatch->concentration_time / pcatch->Tc_Formula;

  for (c = 0; c < nb_cprod; c++) {
    pcprod = pcatch->cprod_catchment[c];
    for (r = 0; r < nreaches; r++) {
      preach = pchyd->p_reach[r];
      if (preach->id[GIS_HYD] == pcprod->id[FP_GIS]) {
        if (preach->pmusk->param[SLOPE] < EPS_HYD)
          LP_error(fp, "Slope is null for reach named %s in reach %d slope %e\n", preach->limits[UPSTREAM]->name, preach->id[GIS_HYD], preach->pmusk->param[SLOPE]);
        if (preach->pmusk->param[AREA] < EPS_HYD)
          LP_error(fp, "Area BV is null for reach named %s in reach %d area %e\n", preach->limits[UPSTREAM]->name, preach->id[GIS_HYD], preach->pmusk->param[AREA]);
        preach->pmusk->param[K] *= Tc_frac;
      }
    }
  }
}
