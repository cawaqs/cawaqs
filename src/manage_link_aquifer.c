/*-------------------------------------------------------------------------------
 *
 * SOFTWARE NAME: cawaqs
 * FILE NAME: manage_link_aquifer.c
 *
 * CONTRIBUTORS: Nicolas GALLOIS, Baptiste LABARTHE, Shuaitao WANG,
 *               Agnès RIVIERE, Deniz KILIC, Mathias MAILLOT,
 *               Fulvia BARATELLI, Pierre GUILLOU, Emmanuel LEDOUX, Nicolas FLIPO
 *
 * SOFTWARE BRIEF DESCRIPTION: Physically based coupled surface–subsurface
 * model for joint water, matter and energy flux balances and flow dynamics
 * simulation within all compartments of a hydrosystem (sub-surface, hydraulic
 * network, vadose zone, aquifer system and stream-aquifer exchanges).
 *
 * Software developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the cawaqs Software.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Software and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <time.h>
#include <stdarg.h>
#include <sys/time.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <malloc.h>
#include <math.h>
#include <libprint.h>

#ifdef OMP
#include <omp.h>
#endif

#include <time_series.h>
#include "libpc.h"
#include "IO.h"
#include "GC.h"
#include "reservoir.h"
#include "spa.h"
#include "FP.h"
#include "NSAT.h"
#include "CHR.h"
#include "HYD.h"
#include "MSH.h"
#include "AQ.h"
#include "WET.h"
#include "TTC_coupled.h"
#include "SEB.h"
#include "CAW.h"
#include "global_CAWAQS.h"

/**
 * \brief Function to fill the parameters of the transport problem (DISPERSIVITY, POROSITY)
 * \return -
 */
void CAW_build_TTC_param_syst_aq(s_cmsh *pcmsh, s_species_ttc *pspecies, int npa, FILE *flog) {

  int i, j, nlayer, nele, id;
  int elettc = 0;
  s_ele_msh *pele;
  s_layer_msh *play;

  nlayer = pcmsh->pcount->nlayer;

  for (i = 0; i < nlayer; i++) {
    play = pcmsh->p_layer[i];
    nele = play->nele;

    for (j = 0; j < nele; j++) {
      pele = play->p_ele[j];

      if (pele->pparam_ttc != NULL) {
        pspecies->p_param_ttc[elettc]->param_syst[npa] = pele->pparam_ttc->param_syst[npa];
      }
      // LP_printf(flog,"TTC-link system parameter : %s - TTC ABS id %d (MSH ABS id %d) = %f\n",TTC_param_syst(npa),
      // elettc,pele->id[ABS_MSH],pspecies->p_param_ttc[elettc]->param_syst[npa]); // NG check.
      elettc++;
    }
  }
}

/**
 * \brief Function to fill the parameters of the transport problem (thermal conductivity, heat capacity)
 * \return -
 */
void CAW_build_TTC_param_therm(s_cmsh *pcmsh, s_species_ttc *pspecies, int nenv, int npa, FILE *flog) {

  int i, j, nlayer, nele;
  int elettc = 0;
  s_ele_msh *pele;
  s_layer_msh *play;

  nlayer = pcmsh->pcount->nlayer;

  for (i = 0; i < nlayer; i++) {
    play = pcmsh->p_layer[i];
    nele = play->nele;

    for (j = 0; j < nele; j++) {
      pele = play->p_ele[j];
      if (pele->pparam_ttc != NULL) {
        pspecies->p_param_ttc[elettc]->param_thermic[nenv][npa] = pele->pparam_ttc->param_thermic[nenv][npa];
      }
      // LP_printf(flog,"TTC-link thermic parameter %s (env : %s) - TTC ABS id %d (MSH ABS id %d) = %f\n",TTC_thermic_param(npa),
      // TTC_environnement(nenv),elettc,pele->id[ABS_MSH],pspecies->p_param_ttc[elettc]->param_thermic[nenv][npa]); // NG check
      elettc++;
    }
  }
}

/**
 * \brief Function to fill the size of each element in the aquifer mesh
 * \return -
 */
void CAW_fill_dist_one_species_aq(s_cmsh *pcmsh, s_species_ttc *pspecies, FILE *flog) {

  int i, j, nlayer, nele, elettc = 0;
  s_ele_msh *pele;
  s_layer_msh *play;

  nlayer = pcmsh->pcount->nlayer;

  for (i = 0; i < nlayer; i++) {

    play = pcmsh->p_layer[i];
    nele = play->nele;

    for (j = 0; j < nele; j++) {

      pele = play->p_ele[j];
      pspecies->p_param_ttc[elettc]->param_syst[SIZE_TTC] = pele->pdescr->l_side;
      pspecies->p_param_ttc[elettc]->thickness = pele->phydro->Thick;
      // LP_printf(flog,"Size of element TTC id abs %d (MSH ABS id %d) = %lf, thickness %lf\n",elettc,pele->id[ABS_MSH],
      //           pspecies->p_param_ttc[elettc]->param_syst[SIZE_TTC],pspecies->p_param_ttc[elettc]->thickness); // NG check
      elettc++;
    }
  }
}

/**
 * \brief Function to fill the distance variable between each neighbouring cell of the elements in the mesh
 * \return -
 *
 *  WARNING : This function returns the delta_L without the adjustment for the aquitards, another
 *  function [name ?] modifies the delta_L to include the thickness distance of the aquitard layer.
 *
 */
void CAW_build_deltaL_aq(s_cmsh *pcmsh, s_species_ttc *pspecies, s_link_ttc *plink, FILE *flog) {
  int i, j, k, idir, itype, nlayer, nele, icard, id;
  double L1, L2, xc_ele, yc_ele, xc_neigh, yc_neigh, dist;
  double zc_ele, zc_neigh;
  int elettc = 0;
  int nsub = 0;
  s_ele_msh *pele;
  s_id_io *neigh;
  s_ele_msh *pneigh;
  s_layer_msh *play;
  s_descr_msh *pdescr;
  char *name_card = NULL;

  nlayer = pcmsh->pcount->nlayer;

  for (i = 0; i < nlayer; i++) {
    play = pcmsh->p_layer[i];
    nele = play->nele;
    for (j = 0; j < nele; j++) {
      pele = play->p_ele[j];
      id = MSH_get_ele_id(pele, ABS_MSH);
      xc_ele = pele->pdescr->center[X_MSH];
      yc_ele = pele->pdescr->center[Y_MSH];

      for (idir = 0; idir < ND_MSH; idir++) {
        for (itype = 0; itype < NFACE_MSH; itype++) {
          k = 0;
          icard = CAW_TTC_card_aq(idir, itype);

          nsub = plink->nsub[elettc][icard];
          name_card = TTC_param_card(icard);
          neigh = pele->p_neigh[idir][itype];
          neigh = IO_browse_id(neigh, BEGINNING_TS);

          // Here we use the absolute distance between two cells in a given direction, not the hypothenus,
          // Since delta_L is used to calculate mat6, it should be calculated for the border cells without neighbors.
          // Hence lines 165:174 facilitate the distances to the borders of the simulated domain

          if (icard == BOTTOM_TTC && neigh == NULL) {
            if (nsub != 0) { // if the face has subfaces then we loop in the subfaces, if no subfaces, we pass the value at the face directly
              for (int isub = 0; isub < nsub; isub++) {
                plink->p_neigh_ttc[elettc]->delta_L[icard][isub] = pele->phydro->Thick;
                // LP_printf(flog,"Distance from %d to noneigh in dir %s neigh %d = %f\n",pele->id[ABS_MSH], name_card,isub,plink->p_neigh_ttc[elettc]->delta_L[icard][k]);
                if (plink->p_neigh_ttc[elettc]->delta_L[icard][isub] < 1.) {
                  LP_warning(flog, "%s delta_L is below 1m thick at ele %d face %s", __func__, i, TTC_param_card(idir));
                }
              }
            } else {
              plink->p_neigh_ttc[elettc]->delta_L[icard][nsub] = pele->phydro->Thick;
              // LP_printf(flog,"Distance from %d to noneigh in dir %s neigh %d = %f\n",pele->id[ABS_MSH], name_card,nsub,plink->p_neigh_ttc[elettc]->delta_L[icard][k]);
              if (plink->p_neigh_ttc[elettc]->delta_L[icard][nsub] < 1.) {
                LP_warning(flog, "%s delta_L is below 1m thick at ele %d face %s", __func__, i, TTC_param_card(idir));
              }
            }
          } else if (icard == TOP_TTC && neigh == NULL) {

            if (nsub != 0) { // if the face has subfaces then we loop in the subfaces, if no subfaces, we pass the value at the face directly
              for (int isub = 0; isub < nsub; isub++) {
                plink->p_neigh_ttc[elettc]->delta_L[icard][isub] = pele->phydro->Thick;
                // LP_printf(flog,"Distance from %d to noneigh in dir %s neigh %d = %f\n",pele->id[ABS_MSH], name_card,isub,plink->p_neigh_ttc[elettc]->delta_L[icard][k]);

                if (plink->p_neigh_ttc[elettc]->delta_L[icard][isub] < 1.) {
                  LP_warning(flog, "%s delta_L is below 1m thick at ele %d face %s", __func__, i, TTC_param_card(idir));
                }
              }
            } else {
              plink->p_neigh_ttc[elettc]->delta_L[icard][nsub] = pele->phydro->Thick;
              // LP_printf(flog,"Distance from %d to noneigh in dir %s neigh %d = %f\n",pele->id[ABS_MSH], name_card,nsub,plink->p_neigh_ttc[elettc]->delta_L[icard][k]);
              if (plink->p_neigh_ttc[elettc]->delta_L[icard][nsub] < 1.) {
                LP_warning(flog, "%s delta_L is below 1m thick at ele %d face %s", __func__, i, TTC_param_card(idir));
              }
            }
          } else if ((icard != TOP_TTC || icard != BOTTOM_TTC) && neigh == NULL) {
            L1 = pele->pdescr->l_side;
            // L2=pneigh->pdescr->l_side;
            if (nsub != 0) {
              for (int isub = 0; isub < nsub; isub++) {
                plink->p_neigh_ttc[elettc]->delta_L[icard][isub] = fabs(L1 + L1) / 2.;
                // LP_printf(flog,"Distance from %d to noneigh in dir %s neigh %d = %f\n",pele->id[ABS_MSH], name_card,isub,plink->p_neigh_ttc[elettc]->delta_L[icard][k]);
                if (plink->p_neigh_ttc[elettc]->delta_L[icard][isub] < 1.) {
                  LP_warning(flog, "%s delta_L is below 1m thick at ele %d face %s", __func__, i, TTC_param_card(idir));
                }
              }
            } else {
              plink->p_neigh_ttc[elettc]->delta_L[icard][nsub] = fabs(L1 + L1) / 2.; //;dist
              // LP_printf(flog,"Distance from %d to noneigh in dir %s neigh %d = %f\n",pele->id[ABS_MSH], name_card,nsub,plink->p_neigh_ttc[elettc]->delta_L[icard][k]);
              if (plink->p_neigh_ttc[elettc]->delta_L[icard][nsub] < 1.) {
                LP_warning(flog, "%s delta_L is below 1m thick at ele %d face %s", __func__, i, TTC_param_card(idir));
              }
            }
          }

          // Finds the distance for the cells with an active neighbor;
          while (neigh != NULL) {
            pneigh = MSH_get_ele(pcmsh, neigh->id_lay, neigh->id, flog);
            xc_neigh = pneigh->pdescr->center[X_MSH];
            yc_neigh = pneigh->pdescr->center[Y_MSH];
            dist = 0.;
            dist = sqrt(pow(xc_neigh - xc_ele, 2) + pow(yc_neigh - yc_ele, 2));
            if (icard == TOP_TTC || icard == BOTTOM_TTC) {
              L1 = pele->phydro->Thick; // TODO The thicknesses are passed here but the diagonal between coordinates should be calculated to be accurate when using nested case
              L2 = pneigh->phydro->Thick;
              // zc_ele = pele->pdescr->center[Z_MSH]; TODO Z_MSH is currently assigned to -9999 so it should be corrected.
              // zc_neigh = pneigh->pdescr->center[Z_MSH];
              // LP_printf(flog,"Distance from %d to %d in dir %s z1 %f, z2: %f\n",pele->id[ABS_MSH],pneigh->id[ABS_MSH],TTC_param_card(icard),L1,L2); // NG check
              // dist = sqrt(pow(dist,2)+pow(zc_neigh-zc_ele,2));
              // dist = sqrt(sqrt(pow(xc_neigh-xc_ele,2)+pow(yc_neigh-yc_ele,2)) + pow(fabs(L1/2+L2/2),2)) ;
              // LP_printf(flog,"in func: %s,from id %d to neigh %d at face %s: xc_neigh = %lf, xc_ele = %lf, yc_neigh = %lf, yc_ele = %lf, dist = %lf \n", __func__ ,
              // pele->id[ABS_MSH],pneigh->id[ABS_MSH],TTC_param_card(icard),xc_neigh, xc_ele, yc_neigh, yc_ele, dist); // NG check

              if (pspecies->pset->aquitard == AQUITARD_ON_TTC) { // Check if the aquitard is activated
                // This case works only for the case where all layers have aquitard cells
                // It does not work for the variable distribution of the aquitard cells.
                // The explicit implementation of the aquitard should be added for variable aquitard
                if (icard == TOP_TTC && pneigh->paquitard_ttc != NULL) { // Check if the TOP and BOTTOM faces have aquitard

                  if (pneigh->paquitard_ttc != NULL && pneigh->paquitard_ttc->param[THICKNESS_AQUITARD] != 0.) {
                    // Check if the aquitard exists and has thickness (meaning that a physical aquifer is assigned to the face)
                    // We check if the aquitard has thickness other than 0 because we want to ake aquitard into account when there is one
                    L2 = pneigh->paquitard_ttc->param[THICKNESS_AQUITARD] / 2;
                  } else {
                    L2 = pneigh->phydro->Thick;
                  }
                  // LP_warning(flog,"%s delta_L is below 1m thick face %s AQUITARD ON thickness: %lf",__func__ ,TTC_param_card(icard), L2);
                } else if (icard == BOTTOM_TTC) {
                  if (pele->paquitard_ttc != NULL && pele->paquitard_ttc->param[THICKNESS_AQUITARD] != 0.) {
                    // Check if the aquitard exists and has thickness (meaning that a physical aquifer is assigned to the face)
                    // We check if the aquitard has thickness other than 0 because we want to ake aquitard into account when there is one
                    L2 = pele->paquitard_ttc->param[THICKNESS_AQUITARD] / 2;
                  } else {
                    L2 = pneigh->phydro->Thick;
                  }
                  // LP_warning(flog,"%s delta_L is below 1m thick face %s AQUITARD ON thickness: %lf ",__func__ ,TTC_param_card(icard), L2);
                }
              }
              dist = fabs(L1 / 2 + L2 / 2);
            } else {
              L1 = pele->pdescr->l_side;
              L2 = pneigh->pdescr->l_side;
              dist = fabs(L1 + L2) / 2.; // sqrt(pow(xc_neigh-xc_ele,2)+pow(yc_neigh-yc_ele,2));
            }
            plink->p_neigh_ttc[elettc]->delta_L[icard][k] = dist; // fabs(L1+L2)/2.; //;dist
            // plink->pneigh_ttc->pdelta_L[id].delta_L[icard][k]=dist;
            // LP_printf(flog,"Distance from %d to %d in dir %s neigh %d = %f\n",pele->id[ABS_MSH],pneigh->id[ABS_MSH], name_card,k,plink->p_neigh_ttc[elettc]->delta_L[icard][k]); // NG check

            if (plink->p_neigh_ttc[elettc]->delta_L[icard][k] < 0.1) {
              LP_error(flog, "CaWaQS%4.2f, File %s in %s line %d : Delta_L is 0 in ele %d face %s subface %d value: %lf.\n", NVERSION_CAW, __FILE__, __func__, __LINE__, elettc, TTC_param_card(icard), k, plink->p_neigh_ttc[elettc]->delta_L[icard][k]);
            }

            k++;
            neigh = neigh->next;
          }

          // if (pele->loc == BOT_MSH) { // Here set the bottom thickness of the layer until the center of the cell
          // L1 = pele->phydro->Thick; // TODO Here the thicknesses of the aquifer and aquitard layers should come DK AR 7 12 2021
          // L2 = pneigh->phydro->Thick;
          // plink->p_neigh_ttc[elettc]->delta_L[BOTTOM_TTC][k]=fabs(L1+L2)/2.;
          // }
          free(name_card);
        }
      }
      elettc++;
    }
  }
}

/**
 * \brief Function to fill the id of neighboring cells of each face of an aquifer mesh element
 * \return -
 */
void CAW_fill_neighbor_one_species_aq(s_cmsh *pcmsh, s_link_ttc *plink, FILE *fpout) {

  int nlayer, nele, i, j, l, idir, itype, id, idneigh, icard, idneigh2, nb_sub;
  int elettc = 0;
  s_ele_msh *pele;
  s_layer_msh *play;
  s_id_io *pneigh;

  nlayer = pcmsh->pcount->nlayer;

  for (i = 0; i < nlayer; i++) {
    play = pcmsh->p_layer[i];
    nele = play->nele;
    // LP_printf(fpout,"Valeur de vois_ttc en %s pour player %d et nele %d, current layer: %d \n",__func__ ,nlayer, nele, i );

    for (j = 0; j < nele; j++) {
      pele = play->p_ele[j];
      id = MSH_get_ele_id(pele, ABS_MSH);
      idneigh = NONE_TTC;

      for (idir = 0; idir < ND_MSH; idir++) {
        for (itype = 0; itype < NFACE_MSH; itype++) {

          icard = CAW_TTC_card_aq(idir, itype);
          pneigh = pele->p_neigh[idir][itype];
          pneigh = IO_browse_id(pneigh, BEGINNING_TS);
          l = 0;
          // LP_printf(fpout,"Valeur de icard = %d, ivois est %d \n");

          // Default initialization  --> NG fix : 19/06/2023. Useless. Already done in CAW_init_neighbor_one_species().
          /* plink->p_neigh_ttc[elettc]->ivois[icard][0] = NONE_TTC;
           plink->p_neigh_ttc[elettc]->ivois[icard][1] = NONE_TTC;
           plink->p_neigh_ttc[elettc]->ivois[icard][2] = NONE_TTC;
           plink->p_neigh_ttc[elettc]->ivois[icard][3] = NONE_TTC; */

          plink->p_neigh_ttc[elettc]->ncell_layer = play->nele;

          while (pneigh != NULL) {
            // LP_printf(fpout,"ID_ABS %d %d %d\n",id,pneigh->id_lay,pneigh->id);
            idneigh = MSH_get_ele_id(pcmsh->p_layer[pneigh->id_lay]->p_ele[pneigh->id], ABS_MSH);
            // LP_printf(fpout,"ele %d Valeur de icard = %s neigh = %d\n", id, TTC_param_card(icard),idneigh);

            plink->p_neigh_ttc[elettc]->ivois[icard][l] = idneigh;

            plink->pspecies->p_boundary_ttc[elettc]->icl[icard][l] = NO_BOUND_TTC; // NG fix : 19/06/2023 : Turning subface back to active status.
            plink->pspecies->p_boundary_ttc[elettc]->valcl[icard][l] = 0.;

            l++;
            pneigh = pneigh->next;
          }
        }
      }
      plink->p_neigh_ttc[elettc]->nvois = pele->nvois_ttc;
      elettc++;
    }
  }
}

/**
 * \brief Fills the number of sub-faces of each face of the aquifer mesh elements
 * \return -
 */
void CAW_fill_nsub_one_species_aq(s_cmsh *pcmsh, s_link_ttc *plink, FILE *fpout) {
  int nlayer, nele, icard, id, i, j, idir, itype;
  s_ele_msh *pele;
  s_id_io *neigh;
  s_ele_msh *pneigh;
  s_layer_msh *play;
  int nsub, elettc = 0;

  nlayer = pcmsh->pcount->nlayer;

  for (i = 0; i < nlayer; i++) {
    play = pcmsh->p_layer[i];
    nele = play->nele;
    for (j = 0; j < nele; j++) {
      pele = play->p_ele[j];
      id = MSH_get_ele_id(pele, ABS_MSH);

      for (idir = 0; idir < ND_MSH; idir++) {

        /* WARNING WARNING WARNING -----------------------------------------------------------------------
         In the Z direction, the pface[idir][itype]->nsubface does not calculate the nested cells in Z direction.
         Hence the approach in this function is to check if at a given direction are there any sub-faces &
         Dereference to 4 if the face is at Z direction and nsubface if the direction is in horizontal
         No sub-face if p_subface is NULL
         ----------------------------------------------------------------------------------------------- */

        for (itype = 0; itype < NFACE_MSH; itype++) {
          nsub = 0;
          icard = CAW_TTC_card_aq(idir, itype);
          neigh = pele->p_neigh[idir][itype];
          neigh = IO_browse_id(neigh, BEGINNING_TS);

          while (neigh != NULL) {
            pneigh = MSH_get_ele(pcmsh, neigh->id_lay, neigh->id, fpout);
            nsub++;
            neigh = neigh->next;
          }

          if (nsub == 1 || nsub == 0) {
            plink->nsub[elettc][icard] = 0;
          } else if (nsub == 2) {
            plink->nsub[elettc][icard] = 2;
          } else if (nsub > 2) {
            plink->nsub[elettc][icard] = 4;
          }

          // LP_printf(fpout,"nsubface element id_ABS %d, idir = %d, itype = %d  and icard %d dir %s nsub: %d  \n",
          // elettc, idir, itype, icard, TTC_param_card(icard), plink->nsub[elettc][icard]);

          // LP_printf(fpout,"nsubface element id_ABS %d, idir = %d, itype = %d  and icard %d dir %s nsub: %d \n",id, idir, itype, icard, TTC_param_card(icard), nsub);
        }
      }
      elettc++;
    }
  }
}

/**
 * \brief Fetching the current aquifer transport values from libmesh/libaq to libttc
 * \return -
 */
void CAW_fill_var_one_species_aq(s_cmsh *pcmsh, s_species_ttc *pspecies, FILE *flog) {

  int i, j, nlayer, nele, id, eletot = 0;
  s_ele_msh *pele;
  s_layer_msh *play;

  nlayer = pcmsh->pcount->nlayer;

  for (i = 0; i < nlayer; i++) {
    play = pcmsh->p_layer[i];
    nele = play->nele;
    for (j = 0; j < nele; j++) {
      pele = play->p_ele[j];
      id = MSH_get_ele_id(pele, ABS_MSH);
      pspecies->pval_ttc->val[eletot] = play->p_ele[j]->phydro->transp_var[pspecies->id];
      // LP_printf(flog,"%s Ele ABS id %d layer: %d, cell %d Var_init value in pspecies : %f ,Var_init value in phydro %f\n",__func__, id,i,j, pspecies->pval_ttc->val[j],play->p_ele[j]->phydro->transp_var[pspecies->id]);
      eletot++;
    }
  }
}

/**
 * \brief Fill the initial value to the pspecies structure for the transport problem
 * \return -
 */
void CAW_build_var_one_species_aq(s_cmsh *pcmsh, s_species_ttc *pspecies, FILE *flog) {
  int i, j, nlayer, nele;
  int elettc = 0;
  s_ele_msh *pele;
  s_layer_msh *play;

  nlayer = pcmsh->pcount->nlayer;

  for (i = 0; i < nlayer; i++) {
    play = pcmsh->p_layer[i];
    nele = play->nele;
    for (j = 0; j < nele; j++) {
      pele = play->p_ele[j];

      if (play->p_ele[j]->pval_ttc != NULL) {
        pspecies->pval_ttc->val[elettc] = play->p_ele[j]->pval_ttc->val[0];
        play->p_ele[j]->phydro->transp_var[pspecies->id] = play->p_ele[j]->pval_ttc->val[0];
      }
      // LP_printf(flog,"TTC-link ABS %d (MSH ABS id %d) Var_init value : %f, inside phydro: %lf\n",elettc,pele->id[ABS_MSH],pspecies->pval_ttc->val[elettc],play->p_ele[j]->phydro->transp_var[pspecies->id]); // NG check
      elettc++;
    }
  }
}

/**
 * \brief Pass the system parameters to the mesh structure to be used for the transport equation (DISPERSIVITY, POROSITY).
 *        Applied to a single element.
 * \return -
 */
void CAW_init_TTC_ele_NPARAM_SYST(s_ele_msh *pele, int npa, double val, FILE *flog) {

  pele->pparam_ttc->param_syst[npa] = val;
  // LP_printf(flog,"Param %s read for ABS ele %d : Value = %f\n",TTC_param_syst(npa),pele->id[ABS_MSH],pele->pparam_ttc->param_syst[npa]); // DK check
}

/**
 * \brief Pass the initial condition value inside the p_ele of the mesh structure to be used for the transport problem
 * \return -
 */
void CAW_init_TTC_ele_INIT(s_layer_msh *player, int id, double val, FILE *flog) {

  player->p_ele[id]->pval_ttc->val[0] = val;
  player->p_ele[id]->pval_ttc->val_old[0] = val;
  //	LP_printf(flog,"Var value at init for ele %d = %f\n",id,player->p_ele[id]->pval_ttc->val[0]); // DK check
}

/**
 * \brief Pass the thermal parameters to the pparam_ttc structure inside mesh.
 * \return -
 */
void CAW_init_TTC_ele_NPARAM_THERM(s_ele_msh *pele, int nenv, int npa, int id, double val, FILE *flog) {
  pele->pparam_ttc->param_thermic[nenv][npa] = val;
  // LP_printf(flog,"Param : %s id_ele %i value = %f \n",TTC_thermic_param(npa),id,pele->pparam_base_ttc->pthermic[nenv]->param[npa][0]); // DK check
}

/**
 * \brief Fills number of neighbors for each aquifer cell for transport resolution.
 * Initializes TTC type and value of boundary.
 * \return -
 */
void CAW_init_TTC_ele_nbound(s_cmsh *pcmsh, FILE *flog) {

  int j, i, idir, itype, nele;
  s_ele_msh *pele;
  s_layer_msh *player;
  s_id_io *pneigh;

  for (i = 0; i < pcmsh->pcount->nlayer; i++) {
    player = pcmsh->p_layer[i];
    nele = player->nele;
    for (j = 0; j < nele; j++) {
      pele = player->p_ele[j];
      for (idir = 0; idir < ND_MSH; idir++) {
        for (itype = 0; itype < NFACE_MSH; itype++) {

          pneigh = pele->p_neigh[idir][itype];
          pneigh = IO_browse_id(pneigh, BEGINNING_TS);

          /* NG 21/06/2023 : I don't understand :
          1) They are as many init at the element face as neighbors elements (?!)
          2) I don't really get the point of having 'val_bc_ttc' and 'icl_bc_ttc' since
           'face->pboundary_ttc->pft' and 'face->pboundary_ttc->type' can be browsed to fetch infos ? */

          while (pneigh != NULL) {
            pele->nvois_ttc++;
            pele->face[idir][itype]->val_bc_ttc = 0.;
            // pele->face[idir][itype]->icl_bc_ttc = NO_BOUND_TTC;
            pele->face[idir][itype]->icl_bc_ttc = NEU_FACE_TTC; // Changing to NEU_FACE_TTC by default to be consistent with CAW_init_TTC_face_bound().
            pneigh = pneigh->next;
          }
        }
      }
      // LP_printf(flog, "In %s : Neighbor number for INT ele id (%d,%d) = %d\n", __func__, pele->id[INTERN_MSH], pele->player->id - 1, pele->nvois_ttc); // NG to debug
    }
  }
}

/**
 * \brief
 * \return
 */
int CAW_init_TTC_calc_nbc(s_cmsh *pcmsh, FILE *flog) {

  int nbc, nlayer, nele, i, j, idir, itype;
  s_ele_msh *pele;
  s_layer_msh *play;
  s_id_io *pneigh;

  nbc = 0;
  nlayer = pcmsh->pcount->nlayer;

  for (i = 0; i < nlayer; i++) {
    play = pcmsh->p_layer[i];
    nele = play->nele;
    for (j = 0; j < nele; j++) {
      pele = play->p_ele[j];

      for (idir = 0; idir < ND_MSH; idir++) {
        for (itype = 0; itype < NFACE_MSH; itype++) {
          pneigh = pele->p_neigh[idir][itype];
          pneigh = IO_browse_id(pneigh, BEGINNING_TS);
          if (pneigh == NULL) {
            nbc++;
            pele->nbound_ttc++;
          }
        }
      }
      // LP_printf(flog,"In %s : Boundaries number for ABS ele %d = %d nbc %d\n",__func__,pele->id[ABS_MSH],pele->nbound_ttc,nbc); // DK check
    }
  }
  return nbc;
}

/**
 * \brief Initializes all elements of an AQ system with BC type and BC value
 * \return -
 */
void CAW_init_TTC_face_bound(s_cmsh *pcmsh, FILE *flog) {

  int i, j, nsub;
  s_layer_msh *player;
  s_ele_msh *pele;
  s_face_msh *pface;

  for (i = 0; i < pcmsh->pcount->nlayer; i++) {
    player = pcmsh->p_layer[i];
    for (j = 0; j < player->nele; j++) {
      pele = player->p_ele[j];

      for (int idir = 0; idir < ND_MSH; idir++) {
        for (int itype = 0; itype < NFACE_MSH; itype++) {

          pface = pele->face[idir][itype];
          nsub = pface->nsubface;

          if (pface->p_subface != NULL) {
            for (int isub = 0; isub < nsub; isub++) {
              // pface->p_subface[isub]->pboundary_ttc = TTC_create_bboundary(NO_BOUND_TTC);
              pface->p_subface[isub]->pboundary_ttc = TTC_create_bboundary(NEU_FACE_TTC); // NG : 19/06/2023. Changing to CST null NEUMANN conditions at init.
              pface->p_subface[isub]->pboundary_ttc->pft = TS_create_function(0, 0);
              pface->p_subface[isub]->pboundary_ttc->freq = CONSTANT_TTC;
            }
          } else {
            // pface->pboundary_ttc = TTC_create_bboundary(NO_BOUND_TTC);
            pface->pboundary_ttc = TTC_create_bboundary(NEU_FACE_TTC);
            pface->pboundary_ttc->pft = TS_create_function(0, 0);
            pface->pboundary_ttc->freq = CONSTANT_TTC;
          }
        }
      }
    }
  }
}

/**
 * \brief
 * \return -
 */
void CAW_init_TTC_face_bound_transient(s_layer_msh *player, int icard, int ikdofb_t, int idx, int time, double timeval, double val) {
  int nele;
  int i, j, idir, itype, idneigh, id;

  s_ele_msh *pele;
  pele = player->p_ele[idx];
  id = MSH_get_ele_id(pele, ABS_MSH);

  itype = CAW_TTC_type_by_card_aq(icard);
  idir = CAW_TTC_dir_by_card_aq(icard);

  // Create function for each face of each ele to call

  pele->face[idir][itype]->val_bc_t_ttc[time] = val; // Create a structre of a table in libmesh
  pele->face[idir][itype]->tval[time] = timeval;     // Create a structre of a table in libmesh
  pele->face[idir][itype]->icl_bc_ttc = ikdofb_t;
  //    printf("\t icl : %i %s %i \n",idx,TTC_param_card(CAW_TTC_card_aq(idir,itype)),pele->face[idir][itype]->icl_bc_ttc);
  //    printf("Func:TTC_init_face_bound_transient, ID: %d, Direction = %s, kind of BC: %s Value: %lf, time is %e\n",id,TTC_param_card(icard), TTC_name_bound(ikdofb_t), pele->face[idir][itype]->val_bc_t_ttc[time], pele->face[idir][itype]->tval[time]);
}

/**
 * \brief
 * \return -
 *
 * NG : 19/06/2023 : This DK function assume the length of the TS is equal to NSTEP !!!
 *
 */
void CAW_build_TTC_face_bound_transient(s_cmsh *pcmsh, s_link_ttc *plink, s_species_ttc *pspecies, int n_time, FILE *flog) {
  int isub, nlayer, nele, id, i, j, icard, idir, nsub, itype, ibound;
  int eletot = 0;
  double vbound;
  s_ele_msh *pele;
  s_layer_msh *play;
  s_ft *pft_ts;

  LP_printf(flog, "Entering %s, total tstep is %d\n", __func__, n_time);

  nlayer = pcmsh->pcount->nlayer;

  for (i = 0; i < nlayer; i++) {
    play = pcmsh->p_layer[i];
    nele = play->nele;
    for (j = 0; j < nele; j++) {
      pele = play->p_ele[j];
      id = MSH_get_ele_id(pele, ABS_MSH);
      pspecies->p_boundary_ttc[eletot]->nbound = pele->nbound_ttc;
      // LP_printf(flog,"Number of faces with BC for ABS ele %d = %d\n",id,plink->pspecies->p_boundary_ttc[j]->nbound);

      for (idir = 0; idir < ND_MSH; idir++) {
        for (itype = 0; itype < NFACE_MSH; itype++) {
          icard = CAW_TTC_card_aq(idir, itype);
          nsub = plink->nsub[eletot][icard];
          LP_printf(flog, "id %d Boundary type direction idir= %d, itype= %d, icard = %s , nsub : %d, tstep: %d \n", id, idir, itype, TTC_param_card(icard), nsub, n_time); // DK check

          // TODO DK TO be changed to accommodate the tstep inside the p_boundary_ttc structure

          if (pele->face[idir][itype]->p_subface != NULL) // If there are subfaces, then pass values from the subfaces to the ttc structures
          {
            for (isub = 0; isub < nsub; isub++) {
              // LP_printf(flog,"Before entering to the loop in lmesh, Number of faces with BC for ABS ele %d , j is %d = %e\n",id,j,play->p_ele[j]->face[idir][itype]->p_subface[isub]->val_bc_t_ttc[0]); // DK check
              pft_ts = pele->face[idir][itype]->p_subface[isub]->pboundary_ttc->pft;

              pspecies->p_boundary_ttc[eletot]->icl[icard][isub] = pele->face[idir][itype]->p_subface[isub]->pboundary_ttc->type;
              pspecies->p_boundary_ttc[eletot]->valcl[icard][isub] = pft_ts->ft;

              // Shortcuts
              ibound = pspecies->p_boundary_ttc[eletot]->icl[icard][isub];
              vbound = pspecies->p_boundary_ttc[eletot]->valcl[icard][isub];
              LP_printf(flog, "Boundary type for ABS ele %d icard %s = %s Value %f\n", id, TTC_param_card(icard), TTC_name_bound(ibound), vbound);

              if (pele->face[idir][itype]->p_subface[isub]->pboundary_ttc->freq == VARIABLE_TTC) {
                pspecies->p_boundary_ttc[eletot]->freq = VARIABLE_TTC;
                if (pft_ts->next != NULL) {
                  pft_ts = pft_ts->next;
                } else if (pft_ts->next != NULL) {
                  LP_error(flog, "In func : %s  The number of BC is lower than tstep.", __func__); // NG : 05/04/2023 : Why ft should be equal to tstep in length ??!
                }
              }
            }
          } else {
            // LP_printf(flog,"No sub card for this ele: %d \n",id);

            if (pele->face[idir][itype]->pboundary_ttc != NULL) {
              isub = 0;
              pft_ts = pele->face[idir][itype]->pboundary_ttc->pft;

              // LP_printf(flog,"Before entering to the loop in lmesh, Number of faces with BC for ABS ele %d, j is %d = %e\n",id,j,play->p_ele[j]->face[idir][itype]->val_bc_t_ttc[tstep]); // DK check
              pspecies->p_boundary_ttc[eletot]->icl[icard][isub] = pele->face[idir][itype]->pboundary_ttc->type;
              pspecies->p_boundary_ttc[eletot]->valcl[icard][isub] = pft_ts->ft;
              ibound = pspecies->p_boundary_ttc[eletot]->icl[icard][isub]; // Shortcuts
              vbound = pspecies->p_boundary_ttc[eletot]->valcl[icard][isub];
              LP_printf(flog, "Boundary type for ABS ele %d, icard %s = %s Value %f\n", id, TTC_param_card(icard), TTC_name_bound(ibound), vbound);

              if (pele->face[idir][itype]->pboundary_ttc->freq == VARIABLE_TTC) {
                pspecies->p_boundary_ttc[eletot]->freq = VARIABLE_TTC;
                if (pft_ts->next != NULL) {
                  pft_ts = pft_ts->next;
                } else if (pft_ts->next != NULL) {
                  LP_error(flog, "In func : %s  The number of BC is lower than tstep.", __func__);
                }
              }
            }
          }
        }
        // LP_printf(flog, "Boundary type for ABS ele %d icard %s ,BCtype = %s, BCvalue= %e \n", id, TTC_param_card(icard), TTC_name_bound(ibound), vbound); // DK check
      }
      eletot++;
    }
  }
}

/**
 * \brief Function calculating the total number of neighboring cells for all cells of the aquifer mesh.
 * \return nvois_ele
 */
int CAW_calc_TTC_nvois_ele_aq(s_cmsh *pcmsh, FILE *flog) {

  int nvois_ele, nlayer, nele, i, j, idir, itype;
  s_ele_msh *pele;
  s_layer_msh *play;
  s_id_io *pneigh;

  nvois_ele = 0;
  nlayer = pcmsh->pcount->nlayer;

  for (i = 0; i < nlayer; i++) {
    play = pcmsh->p_layer[i];
    nele = play->nele;

    for (j = 0; j < nele; j++) {
      pele = play->p_ele[j];

      for (idir = 0; idir < ND_MSH; idir++) {
        for (itype = 0; itype < NFACE_MSH; itype++) {
          pneigh = pele->p_neigh[idir][itype];
          pneigh = IO_browse_id(pneigh, BEGINNING_TS);

          while (pneigh != NULL) {
            nvois_ele++;
            // LP_printf(flog,"id_layer %d id_ele %d nvois_ele %d\n",play->id,pele->id[ABS_MSH],nvois_ele);  // NG check
            pneigh = pneigh->next;
          }
        }
      }
    }
  }
  return nvois_ele;
}

/**
 * \brief Returns the icard (the direction N,S,E,W) variable of each element depending on the direction of the faces of each element
 * \return icard
 */
int CAW_TTC_card_aq(int idir, int itype) {

  int icard;

  if ((idir == X_MSH) && (itype == ONE)) {
    icard = WEST_TTC;
  } else if ((idir == X_MSH) && (itype == TWO)) {
    icard = EAST_TTC;
  } else if ((idir == Y_MSH) && (itype == ONE)) {
    icard = SOUTH_TTC;
  } else if ((idir == Y_MSH) && (itype == TWO)) {
    icard = NORTH_TTC;
  } else if ((idir == Z_MSH) && (itype == ONE)) {
    icard = BOTTOM_TTC; // addition of the Z directions DK AR 4 12 2021
  } else if ((idir == Z_MSH) && (itype == TWO))
    icard = TOP_TTC; // addition of the Z directions

  return icard;
}

/**
 * \brief Returns the dir (axis X or Y) variable of each element depending on the direction of the faces of each element
 * \return idir
 */
int CAW_TTC_dir_by_card_aq(int icard) {
  int idir;

  switch (icard) {
  case WEST_TTC:
    idir = X_MSH;
    break;

  case EAST_TTC:
    idir = X_MSH;
    break;

  case SOUTH_TTC:
    idir = Y_MSH;
    break;

  case NORTH_TTC:
    idir = Y_MSH;
    break;

  case BOTTOM_TTC:
    idir = Z_MSH;
    break;

  case TOP_TTC:
    idir = Z_MSH;
    break;
  }
  return idir;
}

/**
 * \brief Returns the type variable of each element depending on the direction of the faces of each element
 * \return itype
 */
int CAW_TTC_type_by_card_aq(int icard) {
  int itype;

  switch (icard) {
  case WEST_TTC:
    itype = ONE;
    break;

  case EAST_TTC:
    itype = TWO;
    break;

  case SOUTH_TTC:
    itype = ONE;
    break;

  case NORTH_TTC:
    itype = TWO;
    break;

  case BOTTOM_TTC:
    itype = ONE;
    break;

  case TOP_TTC:
    itype = TWO;
    break;
  }
  return itype;
}

/**
 * \brief Pass aquitard parameter values inside the p_ele of the mesh structure to be used for the transport problem
 * \return -
 */
void CAW_init_TTC_aquitard(s_ele_msh *pele, int id, int nlayer, int npa, double val, FILE *flog) {

  pele->paquitard_ttc->active = 1.; // Activate the aquitard cell
  pele->paquitard_ttc->param[npa] = val;
  pele->paquitard_ttc->id = id;                                   // add the cell id information to the aquitard
  pele->paquitard_ttc->id_intern = MSH_get_ele_id(pele, GIS_MSH); // add the cell id information to the aquitard
  pele->paquitard_ttc->nlayer = nlayer;                           // add the layer information to the aquitard
  // LP_printf(flog,"Aquitard parameter in pcmsh for ele %d, parameter value: %f, \n",id,pele->paquitard_ttc->param[npa]); // DK check
}

/**
 * \brief
 * \return -
 */
void CAW_build_TTC_param_aquitard_aq(s_cmsh *pcmsh, s_species_ttc *pspecies, int npa, FILE *flog) {

  int i, j, nlayer, nele, id;
  int elettc = 0;
  s_ele_msh *pele;
  s_layer_msh *play;
  // char *ttc_aquitard_param = NULL;

  nlayer = pcmsh->pcount->nlayer;

  for (i = 0; i < nlayer; i++) {
    play = pcmsh->p_layer[i];
    nele = play->nele;

    for (j = 0; j < nele; j++) {
      pele = play->p_ele[j];
      id = MSH_get_ele_id(pele, ABS_MSH);
      //			plink->pbase_ttc->param_syst[npa][j]=pele->pparam_base_ttc->param_syst[npa][0];
      if (pele->paquitard_ttc != NULL && pele->paquitard_ttc->active == 1) {
        pspecies->p_aquitard_ttc[elettc]->param[npa] = pele->paquitard_ttc->param[npa];
        pspecies->p_aquitard_ttc[elettc]->id = pele->paquitard_ttc->id;
        pspecies->p_aquitard_ttc[elettc]->id_intern = pele->paquitard_ttc->id_intern;
        pspecies->p_aquitard_ttc[elettc]->nlayer = pele->paquitard_ttc->nlayer;
        // LP_printf(flog,"build aquitard %s ABS ele id %d, value: %f \n",TTC_param_aquitard(ttc_aquitard_param,npa),id, pspecies->p_aquitard_ttc[elettc]->param[npa]);
        // free(ttc_aquitard_param);
        elettc++;
      }
    }
  }
}

/**
 * \brief
 * \return
 */
void CAW_build_TTC_var_aquitard_aq(s_cmsh *pcmsh, s_species_ttc *pspecies, FILE *flog) {
  int i, j, nlayer, nele, id;
  int elettc = 0;
  s_ele_msh *pele;
  s_ele_msh *pele_neigh;
  s_layer_msh *play;
  s_id_io *pneigh;
  int id_neigh;

  nlayer = pcmsh->pcount->nlayer;
  for (i = 0; i < nlayer; i++) {
    play = pcmsh->p_layer[i];
    nele = play->nele;

    for (j = 0; j < nele; j++) {
      pele = play->p_ele[j];
      pneigh = pele->p_neigh[Z_MSH][ONE_MSH]; // The neighbour below
      pneigh = IO_browse_id(pneigh, BEGINNING_TS);

      if (pneigh != NULL) {

        id = MSH_get_ele_id(pele, ABS_MSH);
        id_neigh = MSH_get_ele_id(pcmsh->p_layer[pneigh->id_lay]->p_ele[pneigh->id], ABS_MSH);
        pele_neigh = MSH_get_ele_from_abs(pcmsh, id_neigh, flog);

        // plink->pbase_ttc->param_syst[npa][j]=pele->pparam_base_ttc->param_syst[npa][0];
        if (pele->paquitard_ttc != NULL && pele->paquitard_ttc->active == 1) {

          //                double lambda_neigh = pneigh->phydro->pparam_ttc->param_thermic[SOLID_TTC][LAMBDA_TTC];
          pspecies->p_aquitard_ttc[elettc]->lambda[VAR_ABOVE_TTC] = pele->pparam_ttc->param_thermic[SOLID_TTC][LAMBDA_TTC];
          pspecies->p_aquitard_ttc[elettc]->lambda[VAR_AQUITARD_TTC] = pele->paquitard_ttc->param[LAMBDA_AQ_TTC];
          pspecies->p_aquitard_ttc[elettc]->lambda[VAR_BELOW_TTC] = pele_neigh->pparam_ttc->param_thermic[SOLID_TTC][LAMBDA_TTC];

          pspecies->p_aquitard_ttc[elettc]->id_ttc_above = pele->id[ABS_MSH] - 1;
          pspecies->p_aquitard_ttc[elettc]->id_ttc_below = pele_neigh->id[ABS_MSH] - 1;
          /* abs_id-1 because ABS_MSH starts from 1 but libttc index starts from 0 */
          //                    LP_printf(flog, "Inside %s, id is %d, ele_ttc_above: %d ele_ttc_below: %d \n", __func__, elettc, // DK Debug
          //                              pspecies->p_aquitard_ttc[elettc]->id_ttc_above,
          //                              pspecies->p_aquitard_ttc[elettc]->id_ttc_below);

          /*		if( pele_neigh->paquitard_ttc != NULL   ) {
                                      pspecies->p_aquitard_ttc[elettc]->lambda[VAR_AQUITARD_ABOVE_TTC]=pele_neigh->paquitard_ttc->param[LAMBDA_AQ_TTC];

                  } */

          pspecies->p_aquitard_ttc[elettc]->thick[VAR_ABOVE_TTC] = pele->phydro->Thick;
          pspecies->p_aquitard_ttc[elettc]->thick[VAR_AQUITARD_TTC] = pele->paquitard_ttc->param[THICKNESS_AQUITARD];
          pspecies->p_aquitard_ttc[elettc]->thick[VAR_BELOW_TTC] = pele_neigh->phydro->Thick;

          pspecies->p_aquitard_ttc[elettc]->temperature_old = pele->pval_ttc->val[0];

          //                            LP_printf(flog, "Inside %s, id is %d, Thicknes layer is %lf, id of cell below is %d, thicknes of that cell is %lf\n", __func__, id, pele->phydro->Thick, id_neigh, pele_neigh->phydro->Thick );

          /*            LP_printf(flog,"buildaquitard ABS ele id %d, lambda_ab: %f, lambda_aq: %f, lambda_bel: %f \n",id,
            pspecies->p_aquitard_ttc[elettc]->lambda[VAR_ABOVE_TTC],
            pspecies->p_aquitard_ttc[elettc]->lambda[VAR_AQUITARD_TTC],
            pspecies->p_aquitard_ttc[elettc]->lambda[VAR_BELOW_TTC]); // NG check
  LP_printf(flog,"buildaquitard ABS ele id %d, thick_ab: %f, thick_aq: %f, thick_bel: %f \n",id,
            pspecies->p_aquitard_ttc[elettc]->thick[VAR_ABOVE_TTC],
            pspecies->p_aquitard_ttc[elettc]->thick[VAR_AQUITARD_TTC],
            pspecies->p_aquitard_ttc[elettc]->thick[VAR_BELOW_TTC]); // NG check
            */
          elettc++;
        }
      }
    }
  }
}

/**
 * \brief
 * \return
 */
void CAW_build_TTC_temp_aquitard_aq(s_cmsh *pcmsh, s_species_ttc *pspecies, FILE *flog) {
  int i, j, nlayer, nele, id;
  int elettc = 0;
  s_ele_msh *pele;
  s_ele_msh *pele_neigh;
  s_layer_msh *play;
  s_id_io *pneigh;
  int id_neigh;

  nlayer = pcmsh->pcount->nlayer;
  for (i = 0; i < nlayer; i++) {
    play = pcmsh->p_layer[i];
    nele = play->nele;

    for (j = 0; j < nele; j++) {
      pele = play->p_ele[j];
      pneigh = pele->p_neigh[Z_MSH][ONE_MSH]; // The neighbour below
      pneigh = IO_browse_id(pneigh, BEGINNING_TS);

      if (pneigh != NULL) {

        id = MSH_get_ele_id(pele, ABS_MSH);

        id_neigh = MSH_get_ele_id(pcmsh->p_layer[pneigh->id_lay]->p_ele[pneigh->id], ABS_MSH);

        pele_neigh = MSH_get_ele_from_abs(pcmsh, id_neigh, flog);

        // LP_printf(flog, "Inside %s, id is %d, id of cell below is %d\n", __func__ , id, id_neigh);
        if (pele->paquitard_ttc != NULL && pele->paquitard_ttc->active == 1) {

          pspecies->p_aquitard_ttc[elettc]->temperature[VAR_ABOVE_TTC][0] = pele->phydro->transp_var[0];
          pspecies->p_aquitard_ttc[elettc]->temperature[VAR_AQUITARD_TTC][0] = 0;
          pspecies->p_aquitard_ttc[elettc]->temperature[VAR_BELOW_TTC][0] = pele_neigh->phydro->transp_var[0];

          pspecies->p_aquitard_ttc[elettc]->id_neigh = id_neigh; // add the id of the neighbouring cell required to fill mat5
          // LP_printf(flog,"buildaquitard ABS ele id %d, id_neigh: %d \n",id,id_neigh);

          /* LP_printf(flog,"buildaquitard ABS ele id %d, temperature_ab: %f, temperature_aq: %f, temperature_bel: %f \n",id,
                       pspecies->p_aquitard_ttc[elettc]->temperature[VAR_ABOVE_TTC][0],
                       pspecies->p_aquitard_ttc[elettc]->temperature[VAR_AQUITARD_TTC][0],
                       pspecies->p_aquitard_ttc[elettc]->temperature[VAR_BELOW_TTC][0]);
          */
          elettc++;
        }
      }
    }
  }
}

/**
 * \brief
 * \return
 */
void CAW_init_TTC_aquitard_cells(s_carac_ttc *pcarac, FILE *fpout) {

  int naquitard, nb_species;

  naquitard = pcarac->count[NAQUITARD_TTC];
  nb_species = pcarac->count[NSPECIES_TTC];

  for (int p = 0; p < nb_species; p++) {
    pcarac->p_species[p]->p_aquitard_ttc = TTC_init_aquitard(naquitard);
    LP_printf(fpout, "Initializing aquitard : %d\n", naquitard);
  }
}

/**
 * \brief Defines the boundaries of a list of elements
 * \return void
 */
void CAW_init_TTC_define_bound(s_cmsh *pcmsh, s_id_io *id_list, s_ft *values, int dir, int kindof, double ltot, double surf_riv, double ep_riv_bed, double TZ_riv_bed, FILE *fpout) {

  s_layer_msh *player;
  s_ele_msh *pele;
  int id, nlayer;
  s_id_io *pidtmp;
  s_count_ttc *pcount;
  s_boundary_ttc *pbound;
  pidtmp = id_list;
  pcount = pcmsh->pcount->pcount_ttc;
  pbound = TTC_define_bound_val(kindof, values, ltot, fpout);
  // LP_printf(fpout,"In CaWaQS%4.2f %s l.%d dimension of pft : %d,first value : %f\n",VERSION_AQ,__FILE__,__LINE__,TS_length_ts(pbound->pft),pbound->pft->ft);

  if (pidtmp != NULL) {
    AQ_define_elebound(pidtmp, pcount->elebound, pbound->type, fpout);

    while (pidtmp != NULL) {
      nlayer = pidtmp->id_lay;
      player = pcmsh->p_layer[nlayer];
      id = pidtmp->id;

      pele = player->p_ele[id];
      pele = CAW_init_TTC_boundaries(pele, dir, pbound, pcount->elebound, fpout);
      free(pbound);
      pidtmp = pidtmp->next;
    }
  } else {
    LP_warning(fpout, "In CaWaQS%4.2f %s in %s l%d : No cells declared for BC %s -> ignored\n", VERSION_AQ, __func__, __FILE__, __LINE__, AQ_name_BC(kindof));
  }
}

/**
 * \brief Defines the boundary condition
 * \return pele
 */
s_ele_msh *CAW_init_TTC_boundaries(s_ele_msh *pele, int icardinal, s_boundary_ttc *pbound, s_id_io **elebound, FILE *fpout) {

  s_face_msh *pface, *psubf;
  s_ele_msh *pneigh;
  s_id_io *id_list;
  int idir, itype, i, kindof, id;
  kindof = pbound->type;
  char *name_card = NULL;
  char *name_bound = NULL;
  int nsub;

  name_card = TTC_param_card(icardinal);
  name_bound = TTC_name_bound(kindof);

  if (icardinal != CODE_AQ) {
    idir = CAW_TTC_dir_by_card_aq(icardinal);
    itype = CAW_TTC_type_by_card_aq(icardinal);
    // LP_printf(fpout, "In func %s: at the face %s, boundary is %s , idir: %d, itype: %d, icardinal: %d \n", __func__, name_card, name_bound, idir, itype, icardinal);

    pface = pele->face[idir][itype];
    nsub = pele->face[idir][itype]->nsubface;

    if (nsub == 0) {
      pneigh = pface->p_ele[itype];
      pface->pboundary_ttc = TTC_create_boundary_ts(kindof);
      pface->pboundary_ttc->freq = pbound->freq;
      pface->pboundary_ttc->pft = pbound->pft;
    } else {
      for (int isub = 0; isub < nsub; isub++) {
        pneigh = pface->p_subface[isub]->p_ele[itype];
        pface->p_subface[isub]->pboundary_ttc = TTC_create_boundary_ts(kindof);
        pface->p_subface[isub]->pboundary_ttc->freq = pbound->freq;
        pface->p_subface[isub]->pboundary_ttc->pft = pbound->pft;
        // LP_printf(fpout, "Inside %s, adding boundary to subface %d ", MSH_get_ele_id(pele, ABS_MSH), isub);
      }
    }
  }
  free(name_card);
  free(name_bound);

  return pele;
}

/**
 * \brief Initialize source cells with time series attached in each element for each source
 * \return
 */
void CAW_init_TTC_source_cells(s_cmsh *pcmsh, s_id_io *id_list, s_ft *values, int kindof, double stot, FILE *fpout) {

  s_layer_msh *player;
  int nlayer, id, n = 0;
  s_ele_msh *pele;
  s_id_io *ptmp;
  s_ft *pft;
  s_count_ttc *pcount;
  s_source_ttc *psource;

  // LP_printf(fpout," stot: %e \n",stot);

  ptmp = id_list;
  pcount = pcmsh->pcount->pcount_ttc;

  psource = TTC_define_source_val(kindof, values, stot, fpout);

  // LP_printf(fpout,"Fonction %s : Assigning frequency %s.\n",__func__,TTC_name_freq(psource->freq));
  // LP_printf(Simul->poutputs,"dimension of pft : %d\n",TS_length_ts(psource->pft));

  if (ptmp != NULL) {
    nlayer = ptmp->id_lay;
    player = pcmsh->p_layer[nlayer];
    // LP_printf(Simul->poutputs,"here is fine\n"); // BL to debug
    AQ_define_elebound(ptmp, pcount->elesource, kindof, fpout); // uses libaq function to define the elebound for libttc
  } else {
    LP_error(fpout, "The list of id is NULL !!\n");
  }

  while (ptmp != NULL) // for each id element with source
  {
    id = ptmp->id;
    // LP_printf(fpout," id cell : %d layer : %d\n ",id,nlayer);
    pele = player->p_ele[id];

    if (pele->psource_ttc == NULL) {
      // LP_printf(Simul->poutputs,"psource is null, creating psource\n"); // BL to debug
      pele->psource_ttc = TTC_create_source(fpout);
    }

    // LP_printf(Simul->poutputs,"here is fine\n"); // BL to debug
    CAW_add_TTC_source_to_element(pele, kindof, psource, fpout);
    // LP_printf(fpout,"Fonction %s : Assigning frequency below CAW_add_TTC_source_to_element:  %s.\n",__func__,TTC_name_freq(psource->freq));
    ptmp = ptmp->next;
  }
}

/**
 * \brief Reallocates the tables of psource in the transport simulation.
 * \return void. Directly reallocates tables in ***psource and assign the newer pbound in psource for transport
 */
void CAW_add_TTC_source_to_element(s_ele_msh *pele, int kindof, s_source_ttc *psource, FILE *fpout) {

  int nb_source;

  pele->nsource_type_ttc[kindof]++; // Incrementation du nombre de source pour l'element et le kindof considere
  nb_source = pele->nsource_type_ttc[kindof];

  // pele->psource_ttc[kindof] = (s_source_ttc **)realloc(pele->psource_ttc,nb_source*sizeof(s_source_ttc *)); // This line gives error when it is used
  if (pele->psource_ttc[kindof] == NULL) {
    LP_error(fpout, "libttc%4.2f in %s of %s at line %d: Memory reallocation failure for psource pointer.\n", VERSION_TTC, __func__, __FILE__, __LINE__);
  }

  // LP_printf(fpout,"Fonction %s : Valeur de nsource %d Kindof = %s,in number: %d element %d.\n",__func__,nb_source,TTC_name_source(kindof),kindof,pele->id[ABS_MSH]);
  pele->psource_ttc[kindof][nb_source - 1] = psource;
  if (pele->psource_ttc[kindof][nb_source - 1] == NULL) {
    LP_error(fpout, "libttc%4.2f in %s of %s at line %d: Memory reallocation failure for psource pointer.\n", VERSION_TTC, __func__, __FILE__, __LINE__);
  }
  // LP_printf(fpout,"Fonction %s : Value of input geothermal in pele_psource is %e, time %e.\n",__func__,psource->pft->ft,psource->pft->t);
  // LP_printf(fpout,"Fonction %s : Value of input geothermal in pele_psource is %e, time %e.\n",__func__,pele->psource_ttc[kindof][nb_source-1]->pft->ft, pele->psource_ttc[kindof][nb_source-1]->pft->t);
  // LP_printf(fpout,"Fonction %s : Assigning frequency below CAW_add_TTC_source_to_element:  %s.\n",__func__,TTC_name_freq(psource->freq ));
}

/**
 * \brief print the elements which are sources_inputs
 * \return void.
 */
void CAW_print_TTC_elesource(s_cmsh *pcmsh, FILE *fpout) {

  int nb_ele, i, j, div;
  double Qtot, qtmp;
  s_id_io *ptmp;
  s_id_io **elesource;
  s_ele_msh *pele;
  s_ft *pft;
  int nb_sources; // nombre de sources de type kindof pour l'element

  elesource = pcmsh->pcount->pcount_ttc->elesource;

  for (i = 0; i < NB_SOURCE_TTC; i++) // Search the source input to browse the cells to extract the source values for that source type
  {
    LP_printf(fpout, "\nSource_input : %s., i = %d \n", TTC_name_source(i), i);
    nb_ele = 0;
    Qtot = 0;

    if (elesource[i] != NULL) {
      ptmp = IO_browse_id(elesource[i], BEGINNING_TS); // Find the list of elements where source values are stored

      while (ptmp != NULL) {
        nb_ele++;
        pele = MSH_get_ele(pcmsh, ptmp->id_lay, ptmp->id, fpout); // Get the element of a given layer and id from the elesource

        nb_sources = pele->nsource_type_ttc[i];                                                                                                                                                             // Number of sources stored in the element
        LP_printf(fpout, "Layer : %d cell : %d ABS_MSH : %d input type : %s. Total number of input sources : %d\n", ptmp->id_lay, ptmp->id, MSH_get_ele_id(pele, ABS_MSH), TTC_name_source(i), nb_sources); // NG : 04/03/2020

        for (j = 0; j < nb_sources; j++) // Loop over the total number of sources
        {
          LP_printf(fpout, "input %d, source %d \n", i, j);

          LP_printf(fpout, "Frequence of input %d , i = %d, j = %d\n", j, i, j);
          LP_printf(fpout, "Frequence of input %d : %s., i = %d, j = %d\n", j, TTC_name_freq(pele->psource_ttc[i][j]->freq), i, j);

          pft = TS_browse_ft(pele->psource_ttc[i][j]->pft, BEGINNING_TS);
          div = 0;
          qtmp = 0;
          while (pft != NULL) {
            div++;
            LP_printf(fpout, " value of source : %e at time %f\n", pft->ft, pft->t);
            // qtmp+=pft->ft*pele->pdescr->surf;
            pft = pft->next;
          }
          if (div > 0) {
            Qtot += qtmp / div;
          } else {
            LP_error(fpout, "No value stored if source %s at ele %d ABS_MSH %d\n", TTC_name_source(i), ptmp->id, MSH_get_ele_id(pele, ABS_MSH));
          }
        }

        ptmp = ptmp->next;
      }
    }
    LP_printf(fpout, "Number of aquifer cells submitted to %s : %d. Total discharge %f\n", TTC_name_source(i), nb_ele, Qtot);
  }
}

/**
 * \brief
 * \return
 */
void CAW_build_TTC_source_imposed(s_cmsh *pcmsh, s_species_ttc *pspecies, FILE *flog) {

  int nb_source, i, j, nlayer, nele, id;
  int elettc = 0;
  s_ele_msh *pele;
  s_layer_msh *play;

  nlayer = pcmsh->pcount->nlayer;
  for (i = 0; i < nlayer; i++) {
    play = pcmsh->p_layer[i];
    nele = play->nele;
    for (j = 0; j < nele; j++) {
      pele = play->p_ele[j];
      nb_source = pele->nsource_type_ttc[0];
      id = MSH_get_ele_id(pele, ABS_MSH);
      // plink->pbase_ttc->param_syst[npa][j]=pele->pparam_base_ttc->param_syst[npa][0];
      if (pele->psource_ttc != NULL) {
        pspecies->p_source_ttc[elettc]->icl[0][0] = GEOTHERMAL_TTC;
        pspecies->p_source_ttc[elettc]->valcl[0][0] = pele->psource_ttc[0][0]->pft->ft;
        // LP_printf(flog,"buildaquitardABS ele id %d, value: %f \n",id, pspecies->p_source_ttc[elettc]->valcl[0]);
        elettc++;
      } else {
        pspecies->p_source_ttc[elettc]->valcl[0][0] = 0;
      }
    }
  }
}

/**
 * \brief Description please ???
 * \return
 */
void CAW_build_TTC_face_source_transient(s_cmsh *pcmsh, s_link_ttc *plink, s_species_ttc *pspecies, int n_time, FILE *flog) {
  int isub, nlayer, nele, id, i, j, icard, idir, nsub, itype, ibound;
  int eletot = 0;
  double vbound;
  s_ele_msh *pele;
  s_layer_msh *play;
  s_source_ttc *psource;

  nlayer = pcmsh->pcount->nlayer;

  for (i = 0; i < nlayer; i++) {
    play = pcmsh->p_layer[i];
    nele = play->nele;
    for (j = 0; j < nele; j++) {
      pele = play->p_ele[j];
      id = MSH_get_ele_id(pele, ABS_MSH);
      if (pele->loc == BOT_MSH) { // To add the geothermal source
        pspecies->p_boundary_ttc[eletot]->nbound = pele->nbound_ttc;
        // LP_printf(flog,"Number of faces with BC for ABS ele %d = %d\n",id,plink->pspecies->p_boundary_ttc[id]->nbound);

        idir = 2;
        itype = 0;

        icard = CAW_TTC_card_aq(idir, itype);
        nsub = plink->nsub[eletot][icard];
        // LP_printf(flog,"Boundary type direction idir= %d, itype= %d, icard = %d \n",idir, itype, icard); // DK check

        if (pele->face[idir][itype]->p_subface != NULL) // If there are subfaces, then pass values from the subfaces to the ttc structures
        {
          for (int isub = 0; isub < nsub; isub++) {
            // LP_printf(flog,"Before entering to the loop in lmesh, Number of faces with BC for ABS ele %d , j is %d = %e\n",id,j,play->p_ele[j]->face[idir][itype]->p_subface[isub]->val_bc_t_ttc[0]); // DK check

            pspecies->p_boundary_ttc[eletot]->icl[icard][isub] = NEU_FACE_TTC;
            pspecies->p_boundary_ttc[eletot]->valcl[icard][isub] = pele->psource_ttc[0][0]->pft->ft;
            ibound = pspecies->p_boundary_ttc[eletot]->icl[icard][isub]; // Shortcuts
            vbound = pspecies->p_boundary_ttc[eletot]->valcl[icard][isub];
            // LP_printf(flog,"Boundary type for ABS ele %d tstep: %d, icard %s = %s Value %f\n",id,tstep,TTC_param_card(icard),TTC_name_bound(ibound),vbound);
          }
        } else {
          // LP_printf(flog,"No sub card for this ele: %d \n",id);
          isub = 0;
          // LP_printf(flog,"Before entering to the loop in lmesh, Number of faces with BC for ABS ele %d, j is %d = %e\n",id,j,play->p_ele[j]->face[idir][itype]->val_bc_t_ttc[tstep]); // DK check
          pspecies->p_boundary_ttc[eletot]->icl[icard][isub] = NEU_FACE_TTC;
          pspecies->p_boundary_ttc[eletot]->valcl[icard][isub] = pele->psource_ttc[0][0]->pft->ft;
          ibound = pspecies->p_boundary_ttc[eletot]->icl[icard][isub]; // Shortcuts
          vbound = pspecies->p_boundary_ttc[eletot]->valcl[icard][isub];
          // LP_printf(flog,"Boundary type for ABS ele %d tstep: %d, icard %s = %s Value %f\n",id,tstep,TTC_param_card(icard),TTC_name_bound(ibound),vbound);
        }
        // LP_printf(flog,"Boundary type for ABS ele %d icard %d ,BCtype = %d, BCvalue= %e \n",id,icard, ibound, vbound); // DK check
      }
      eletot++;
    }
  }
}

/**
 * \brief
 * \return -
 */
void CAW_init_TTC_riverbed(s_face_hyd *pele, int id, int nlayer, int npa, double val, FILE *flog) {
  pele->hydro->param_riverbed[npa] = val;

  if (pele->id_gw == NULL) {
    LP_error(flog, "%s PELE FACE NULL  abs id %d \n", __func__, pele->id[ABS_HYD]);

  } /* else {
             LP_printf(flog, "%s: rivaq heat exchanges initialized for ABS_riv_ele:\t %d\t id_hyd %d\t Aquifer_layer: %d\t  aquifer_cell %d \n",
                         __func__, pele->id[ABS_HYD], pele->id[GIS_HYD], pele->id_gw->id_lay, pele->id_gw->id );
         }*/
  // DK Check
  //     LP_printf(flog,"riverbed parameter in pcmsh for ele %d, parameter value: %f, \n",id,pele->hydro->param_riverbed[npa]); // DK check
}

/**
 * \brief
 * \return
 */
void CAW_init_TTC_riverbed_cells(s_carac_ttc *pcarac, FILE *fpout) {

  int nriver, nb_species;

  nriver = pcarac->count[NRIVERBED_TTC];
  nb_species = pcarac->count[NSPECIES_TTC];

  for (int p = 0; p < nb_species; p++) {
    pcarac->p_species[p]->p_riverbed_ttc = TTC_init_riverbed(nriver);
  }
}

/**
 * \brief
 * \return -
 */
void CAW_build_TTC_param_riverbed_aq(s_chyd *pchyd, s_species_ttc *pspecies, int nriverbed, int npa, FILE *flog) {

  int j, nlayer, nele, id;
  s_reach_hyd *preach;
  s_face_hyd *pface;

  preach = pchyd->p_reach[0];
  pface = preach->p_faces[0];

  for (int k = 0; k < nriverbed; k++) {

    pspecies->p_riverbed_ttc[k]->param[LAMBDA_AQ_TTC] = pface->hydro->param_riverbed[LAMBDA_AQ_TTC];
    pspecies->p_riverbed_ttc[k]->param[THICKNESS_AQUITARD] = pface->hyporeic_sets[EP_HYD];

    // pspecies->p_riverbed_ttc[k]->id          = pface->id[ABS_HYD];
    // pspecies->p_riverbed_ttc[k]->id_neigh    = pface->id_gw->id;
    // pspecies->p_riverbed_ttc[k]->nlayer      = pface->id_gw->id_lay;

    // LP_printf(flog,"Inside %s Priverbed: %f, npa is %d, in river %s k is %d \n", __func__ , pspecies->p_riverbed_ttc[k]->param[npa], npa, pface->name, k);

    if (pface->next != NULL) {
      pface = pface->next;
      // LP_printf(Simul->poutputs,"Going to next face \n");
    } else {
      pface = HYD_rewind_faces(pface);
      // LP_printf(Simul->poutputs,"Going to first face, next face is NULL\n");
    }

    if (pface->fnode == -9999) {
      // LP_printf(Simul->poutputs,"Fnode is -9999, skipping to next face \n");
      if (pface->next != NULL) {
        pface = pface->next;
      }
    }
  }
  // LP_printf(flog,"buildaquitard %s ABS ele id %d, value: %f \n",TTC_param_aquitard(npa),id, pspecies->p_aquitard_ttc[elettc]->param[npa]);
}

/**
 * \brief Function to fill the underlying aquifer cells
 * \return -
 */
void CAW_build_TTC_neighbours_riverbed_aq(s_chyd *pchyd, s_species_ttc *pspecies, int nriverbed, int npa, FILE *flog) {

  int j, nlayer, nele, id;
  int i = 0;
  s_reach_hyd *preach;
  s_face_hyd *pface;

  preach = pchyd->p_reach[0];
  pface = preach->p_faces[0];

  for (int k = 0; k < nriverbed; k++) {

    if (pface->id_gw != NULL) {
      pspecies->p_riverbed_ttc[k]->id = pface->id[ABS_HYD];
      pspecies->p_riverbed_ttc[k]->id_neigh = pface->id_gw->id;
      pspecies->p_riverbed_ttc[k]->nlayer = pface->id_gw->id_lay;
      // LP_printf(Simul->poutputs,"Inside %s  river %s  k is %d id_neigh %d id_lay %d \n", __func__ ,pface->name, k, pface->id_gw->id, pface->id_gw->id_lay);
      i++;
    } else {
      // LP_printf(Simul->poutputs,"Inside %s  river %s  k is %d \n", __func__ , pface->name, k);
      k = k - 1;
      i++;
    }

    if (pface->next != NULL) {
      pface = pface->next;
      // LP_printf(Simul->poutputs,"Going to next face \n");
    } else {
      pface = HYD_rewind_faces(pface);
      // LP_printf(Simul->poutputs,"Going to first face, next face is NULL\n");
    }
    if (pface->fnode == -9999) {
      // LP_printf(Simul->poutputs,"Fnode is -9999, skipping to next face \n");
      if (pface->next != NULL) {
        pface = pface->next;
      }
    }
  }
  //    LP_printf(flog,"In build aquitard %s ABS ele id %d, value: %f \n",TTC_param_aquitard(npa),id, pspecies->p_aquitard_ttc[elettc]->param[npa]);
}

/**
 * \brief
 * \return
 */
void CAW_build_TTC_var_riverbed_aq(s_cmsh *pcmsh, s_chyd *pchyd, s_species_ttc *pspecies, int nriverbed, FILE *flog) {

  int i, j, nlayer, nele, id;
  s_ele_msh *pele_neigh;
  s_layer_msh *play;
  int id_neigh;
  s_face_hyd *pface;
  s_reach_hyd *preach;

  preach = pchyd->p_reach[0];
  pface = preach->p_faces[0];

  for (int k = 0; k < nriverbed; k++) {

    nlayer = pspecies->p_riverbed_ttc[k]->nlayer;
    id = pspecies->p_riverbed_ttc[k]->id_neigh;
    // LP_printf(Simul->poutputs,"Inside %s  priverbed> above,  nlayer %d id_neigh %d \n", __func__ , nlayer, id);

    play = pcmsh->p_layer[nlayer];
    pele_neigh = play->p_ele[id];

    pele_neigh->priverbed_ttc = new_aquitard_ttc(); // Initializes the riverbed structure for the aquifer cells under the river

    pele_neigh->priverbed_ttc->flux_adv[VAR_ABOVE_TTC] = 0;
    pele_neigh->priverbed_ttc->flux_cond[VAR_ABOVE_TTC] = 0;

    if (pele_neigh->priverbed_ttc == NULL) {
      LP_error(Simul->poutputs, "Inside %s  priverbed can't be initialized in  layer %d id_aquifer %d \n", __func__, nlayer, id);
    }

    pspecies->p_riverbed_ttc[k]->lambda[VAR_AQUITARD_TTC] = pface->hydro->param_riverbed[LAMBDA_AQ_TTC];
    pspecies->p_riverbed_ttc[k]->thick[VAR_AQUITARD_TTC] = pface->hydro->param_riverbed[THICKNESS_AQUITARD];
    // LP_printf(Simul->poutputs,"Inside %s  priverbed> above, parameter value:: %f, thickness is %f \n", __func__ , pface->hydro->param_riverbed[LAMBDA_AQ_TTC], pface->hydro->param_riverbed[THICKNESS_AQUITARD]);

    // Passes lambda thickness values from the below layer
    pspecies->p_riverbed_ttc[k]->lambda[VAR_BELOW_TTC] = pele_neigh->pparam_ttc->param_thermic[SOLID_TTC][LAMBDA_TTC];
    pspecies->p_riverbed_ttc[k]->thick[VAR_BELOW_TTC] = pele_neigh->phydro->Thick;
  }
}

/**
 * \brief
 * \return
 */
void CAW_build_TTC_temp_riverbed_aq(s_cmsh *pcmsh, s_chyd *pchyd, s_species_ttc *pspecies, int nriverbed, FILE *flog) {
  int i, j, nlayer, nele, id, id_hyd;
  int k = 0;
  int id_abs_ele = 0;      // Absolute id of the riverbed element associated with the river element
  double t_river = 288.15; // Temperature of the river, default value is 288.15K, 15C

  s_ele_msh *pele_neigh;
  s_layer_msh *play;

  s_reach_hyd *preach = pchyd->p_reach[0];
  s_face_hyd *pface = preach->p_faces[0];
  s_element_hyd *pele_hyd = preach->p_ele[0];

  for (int r = 0; r < pchyd->counter->nreaches; r++) {
    preach = pchyd->p_reach[r];
    for (int n = 0; n < preach->nele; n++) {
      pele_hyd = preach->p_ele[n];
      // id_abs_ele = pele_hyd->id[ABS_HYD];

      if (id_abs_ele < nriverbed) {
        if (pele_hyd->face[0][0]->id_gw != NULL) {
          nlayer = pspecies->p_riverbed_ttc[id_abs_ele]->nlayer; // Layer id of the aquifer cell
          id = pspecies->p_riverbed_ttc[id_abs_ele]->id_neigh;   // Intern id of the aquifer cell

          play = pcmsh->p_layer[nlayer];
          pele_neigh = play->p_ele[id];

          pspecies->p_riverbed_ttc[id_abs_ele]->abs_ele_id = pele_neigh->id[ABS_MSH]; // Update the absolute id of the aquifer cell

          t_river = pele_hyd->center->hydro->transp_var[0];

          // NG 19/06/2023 : DK NASTY FIX BELOW --> adding lots of WARNINGS to inform user to deal with this properly.
          LP_warning(flog, "In CaWaQS%4.2f : In %s : Entering a NASTY QUICK FIX loop for %s transport in hydraulic network. CODE CHECK NEEDED !\n", NVERSION_CAW, __func__, TTC_name_equa_type(pspecies->pset->type));

          /*
           * WORKAROUND DK 1 4 2022
           * The river hydraulics initialization at the first time steps doesn't work and the water head levels
           * can cause river temperature calculations to be very large or very low that is numerically not feasible
           * to apply in the river.
           * Until that is corrected, we set the river temperature to 15C for the river temperature values
           * above 35C and below 0C.
           */

          if (t_river < 273 || t_river > 308.15) {
            t_river = 288.15;
          }

          pspecies->p_riverbed_ttc[id_abs_ele]->temperature[VAR_ABOVE_TTC][0] = t_river;
          pspecies->p_riverbed_ttc[id_abs_ele]->temperature[VAR_BELOW_TTC][0] = pele_neigh->phydro->transp_var[0];

          // LP_printf(Simul->poutputs,"Inside %s Temps in priverbed, the T_river[%d] %lf T_aq[%d] %lf \n", __func__ ,pele_hyd->id[GIS_HYD], pele_hyd->center->hydro->transp_var[0],
          // pspecies->p_riverbed_ttc[id_abs_ele]->abs_ele_id, pele_neigh->phydro->transp_var[0]); // DK check
          //        q = pele_hyd->mb->qapp[0]/86400;
          //        LP_printf(Simul->poutputs,"Inside %s Temps in priverbed, the Q[%d] to RIVAQ from libaq is %lf \n", __func__ ,pele_hyd->id[GIS_HYD], q);
          //        pspecies->p_riverbed_ttc[k]->q = 0;
          //        LP_printf(Simul->poutputs,"Inside %s Temps in priverbed> above: %f, below %f\n", __func__ ,
          //                  pspecies->p_riverbed_ttc[k]->temperature[VAR_ABOVE_TTC][0],
          //                  pspecies->p_riverbed_ttc[k]->temperature[VAR_BELOW_TTC][0]);
          id_abs_ele++;
        }
      }
    }
  }
}

/**
 * \brief Links the infiltrated transport fluxes (i.e. matter, energy) to the aquifer system.
 * \return void
 *
 * NG : 12/04/2023 : Adapted to dissociate HEAT and SOLUTE cases.
 * **WARNING** : Only NSAT-AQ link case coded so far.
 *
 */
void CAW_cAQ_TTC_calculate_transport_fluxes(double t, double dt, int linked_module, int id_species, s_carac_aq *pchar_aq, s_carac_zns *pchar_zns, s_chyd *pchyd_hderm, s_carac_ttc *pcarac_ttc, FILE *fp) {

  s_id_io *id_list;
  s_ele_msh *pele;
  s_zns *pzns;
  s_id_spa *link;
  s_ft *pft;
  s_link_ttc *plink = pcarac_ttc->p_link[id_species];
  s_species_ttc *pspecies = pcarac_ttc->p_species[id_species];

  int nsub, k, nb_rech, r, nb_bound, pos_pbound;
  double ft_old, t_old, area_tot, q_tot, area_onaq;
  double c, q_sout, q, prct, trflux, vol;
  double *flux;

  int id_abs = 0;
  char *modname = NULL;

  modname = CAW_name_mod(linked_module);

  nb_rech = 0;
  area_tot = 0;
  q_tot = 0;

  // Liste de TOUS les éléments soumis à une recharge (i.e multiples recharges isolées et/ou recharge depuis le NSAT et/ou depuis le HDERM)
  id_list = pchar_aq->pcmsh->pcount->pcount_hydro->elesource[RECHARGE_AQ];
  id_list = IO_browse_id(id_list, BEGINNING_IO);

  while (id_list != NULL) {

    pele = pchar_aq->pcmsh->p_layer[id_list->id_lay]->p_ele[id_list->id];

    switch (linked_module) {

    case NSAT_CAW: {
      if (pele->link[NSAT_AQ] != NULL) {

        link = (s_id_spa *)SPA_browse_all_struct(IDS_SPA, pele->link[NSAT_AQ], BEGINNING_SPA);
        q = 0;
        trflux = 0.;
        prct = 0;
        c = 0.;
        nb_rech++;
        id_abs = MSH_get_ele_id(pele, ABS_MSH) - 1;
        nsub = pcarac_ttc->p_link[id_species]->nsub[id_abs][TOP_TTC];

        // Calculating input transport and water flux on aquifer cell, while accounting for intersection with NSAT grid
        while (link != NULL) {
          pzns = pchar_zns->p_zns[link->id];
          area_onaq = pele->pdescr->surf * link->prct;
          area_tot += area_onaq;

          if (pzns->area > 0) {
            prct = area_onaq / pzns->area;
          } else {
            prct = 0;
          }

          // water
          q += (pzns->wat_bal->q[RSV_SOUT] * prct);
          q_tot += q;

          // transport flux (either energy in W/m2 or matter in g/m2)
          flux = SPA_calcul_transport_fluxes(NSAT_AQ_SPA, pzns, id_species, prct, t, dt, fp);
          trflux += flux[0];

          if ((isnan(pzns->wat_bal->q[RSV_SOUT]) || isinf(pzns->wat_bal->q[RSV_SOUT])) || pzns->wat_bal->q[RSV_SOUT] > RECH_MAX_CAW) {
            LP_error(fp, "In function %s Abnormal MATTER FLUX value calculated : %f for AQ cell %d layer %d and ZNS cell INTERN_ID %d ABS_ID %d", __func__, pzns->wat_bal->q[RSV_SOUT], pele->id[ABS_MSH], id_list->id_lay + 1, link->id, link->id + 1);
          }

          free(flux);
          link = link->next;
        }

        if (q == 0) { // Security check to adapt
          q = 1e-15;
        }

        // Affecting values to aquifer system element according to specie type
        if (pspecies->pset->type == HEAT_TTC) {

          if (nsub == 0) {
            pspecies->p_boundary_ttc[id_abs]->valcl[TOP_TTC][0] = trflux * area_onaq / (HEAT_CAPW_TTC * RHOW_TTC) / q;

            /* DKDK WORKAROUND, LIBFP reports in certain cells at the edges of the simulated domain very large or very low temperature values
             * In the case of these values are encountered, fix the top boundary of aquifer temperature to 278.15K */

            if (pspecies->p_boundary_ttc[id_abs]->valcl[TOP_TTC][0] < T_0 || pspecies->p_boundary_ttc[id_abs]->valcl[TOP_TTC][0] > T_LIMIT_UP) {
              pspecies->p_boundary_ttc[id_abs]->valcl[TOP_TTC][0] = T_DEFAULT - 10;
            }
            pspecies->p_boundary_ttc[id_abs]->icl[TOP_TTC][0] = DIRI_FACE_TTC;

            // LP_printf(fp, "func %s flux is %lf i is %d, id %d, area_aq: %lf area_zns: %lf area_tot: %lf, id_cell: %d tstep: %d, t %lf dt %lf\n", __func__ ,
            // flux[0], id_abs, link->id, area_onaq, pzns->area, area_tot, id_list->id, tstep, t, dt);
            // if ( id_abs == 63588 || id_abs == 63578 || id_abs == 57214 || id_abs == 63589 || id_abs == 63879  ) { DK DEBUG
            // if ( id_abs == 29139 || id_abs == 20954 || id_abs == 13401 || id_abs == 8040 || id_abs == 1719    ) {

            // LP_printf(fp, "func %s flux is %lf q %e TOPBC %lf  i is %d, id %d, area_aq: %lf area_zns: %lf area_tot: %lf, id_cell: %d tstep: %d, t %lf dt %lf\n", __func__ ,
            // flux[0],q, pspecies->p_boundary_ttc[id_abs]->valcl[TOP_TTC][0], id_abs, link->id, area_onaq, pzns->area, area_tot, id_list->id, tstep, t, dt);
            // }

          } else {
            for (int sub_icard = 0; sub_icard < nsub; ++sub_icard) {
              pspecies->p_boundary_ttc[id_abs]->icl[TOP_TTC][sub_icard] = DIRI_FACE_TTC;
              pspecies->p_boundary_ttc[id_abs]->valcl[TOP_TTC][sub_icard] = trflux * area_onaq / (HEAT_CAPW_TTC * RHOW_TTC) / q;

              /*
               * DKDK
               * WORKAROUND, LIBFP reports in certain cells at the edges of the simulated domain very large or very low temperature values
               * In the case of these values are encountered, fix the top boundary of aquifer temperature to 278.15K
               * */
              if (pspecies->p_boundary_ttc[id_abs]->valcl[TOP_TTC][0] < T_0 || pspecies->p_boundary_ttc[id_abs]->valcl[TOP_TTC][0] > T_LIMIT_UP) {
                pspecies->p_boundary_ttc[id_abs]->valcl[TOP_TTC][0] = T_DEFAULT - 10;
              }

              // if ( id_abs == 29139 || id_abs == 20954 || id_abs == 13401 || id_abs == 8040 || id_abs == 1719    ) { DK DEBUG
              // LP_printf(fp, "func %s flux is %lf q %e TOPBC %lf i is %d, id %d, area_aq: %lf area_zns: %lf area_tot: %lf, id_cell: %d tstep: %d, t %lf dt %lf\n", __func__ ,
              // flux[0],q, pspecies->p_boundary_ttc[id_abs]->valcl[TOP_TTC][sub_icard], id_abs, link->id, area_onaq, pzns->area, area_tot, id_list->id, tstep, t, dt);
              // }
            }
          }
        } else // Solute case
        {
          // NSAT discharge to volume (m3) over time step
          vol = RSV_q_to_volume(q, dt, SEC_TS, fp);

          if (nsub == 0) {

            c = (trflux * area_onaq) / vol;
            pspecies->p_boundary_ttc[id_abs]->valcl[TOP_TTC][0] = (trflux * area_onaq) / vol; // Concentration in g/m3
            pspecies->p_boundary_ttc[id_abs]->icl[TOP_TTC][0] = DIRI_FACE_TTC;
          } else {
            c = (trflux * area_onaq) / vol;

            for (int sub_icard = 0; sub_icard < nsub; ++sub_icard) {
              pspecies->p_boundary_ttc[id_abs]->icl[TOP_TTC][sub_icard] = DIRI_FACE_TTC;
              pspecies->p_boundary_ttc[id_abs]->valcl[TOP_TTC][sub_icard] = (trflux * area_onaq) / vol; // Concentration in g/m3
            }
          }
        }
        break;
      }
    }

    case HDERM_CAW: {
      LP_error(fp, "In CaWaQS%4.2f : Function %s : %s-AQ link for transport not implemented yet.\n", NVERSION_CAW, __func__, CAW_name_mod(linked_module));
    }

    default:
      LP_error(fp, "Aquifer recharge calculations not implemented yet for module %s. Error in function %s, file %s at line %d.\n", CAW_name_mod(linked_module), __func__, __FILE__, __LINE__);
    }
    id_list = id_list->next;
  }
  free(modname);
}

/**
 * \brief
 * \return
 */
void CAW_cAQ_TTC_river_impact_on_aq(double t, double dt, int id_species, s_carac_aq *pchar_aq, s_chyd *pchyd, s_carac_ttc *pcarac_ttc, FILE *flog) {

  int nb_rech, nb_bound, pos_pboun, id_hyd, id, nlayer;
  double ft_old, t_old, area_tot, q_tot, area_onaq;
  double t_sout, t_river, prct;
  double *flux;
  int id_neigh;
  // Warning timestep normally starts from 1, but libttc time index for the boundary starts at 0. hence we subtract 1
  int tstep = t / dt;
  tstep = tstep - 1;
  int pp = 0;
  int nriverbed = pcarac_ttc->count[NRIVERBED_TTC];
  ;

  double area_vertical = 0;
  double area_river = 0;
  double flux_advection = 0;
  double flux_conduction = 0;

  int id_abs_ele = 0;
  int id_abs_aq;
  s_cmsh *pcmsh = pchar_aq->pcmsh;
  s_ele_msh *pele_neigh;
  s_layer_msh *play;

  s_link_ttc *plink = pcarac_ttc->p_link[id_species];
  s_species_ttc *pspecies = pcarac_ttc->p_species[id_species];

  s_reach_hyd *preach = pchyd->p_reach[0];
  s_element_hyd *pele_hyd = preach->p_ele[0];
  s_face_hyd *pface = preach->p_faces[0];

  for (int r = 0; r < pchyd->counter->nreaches; r++) {
    preach = pchyd->p_reach[r];
    for (int n = 0; n < preach->nele; n++) {

      pele_hyd = preach->p_ele[n];
      if (id_abs_ele < nriverbed) {
        if (pele_hyd->face[0][0]->id_gw != NULL) {

          nlayer = pspecies->p_riverbed_ttc[id_abs_ele]->nlayer; // Layer id of the aquifer cell
          id = pspecies->p_riverbed_ttc[id_abs_ele]->id_neigh;   // Intern id of the aquifer cell

          id_hyd = pface->id[GIS_HYD]; // ID_GIS in pface is same as ID_GIS in pele_hyd (Which is not the ABS id which starts at 0 but does not have same index with the pele_hyd)

          //            LP_printf(flog, "%s  k is %d id_face: %d id_ele_hyd: %d abs_face_id %d abs_ele_id %d face_int_id %d ele_int_id %d\n",
          //                      __func__ , k, id_hyd, pele_hyd->id[GIS_HYD], pface->id[ABS_HYD], pele_hyd->id[ABS_HYD], pface->id[INTERN_HYD], pele_hyd->id[INTERN_HYD] );
          //      LP_printf(flog, "%s  k is %d id_neigh_aq: %d id_aq: %d absid %d, id_intern %d id_face: %d id_ele_hyd: %d abs_face_id %d abs_ele_id %d face_int_id %d ele_int_id %d\n",
          //                  __func__ , k, id_neigh, pele_neigh->id[ABS_MSH]-1, pspecies->p_riverbed_ttc[id_abs_ele]->abs_ele_id, pspecies->p_riverbed_ttc[id_abs_ele]->id_intern,
          //                  id_hyd,  pele_hyd->id[GIS_HYD], pface->id[ABS_HYD], pele_hyd->id[ABS_HYD], pface->id[INTERN_HYD], pele_hyd->id[INTERN_HYD] );

          play = pcmsh->p_layer[nlayer];
          pele_neigh = play->p_ele[id];
          id_abs_aq = pele_neigh->id[ABS_MSH] - 1;
          id_neigh = pspecies->p_riverbed_ttc[id_abs_ele]->id_neigh;

          if (pele_neigh->priverbed_ttc != NULL) {
            flux_advection = pele_neigh->priverbed_ttc->flux_adv[VAR_ABOVE_TTC];
            flux_conduction = pele_neigh->priverbed_ttc->flux_cond[VAR_ABOVE_TTC];
          }

          area_vertical = pele_neigh->pdescr->l_side * pele_neigh->pdescr->l_side;
          area_river = pele_hyd->length * pele_hyd->reach->width;

          t_sout = pspecies->p_boundary_ttc[id_abs_aq]->valcl[TOP_TTC][0]; // this should be the index of abs aquifer id-1
          t_river = pele_hyd->center->hydro->transp_var[0];

          // NG 19/06/2023 : DK NASTY FIX BELOW --> Adding lots of WARNINGS to inform user to deal with this properly.
          LP_warning(flog, "In CaWaQS%4.2f : In %s : Entering a NASTY QUICK FIX loop for %s transport in hydraulic network. CODE CHECK NEEDED !\n", NVERSION_CAW, __func__, TTC_name_equa_type(pspecies->pset->type));

          /* DKDK 23 04 2022
           * Here is the workaround on the River temperatures, in case the temperature of river is < 273.15K (0°C) or 308.15K (35°C)
           *  Since the river hydraulics initialization cause problem in the transport at the river, we have to limit the temperature
           *  applied in the aquifer to the T range of the river we observe in the Seine basin.
           *
           *  It could be removed once the river hydraulics initialization is properly handled and river temperatures are calculated correctly
           *  Variables T_LIMIT_UP T_0 and T_DEFAULT are defined in LIBSEB
           */

          if (t_river > T_LIMIT_UP || t_river < T_0) {
            t_river = T_DEFAULT;
          }

          pspecies->p_riverbed_ttc[id_abs_ele]->temperature[VAR_ABOVE_TTC][0] = pele_hyd->center->hydro->transp_var[0];
          pspecies->p_riverbed_ttc[id_abs_ele]->temperature[VAR_BELOW_TTC][0] = pele_neigh->phydro->transp_var[0];

          pspecies->p_boundary_ttc[id_abs_aq]->valcl[TOP_TTC][0] = (t_river * area_river + t_sout * (area_vertical - area_river)) / area_vertical;
          // LP_printf(Simul->poutputs,"Inside %s Temps in priverbed, the T_river[%d] %lf T_aq[%d] abs_aq %d %lf areavert: %lf, area_riv: %lf, topbc: %lf advection %lf, conduction %lf\n", __func__ ,
          //                                      pele_hyd->id[GIS_HYD], t_river,
          //                                      id_neigh, id_abs_aq, t_sout, area_vertical, area_river,
          //                                      pspecies->p_boundary_ttc[id_neigh]->valcl[TOP_TTC][0],  flux_advection, flux_conduction); // DK check
          id_abs_ele++;
        }
      }
    }
  }
}

/**
 * \brief
 * \return
 */
void CAW_update_TTC_velocity_riverbed_aq(s_chyd *pchyd, s_species_ttc *pspecies, int nriverbed, FILE *flog) {

  int id_abs_ele = 0;

  s_reach_hyd *preach = pchyd->p_reach[0];
  s_face_hyd *pface = preach->p_faces[0];
  s_element_hyd *pele_hyd = preach->p_ele[0];

  for (int r = 0; r < pchyd->counter->nreaches; r++) {
    preach = pchyd->p_reach[r];

    for (int n = 0; n < preach->nele; n++) {
      pele_hyd = preach->p_ele[n];
      // id_abs_ele = pele_hyd->id[ABS_HYD];
      if (id_abs_ele < nriverbed) {
        if (pele_hyd->face[0][0]->id_gw != NULL) {
          pspecies->p_riverbed_ttc[id_abs_ele]->q = (-1.) * pele_hyd->center->hydro->qapp[INR_HYD][T_HYD] / preach->width; // Velocity at the river-aquifer interface
          // The velocity is multiplied with -1 to reverse the sign. pele_hyd includes
          // The sign reversal makes negative flow towards river and positive flow towards aquifer
          //
          // LP_printf(flog, "%s: name %s\t  id_abs: %d \t ABS_riv_ele:\t %d\t ele_id_hyd %d\t face_id_abs %d \t face_id_gis %d\t Aquifer_layer: %d\t  aquifer_cell %d \n",
          // __func__,  pele_hyd->face[0][0]->name, id_abs_ele, pele_hyd->id[ABS_HYD], pele_hyd->id[GIS_HYD], pele_hyd->face[0][0]->id[ABS_HYD], pele_hyd->face[0][0]->id[GIS_HYD], pele_hyd->face[0][0]->id_gw->id_lay, pele_hyd->face[0][0]->id_gw->id );
          // LP_printf(Simul->poutputs, "Inside %s Temps in priverbed: %d, Velocity: %e \n",
          // __func__, id_abs_ele,  pele_hyd->center->hydro->qapp[INR_HYD][T_HYD] );
          id_abs_ele++;
        }
        /*	if (pele_hyd->face[0][1]->id_gw != NULL ) {
            pspecies->p_riverbed_ttc[id_abs_ele]->q = pele_hyd->center->hydro->qapp[INR_HYD][T_HYD] ; // Velocity at the river-aquifer interface
            LP_printf(flog, "%s: leftbank face name %s\t ABS_riv_ele:\t %d\t id_hyd %d\t gis_hydupstream %d\t Aquifer_layer: %d\t  aquifer_cell %d \n",
            __func__, pele_hyd->face[0][1]->name, pele_hyd->id[ABS_HYD], pele_hyd->id[GIS_HYD],pele_hyd->face[0][1]->id[GIS_HYD],
            pele_hyd->face[0][1]->id_gw->id_lay, pele_hyd->face[0][1]->id_gw->id );  */
      }
    }
  }
}

/**
 * \brief
 * \return -
 */
void CAW_update_velocities_TTC(s_cmsh *pcmsh, s_carac_ttc *pcarac_ttc, int id_species, FILE *fpout) {

  int nlayer, nele, icard, id, i, j, idir, itype;
  s_ele_msh *pele;   // Aquifer element
  s_id_io *neigh;    // ID of the neighbouring cell at the face
  s_ele_msh *pneigh; // Neighbour element in the case of subfaces
  s_layer_msh *play; // Aquifer layer
  int eletot = 0;    // Libttc element
  int nsub = 0;      // Number of sub-elements
  int isub;          // Index of subface
  int reverse = 0;   // Index of the face reversal for the neighbour
  double u;
  double area_neigh = 0;

  double area_vertical;   // Area of the vertical faces of a cell
  double area_horizontal; // Area of the horizontal faces of a cell

  int nbound, nsource;
  nlayer = pcmsh->pcount->nlayer;
  int sub_card;

  for (i = 0; i < nlayer; i++) {
    play = pcmsh->p_layer[i];
    nele = play->nele;
    for (j = 0; j < nele; j++) {
      pele = play->p_ele[j];
      id = MSH_get_ele_id(pele, ABS_MSH);

      area_vertical = pele->pdescr->l_side * pele->pdescr->l_side;
      area_horizontal = pele->pdescr->l_side * pele->phydro->Thick;

      // Change of storage (dV_dt)
      pcarac_ttc->dVdt[eletot] = pele->phydro->pmb->mass[DMASS_AQ]; // area_vertical; // Add the change of stock
      // LP_printf(fpout,"%s: eletot %d abs_ele %d size cell %lf dVdT  %e \n  ",
      // __func__ , eletot, pele->id[ABS_MSH], pele->pdescr->l_side,  pcarac_ttc->dVdt[eletot]);

      for (idir = 0; idir < ND_MSH; idir++) {

        for (itype = 0; itype < NFACE_MSH; itype++) {
          isub = 0; // Index of subface
          icard = CAW_TTC_card_aq(idir, itype);
          nsub = pcarac_ttc->p_link[id_species]->nsub[eletot][icard];

          // LP_printf(fpout, "%s NSUB is %d \n",__func__ ,nsub);
          neigh = pele->p_neigh[idir][itype];
          neigh = IO_browse_id(neigh, BEGINNING_TS);

          if (nsub > 0) {
            while (neigh != NULL) {
              pneigh = MSH_get_ele(pcmsh, neigh->id_lay, neigh->id, fpout);

              if (itype == ONE_MSH) { // Opposite face
                reverse = TWO_MSH;
              } else if (itype == TWO_MSH) {
                reverse = ONE_MSH;
              }
              /* DK 5 4 2022
               * Here we calculate the surface area of the neighbouring cell facing to our *pele
               * Warning: the sign of the area is negative because we change signs of the velocity field as the
               * flow is from neighbouring cell to current cell
               */
              if (idir == Z_MSH) {
                area_neigh = pneigh->pdescr->l_side * pneigh->pdescr->l_side;
              } else {
                area_neigh = pneigh->pdescr->l_side * pneigh->phydro->Thick;
              }

              pcarac_ttc->uface[eletot][icard][isub] = -pneigh->phydro->pmb->flux[idir][reverse][DMASS_AQ]; // area_neigh; // We reverse the sign from the area
              // LP_printf(fpout,"%s: eletot %d abs_ele %d face %s | neigh u[%d] %e area_neigh %e  velocity %e\n  ",
              // __func__ , eletot, pele->id[ABS_MSH], TTC_param_card(icard), pneigh->id[ABS_MSH],
              // pneigh->phydro->pmb->flux[idir][reverse][DMASS_AQ],
              // area_neigh,
              // pneigh->phydro->pmb->flux[idir][reverse][DMASS_AQ]/area_neigh );
              isub++;
              neigh = neigh->next;
            }
          } else { // If we dont have a subface
            if (idir == Z_MSH) {
              pcarac_ttc->uface[eletot][icard][nsub] = pele->phydro->pmb->flux[idir][itype][DMASS_AQ]; // area_vertical;
            } else {
              pcarac_ttc->uface[eletot][icard][nsub] = pele->phydro->pmb->flux[idir][itype][DMASS_AQ]; // area_horizontal;
            }
          }

          u = 0;
          pcarac_ttc->qSink[eletot] = 0;
          pcarac_ttc->qSource[eletot] = 0;
          // if (idir == Z_MSH) {
          //  u += pele->phydro->pmb->flux[idir][itype][DMASS_AQ]/(pele->pdescr->l_side*pele->pdescr->l_side);
          // } else {
          // u += pele->phydro->pmb->flux[idir][itype][DMASS_AQ]/(pele->pdescr->l_side*pele->phydro->Thick);
          // }

          // 2 columns (recharge, uptake)
          if (icard == TOP_TTC) {
            for (nsource = 0; nsource < NSOURCE; nsource++) {
              if (nsource == UPTAKE_AQ) {
                // Uptake goes at the center of the cell to the stock term, it does not go into transport

                // Verify the sign
                pcarac_ttc->qSink[eletot] += pele->phydro->pmb->source[nsource][DMASS_AQ] * (-1.); // area_vertical; // Change in the stock variation in case of water uptake

              } else {
                // Recharge goes at the TOP_TTC face
                u += pele->phydro->pmb->source[nsource][DMASS_AQ] * (-1.); // area_vertical;
              }

              // LP_printf(fpout, "Function %s Cell %d FACE %s MB Q[%s] %e Area: %e  velocity %e  \n", __func__ ,
              // eletot, TTC_param_card(icard),
              // AQ_name_source(nsource),
              // pele->phydro->pmb->source[nsource][DMASS_AQ]*(-1.),
              // area_vertical,
              // pele->phydro->pmb->source[nsource][DMASS_AQ]*(-1.)/area_vertical );
            }
          }

          // 4 columns Dirichlet, Neumann, Overflow, Stream-aquifer exchange
          for (nbound = 0; nbound < NBOUND; nbound++) {
            if (nbound == CAUCHY) {
              if (pele->loc == TOP_MSH) {
                if (icard == TOP_TTC) {

                  if (pele->phydro->pmb->pbound[nbound][DMASS_AQ] < 0) {
                    pcarac_ttc->qSink[eletot] += pele->phydro->pmb->pbound[nbound][DMASS_AQ]; // area_vertical;//output_i=SOL in enum out_aq_mb
                  } else {
                    pcarac_ttc->qSource[eletot] += pele->phydro->pmb->pbound[nbound][DMASS_AQ]; // area_vertical;//output_i=SOL in enum out_aq_mb
                  }

                  // LP_printf(fpout, "Function %s Cell %d FACE %s MB Q[%s] %e Area: %e  velocity %e  \n",
                  // __func__,
                  // eletot, TTC_param_card(icard),
                  // AQ_name_bc_mb(nbound),
                  // pele->phydro->pmb->pbound[nbound][DMASS_AQ],
                  // area_vertical,
                  // pele->phydro->pmb->pbound[nbound][DMASS_AQ] / area_vertical);
                }
              } else if (pele->loc == RIV_MSH) {
                if (icard == TOP_TTC) {

                  if (pele->phydro->pmb->pbound[nbound][DMASS_AQ] < 0) {
                    pcarac_ttc->qSink[eletot] += pele->phydro->pmb->pbound[nbound][DMASS_AQ]; // area_vertical;
                  } else {
                    pcarac_ttc->qSource[eletot] += pele->phydro->pmb->pbound[nbound][DMASS_AQ]; // area_vertical;
                  }
                  // LP_printf(fpout,
                  // "VERIF Function %s Cell %d FACE %s MB Q[%s] %e Area: %e  velocity %e qsink %e \n",
                  // __func__,
                  // eletot, TTC_param_card(icard),
                  // AQ_name_bc_mb(nbound),
                  // pele->phydro->pmb->pbound[nbound][DMASS_AQ],
                  // area_vertical,
                  // pele->phydro->pmb->pbound[nbound][DMASS_AQ] / area_vertical,
                  // pcarac_ttc->qSink[eletot]);
                } else {
                  u += 0.; // output_i=SOL in enum out_aq_mb
                }
              }
            } else {
              if (icard == TOP_TTC) {

                if (pele->phydro->pmb->pbound[nbound][DMASS_AQ] < 0) {
                  pcarac_ttc->qSink[eletot] += pele->phydro->pmb->pbound[nbound][DMASS_AQ]; // area_vertical;
                } else {
                  pcarac_ttc->qSource[eletot] += pele->phydro->pmb->pbound[nbound][DMASS_AQ]; // area_vertical;
                }
                // LP_printf(fpout, "Function %s Cell %d FACE %s MB Q[%s] %e Area: %e  velocity %e qsink %e \n", __func__ ,
                // eletot, TTC_param_card(icard),
                // AQ_name_bc_mb(nbound),
                // pele->phydro->pmb->pbound[nbound][DMASS_AQ],
                // area_vertical,
                // pele->phydro->pmb->pbound[nbound][DMASS_AQ]/area_vertical,
                // pcarac_ttc->qSink[eletot]);
                // output_i=FLUX_DIRICHLET,FLUX_NEUMANN in enum out_aq_mb
              }
            }
          }

          switch (nsub) {
          case 0:
            sub_card = 0;
            pcarac_ttc->uface[eletot][icard][sub_card] += u;
            // LP_printf(fpout,"uface in plink nlayer: %d id_ABS %d icard %s, idir= %d, itype= %d = %e, l_side is %e, thickness is %e, nsub: %d\n",
            // i, id,TTC_param_card(icard),idir, itype,pcarac_ttc->uface[eletot][icard][0],pele->pdescr->l_side,pele->phydro->Thick, nsub);
            break;
          default:
            for (sub_card = 0; sub_card < nsub; sub_card++) {
              pcarac_ttc->uface[eletot][icard][sub_card] += u;
              // LP_printf(fpout,"uface in plink id_ABS %d icard %s, idir= %d, itype= %d = %e, l_side is %e, thickness is %e, nsub = %d\n",
              // id,TTC_param_card(icard),idir, itype,pcarac_ttc->uface[eletot][icard][sub_card],pele->pdescr->l_side,pele->phydro->Thick, nsub);
            }
            break;
          }
        }
      }
      // LP_printf(fpout,"uface in plink id_ABS %d dVdt = %e qSink %e qSource %e, Recharge %e  \n",
      // id, pcarac_ttc->dVdt[eletot],   pcarac_ttc->qSink[eletot], pcarac_ttc->qSource[eletot], u );
      eletot++;
    }
  }
}

/**
 * \brief
 * \return -
 *
 * INFO : libttc and CaWaQS have different way of treatment of the directions.
 * In CaWaQS the sign of the flow defines whether the flow is in (positive) or out (negative) of the cell at a give faces
 * In libttc the sign of the flow defines whether the flow is directed to North/South or East/West
 *
 */
void CAW_update_velocity_directions_TTC(s_cmsh *pcmsh, s_carac_ttc *pcarac_ttc, int id_species, FILE *fpout) {
  int nsub, nlayer, nele, icard, id, i, j, idir, itype;
  int eletot = 0;
  s_ele_msh *pele;
  s_layer_msh *play;
  double u;

  nlayer = pcmsh->pcount->nlayer;

  for (i = 0; i < nlayer; i++) {
    play = pcmsh->p_layer[i];
    nele = play->nele;
    for (j = 0; j < nele; j++) {
      pele = play->p_ele[j];
      id = MSH_get_ele_id(pele, ABS_MSH);
      for (idir = 0; idir < ND_MSH; idir++) {
        for (itype = 0; itype < NFACE_MSH; itype++) {
          icard = CAW_TTC_card_aq(idir, itype);

          nsub = pcarac_ttc->p_link[id_species]->nsub[eletot][icard];
          /*
           * If Velocity at East is positive, flow is outwards Reversal
           *                East is negative, flow is inwards
           *                West is positive, flow is inwards
           *                West is negative, flow is outwards
           *                North is positive flow is outwards Reversal
           *                North is negative, flow is inwards
           *                South is positive flow is inwards
           *                South is negative flow is outwards
           *                Top is positive flow is outwards   Reversal
           *                Top is negative flow is inwards
           *                Bottom is positive flow is inwards
           *                Bottom is negative flow is outwards
           */
          switch (nsub) {
          case 0:
            u = pcarac_ttc->uface[eletot][icard][0];
            switch (icard) {
            case EAST_TTC: {
              if (u == 0) {
                pcarac_ttc->uface[eletot][icard][0] = u;
              } else {
                pcarac_ttc->uface[eletot][icard][0] = -u;
              }
              break;
            }
            case WEST_TTC: {

              pcarac_ttc->uface[eletot][icard][0] = u;
              break;
            }
            case NORTH_TTC: {

              if (u == 0) {
                pcarac_ttc->uface[eletot][icard][0] = u;
              } else {
                pcarac_ttc->uface[eletot][icard][0] = -u;
              }
              break;
            }
            case SOUTH_TTC: {
              pcarac_ttc->uface[eletot][icard][0] = u;
              break;
            }
            case TOP_TTC: {
              if (u == 0) {
                pcarac_ttc->uface[eletot][icard][0] = u;
              } else {
                pcarac_ttc->uface[eletot][icard][0] = -u;
              }
              break;
            }
            case BOTTOM_TTC: {
              pcarac_ttc->uface[eletot][icard][0] = u;
              break;
            }
            }
            break;

          default:
            for (int sub_card = 0; sub_card < nsub; sub_card++) {
              u = pcarac_ttc->uface[eletot][icard][sub_card];

              switch (icard) {
              case EAST_TTC: {
                if (u == 0) {
                  pcarac_ttc->uface[eletot][icard][sub_card] = u;
                } else {
                  pcarac_ttc->uface[eletot][icard][sub_card] = -u;
                }
                break;
              }
              case WEST_TTC: {
                pcarac_ttc->uface[eletot][icard][sub_card] = u;
                break;
              }
              case NORTH_TTC: {
                if (u == 0) {
                  pcarac_ttc->uface[eletot][icard][sub_card] = u;
                } else {
                  pcarac_ttc->uface[eletot][icard][sub_card] = -u;
                }
                break;
              }
              case SOUTH_TTC: {
                pcarac_ttc->uface[eletot][icard][sub_card] = u;
                break;
              }
              case TOP_TTC: {
                if (u == 0) {
                  pcarac_ttc->uface[eletot][icard][sub_card] = u;
                } else {
                  pcarac_ttc->uface[eletot][icard][sub_card] = -u;
                }
                break;
              }
              case BOTTOM_TTC: {
                pcarac_ttc->uface[eletot][icard][sub_card] = u;
                break;
              }
              }
            }
            break;
          }
          // LP_printf(fpout,"Updated uface in plink id_ABS %d icard %s = %e\n",id,TTC_param_card(icard),plink->uface[eletot][icard][0]);
        }
      }
      eletot++;
    }
  }
}

/**
 * \brief Refreshes transport variable (ie. C or T depending on the specie type) for one specie in libaq, after TTC calculations
 * \return -
 */
void CAW_update_variable_one_specie_aq(s_species_ttc *pspecies, s_carac_aq *pcarac_aq, s_carac_ttc *pcarac_ttc, int id_species, FILE *flog) {

  int nlayer, nele, i, j;
  int eletot = 0;
  int nflux = 0;
  int nriverbed = pcarac_ttc->count[NRIVERBED_TTC];
  int naquitard = pcarac_ttc->count[NAQUITARD_TTC];
  int gw_id_lay; // the id of layer that has an allocated riverbed cell (riverbed == on)
  int gw_id;     // the id cell that has an allocated riverbed cell (riverbed == on)
  int offset;
  s_ele_msh *pele;
  s_ele_msh *pele_riv;
  s_layer_msh *play;

  offset = id_species * NB_MB_FLUX_AQ_TRANSPORT; // NG fix : 28/04/2023 : Offset to determine the position of fluxes in *trans_flux, in case of several species

  nlayer = pcarac_aq->pcmsh->pcount->nlayer;

  for (i = 0; i < nlayer; i++) {
    play = pcarac_aq->pcmsh->p_layer[i];
    nele = play->nele;

    for (j = 0; j < nele; j++) {
      pele = play->p_ele[j];
      pele->phydro->transp_var[id_species] = pspecies->pgc->x[eletot];
      // LP_printf(flog,"%s Updated val in pele id_ABS %d, nlayer: %d, ele: %d, val: %lf\n",__func__, pele->id[ABS_MSH],i, j, pspecies->pgc->x[eletot]);

      pele->phydro->transp_flux[offset + 0] = pspecies->p_flux_ttc[eletot]->condface[EAST_TTC][0];
      pele->phydro->transp_flux[offset + 1] = pspecies->p_flux_ttc[eletot]->advface[EAST_TTC][0];
      pele->phydro->transp_flux[offset + 2] = pspecies->p_flux_ttc[eletot]->totface[EAST_TTC][0];
      pele->phydro->transp_flux[offset + 3] = pspecies->p_flux_ttc[eletot]->condface[WEST_TTC][0];
      pele->phydro->transp_flux[offset + 4] = pspecies->p_flux_ttc[eletot]->advface[WEST_TTC][0];
      pele->phydro->transp_flux[offset + 5] = pspecies->p_flux_ttc[eletot]->totface[WEST_TTC][0];
      pele->phydro->transp_flux[offset + 6] = pspecies->p_flux_ttc[eletot]->condface[NORTH_TTC][0];
      pele->phydro->transp_flux[offset + 7] = pspecies->p_flux_ttc[eletot]->advface[NORTH_TTC][0];
      pele->phydro->transp_flux[offset + 8] = pspecies->p_flux_ttc[eletot]->totface[NORTH_TTC][0];
      pele->phydro->transp_flux[offset + 9] = pspecies->p_flux_ttc[eletot]->condface[SOUTH_TTC][0];
      pele->phydro->transp_flux[offset + 10] = pspecies->p_flux_ttc[eletot]->advface[SOUTH_TTC][0];
      pele->phydro->transp_flux[offset + 11] = pspecies->p_flux_ttc[eletot]->totface[SOUTH_TTC][0];
      pele->phydro->transp_flux[offset + 12] = pspecies->p_flux_ttc[eletot]->condface[BOTTOM_TTC][0];
      pele->phydro->transp_flux[offset + 13] = pspecies->p_flux_ttc[eletot]->advface[BOTTOM_TTC][0];
      pele->phydro->transp_flux[offset + 14] = pspecies->p_flux_ttc[eletot]->totface[BOTTOM_TTC][0];
      pele->phydro->transp_flux[offset + 15] = pspecies->p_flux_ttc[eletot]->condface[TOP_TTC][0];
      pele->phydro->transp_flux[offset + 16] = pspecies->p_flux_ttc[eletot]->advface[TOP_TTC][0];
      pele->phydro->transp_flux[offset + 17] = pspecies->p_flux_ttc[eletot]->totface[TOP_TTC][0];

      if (pele->priverbed_ttc != NULL) {
        pele->phydro->transp_flux[offset + 18] = pele->priverbed_ttc->flux_adv[VAR_ABOVE_TTC];
        pele->phydro->transp_flux[offset + 19] = pele->priverbed_ttc->flux_cond[VAR_ABOVE_TTC];
        pele->phydro->transp_flux[offset + 20] = pele->priverbed_ttc->temperature[VAR_AQUITARD_TTC][0];
      } else // NG : 14/06/2023. DK bug fix. Did not account for case with riverbed not activated
      {
        pele->phydro->transp_flux[offset + 18] = 0.;
        pele->phydro->transp_flux[offset + 19] = 0.;
        pele->phydro->transp_flux[offset + 20] = 0.;
      }

      if (pele->paquitard_ttc != NULL) {
        pele->phydro->transp_flux[offset + 21] = pele->paquitard_ttc->flux_adv[VAR_ABOVE_TTC];
        pele->phydro->transp_flux[offset + 22] = pele->paquitard_ttc->flux_cond[VAR_ABOVE_TTC];
        pele->phydro->transp_flux[offset + 23] = pele->paquitard_ttc->temperature[VAR_AQUITARD_TTC][0];
      } else // NG : 14/06/2023. DK bug fix. Did not account for case with aquitard not activated
      {
        pele->phydro->transp_flux[offset + 21] = 0.;
        pele->phydro->transp_flux[offset + 22] = 0.;
        pele->phydro->transp_flux[offset + 23] = 0.;
      }
      pele->phydro->transp_flux[offset + 24] = pspecies->p_flux_ttc[eletot]->dTdt;
      pele->phydro->transp_flux[offset + 25] = pspecies->p_flux_ttc[eletot]->mb_transport;

      // LP_printf(flog,"Updated val in pele id_ABS %d, TOP cond: %e, TOP adv: %e, TOP tot: %e, BOT cond: %e BOT adv: %e BOT tot: %e \n",pele->id[ABS_MSH],pele->phydro->transp_flux[12],
      // pele->phydro->transp_flux[13],pele->phydro->transp_flux[14],pele->phydro->transp_flux[15],pele->phydro->transp_flux[16],pele->phydro->transp_flux[17]);
      // LP_printf(flog,"Updated val in pele id_ABS %d, totflux_east: %lf, totflux_west \n",pele->id[ABS_MSH],pele->phydro->transp_flux[1],
      // pele->phydro->transp_flux[4]);
      eletot++;
    }
  }

  eletot = 0;
  if (pspecies->pset->riverbed == AQUITARD_ON_TTC) {

    while (eletot < nriverbed) {
      gw_id_lay = pspecies->p_riverbed_ttc[eletot]->nlayer; // Layer id of the aquifer cell under river
      gw_id = pspecies->p_riverbed_ttc[eletot]->id_neigh;   // Cell id of the aquifer cell under river

      play = pcarac_aq->pcmsh->p_layer[gw_id_lay];
      pele = play->p_ele[gw_id];
      // LP_printf(flog, "func %s active abs id %d river ele id is %d , nlay= %d \n", __func__ , MSH_get_ele_id(pele,ABS_MSH),gw_id,gw_id_lay);
      if (pele->priverbed_ttc != NULL) {
        pele->priverbed_ttc->flux_cond[VAR_ABOVE_TTC] = pspecies->p_riverbed_ttc[eletot]->flux_cond[VAR_BELOW_TTC];
        pele->priverbed_ttc->flux_adv[VAR_ABOVE_TTC] = pspecies->p_riverbed_ttc[eletot]->flux_adv[VAR_ABOVE_TTC];
        pele->priverbed_ttc->temperature[VAR_AQUITARD_TTC][0] = pspecies->p_riverbed_ttc[eletot]->temperature[VAR_AQUITARD_TTC][0];
        // LP_printf(flog, "func %s active river ele id is %d condflux: %lf, flux_adv: %lf\n", __func__ ,MSH_get_ele_id(pele,ABS_MSH),
        // pspecies->p_riverbed_ttc[eletot]->flux_cond[VAR_BELOW_TTC], pspecies->p_riverbed_ttc[eletot]->flux_adv[VAR_ABOVE_TTC]);
      }
      eletot++;
    }
    // LP_printf(flog, "func %s ele id is %d\n", __func__ ,MSH_get_ele_id(pele,ABS_MSH)); // DK Check
  }

  eletot = 0;
  if (pspecies->pset->aquitard == AQUITARD_ON_TTC) {
    while (eletot < naquitard) {
      gw_id_lay = pspecies->p_aquitard_ttc[eletot]->nlayer;
      gw_id = pspecies->p_aquitard_ttc[eletot]->id_intern;

      play = pcarac_aq->pcmsh->p_layer[gw_id_lay];
      pele = play->p_ele[gw_id];
      // LP_printf(flog, "func %s active abs id %d river ele id is %d , nlay= %d \n", __func__ , MSH_get_ele_id(pele,GIS_MSH),gw_id,gw_id_lay);
      if (pele->paquitard_ttc != NULL) {
        pele->paquitard_ttc->flux_cond[VAR_ABOVE_TTC] = pspecies->p_aquitard_ttc[eletot]->flux_cond[VAR_BELOW_TTC];
        pele->paquitard_ttc->flux_adv[VAR_ABOVE_TTC] = pspecies->p_aquitard_ttc[eletot]->flux_adv[VAR_ABOVE_TTC];
        pele->paquitard_ttc->temperature[VAR_AQUITARD_TTC][0] = pspecies->p_aquitard_ttc[eletot]->temperature[VAR_AQUITARD_TTC][0];
        // LP_printf(flog, "func %s active aquifer ele id is %d condflux: %lf, flux_adv: %lf\n", __func__ ,MSH_get_ele_id(pele,GIS_MSH),
        // pspecies->p_aquitard_ttc[eletot]->flux_cond[VAR_BELOW_TTC], pspecies->p_aquitard_ttc[eletot]->flux_adv[VAR_ABOVE_TTC]);
      }
      eletot++;
    }
    // LP_printf(flog, "func %s ele id is %d\n", __func__ ,MSH_get_ele_id(pele,ABS_MSH));
  }
}

/**
 * \brief Refreshes transport variable in the aquitard-riverbed (ie. C or T depending on the specie type) for one specie in libaq, after TTC calculations
 * \return -
 */
void CAW_update_variable_aquitard_one_specie_aq(s_species_ttc *pspecies, s_carac_aq *pcarac_aq, s_carac_ttc *pcarac_ttc, int id_species, FILE *flog) {
  int nlayer, nele, i, j;
  int eletot = 0;
  int nflux = 0;

  int nriverbed = pcarac_ttc->count[NRIVERBED_TTC];
  int naquitard = pcarac_ttc->count[NAQUITARD_TTC];
  int gw_id_lay; // the id of layer that has an allocated riverbed cell (riverbed == on)
  int gw_id;     // the id cell that has an allocated riverbed cell (riverbed == on)

  s_ele_msh *pele;
  s_ele_msh *pele_riv;
  s_layer_msh *play;

  nlayer = pcarac_aq->pcmsh->pcount->nlayer;
  eletot = 0;

  if (pspecies->pset->riverbed == AQUITARD_ON_TTC) {

    while (eletot < nriverbed) {
      gw_id_lay = pspecies->p_riverbed_ttc[eletot]->nlayer;
      gw_id = pspecies->p_riverbed_ttc[eletot]->id_neigh;

      play = pcarac_aq->pcmsh->p_layer[gw_id_lay];
      pele = play->p_ele[gw_id];
      // LP_printf(flog, "func %s active abs id %d river ele id is %d , nlay= %d \n", __func__ , MSH_get_ele_id(pele,ABS_MSH),gw_id,gw_id_lay);

      if (pele->priverbed_ttc != NULL) {
        pele->priverbed_ttc->flux_cond[VAR_ABOVE_TTC] = pspecies->p_riverbed_ttc[eletot]->flux_cond[VAR_BELOW_TTC];
        pele->priverbed_ttc->flux_adv[VAR_ABOVE_TTC] = pspecies->p_riverbed_ttc[eletot]->flux_adv[VAR_ABOVE_TTC];
        pele->priverbed_ttc->temperature[VAR_AQUITARD_TTC][0] = pspecies->p_riverbed_ttc[eletot]->temperature[VAR_AQUITARD_TTC][0];
        // LP_printf(flog, "func %s active river ele id is %d condflux: %lf, flux_adv: %lf\n", __func__ ,MSH_get_ele_id(pele,ABS_MSH),
        // pspecies->p_riverbed_ttc[eletot]->flux_cond[VAR_BELOW_TTC], pspecies->p_riverbed_ttc[eletot]->flux_adv[VAR_ABOVE_TTC]);
      } else {
        LP_error(flog, "%s Riverbed cell in this element is NULL: layer=%d ele= %d", __func__, gw_id_lay, gw_id);
      }
      eletot++;
    }
    // LP_printf(flog, "func %s ele id is %d\n", __func__ ,MSH_get_ele_id(pele,ABS_MSH));
  }

  if (pspecies->pset->aquitard == AQUITARD_ON_TTC) {
    eletot = 0;
    while (eletot < naquitard) {
      gw_id_lay = pspecies->p_aquitard_ttc[eletot]->nlayer;
      gw_id = pspecies->p_aquitard_ttc[eletot]->id_intern;
      // LP_printf(flog, "func %s  gw id layer: %d  gw id %d  \n ",__func__ , gw_id_lay, gw_id);
      play = pcarac_aq->pcmsh->p_layer[gw_id_lay];
      pele = play->p_ele[gw_id];
      // LP_printf(flog, "func %s active abs id %d river ele id is %d , nlay= %d \n", __func__ , MSH_get_ele_id(pele,GIS_MSH),gw_id,gw_id_lay);

      if (pele->paquitard_ttc != NULL) {
        pele->paquitard_ttc->flux_cond[VAR_ABOVE_TTC] = pspecies->p_aquitard_ttc[eletot]->flux_cond[VAR_BELOW_TTC];
        pele->paquitard_ttc->flux_adv[VAR_ABOVE_TTC] = pspecies->p_aquitard_ttc[eletot]->flux_adv[VAR_ABOVE_TTC];
        pele->paquitard_ttc->temperature[VAR_AQUITARD_TTC][0] = pspecies->p_aquitard_ttc[eletot]->temperature[VAR_AQUITARD_TTC][0];
        // LP_printf(flog, "func %s active aquifer ele id is %d condflux: %lf, flux_adv: %lf\n", __func__ ,MSH_get_ele_id(pele,GIS_MSH),
        // pspecies->p_aquitard_ttc[eletot]->flux_cond[VAR_BELOW_TTC], pspecies->p_aquitard_ttc[eletot]->flux_adv[VAR_ABOVE_TTC]);
      } else {
        LP_error(flog, "%s Aquitard cell in this element is NULL: layer=%d ele= %d", __func__, gw_id_lay, gw_id);
      }
      // LP_printf(flog, "func %s eletot is %d\n", __func__ ,eletot);
      eletot++;
    }
  }
}
