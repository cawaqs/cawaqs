<br />
<div align="center">
  <a href="img/logo_cawaqs.png">
    <img src="img/logo_cawaqs.png" alt="Logo" width="544" height="183">
  </a>

  [![License](https://img.shields.io/badge/License-EPL_2.0-blue.svg)](https://opensource.org/licenses/EPL-2.0)
  [![Software](https://img.shields.io/badge/Latest_major_release-3.45-green.svg)](https://gitlab.com/cawaqs/cawaqs)
  [![Software](https://img.shields.io/badge/Current_version-3.50-orange.svg)](https://gitlab.com/cawaqs/cawaqs/-/raw/main/docs/CaWaQS_348_Documentation.pdf?ref_type=heads)
  [![Software](https://img.shields.io/badge/Documentation-3.50-blue.svg)](https://gitlab.com/cawaqs/cawaqs/-/raw/main/docs/CaWaQS_348_Documentation.pdf?ref_type=heads)

  <h2 align="center">CaWaQS : CAtchment WAter Quality Simulator</h2>
  <p align="left">
    Physically-based coupled surface-subsurface model for joint water, matter and energy flux balances and flow dynamics simulation within all compartments of a hydrosystem (sub-surface, hydraulic network, vadose zone, aquifer system and stream-aquifer exchanges).
    <br />
    <br />
    Software developed at the Centre for geosciences and geoengineering, Mines Paris/ARMINES, PSL University, Fontainebleau, France.
    <br />
    <br />
    <a href="https://gitlab.com/cawaqs/cawaqs/-/raw/main/docs/CaWaQS_350_Documentation.pdf?ref_type=heads"><strong> >>> Explore the CaWaQS documentation : user guide, concepts and equations <<< </strong></a>
    <br />
    <br />
    <strong>Contributors</strong>
    <br />
    Nicolas GALLOIS, Baptiste LABARTHE, Shuaitao WANG, Agnès RIVIERE, Deniz KILIC
    <br />
    Mathias MAILLOT, Fulvia BARATELLI, Pierre GUILLOU, Emmanuel LEDOUX, Nicolas FLIPO
    <br />
    <br />
    <strong>Project manager</strong>
    <br />
    Nicolas FLIPO
    <br />
    <br />
    <strong>Contact</strong>
    <br />
    Nicolas FLIPO <a href="mailto:nicolas.flipo@minesparis.psl.eu">nicolas.flipo@minesparis.psl.eu</a>
    <br />
    Nicolas GALLOIS <a href="mailto:nicolas.gallois@minesparis.psl.eu">nicolas.gallois@minesparis.psl.eu</a>
  </p>
</div>

CaWaQS couples multiple C-ANSI libraries (also under EPL v2.0 licence), available on GitLab *via* the following groups URLs :
* https://gitlab.com/ghydro
* https://gitlab.com/gtransp
* https://gitlab.com/gmesh
* https://gitlab.com/gutil


<img src="img/cawaqs3x.png" alt="cawaqs" width="850"/>

## Copyright

&copy; 2024 Contributors to the CaWaQS software.

*All rights reserved*. This software and the accompanying materials are made available under the terms of the Eclipse Public License (EPL) v2.0 which accompanies this distribution, and is available at http://www.eclipse.org/legal/epl-v20.html.

## How to install ?

**Prerequisite** : For a successfull installation, first, make sure the following packages are installed on your system : **git**, **make**, **flex**, **bison**, **gfortran** and **gcc**.

**If you want to install CaWaQS from the main branch of CaWaQS and all librairies, use:**

| Command                        | Installation type                                                                                                                                                                | 
|:--------------------------------------------------------|:---------------------------------------------------------------------------------------------------------------------------------------| 
| `./make_Cawaqs.sh all`                                  | Re-installs and compiles each CaWaQS library at `$LIB_HYDROSYSTEM_PATH` location. Compiles cawaqs3.x at the `.` location.              |
| `./make_Cawaqs.sh PATH`                                 | Installs and compiles each CaWaQS library at `$PATH` location. Compiles cawaqs3.x at `.` location.                                     |
| `./make_Cawaqs.sh PATH update`                          | Compiles cawaqs3.x at `.` location, assuming that all libraries needed for compiling are located at `$PATH`, and recompiles them.      |
| `./make_Cawaqs.sh`                                      | Re-installs and compiles each library at `./.` location. Compiles cawaqs3.x at `.` location. equivalent to   `./make_Cawaqs.sh ./`                                        |


**If you want to install CaWaQS from a branch :**

First, please make sure that :
1. the name of the branch is the same in each library. If the branch doesn't exist in the library, then the main branch is used as default.
2. the branch does exist in the cawaqs project. To do so, use the command  `git checkout <branch_name> `.

Then, choose your compilation option :

| Command                                                 | Installation type                                                                                                                                                                 | 
|:--------------------------------------------------------|:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------| 
| `./make_Cawaqs_from_branches.sh -b <branch_name>`       | Re-installs and compiles each library at `$LIB_HYDROSYSTEM_PATH` location, using `<branch_name>` as a branch name if found, `main` otherwise. Compiles cawaqs3.x at `.` location. |
| `./make_Cawaqs_from_branches.sh -b <branch_name> PATH`  | Installs each library and recompiles them at `$PATH` location using `<branch_name>` as a branch name if found, `main` otherwise. Compiles cawaqs3.x at `.` location.              |
| `./make_Cawaqs_from_branches.sh all`                    | Calls `make_Cawaqs.sh all`. Re-installs and compiles each library at `$LIB_HYDROSYSTEM_PATH` location. Compiles cawaqs3.x at `.` location.                                        |
| `./make_Cawaqs_from_branches.sh PATH`                   | Calls `make_Cawaqs.sh PATH` : Installs each library and recompiles them at `$PATH` location. Compiles cawaqs3.x at `.` location.                                                  |
| `./make_Cawaqs_from_branches.sh`                        | Calls `make_Cawaqs.sh` : Re-installs and compiles each library at `./.` location. Compiles cawaqs3.x at `.` location.                                                             |



